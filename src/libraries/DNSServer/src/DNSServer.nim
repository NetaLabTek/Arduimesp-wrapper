const
  DNS_QR_QUERY* = 0
  DNS_QR_RESPONSE* = 1
  DNS_OPCODE_QUERY* = 0

type
  DNSReplyCode* {.size: sizeof(cint), importcpp: "DNSReplyCode",
                 header: "DNSServer.h".} = enum
    NoError = 0, FormError = 1, ServerFailure = 2, NonExistentDomain = 3,
    NotImplemented = 4, Refused = 5, YXDomain = 6, YXRRSet = 7, NXRRSet = 8


type
  DNSHeader* {.importcpp: "DNSHeader", header: "DNSServer.h", bycopy.} = object
    ID* {.importc: "ID".}: uint16_t ##  identification number
    RD* {.importc: "RD".}: cuchar ##  recursion desired
    TC* {.importc: "TC".}: cuchar ##  truncated message
    AA* {.importc: "AA".}: cuchar ##  authoritive answer
    OPCode* {.importc: "OPCode".}: cuchar ##  message_type
    QR* {.importc: "QR".}: cuchar ##  query/response flag
    RCode* {.importc: "RCode".}: cuchar ##  response code
    Z* {.importc: "Z".}: cuchar  ##  its z! reserved
    RA* {.importc: "RA".}: cuchar ##  recursion available
    QDCount* {.importc: "QDCount".}: uint16_t ##  number of question entries
    ANCount* {.importc: "ANCount".}: uint16_t ##  number of answer entries
    NSCount* {.importc: "NSCount".}: uint16_t ##  number of authority entries
    ARCount* {.importc: "ARCount".}: uint16_t ##  number of resource entries
  
  DNSServer* {.importcpp: "DNSServer", header: "DNSServer.h", bycopy.} = object
  

proc constructDNSServer*(): DNSServer {.cdecl, constructor,
                                     importcpp: "DNSServer(@)",
                                     header: "DNSServer.h".}
proc processNextRequest*(this: var DNSServer) {.cdecl,
    importcpp: "processNextRequest", header: "DNSServer.h".}
proc setErrorReplyCode*(this: var DNSServer; replyCode: DNSReplyCode) {.cdecl,
    importcpp: "setErrorReplyCode", header: "DNSServer.h".}
proc setTTL*(this: var DNSServer; ttl: uint32_t) {.cdecl, importcpp: "setTTL",
    header: "DNSServer.h".}
proc start*(this: var DNSServer; port: uint16_t; domainName: String;
           resolvedIP: IPAddress): bool {.cdecl, importcpp: "start",
                                       header: "DNSServer.h".}
proc stop*(this: var DNSServer) {.cdecl, importcpp: "stop", header: "DNSServer.h".}