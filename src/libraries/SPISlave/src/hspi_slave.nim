## 
##   SPISlave library for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  Arduino

## Start SPI SLave

proc hspi_slave_begin*(status_len: uint8_t; arg: pointer) {.cdecl,
    importcpp: "hspi_slave_begin(@)", header: "hspi_slave.h".}
## End SPI SLave

proc hspi_slave_end*() {.cdecl, importcpp: "hspi_slave_end(@)",
                       header: "hspi_slave.h".}
## set the status register so the master can read it

proc hspi_slave_setStatus*(status: uint32_t) {.cdecl,
    importcpp: "hspi_slave_setStatus(@)", header: "hspi_slave.h".}
## set the data registers (max 32 bytes at a time)

proc hspi_slave_setData*(data: ptr uint8_t; len: uint8_t) {.cdecl,
    importcpp: "hspi_slave_setData(@)", header: "hspi_slave.h".}
## set the callbacks

proc hspi_slave_onData*(rxd_cb: proc (a2: pointer; a3: ptr uint8_t; a4: uint8_t) {.cdecl.}) {.
    cdecl, importcpp: "hspi_slave_onData(@)", header: "hspi_slave.h".}
proc hspi_slave_onDataSent*(txd_cb: proc (a2: pointer) {.cdecl.}) {.cdecl,
    importcpp: "hspi_slave_onDataSent(@)", header: "hspi_slave.h".}
proc hspi_slave_onStatus*(rxs_cb: proc (a2: pointer; a3: uint32_t) {.cdecl.}) {.cdecl,
    importcpp: "hspi_slave_onStatus(@)", header: "hspi_slave.h".}
proc hspi_slave_onStatusSent*(txs_cb: proc (a2: pointer) {.cdecl.}) {.cdecl,
    importcpp: "hspi_slave_onStatusSent(@)", header: "hspi_slave.h".}