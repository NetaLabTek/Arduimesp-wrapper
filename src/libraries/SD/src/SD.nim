## 
## 
##  SD - a slightly more friendly wrapper for sdfatlib
## 
##  This library aims to expose a subset of SD card functionality
##  in the form of a higher level "wrapper" object.
## 
##  License: GNU General Public License V3
##           (Because sdfatlib is licensed with this.)
## 
##  (C) Copyright 2010 SparkFun Electronics
## 
## 

const
  FILE_READ* = O_READ
  FILE_WRITE* = (O_READ or O_WRITE or O_CREAT)

type
  File* {.importcpp: "File", header: "SD.h", bycopy.} = object of Stream
    ##  our name
    ##  underlying file pointer
  

proc constructFile*(f: SdFile; name: cstring): File {.cdecl, constructor,
    importcpp: "File(@)", header: "SD.h".}
proc constructFile*(): File {.cdecl, constructor, importcpp: "File(@)", header: "SD.h".}
proc write*(this: var File; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "SD.h".}
proc write*(this: var File; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "SD.h".}
proc read*(this: var File): cint {.cdecl, importcpp: "read", header: "SD.h".}
proc peek*(this: var File): cint {.cdecl, importcpp: "peek", header: "SD.h".}
proc available*(this: var File): cint {.cdecl, importcpp: "available", header: "SD.h".}
proc flush*(this: var File) {.cdecl, importcpp: "flush", header: "SD.h".}
proc read*(this: var File; buf: pointer; nbyte: uint16_t): cint {.cdecl,
    importcpp: "read", header: "SD.h".}
proc seek*(this: var File; pos: uint32_t): boolean {.cdecl, importcpp: "seek",
    header: "SD.h".}
proc position*(this: var File): uint32_t {.cdecl, importcpp: "position", header: "SD.h".}
proc size*(this: var File): uint32_t {.cdecl, importcpp: "size", header: "SD.h".}
proc close*(this: var File) {.cdecl, importcpp: "close", header: "SD.h".}
converter `bool`*(this: var File): bool {.cdecl, importcpp: "File::operator bool",
                                     header: "SD.h".}
proc name*(this: var File): cstring {.cdecl, importcpp: "name", header: "SD.h".}
proc isDirectory*(this: var File): boolean {.cdecl, importcpp: "isDirectory",
                                        header: "SD.h".}
proc openNextFile*(this: var File; mode: uint8_t = O_RDONLY): File {.cdecl,
    importcpp: "openNextFile", header: "SD.h".}
proc rewindDirectory*(this: var File) {.cdecl, importcpp: "rewindDirectory",
                                    header: "SD.h".}
proc write*[T](this: var File; src: var T): csize {.cdecl, importcpp: "write",
    header: "SD.h".}
type
  SDClass* {.importcpp: "SDClass", header: "SD.h", bycopy.} = object ##  These are required for initialisation and use of sdfatlib
                                                             ##  This needs to be called to set up the connection to the SD card
                                                             ##  before other methods are used.
                                                             ##  This is used to determine the mode used to open a file
                                                             ##  it's here because it's the easiest place to pass the 
                                                             ##  information through the directory walking function. But
                                                             ##  it's probably not the best place for it.
                                                             ##  It shouldn't be set directly--it is set via the parameters to `open`.
    ##  my quick&dirty iterator, should be replaced
  

proc begin*(this: var SDClass; csPin: uint8_t = SD_CHIP_SELECT_PIN;
           speed: uint32_t = SPI_HALF_SPEED): boolean {.cdecl, importcpp: "begin",
    header: "SD.h".}
proc open*(this: var SDClass; filename: cstring; mode: uint8_t = FILE_READ): File {.cdecl,
    importcpp: "open", header: "SD.h".}
proc open*(this: var SDClass; filename: String; mode: uint8_t = FILE_READ): File {.cdecl,
    importcpp: "open", header: "SD.h".}
proc exists*(this: var SDClass; filepath: cstring): boolean {.cdecl,
    importcpp: "exists", header: "SD.h".}
proc exists*(this: var SDClass; filepath: String): boolean {.cdecl, importcpp: "exists",
    header: "SD.h".}
proc mkdir*(this: var SDClass; filepath: cstring): boolean {.cdecl, importcpp: "mkdir",
    header: "SD.h".}
proc mkdir*(this: var SDClass; filepath: String): boolean {.cdecl, importcpp: "mkdir",
    header: "SD.h".}
proc remove*(this: var SDClass; filepath: cstring): boolean {.cdecl,
    importcpp: "remove", header: "SD.h".}
proc remove*(this: var SDClass; filepath: String): boolean {.cdecl, importcpp: "remove",
    header: "SD.h".}
proc rmdir*(this: var SDClass; filepath: cstring): boolean {.cdecl, importcpp: "rmdir",
    header: "SD.h".}
proc rmdir*(this: var SDClass; filepath: String): boolean {.cdecl, importcpp: "rmdir",
    header: "SD.h".}
proc `type`*(this: var SDClass): uint8_t {.cdecl, importcpp: "type", header: "SD.h".}
proc fatType*(this: var SDClass): uint8_t {.cdecl, importcpp: "fatType", header: "SD.h".}
proc blocksPerCluster*(this: var SDClass): csize {.cdecl,
    importcpp: "blocksPerCluster", header: "SD.h".}
proc totalClusters*(this: var SDClass): csize {.cdecl, importcpp: "totalClusters",
    header: "SD.h".}
proc blockSize*(this: var SDClass): csize {.cdecl, importcpp: "blockSize",
                                       header: "SD.h".}
proc totalBlocks*(this: var SDClass): csize {.cdecl, importcpp: "totalBlocks",
    header: "SD.h".}
proc clusterSize*(this: var SDClass): csize {.cdecl, importcpp: "clusterSize",
    header: "SD.h".}
proc size*(this: var SDClass): csize {.cdecl, importcpp: "size", header: "SD.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_SD):
  var SD* {.importcpp: "SD", header: "SD.h".}: SDClass