##  Arduino SdFat Library
##  Copyright (C) 2010 by William Greiman
## 
##  This file is part of the Arduino SdFat Library
## 
##  This Library is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
## 
##  This Library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
## 
##  You should have received a copy of the GNU General Public License
##  along with the Arduino SdFat Library.  If not, see
##  <http://www.gnu.org/licenses/>.
## 

when defined(__arm__):         ##  Arduino Due Board follows
  var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
  var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
  var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
  var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
elif defined(ESP8266):         ##  Other AVR based Boards follows
  var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
  var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
  var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
  var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
elif defined(__AVR__):         ##  Other AVR based Boards follows
  ##  Warning this file was generated by a program.


  ## ------------------------------------------------------------------------------
  ## * struct for mapping digital pins
  type
    pin_map_t* {.importcpp: "pin_map_t", header: "Sd2PinMap.h", bycopy.} = object
      ddr* {.importc: "ddr".}: ptr uint8_t
      pin* {.importc: "pin".}: ptr uint8_t
      port* {.importc: "port".}: ptr uint8_t
      bit* {.importc: "bit".}: uint8_t


  ## ------------------------------------------------------------------------------
  when defined(__AVR_ATmega1280__) or defined(__AVR_ATmega2560__):
    ##  Mega
    ##  Two Wire (aka I2C) ports
    var SDA_PIN* {.importcpp: "SDA_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCL_PIN* {.importcpp: "SCL_PIN", header: "Sd2PinMap.h".}: uint8_t


    ##  SPI port
    var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
    var digitalPinMap* {.importcpp: "digitalPinMap", header: "Sd2PinMap.h".}: ptr pin_map_t


    ## ------------------------------------------------------------------------------
  elif defined(__AVR_ATmega644P__) or defined(__AVR_ATmega644__):
    ##  Sanguino
    ##  Two Wire (aka I2C) ports
    var SDA_PIN* {.importcpp: "SDA_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCL_PIN* {.importcpp: "SCL_PIN", header: "Sd2PinMap.h".}: uint8_t


    ##  SPI port
    var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
    var digitalPinMap* {.importcpp: "digitalPinMap", header: "Sd2PinMap.h".}: ptr pin_map_t


    ## ------------------------------------------------------------------------------
  elif defined(__AVR_ATmega32U4__):
    ##  Leonardo
    ##  Two Wire (aka I2C) ports
    var SDA_PIN* {.importcpp: "SDA_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCL_PIN* {.importcpp: "SCL_PIN", header: "Sd2PinMap.h".}: uint8_t


    ##  SPI port
    var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
    var digitalPinMap* {.importcpp: "digitalPinMap", header: "Sd2PinMap.h".}: ptr pin_map_t


    ## ------------------------------------------------------------------------------
  elif defined(__AVR_AT90USB646__) or defined(__AVR_AT90USB1286__):
    ##  Teensy++ 1.0 & 2.0
    ##  Two Wire (aka I2C) ports
    var SDA_PIN* {.importcpp: "SDA_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCL_PIN* {.importcpp: "SCL_PIN", header: "Sd2PinMap.h".}: uint8_t


    ##  SPI port
    var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
    var digitalPinMap* {.importcpp: "digitalPinMap", header: "Sd2PinMap.h".}: ptr pin_map_t


    ## ------------------------------------------------------------------------------
  else:
    ##  Two Wire (aka I2C) ports
    var SDA_PIN* {.importcpp: "SDA_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCL_PIN* {.importcpp: "SCL_PIN", header: "Sd2PinMap.h".}: uint8_t


    ##  SPI port
    var SS_PIN* {.importcpp: "SS_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MOSI_PIN* {.importcpp: "MOSI_PIN", header: "Sd2PinMap.h".}: uint8_t
    var MISO_PIN* {.importcpp: "MISO_PIN", header: "Sd2PinMap.h".}: uint8_t
    var SCK_PIN* {.importcpp: "SCK_PIN", header: "Sd2PinMap.h".}: uint8_t
    var digitalPinMap* {.importcpp: "digitalPinMap", header: "Sd2PinMap.h".}: ptr pin_map_t
  proc badPinNumber*(): uint8_t {.cdecl.}

  nil
  proc getPinMode*(pin: uint8_t): uint8_t {.cdecl.}
  proc setPinMode*(pin: uint8_t; mode: uint8_t) {.cdecl.}
  proc fastDigitalRead*(pin: uint8_t): uint8_t {.cdecl.}
  proc fastDigitalWrite*(pin: uint8_t; value: uint8_t) {.cdecl.}
else: