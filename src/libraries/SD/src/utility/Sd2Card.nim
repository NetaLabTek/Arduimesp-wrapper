##  Arduino Sd2Card Library
##  Copyright (C) 2009 by William Greiman
## 
##  This file is part of the Arduino Sd2Card Library
## 
##  This Library is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
## 
##  This Library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
## 
##  You should have received a copy of the GNU General Public License
##  along with the Arduino Sd2Card Library.  If not, see
##  <http://www.gnu.org/licenses/>.
## 

## *
##  \file
##  Sd2Card class
## 

import
  Sd2PinMap, SdInfo

when defined(ESP8266):
  import
    SPI

  var SPI_FULL_SPEED* {.importcpp: "SPI_FULL_SPEED", header: "Sd2Card.h".}: uint32_t
  var SPI_HALF_SPEED* {.importcpp: "SPI_HALF_SPEED", header: "Sd2Card.h".}: uint32_t
  var SPI_QUARTER_SPEED* {.importcpp: "SPI_QUARTER_SPEED", header: "Sd2Card.h".}: uint32_t
else:
  ## * Set SCK to max rate of F_CPU/2. See Sd2Card::setSckRate().
  var SPI_FULL_SPEED* {.importcpp: "SPI_FULL_SPEED", header: "Sd2Card.h".}: uint8_t
  ## * Set SCK rate to F_CPU/4. See Sd2Card::setSckRate().
  var SPI_HALF_SPEED* {.importcpp: "SPI_HALF_SPEED", header: "Sd2Card.h".}: uint8_t
  ## * Set SCK rate to F_CPU/8. Sd2Card::setSckRate().
  var SPI_QUARTER_SPEED* {.importcpp: "SPI_QUARTER_SPEED", header: "Sd2Card.h".}: uint8_t
## *
##  USE_SPI_LIB: if set, use the SPI library bundled with Arduino IDE, otherwise
##  run with a standalone driver for AVR.
## 

const
  USE_SPI_LIB* = true

## *
##  Define MEGA_SOFT_SPI non-zero to use software SPI on Mega Arduinos.
##  Pins used are SS 10, MOSI 11, MISO 12, and SCK 13.
## 
##  MEGA_SOFT_SPI allows an unmodified Adafruit GPS Shield to be used
##  on Mega Arduinos.  Software SPI works well with GPS Shield V1.1
##  but many SD cards will fail with GPS Shield V1.0.
## 

const
  MEGA_SOFT_SPI* = 0

## ------------------------------------------------------------------------------

when MEGA_SOFT_SPI and
    (defined(__AVR_ATmega1280__) or defined(__AVR_ATmega2560__)):
  const
    SOFTWARE_SPI* = true
when not defined(SOFTWARE_SPI):
  ##  hardware pin defs
  ## *
  ##  SD Chip Select pin
  ## 
  ##  Warning if this pin is redefined the hardware SS will pin will be enabled
  ##  as an output by init().  An avr processor will not function as an SPI
  ##  master unless SS is set to output mode.
  ## 
  ## * The default chip select pin for the SD card is SS.
  var SD_CHIP_SELECT_PIN* {.importcpp: "SD_CHIP_SELECT_PIN", header: "Sd2Card.h".}: uint8_t

  ##  The following three pins must not be redefined for hardware SPI.
  ## * SPI Master Out Slave In pin
  var SPI_MOSI_PIN* {.importcpp: "SPI_MOSI_PIN", header: "Sd2Card.h".}: uint8_t
  ## * SPI Master In Slave Out pin
  var SPI_MISO_PIN* {.importcpp: "SPI_MISO_PIN", header: "Sd2Card.h".}: uint8_t
  ## * SPI Clock pin
  var SPI_SCK_PIN* {.importcpp: "SPI_SCK_PIN", header: "Sd2Card.h".}: uint8_t
  ## * optimize loops for hardware SPI
  when not defined(USE_SPI_LIB):
    const
      OPTIMIZE_HARDWARE_SPI* = true
else:
  var SD_CHIP_SELECT_PIN* {.importcpp: "SD_CHIP_SELECT_PIN", header: "Sd2Card.h".}: uint8_t
  ## * SPI Master Out Slave In pin
  var SPI_MOSI_PIN* {.importcpp: "SPI_MOSI_PIN", header: "Sd2Card.h".}: uint8_t
  ## * SPI Master In Slave Out pin
  var SPI_MISO_PIN* {.importcpp: "SPI_MISO_PIN", header: "Sd2Card.h".}: uint8_t
  ## * SPI Clock pin
  var SPI_SCK_PIN* {.importcpp: "SPI_SCK_PIN", header: "Sd2Card.h".}: uint8_t
const
  SD_PROTECT_BLOCK_ZERO* = 1

## * init timeout ms

var SD_INIT_TIMEOUT* {.importcpp: "SD_INIT_TIMEOUT", header: "Sd2Card.h".}: uint16_t

## * erase timeout ms

var SD_ERASE_TIMEOUT* {.importcpp: "SD_ERASE_TIMEOUT", header: "Sd2Card.h".}: uint16_t

## * read timeout ms

var SD_READ_TIMEOUT* {.importcpp: "SD_READ_TIMEOUT", header: "Sd2Card.h".}: uint16_t

## * write time out ms

var SD_WRITE_TIMEOUT* {.importcpp: "SD_WRITE_TIMEOUT", header: "Sd2Card.h".}: uint16_t

## ------------------------------------------------------------------------------
##  SD card errors
## * timeout error for command CMD0

var SD_CARD_ERROR_CMD0* {.importcpp: "SD_CARD_ERROR_CMD0", header: "Sd2Card.h".}: uint8_t

## * CMD8 was not accepted - not a valid SD card

var SD_CARD_ERROR_CMD8* {.importcpp: "SD_CARD_ERROR_CMD8", header: "Sd2Card.h".}: uint8_t

## * card returned an error response for CMD17 (read block)

var SD_CARD_ERROR_CMD17* {.importcpp: "SD_CARD_ERROR_CMD17", header: "Sd2Card.h".}: uint8_t

## * card returned an error response for CMD24 (write block)

var SD_CARD_ERROR_CMD24* {.importcpp: "SD_CARD_ERROR_CMD24", header: "Sd2Card.h".}: uint8_t

## *  WRITE_MULTIPLE_BLOCKS command failed

var SD_CARD_ERROR_CMD25* {.importcpp: "SD_CARD_ERROR_CMD25", header: "Sd2Card.h".}: uint8_t

## * card returned an error response for CMD58 (read OCR)

var SD_CARD_ERROR_CMD58* {.importcpp: "SD_CARD_ERROR_CMD58", header: "Sd2Card.h".}: uint8_t

## * SET_WR_BLK_ERASE_COUNT failed

var SD_CARD_ERROR_ACMD23* {.importcpp: "SD_CARD_ERROR_ACMD23", header: "Sd2Card.h".}: uint8_t

## * card's ACMD41 initialization process timeout

var SD_CARD_ERROR_ACMD41* {.importcpp: "SD_CARD_ERROR_ACMD41", header: "Sd2Card.h".}: uint8_t

## * card returned a bad CSR version field

var SD_CARD_ERROR_BAD_CSD* {.importcpp: "SD_CARD_ERROR_BAD_CSD", header: "Sd2Card.h".}: uint8_t

## * erase block group command failed

var SD_CARD_ERROR_ERASE* {.importcpp: "SD_CARD_ERROR_ERASE", header: "Sd2Card.h".}: uint8_t

## * card not capable of single block erase

var SD_CARD_ERROR_ERASE_SINGLE_BLOCK* {.importcpp: "SD_CARD_ERROR_ERASE_SINGLE_BLOCK",
                                      header: "Sd2Card.h".}: uint8_t

## * Erase sequence timed out

var SD_CARD_ERROR_ERASE_TIMEOUT* {.importcpp: "SD_CARD_ERROR_ERASE_TIMEOUT",
                                 header: "Sd2Card.h".}: uint8_t

## * card returned an error token instead of read data

var SD_CARD_ERROR_READ* {.importcpp: "SD_CARD_ERROR_READ", header: "Sd2Card.h".}: uint8_t

## * read CID or CSD failed

var SD_CARD_ERROR_READ_REG* {.importcpp: "SD_CARD_ERROR_READ_REG",
                            header: "Sd2Card.h".}: uint8_t

## * timeout while waiting for start of read data

var SD_CARD_ERROR_READ_TIMEOUT* {.importcpp: "SD_CARD_ERROR_READ_TIMEOUT",
                                header: "Sd2Card.h".}: uint8_t

## * card did not accept STOP_TRAN_TOKEN

var SD_CARD_ERROR_STOP_TRAN* {.importcpp: "SD_CARD_ERROR_STOP_TRAN",
                             header: "Sd2Card.h".}: uint8_t

## * card returned an error token as a response to a write operation

var SD_CARD_ERROR_WRITE* {.importcpp: "SD_CARD_ERROR_WRITE", header: "Sd2Card.h".}: uint8_t

## * attempt to write protected block zero

var SD_CARD_ERROR_WRITE_BLOCK_ZERO* {.importcpp: "SD_CARD_ERROR_WRITE_BLOCK_ZERO",
                                    header: "Sd2Card.h".}: uint8_t

## * card did not go ready for a multiple block write

var SD_CARD_ERROR_WRITE_MULTIPLE* {.importcpp: "SD_CARD_ERROR_WRITE_MULTIPLE",
                                  header: "Sd2Card.h".}: uint8_t

## * card returned an error to a CMD13 status check after a write

var SD_CARD_ERROR_WRITE_PROGRAMMING* {.importcpp: "SD_CARD_ERROR_WRITE_PROGRAMMING",
                                     header: "Sd2Card.h".}: uint8_t

## * timeout occurred during write programming

var SD_CARD_ERROR_WRITE_TIMEOUT* {.importcpp: "SD_CARD_ERROR_WRITE_TIMEOUT",
                                 header: "Sd2Card.h".}: uint8_t

## * incorrect rate selected

var SD_CARD_ERROR_SCK_RATE* {.importcpp: "SD_CARD_ERROR_SCK_RATE",
                            header: "Sd2Card.h".}: uint8_t

## ------------------------------------------------------------------------------
##  card types
## * Standard capacity V1 SD card

var SD_CARD_TYPE_SD1* {.importcpp: "SD_CARD_TYPE_SD1", header: "Sd2Card.h".}: uint8_t

## * Standard capacity V2 SD card

var SD_CARD_TYPE_SD2* {.importcpp: "SD_CARD_TYPE_SD2", header: "Sd2Card.h".}: uint8_t

## * High Capacity SD card

var SD_CARD_TYPE_SDHC* {.importcpp: "SD_CARD_TYPE_SDHC", header: "Sd2Card.h".}: uint8_t

## ------------------------------------------------------------------------------
## *
##  \class Sd2Card
##  \brief Raw access to SD and SDHC flash memory cards.
## 

type
  Sd2Card* {.importcpp: "Sd2Card", header: "Sd2Card.h", bycopy.} = object ## * Construct an instance of Sd2Card.
    ##  private functions
  

proc constructSd2Card*(): Sd2Card {.cdecl, constructor, importcpp: "Sd2Card(@)",
                                 header: "Sd2Card.h".}
proc cardSize*(this: var Sd2Card): uint32_t {.cdecl, importcpp: "cardSize",
    header: "Sd2Card.h".}
proc erase*(this: var Sd2Card; firstBlock: uint32_t; lastBlock: uint32_t): uint8_t {.
    cdecl, importcpp: "erase", header: "Sd2Card.h".}
proc eraseSingleBlockEnable*(this: var Sd2Card): uint8_t {.cdecl,
    importcpp: "eraseSingleBlockEnable", header: "Sd2Card.h".}
proc errorCode*(this: Sd2Card): uint8_t {.noSideEffect, cdecl, importcpp: "errorCode",
                                      header: "Sd2Card.h".}
proc errorData*(this: Sd2Card): uint8_t {.noSideEffect, cdecl, importcpp: "errorData",
                                      header: "Sd2Card.h".}
proc init*(this: var Sd2Card): uint8_t {.cdecl, importcpp: "init", header: "Sd2Card.h".}
proc init*(this: var Sd2Card; sckRateID: uint32_t): uint8_t {.cdecl, importcpp: "init",
    header: "Sd2Card.h".}
proc init*(this: var Sd2Card; sckRateID: uint32_t; chipSelectPin: uint8_t): uint8_t {.
    cdecl, importcpp: "init", header: "Sd2Card.h".}
proc init*(this: var Sd2Card; sckRateID: uint8_t): uint8_t {.cdecl, importcpp: "init",
    header: "Sd2Card.h".}
proc init*(this: var Sd2Card; sckRateID: uint8_t; chipSelectPin: uint8_t): uint8_t {.
    cdecl, importcpp: "init", header: "Sd2Card.h".}
proc partialBlockRead*(this: var Sd2Card; value: uint8_t) {.cdecl,
    importcpp: "partialBlockRead", header: "Sd2Card.h".}
proc partialBlockRead*(this: Sd2Card): uint8_t {.noSideEffect, cdecl,
    importcpp: "partialBlockRead", header: "Sd2Card.h".}
proc readBlock*(this: var Sd2Card; `block`: uint32_t; dst: ptr uint8_t): uint8_t {.cdecl,
    importcpp: "readBlock", header: "Sd2Card.h".}
proc readData*(this: var Sd2Card; `block`: uint32_t; offset: uint16_t; count: uint16_t;
              dst: ptr uint8_t): uint8_t {.cdecl, importcpp: "readData",
                                       header: "Sd2Card.h".}
proc readCID*(this: var Sd2Card; cid: ptr cid_t): uint8_t {.cdecl, importcpp: "readCID",
    header: "Sd2Card.h".}
proc readCSD*(this: var Sd2Card; csd: ptr csd_t): uint8_t {.cdecl, importcpp: "readCSD",
    header: "Sd2Card.h".}
proc readEnd*(this: var Sd2Card) {.cdecl, importcpp: "readEnd", header: "Sd2Card.h".}
proc setSckRate*(this: var Sd2Card; sckRateID: uint32_t): uint8_t {.cdecl,
    importcpp: "setSckRate", header: "Sd2Card.h".}
proc setSckRate*(this: var Sd2Card; sckRateID: uint8_t): uint8_t {.cdecl,
    importcpp: "setSckRate", header: "Sd2Card.h".}
proc `type`*(this: Sd2Card): uint8_t {.noSideEffect, cdecl, importcpp: "type",
                                   header: "Sd2Card.h".}
proc writeBlock*(this: var Sd2Card; blockNumber: uint32_t; src: ptr uint8_t): uint8_t {.
    cdecl, importcpp: "writeBlock", header: "Sd2Card.h".}
proc writeData*(this: var Sd2Card; src: ptr uint8_t): uint8_t {.cdecl,
    importcpp: "writeData", header: "Sd2Card.h".}
proc writeStart*(this: var Sd2Card; blockNumber: uint32_t; eraseCount: uint32_t): uint8_t {.
    cdecl, importcpp: "writeStart", header: "Sd2Card.h".}
proc writeStop*(this: var Sd2Card): uint8_t {.cdecl, importcpp: "writeStop",
    header: "Sd2Card.h".}