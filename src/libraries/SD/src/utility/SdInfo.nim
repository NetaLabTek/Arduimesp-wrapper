##  Arduino Sd2Card Library
##  Copyright (C) 2009 by William Greiman
## 
##  This file is part of the Arduino Sd2Card Library
## 
##  This Library is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
## 
##  This Library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
## 
##  You should have received a copy of the GNU General Public License
##  along with the Arduino Sd2Card Library.  If not, see
##  <http://www.gnu.org/licenses/>.
## 

##  Based on the document:
## 
##  SD Specifications
##  Part 1
##  Physical Layer
##  Simplified Specification
##  Version 2.00
##  September 25, 2006
## 
##  www.sdcard.org/developers/tech/sdcard/pls/Simplified_Physical_Layer_Spec.pdf
## ------------------------------------------------------------------------------
##  SD card commands
## * GO_IDLE_STATE - init card in spi mode if CS low

var CMD0* {.importcpp: "CMD0", header: "SdInfo.h".}: uint8_t

## * SEND_IF_COND - verify SD Memory Card interface operating condition.

var CMD8* {.importcpp: "CMD8", header: "SdInfo.h".}: uint8_t

## * SEND_CSD - read the Card Specific Data (CSD register)

var CMD9* {.importcpp: "CMD9", header: "SdInfo.h".}: uint8_t

## * SEND_CID - read the card identification information (CID register)

var CMD10* {.importcpp: "CMD10", header: "SdInfo.h".}: uint8_t

## * SEND_STATUS - read the card status register

var CMD13* {.importcpp: "CMD13", header: "SdInfo.h".}: uint8_t

## * READ_BLOCK - read a single data block from the card

var CMD17* {.importcpp: "CMD17", header: "SdInfo.h".}: uint8_t

## * WRITE_BLOCK - write a single data block to the card

var CMD24* {.importcpp: "CMD24", header: "SdInfo.h".}: uint8_t

## * WRITE_MULTIPLE_BLOCK - write blocks of data until a STOP_TRANSMISSION

var CMD25* {.importcpp: "CMD25", header: "SdInfo.h".}: uint8_t

## * ERASE_WR_BLK_START - sets the address of the first block to be erased

var CMD32* {.importcpp: "CMD32", header: "SdInfo.h".}: uint8_t

## * ERASE_WR_BLK_END - sets the address of the last block of the continuous
##     range to be erased

var CMD33* {.importcpp: "CMD33", header: "SdInfo.h".}: uint8_t

## * ERASE - erase all previously selected blocks

var CMD38* {.importcpp: "CMD38", header: "SdInfo.h".}: uint8_t

## * APP_CMD - escape for application specific command

var CMD55* {.importcpp: "CMD55", header: "SdInfo.h".}: uint8_t

## * READ_OCR - read the OCR register of a card

var CMD58* {.importcpp: "CMD58", header: "SdInfo.h".}: uint8_t

## * SET_WR_BLK_ERASE_COUNT - Set the number of write blocks to be
##      pre-erased before writing

var ACMD23* {.importcpp: "ACMD23", header: "SdInfo.h".}: uint8_t

## * SD_SEND_OP_COMD - Sends host capacity support information and
##     activates the card's initialization process

var ACMD41* {.importcpp: "ACMD41", header: "SdInfo.h".}: uint8_t

## ------------------------------------------------------------------------------
## * status for card in the ready state

var R1_READY_STATE* {.importcpp: "R1_READY_STATE", header: "SdInfo.h".}: uint8_t

## * status for card in the idle state

var R1_IDLE_STATE* {.importcpp: "R1_IDLE_STATE", header: "SdInfo.h".}: uint8_t

## * status bit for illegal command

var R1_ILLEGAL_COMMAND* {.importcpp: "R1_ILLEGAL_COMMAND", header: "SdInfo.h".}: uint8_t

## * start data token for read or write single block

var DATA_START_BLOCK* {.importcpp: "DATA_START_BLOCK", header: "SdInfo.h".}: uint8_t

## * stop token for write multiple blocks

var STOP_TRAN_TOKEN* {.importcpp: "STOP_TRAN_TOKEN", header: "SdInfo.h".}: uint8_t

## * start data token for write multiple blocks

var WRITE_MULTIPLE_TOKEN* {.importcpp: "WRITE_MULTIPLE_TOKEN", header: "SdInfo.h".}: uint8_t

## * mask for data response tokens after a write block operation

var DATA_RES_MASK* {.importcpp: "DATA_RES_MASK", header: "SdInfo.h".}: uint8_t

## * write data accepted token

var DATA_RES_ACCEPTED* {.importcpp: "DATA_RES_ACCEPTED", header: "SdInfo.h".}: uint8_t

## ------------------------------------------------------------------------------

type
  cid_t* {.importcpp: "cid_t", header: "SdInfo.h", bycopy.} = object
    mid* {.importc: "mid".}: uint8_t ##  byte 0
    ##  Manufacturer ID
    ##  byte 1-2
    oid* {.importc: "oid".}: array[2, char] ##  OEM/Application ID
                                       ##  byte 3-7
    pnm* {.importc: "pnm".}: array[5, char] ##  Product name
                                       ##  byte 8
    prv_m* {.importc: "prv_m".} {.bitsize: 4.}: cuint ##  Product revision n.m
    prv_n* {.importc: "prv_n".} {.bitsize: 4.}: cuint ##  byte 9-12
    psn* {.importc: "psn".}: uint32_t ##  Product serial number
                                  ##  byte 13
    mdt_year_high* {.importc: "mdt_year_high".} {.bitsize: 4.}: cuint ##  Manufacturing date
    reserved* {.importc: "reserved".} {.bitsize: 4.}: cuint ##  byte 14
    mdt_month* {.importc: "mdt_month".} {.bitsize: 4.}: cuint
    mdt_year_low* {.importc: "mdt_year_low".} {.bitsize: 4.}: cuint ##  byte 15
    always1* {.importc: "always1".} {.bitsize: 1.}: cuint
    crc* {.importc: "crc".} {.bitsize: 7.}: cuint


## ------------------------------------------------------------------------------
##  CSD for version 1.00 cards

type
  csd1_t* {.importcpp: "csd1_t", header: "SdInfo.h", bycopy.} = object
    reserved1* {.importc: "reserved1".} {.bitsize: 6.}: cuint ##  byte 0
    csd_ver* {.importc: "csd_ver".} {.bitsize: 2.}: cuint ##  byte 1
    taac* {.importc: "taac".}: uint8_t ##  byte 2
    nsac* {.importc: "nsac".}: uint8_t ##  byte 3
    tran_speed* {.importc: "tran_speed".}: uint8_t ##  byte 4
    ccc_high* {.importc: "ccc_high".}: uint8_t ##  byte 5
    read_bl_len* {.importc: "read_bl_len".} {.bitsize: 4.}: cuint
    ccc_low* {.importc: "ccc_low".} {.bitsize: 4.}: cuint ##  byte 6
    c_size_high* {.importc: "c_size_high".} {.bitsize: 2.}: cuint
    reserved2* {.importc: "reserved2".} {.bitsize: 2.}: cuint
    dsr_imp* {.importc: "dsr_imp".} {.bitsize: 1.}: cuint
    read_blk_misalign* {.importc: "read_blk_misalign".} {.bitsize: 1.}: cuint
    write_blk_misalign* {.importc: "write_blk_misalign".} {.bitsize: 1.}: cuint
    read_bl_partial* {.importc: "read_bl_partial".} {.bitsize: 1.}: cuint ##  byte 7
    c_size_mid* {.importc: "c_size_mid".}: uint8_t ##  byte 8
    vdd_r_curr_max* {.importc: "vdd_r_curr_max".} {.bitsize: 3.}: cuint
    vdd_r_curr_min* {.importc: "vdd_r_curr_min".} {.bitsize: 3.}: cuint
    c_size_low* {.importc: "c_size_low".} {.bitsize: 2.}: cuint ##  byte 9
    c_size_mult_high* {.importc: "c_size_mult_high".} {.bitsize: 2.}: cuint
    vdd_w_cur_max* {.importc: "vdd_w_cur_max".} {.bitsize: 3.}: cuint
    vdd_w_curr_min* {.importc: "vdd_w_curr_min".} {.bitsize: 3.}: cuint ##  byte 10
    sector_size_high* {.importc: "sector_size_high".} {.bitsize: 6.}: cuint
    erase_blk_en* {.importc: "erase_blk_en".} {.bitsize: 1.}: cuint
    c_size_mult_low* {.importc: "c_size_mult_low".} {.bitsize: 1.}: cuint ##  byte 11
    wp_grp_size* {.importc: "wp_grp_size".} {.bitsize: 7.}: cuint
    sector_size_low* {.importc: "sector_size_low".} {.bitsize: 1.}: cuint ##  byte 12
    write_bl_len_high* {.importc: "write_bl_len_high".} {.bitsize: 2.}: cuint
    r2w_factor* {.importc: "r2w_factor".} {.bitsize: 3.}: cuint
    reserved3* {.importc: "reserved3".} {.bitsize: 2.}: cuint
    wp_grp_enable* {.importc: "wp_grp_enable".} {.bitsize: 1.}: cuint ##  byte 13
    reserved4* {.importc: "reserved4".} {.bitsize: 5.}: cuint
    write_partial* {.importc: "write_partial".} {.bitsize: 1.}: cuint
    write_bl_len_low* {.importc: "write_bl_len_low".} {.bitsize: 2.}: cuint ##  byte 14
    reserved5* {.importc: "reserved5".} {.bitsize: 2.}: cuint
    file_format* {.importc: "file_format".} {.bitsize: 2.}: cuint
    tmp_write_protect* {.importc: "tmp_write_protect".} {.bitsize: 1.}: cuint
    perm_write_protect* {.importc: "perm_write_protect".} {.bitsize: 1.}: cuint
    copy* {.importc: "copy".} {.bitsize: 1.}: cuint
    file_format_grp* {.importc: "file_format_grp".} {.bitsize: 1.}: cuint ##  byte 15
    always1* {.importc: "always1".} {.bitsize: 1.}: cuint
    crc* {.importc: "crc".} {.bitsize: 7.}: cuint


## ------------------------------------------------------------------------------
##  CSD for version 2.00 cards

type
  csd2_t* {.importcpp: "csd2_t", header: "SdInfo.h", bycopy.} = object
    reserved1* {.importc: "reserved1".} {.bitsize: 6.}: cuint ##  byte 0
    csd_ver* {.importc: "csd_ver".} {.bitsize: 2.}: cuint ##  byte 1
    taac* {.importc: "taac".}: uint8_t ##  byte 2
    nsac* {.importc: "nsac".}: uint8_t ##  byte 3
    tran_speed* {.importc: "tran_speed".}: uint8_t ##  byte 4
    ccc_high* {.importc: "ccc_high".}: uint8_t ##  byte 5
    read_bl_len* {.importc: "read_bl_len".} {.bitsize: 4.}: cuint
    ccc_low* {.importc: "ccc_low".} {.bitsize: 4.}: cuint ##  byte 6
    reserved2* {.importc: "reserved2".} {.bitsize: 4.}: cuint
    dsr_imp* {.importc: "dsr_imp".} {.bitsize: 1.}: cuint
    read_blk_misalign* {.importc: "read_blk_misalign".} {.bitsize: 1.}: cuint
    write_blk_misalign* {.importc: "write_blk_misalign".} {.bitsize: 1.}: cuint
    read_bl_partial* {.importc: "read_bl_partial".} {.bitsize: 1.}: cuint ##  byte 7
    reserved3* {.importc: "reserved3".} {.bitsize: 2.}: cuint
    c_size_high* {.importc: "c_size_high".} {.bitsize: 6.}: cuint ##  byte 8
    c_size_mid* {.importc: "c_size_mid".}: uint8_t ##  byte 9
    c_size_low* {.importc: "c_size_low".}: uint8_t ##  byte 10
    sector_size_high* {.importc: "sector_size_high".} {.bitsize: 6.}: cuint
    erase_blk_en* {.importc: "erase_blk_en".} {.bitsize: 1.}: cuint
    reserved4* {.importc: "reserved4".} {.bitsize: 1.}: cuint ##  byte 11
    wp_grp_size* {.importc: "wp_grp_size".} {.bitsize: 7.}: cuint
    sector_size_low* {.importc: "sector_size_low".} {.bitsize: 1.}: cuint ##  byte 12
    write_bl_len_high* {.importc: "write_bl_len_high".} {.bitsize: 2.}: cuint
    r2w_factor* {.importc: "r2w_factor".} {.bitsize: 3.}: cuint
    reserved5* {.importc: "reserved5".} {.bitsize: 2.}: cuint
    wp_grp_enable* {.importc: "wp_grp_enable".} {.bitsize: 1.}: cuint ##  byte 13
    reserved6* {.importc: "reserved6".} {.bitsize: 5.}: cuint
    write_partial* {.importc: "write_partial".} {.bitsize: 1.}: cuint
    write_bl_len_low* {.importc: "write_bl_len_low".} {.bitsize: 2.}: cuint ##  byte 14
    reserved7* {.importc: "reserved7".} {.bitsize: 2.}: cuint
    file_format* {.importc: "file_format".} {.bitsize: 2.}: cuint
    tmp_write_protect* {.importc: "tmp_write_protect".} {.bitsize: 1.}: cuint
    perm_write_protect* {.importc: "perm_write_protect".} {.bitsize: 1.}: cuint
    copy* {.importc: "copy".} {.bitsize: 1.}: cuint
    file_format_grp* {.importc: "file_format_grp".} {.bitsize: 1.}: cuint ##  byte 15
    always1* {.importc: "always1".} {.bitsize: 1.}: cuint
    crc* {.importc: "crc".} {.bitsize: 7.}: cuint


## ------------------------------------------------------------------------------
##  union of old and new style CSD register

type
  csd_t* {.importcpp: "csd_t", header: "SdInfo.h", bycopy.} = object {.union.}
    v1* {.importc: "v1".}: csd1_t
    v2* {.importc: "v2".}: csd2_t

