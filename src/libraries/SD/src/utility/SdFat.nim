##  Arduino SdFat Library
##  Copyright (C) 2009 by William Greiman
## 
##  This file is part of the Arduino SdFat Library
## 
##  This Library is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
## 
##  This Library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
## 
##  You should have received a copy of the GNU General Public License
##  along with the Arduino SdFat Library.  If not, see
##  <http://www.gnu.org/licenses/>.
## 

## *
##  \file
##  SdFile and SdVolume classes
## 

when defined(__AVR__):
import
  Sd2Card, FatStructs, Print

## ------------------------------------------------------------------------------
## *
##  Allow use of deprecated functions if non-zero
## 

const
  ALLOW_DEPRECATED_FUNCTIONS* = 1

## ------------------------------------------------------------------------------
##  forward declaration since SdVolume is used in SdFile

discard "forward decl of SdVolume"
var LS_DATE* {.importcpp: "LS_DATE", header: "SdFat.h".}: uint8_t

## * ls() flag to print file size

var LS_SIZE* {.importcpp: "LS_SIZE", header: "SdFat.h".}: uint8_t

## * ls() flag for recursive list of subdirectories

var LS_R* {.importcpp: "LS_R", header: "SdFat.h".}: uint8_t

##  use the gnu style oflag in open()
## * open() oflag for reading

var O_READ* {.importcpp: "O_READ", header: "SdFat.h".}: uint8_t

## * open() oflag - same as O_READ

var O_RDONLY* {.importcpp: "O_RDONLY", header: "SdFat.h".}: uint8_t

## * open() oflag for write

var O_WRITE* {.importcpp: "O_WRITE", header: "SdFat.h".}: uint8_t

## * open() oflag - same as O_WRITE

var O_WRONLY* {.importcpp: "O_WRONLY", header: "SdFat.h".}: uint8_t

## * open() oflag for reading and writing

var O_RDWR* {.importcpp: "O_RDWR", header: "SdFat.h".}: uint8_t

## * open() oflag mask for access modes

var O_ACCMODE* {.importcpp: "O_ACCMODE", header: "SdFat.h".}: uint8_t

## * The file offset shall be set to the end of the file prior to each write.

var O_APPEND* {.importcpp: "O_APPEND", header: "SdFat.h".}: uint8_t

## * synchronous writes - call sync() after each write

var O_SYNC* {.importcpp: "O_SYNC", header: "SdFat.h".}: uint8_t

## * create the file if nonexistent

var O_CREAT* {.importcpp: "O_CREAT", header: "SdFat.h".}: uint8_t

## * If O_CREAT and O_EXCL are set, open() shall fail if the file exists

var O_EXCL* {.importcpp: "O_EXCL", header: "SdFat.h".}: uint8_t

## * truncate the file to zero length

var O_TRUNC* {.importcpp: "O_TRUNC", header: "SdFat.h".}: uint8_t

##  flags for timestamp
## * set the file's last access date

var T_ACCESS* {.importcpp: "T_ACCESS", header: "SdFat.h".}: uint8_t

## * set the file's creation date and time

var T_CREATE* {.importcpp: "T_CREATE", header: "SdFat.h".}: uint8_t

## * Set the file's write date and time

var T_WRITE* {.importcpp: "T_WRITE", header: "SdFat.h".}: uint8_t

##  values for type_
## * This SdFile has not been opened.

var FAT_FILE_TYPE_CLOSED* {.importcpp: "FAT_FILE_TYPE_CLOSED", header: "SdFat.h".}: uint8_t

## * SdFile for a file

var FAT_FILE_TYPE_NORMAL* {.importcpp: "FAT_FILE_TYPE_NORMAL", header: "SdFat.h".}: uint8_t

## * SdFile for a FAT16 root directory

var FAT_FILE_TYPE_ROOT16* {.importcpp: "FAT_FILE_TYPE_ROOT16", header: "SdFat.h".}: uint8_t

## * SdFile for a FAT32 root directory

var FAT_FILE_TYPE_ROOT32* {.importcpp: "FAT_FILE_TYPE_ROOT32", header: "SdFat.h".}: uint8_t

## * SdFile for a subdirectory

var FAT_FILE_TYPE_SUBDIR* {.importcpp: "FAT_FILE_TYPE_SUBDIR", header: "SdFat.h".}: uint8_t

## * Test value for directory type

var FAT_FILE_TYPE_MIN_DIR* {.importcpp: "FAT_FILE_TYPE_MIN_DIR", header: "SdFat.h".}: uint8_t

## * date field for FAT directory entry

proc FAT_DATE*(year: uint16_t; month: uint8_t; day: uint8_t): uint16_t {.cdecl.}
## * year part of FAT directory date field

proc FAT_YEAR*(fatDate: uint16_t): uint16_t {.cdecl.}
## * month part of FAT directory date field

proc FAT_MONTH*(fatDate: uint16_t): uint8_t {.cdecl.}
## * day part of FAT directory date field

proc FAT_DAY*(fatDate: uint16_t): uint8_t {.cdecl.}
## * time field for FAT directory entry

proc FAT_TIME*(hour: uint8_t; minute: uint8_t; second: uint8_t): uint16_t {.cdecl.}
## * hour part of FAT directory time field

proc FAT_HOUR*(fatTime: uint16_t): uint8_t {.cdecl.}
## * minute part of FAT directory time field

proc FAT_MINUTE*(fatTime: uint16_t): uint8_t {.cdecl.}
## * second part of FAT directory time field

proc FAT_SECOND*(fatTime: uint16_t): uint8_t {.cdecl.}
## * Default date for file timestamps is 1 Jan 2000

var FAT_DEFAULT_DATE* {.importcpp: "FAT_DEFAULT_DATE", header: "SdFat.h".}: uint16_t

## * Default time for file timestamp is 1 am

var FAT_DEFAULT_TIME* {.importcpp: "FAT_DEFAULT_TIME", header: "SdFat.h".}: uint16_t

## ------------------------------------------------------------------------------
## *
##  \class SdFile
##  \brief Access FAT16 and FAT32 files on SD and SDHC cards.
## 

type
  SdFile* {.importcpp: "SdFile", header: "SdFat.h", bycopy.} = object of Print ## * Create an instance of SdFile.
                                                                     ##  bits defined in flags_
                                                                     ##  should be 0XF
    ##  available bits
    ##  use unbuffered SD read
    ##  sync of directory entry required
    ##  make sure F_OFLAG is ok
    ##  private data
    ##  See above for definition of flags_ bits
    ##  type of file see above for values
    ##  cluster for current file position
    ##  current file position in bytes from beginning
    ##  SD block that contains directory entry for file
    ##  index of entry in dirBlock 0 <= dirIndex_ <= 0XF
    ##  file size in bytes
    ##  first cluster of file
    ##  volume where file is located
    ##  private functions
  

proc constructSdFile*(): SdFile {.cdecl, constructor, importcpp: "SdFile(@)",
                               header: "SdFat.h".}
proc clearUnbufferedRead*(this: var SdFile) {.cdecl,
    importcpp: "clearUnbufferedRead", header: "SdFat.h".}
proc close*(this: var SdFile): uint8_t {.cdecl, importcpp: "close", header: "SdFat.h".}
proc contiguousRange*(this: var SdFile; bgnBlock: ptr uint32_t; endBlock: ptr uint32_t): uint8_t {.
    cdecl, importcpp: "contiguousRange", header: "SdFat.h".}
proc createContiguous*(this: var SdFile; dirFile: ptr SdFile; fileName: cstring;
                      size: uint32_t): uint8_t {.cdecl,
    importcpp: "createContiguous", header: "SdFat.h".}
proc curCluster*(this: SdFile): uint32_t {.noSideEffect, cdecl,
                                       importcpp: "curCluster", header: "SdFat.h".}
proc curPosition*(this: SdFile): uint32_t {.noSideEffect, cdecl,
                                        importcpp: "curPosition",
                                        header: "SdFat.h".}
proc dateTimeCallback*(dateTime: proc (date: ptr uint16_t; time: ptr uint16_t) {.cdecl.}) {.
    cdecl, importcpp: "SdFile::dateTimeCallback(@)", header: "SdFat.h".}
proc dateTimeCallbackCancel*() {.cdecl,
                               importcpp: "SdFile::dateTimeCallbackCancel(@)",
                               header: "SdFat.h".}
proc dirBlock*(this: SdFile): uint32_t {.noSideEffect, cdecl, importcpp: "dirBlock",
                                     header: "SdFat.h".}
proc dirEntry*(this: var SdFile; dir: ptr dir_t): uint8_t {.cdecl, importcpp: "dirEntry",
    header: "SdFat.h".}
proc dirIndex*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "dirIndex",
                                    header: "SdFat.h".}
proc dirName*(dir: dir_t; name: cstring) {.cdecl, importcpp: "SdFile::dirName(@)",
                                      header: "SdFat.h".}
proc fileSize*(this: SdFile): uint32_t {.noSideEffect, cdecl, importcpp: "fileSize",
                                     header: "SdFat.h".}
proc firstCluster*(this: SdFile): uint32_t {.noSideEffect, cdecl,
    importcpp: "firstCluster", header: "SdFat.h".}
proc isDir*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "isDir",
                                 header: "SdFat.h".}
proc isFile*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "isFile",
                                  header: "SdFat.h".}
proc isOpen*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "isOpen",
                                  header: "SdFat.h".}
proc isSubDir*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "isSubDir",
                                    header: "SdFat.h".}
proc isRoot*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "isRoot",
                                  header: "SdFat.h".}
proc ls*(this: var SdFile; flags: uint8_t = 0; indent: uint8_t = 0) {.cdecl, importcpp: "ls",
    header: "SdFat.h".}
proc makeDir*(this: var SdFile; dir: ptr SdFile; dirName: cstring): uint8_t {.cdecl,
    importcpp: "makeDir", header: "SdFat.h".}
proc open*(this: var SdFile; dirFile: ptr SdFile; index: uint16_t; oflag: uint8_t): uint8_t {.
    cdecl, importcpp: "open", header: "SdFat.h".}
proc open*(this: var SdFile; dirFile: ptr SdFile; fileName: cstring; oflag: uint8_t): uint8_t {.
    cdecl, importcpp: "open", header: "SdFat.h".}
proc openRoot*(this: var SdFile; vol: ptr SdVolume): uint8_t {.cdecl,
    importcpp: "openRoot", header: "SdFat.h".}
proc printDirName*(dir: dir_t; width: uint8_t) {.cdecl,
    importcpp: "SdFile::printDirName(@)", header: "SdFat.h".}
proc printFatDate*(fatDate: uint16_t) {.cdecl, importcpp: "SdFile::printFatDate(@)",
                                     header: "SdFat.h".}
proc printFatTime*(fatTime: uint16_t) {.cdecl, importcpp: "SdFile::printFatTime(@)",
                                     header: "SdFat.h".}
proc printTwoDigits*(v: uint8_t) {.cdecl, importcpp: "SdFile::printTwoDigits(@)",
                                header: "SdFat.h".}
proc read*(this: var SdFile): int16_t {.cdecl, importcpp: "read", header: "SdFat.h".}
proc read*(this: var SdFile; buf: pointer; nbyte: uint16_t): int16_t {.cdecl,
    importcpp: "read", header: "SdFat.h".}
proc readDir*(this: var SdFile; dir: ptr dir_t): int8_t {.cdecl, importcpp: "readDir",
    header: "SdFat.h".}
proc remove*(dirFile: ptr SdFile; fileName: cstring): uint8_t {.cdecl,
    importcpp: "SdFile::remove(@)", header: "SdFat.h".}
proc remove*(this: var SdFile): uint8_t {.cdecl, importcpp: "remove", header: "SdFat.h".}
proc rewind*(this: var SdFile) {.cdecl, importcpp: "rewind", header: "SdFat.h".}
proc rmDir*(this: var SdFile): uint8_t {.cdecl, importcpp: "rmDir", header: "SdFat.h".}
proc rmRfStar*(this: var SdFile): uint8_t {.cdecl, importcpp: "rmRfStar",
                                       header: "SdFat.h".}
proc seekCur*(this: var SdFile; pos: uint32_t): uint8_t {.cdecl, importcpp: "seekCur",
    header: "SdFat.h".}
proc seekEnd*(this: var SdFile): uint8_t {.cdecl, importcpp: "seekEnd",
                                      header: "SdFat.h".}
proc seekSet*(this: var SdFile; pos: uint32_t): uint8_t {.cdecl, importcpp: "seekSet",
    header: "SdFat.h".}
proc setUnbufferedRead*(this: var SdFile) {.cdecl, importcpp: "setUnbufferedRead",
                                        header: "SdFat.h".}
proc timestamp*(this: var SdFile; flag: uint8_t; year: uint16_t; month: uint8_t;
               day: uint8_t; hour: uint8_t; minute: uint8_t; second: uint8_t): uint8_t {.
    cdecl, importcpp: "timestamp", header: "SdFat.h".}
proc sync*(this: var SdFile): uint8_t {.cdecl, importcpp: "sync", header: "SdFat.h".}
proc `type`*(this: SdFile): uint8_t {.noSideEffect, cdecl, importcpp: "type",
                                  header: "SdFat.h".}
proc truncate*(this: var SdFile; size: uint32_t): uint8_t {.cdecl,
    importcpp: "truncate", header: "SdFat.h".}
proc unbufferedRead*(this: SdFile): uint8_t {.noSideEffect, cdecl,
    importcpp: "unbufferedRead", header: "SdFat.h".}
proc volume*(this: SdFile): ptr SdVolume {.noSideEffect, cdecl, importcpp: "volume",
                                      header: "SdFat.h".}
proc write*(this: var SdFile; b: uint8_t): csize {.cdecl, importcpp: "write",
    header: "SdFat.h".}
proc write*(this: var SdFile; buf: pointer; nbyte: uint16_t): csize {.cdecl,
    importcpp: "write", header: "SdFat.h".}
proc write*(this: var SdFile; str: cstring): csize {.cdecl, importcpp: "write",
    header: "SdFat.h".}
proc write_P*(this: var SdFile; str: PGM_P) {.cdecl, importcpp: "write_P",
                                        header: "SdFat.h".}
proc writeln_P*(this: var SdFile; str: PGM_P) {.cdecl, importcpp: "writeln_P",
    header: "SdFat.h".}
proc contiguousRange*(this: var SdFile; bgnBlock: var uint32_t; endBlock: var uint32_t): uint8_t {.
    cdecl, importcpp: "contiguousRange", header: "SdFat.h".}
proc createContiguous*(this: var SdFile; dirFile: var SdFile; fileName: cstring;
                      size: uint32_t): uint8_t {.cdecl,
    importcpp: "createContiguous", header: "SdFat.h".}
  ##  NOLINT
proc dateTimeCallback*(dateTime: proc (date: var uint16_t; time: var uint16_t) {.cdecl.}) {.
    cdecl, importcpp: "SdFile::dateTimeCallback(@)", header: "SdFat.h".}
proc dirEntry*(this: var SdFile; dir: var dir_t): uint8_t {.cdecl, importcpp: "dirEntry",
    header: "SdFat.h".}
proc makeDir*(this: var SdFile; dir: var SdFile; dirName: cstring): uint8_t {.cdecl,
    importcpp: "makeDir", header: "SdFat.h".}
proc open*(this: var SdFile; dirFile: var SdFile; fileName: cstring; oflag: uint8_t): uint8_t {.
    cdecl, importcpp: "open", header: "SdFat.h".}
  ##  NOLINT
proc open*(this: var SdFile; dirFile: var SdFile; fileName: cstring): uint8_t {.cdecl,
    importcpp: "open", header: "SdFat.h".}
proc open*(this: var SdFile; dirFile: var SdFile; index: uint16_t; oflag: uint8_t): uint8_t {.
    cdecl, importcpp: "open", header: "SdFat.h".}
proc openRoot*(this: var SdFile; vol: var SdVolume): uint8_t {.cdecl,
    importcpp: "openRoot", header: "SdFat.h".}
proc readDir*(this: var SdFile; dir: var dir_t): int8_t {.cdecl, importcpp: "readDir",
    header: "SdFat.h".}
proc remove*(dirFile: var SdFile; fileName: cstring): uint8_t {.cdecl,
    importcpp: "SdFile::remove(@)", header: "SdFat.h".}
## ==============================================================================
##  SdVolume class
## *
##  \brief Cache for an SD data block
## 

type
  cache_t* {.importcpp: "cache_t", header: "SdFat.h", bycopy.} = object {.union.}
    data* {.importc: "data".}: array[512, uint8_t] ## * Used to access cached file data blocks.
    ## * Used to access cached FAT16 entries.
    fat16* {.importc: "fat16".}: array[256, uint16_t] ## * Used to access cached FAT32 entries.
    fat32* {.importc: "fat32".}: array[128, uint32_t] ## * Used to access cached directory entries.
    dir* {.importc: "dir".}: array[16, dir_t] ## * Used to access a cached MasterBoot Record.
    mbr* {.importc: "mbr".}: mbr_t ## * Used to access to a cached FAT boot sector.
    fbs* {.importc: "fbs".}: fbs_t


## ------------------------------------------------------------------------------
## *
##  \class SdVolume
##  \brief Access FAT16 and FAT32 volumes on SD and SDHC cards.
## 

type
  SdVolume* {.importcpp: "SdVolume", header: "SdFat.h", bycopy.} = object ## * Create an instance of SdVolume
                                                                  ##  Allow SdFile access to SdVolume private data.
    ##  value for action argument in cacheRawBlock to indicate cache dirty
    ##  512 byte cache for device blocks
    ##  Logical number of block in the cache
    ##  Sd2Card object for cache
    ##  cacheFlush() will write block if true
    ##  block number for mirror FAT
    ## 
    ##  start cluster for alloc search
    ##  cluster size in blocks
    ##  FAT size in blocks
    ##  clusters in one FAT
    ##  shift to convert cluster count to block count
    ##  first data block number
    ##  number of FATs on volume
    ##  start block for first FAT
    ##  volume type (12, 16, OR 32)
    ##  number of entries in FAT16 root dir
    ##  root start block for FAT16, cluster for FAT32
    ## ----------------------------------------------------------------------------
  

proc constructSdVolume*(): SdVolume {.cdecl, constructor, importcpp: "SdVolume(@)",
                                   header: "SdFat.h".}
proc cacheClear*(): ptr uint8_t {.cdecl, importcpp: "SdVolume::cacheClear(@)",
                              header: "SdFat.h".}
proc init*(this: var SdVolume; dev: ptr Sd2Card): uint8_t {.cdecl, importcpp: "init",
    header: "SdFat.h".}
proc init*(this: var SdVolume; dev: ptr Sd2Card; part: uint8_t): uint8_t {.cdecl,
    importcpp: "init", header: "SdFat.h".}
proc blocksPerCluster*(this: SdVolume): uint8_t {.noSideEffect, cdecl,
    importcpp: "blocksPerCluster", header: "SdFat.h".}
proc blocksPerFat*(this: SdVolume): uint32_t {.noSideEffect, cdecl,
    importcpp: "blocksPerFat", header: "SdFat.h".}
proc clusterCount*(this: SdVolume): uint32_t {.noSideEffect, cdecl,
    importcpp: "clusterCount", header: "SdFat.h".}
proc clusterSizeShift*(this: SdVolume): uint8_t {.noSideEffect, cdecl,
    importcpp: "clusterSizeShift", header: "SdFat.h".}
proc dataStartBlock*(this: SdVolume): uint32_t {.noSideEffect, cdecl,
    importcpp: "dataStartBlock", header: "SdFat.h".}
proc fatCount*(this: SdVolume): uint8_t {.noSideEffect, cdecl, importcpp: "fatCount",
                                      header: "SdFat.h".}
proc fatStartBlock*(this: SdVolume): uint32_t {.noSideEffect, cdecl,
    importcpp: "fatStartBlock", header: "SdFat.h".}
proc fatType*(this: SdVolume): uint8_t {.noSideEffect, cdecl, importcpp: "fatType",
                                     header: "SdFat.h".}
proc rootDirEntryCount*(this: SdVolume): uint32_t {.noSideEffect, cdecl,
    importcpp: "rootDirEntryCount", header: "SdFat.h".}
proc rootDirStart*(this: SdVolume): uint32_t {.noSideEffect, cdecl,
    importcpp: "rootDirStart", header: "SdFat.h".}
proc sdCard*(): ptr Sd2Card {.cdecl, importcpp: "SdVolume::sdCard(@)",
                          header: "SdFat.h".}
proc init*(this: var SdVolume; dev: var Sd2Card): uint8_t {.cdecl, importcpp: "init",
    header: "SdFat.h".}
proc init*(this: var SdVolume; dev: var Sd2Card; part: uint8_t): uint8_t {.cdecl,
    importcpp: "init", header: "SdFat.h".}