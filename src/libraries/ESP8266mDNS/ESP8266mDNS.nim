## 
## ESP8266 Multicast DNS (port of CC3000 Multicast DNS library)
## Version 1.1
## Copyright (c) 2013 Tony DiCola (tony@tonydicola.com)
## ESP8266 port (c) 2015 Ivan Grokhotkov (ivan@esp8266.com)
## Extended MDNS-SD support 2016 Lars Englund (lars.englund@gmail.com)
## 
## This is a simple implementation of multicast DNS query support for an Arduino
## running on ESP8266 chip. Only support for resolving address queries is currently
## implemented.
## 
## Requirements:
## - ESP8266WiFi library
## 
## Usage:
## - Include the ESP8266 Multicast DNS library in the sketch.
## - Call the begin method in the sketch's setup and provide a domain name (without
##   the '.local' suffix, i.e. just provide 'foo' to resolve 'foo.local'), and the
##   Adafruit CC3000 class instance.  Optionally provide a time to live (in seconds)
##   for the DNS record--the default is 1 hour.
## - Call the update method in each iteration of the sketch's loop function.
## 
## License (MIT license):
##   Permission is hereby granted, free of charge, to any person obtaining a copy
##   of this software and associated documentation files (the "Software"), to deal
##   in the Software without restriction, including without limitation the rights
##   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##   copies of the Software, and to permit persons to whom the Software is
##   furnished to do so, subject to the following conditions:
## 
##   The above copyright notice and this permission notice shall be included in
##   all copies or substantial portions of the Software.
## 
##   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##   THE SOFTWARE.
## 
## 

import
  ESP8266WiFi, WiFiUdp

## this should be defined at build time

discard "forward decl of UdpContext"
discard "forward decl of MDNSService"
discard "forward decl of MDNSTxt"
discard "forward decl of MDNSAnswer"
type
  MDNSResponder* {.importcpp: "MDNSResponder", header: "ESP8266mDNS.h", bycopy.} = object
  

proc constructMDNSResponder*(): MDNSResponder {.cdecl, constructor,
    importcpp: "MDNSResponder(@)", header: "ESP8266mDNS.h".}
proc destroyMDNSResponder*(this: var MDNSResponder) {.cdecl,
    importcpp: "#.~MDNSResponder()", header: "ESP8266mDNS.h".}
proc begin*(this: var MDNSResponder; hostName: cstring): bool {.cdecl,
    importcpp: "begin", header: "ESP8266mDNS.h".}
proc begin*(this: var MDNSResponder; hostName: cstring; ip: IPAddress;
           ttl: uint32_t = 120): bool {.cdecl, importcpp: "begin",
                                   header: "ESP8266mDNS.h".}
proc notifyAPChange*(this: var MDNSResponder) {.cdecl, importcpp: "notifyAPChange",
    header: "ESP8266mDNS.h".}
proc update*(this: var MDNSResponder) {.cdecl, importcpp: "update",
                                    header: "ESP8266mDNS.h".}
proc addService*(this: var MDNSResponder; service: cstring; proto: cstring;
                port: uint16_t) {.cdecl, importcpp: "addService",
                                header: "ESP8266mDNS.h".}
proc addService*(this: var MDNSResponder; service: cstring; proto: cstring;
                port: uint16_t) {.cdecl, importcpp: "addService",
                                header: "ESP8266mDNS.h".}
proc addService*(this: var MDNSResponder; service: String; proto: String; port: uint16_t) {.
    cdecl, importcpp: "addService", header: "ESP8266mDNS.h".}
proc addServiceTxt*(this: var MDNSResponder; name: cstring; proto: cstring;
                   key: cstring; value: cstring): bool {.cdecl,
    importcpp: "addServiceTxt", header: "ESP8266mDNS.h".}
proc addServiceTxt*(this: var MDNSResponder; name: cstring; proto: cstring;
                   key: cstring; value: cstring): bool {.cdecl,
    importcpp: "addServiceTxt", header: "ESP8266mDNS.h".}
proc addServiceTxt*(this: var MDNSResponder; name: String; proto: String; key: String;
                   value: String): bool {.cdecl, importcpp: "addServiceTxt",
                                       header: "ESP8266mDNS.h".}
proc queryService*(this: var MDNSResponder; service: cstring; proto: cstring): cint {.
    cdecl, importcpp: "queryService", header: "ESP8266mDNS.h".}
proc queryService*(this: var MDNSResponder; service: cstring; proto: cstring): cint {.
    cdecl, importcpp: "queryService", header: "ESP8266mDNS.h".}
proc queryService*(this: var MDNSResponder; service: String; proto: String): cint {.
    cdecl, importcpp: "queryService", header: "ESP8266mDNS.h".}
proc hostname*(this: var MDNSResponder; idx: cint): String {.cdecl,
    importcpp: "hostname", header: "ESP8266mDNS.h".}
proc IP*(this: var MDNSResponder; idx: cint): IPAddress {.cdecl, importcpp: "IP",
    header: "ESP8266mDNS.h".}
proc port*(this: var MDNSResponder; idx: cint): uint16_t {.cdecl, importcpp: "port",
    header: "ESP8266mDNS.h".}
proc enableArduino*(this: var MDNSResponder; port: uint16_t; auth: bool = false) {.cdecl,
    importcpp: "enableArduino", header: "ESP8266mDNS.h".}
proc setInstanceName*(this: var MDNSResponder; name: String) {.cdecl,
    importcpp: "setInstanceName", header: "ESP8266mDNS.h".}
proc setInstanceName*(this: var MDNSResponder; name: cstring) {.cdecl,
    importcpp: "setInstanceName", header: "ESP8266mDNS.h".}
proc setInstanceName*(this: var MDNSResponder; name: cstring) {.cdecl,
    importcpp: "setInstanceName", header: "ESP8266mDNS.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_MDNS):
  var MDNS* {.importcpp: "MDNS", header: "ESP8266mDNS.h".}: MDNSResponder