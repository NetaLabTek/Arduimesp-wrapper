## 
##   Servo.h - Interrupt driven Servo library for Esp8266 using timers
##   Original Copyright (c) 2015 Michael C. Miller. All right reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##    A servo is activated by creating an instance of the Servo class passing
##    the desired pin to the attach() method.
##    The servos are pulsed in the background using the value most recently
##    written using the write() method.
## 
##    The methods are:
## 
##    Servo - Class for manipulating servo motors connected to Arduino pins.
## 
##    attach(pin )  - Attaches a servo motor to an i/o pin.
##    attach(pin, min, max  ) - Attaches to a pin setting min and max values in microseconds
##    default min is 544, max is 2400
## 
##    write()     - Sets the servo angle in degrees.  (invalid angle that is valid as pulse in microseconds is treated as microseconds)
##    writeMicroseconds() - Sets the servo pulse width in microseconds
##    read()      - Gets the last written servo pulse width as an angle between 0 and 180.
##    readMicroseconds()   - Gets the last written servo pulse width in microseconds. (was read_us() in first release)
##    attached()  - Returns true if there is a servo attached.
##    detach()    - Stops an attached servos from pulsing its i/o pin.

##  the following are in us (microseconds)
## 

const
  MIN_PULSE_WIDTH* = 544
  MAX_PULSE_WIDTH* = 2400
  DEFAULT_PULSE_WIDTH* = 1500
  REFRESH_INTERVAL* = 20000

when not defined(ESP8266):
type
  Servo* {.importcpp: "Servo", header: "Servo.h", bycopy.} = object
  

proc constructServo*(): Servo {.cdecl, constructor, importcpp: "Servo(@)",
                             header: "Servo.h".}
proc destroyServo*(this: var Servo) {.cdecl, importcpp: "#.~Servo()", header: "Servo.h".}
proc attach*(this: var Servo; pin: cint): uint8_t {.cdecl, importcpp: "attach",
    header: "Servo.h".}
proc attach*(this: var Servo; pin: cint; min: uint16_t; max: uint16_t): uint8_t {.cdecl,
    importcpp: "attach", header: "Servo.h".}
proc detach*(this: var Servo) {.cdecl, importcpp: "detach", header: "Servo.h".}
proc write*(this: var Servo; value: cint) {.cdecl, importcpp: "write", header: "Servo.h".}
proc writeMicroseconds*(this: var Servo; value: cint) {.cdecl,
    importcpp: "writeMicroseconds", header: "Servo.h".}
proc read*(this: var Servo): cint {.cdecl, importcpp: "read", header: "Servo.h".}
proc readMicroseconds*(this: var Servo): cint {.cdecl, importcpp: "readMicroseconds",
    header: "Servo.h".}
proc attached*(this: var Servo): bool {.cdecl, importcpp: "attached", header: "Servo.h".}