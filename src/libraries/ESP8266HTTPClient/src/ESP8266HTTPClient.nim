## *
##  ESP8266HTTPClient.h
## 
##  Created on: 02.11.2015
## 
##  Copyright (c) 2015 Markus Sattler. All rights reserved.
##  This file is part of the ESP8266HTTPClient for Arduino.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
## 

when defined(DEBUG_ESP_HTTP_CLIENT):
  when defined(DEBUG_ESP_PORT):
    template DEBUG_HTTPCLIENT*(): untyped =
      DEBUG_ESP_PORT.printf(__VA_ARGS__)

when not defined(DEBUG_HTTPCLIENT):
  template DEBUG_HTTPCLIENT*(): void =
    nil

const
  HTTPCLIENT_DEFAULT_TCP_TIMEOUT* = (5000)

## / HTTP client errors

const
  HTTPC_ERROR_CONNECTION_REFUSED* = (-1)
  HTTPC_ERROR_SEND_HEADER_FAILED* = (-2)
  HTTPC_ERROR_SEND_PAYLOAD_FAILED* = (-3)
  HTTPC_ERROR_NOT_CONNECTED* = (-4)
  HTTPC_ERROR_CONNECTION_LOST* = (-5)
  HTTPC_ERROR_NO_STREAM* = (-6)
  HTTPC_ERROR_NO_HTTP_SERVER* = (-7)
  HTTPC_ERROR_TOO_LESS_RAM* = (-8)
  HTTPC_ERROR_ENCODING* = (-9)
  HTTPC_ERROR_STREAM_WRITE* = (-10)
  HTTPC_ERROR_READ_TIMEOUT* = (-11)

## / size for the stream handling

const
  HTTP_TCP_BUFFER_SIZE* = (1460)

## / HTTP codes see RFC7231

type
  t_http_codes* {.size: sizeof(cint), importcpp: "t_http_codes",
                 header: "ESP8266HTTPClient.h".} = enum
    HTTP_CODE_CONTINUE = 100, HTTP_CODE_SWITCHING_PROTOCOLS = 101,
    HTTP_CODE_PROCESSING = 102, HTTP_CODE_OK = 200, HTTP_CODE_CREATED = 201,
    HTTP_CODE_ACCEPTED = 202, HTTP_CODE_NON_AUTHORITATIVE_INFORMATION = 203,
    HTTP_CODE_NO_CONTENT = 204, HTTP_CODE_RESET_CONTENT = 205,
    HTTP_CODE_PARTIAL_CONTENT = 206, HTTP_CODE_MULTI_STATUS = 207,
    HTTP_CODE_ALREADY_REPORTED = 208, HTTP_CODE_IM_USED = 226,
    HTTP_CODE_MULTIPLE_CHOICES = 300, HTTP_CODE_MOVED_PERMANENTLY = 301,
    HTTP_CODE_FOUND = 302, HTTP_CODE_SEE_OTHER = 303, HTTP_CODE_NOT_MODIFIED = 304,
    HTTP_CODE_USE_PROXY = 305, HTTP_CODE_TEMPORARY_REDIRECT = 307,
    HTTP_CODE_PERMANENT_REDIRECT = 308, HTTP_CODE_BAD_REQUEST = 400,
    HTTP_CODE_UNAUTHORIZED = 401, HTTP_CODE_PAYMENT_REQUIRED = 402,
    HTTP_CODE_FORBIDDEN = 403, HTTP_CODE_NOT_FOUND = 404,
    HTTP_CODE_METHOD_NOT_ALLOWED = 405, HTTP_CODE_NOT_ACCEPTABLE = 406,
    HTTP_CODE_PROXY_AUTHENTICATION_REQUIRED = 407, HTTP_CODE_REQUEST_TIMEOUT = 408,
    HTTP_CODE_CONFLICT = 409, HTTP_CODE_GONE = 410, HTTP_CODE_LENGTH_REQUIRED = 411,
    HTTP_CODE_PRECONDITION_FAILED = 412, HTTP_CODE_PAYLOAD_TOO_LARGE = 413,
    HTTP_CODE_URI_TOO_LONG = 414, HTTP_CODE_UNSUPPORTED_MEDIA_TYPE = 415,
    HTTP_CODE_RANGE_NOT_SATISFIABLE = 416, HTTP_CODE_EXPECTATION_FAILED = 417,
    HTTP_CODE_MISDIRECTED_REQUEST = 421, HTTP_CODE_UNPROCESSABLE_ENTITY = 422,
    HTTP_CODE_LOCKED = 423, HTTP_CODE_FAILED_DEPENDENCY = 424,
    HTTP_CODE_UPGRADE_REQUIRED = 426, HTTP_CODE_PRECONDITION_REQUIRED = 428,
    HTTP_CODE_TOO_MANY_REQUESTS = 429,
    HTTP_CODE_REQUEST_HEADER_FIELDS_TOO_LARGE = 431,
    HTTP_CODE_INTERNAL_SERVER_ERROR = 500, HTTP_CODE_NOT_IMPLEMENTED = 501,
    HTTP_CODE_BAD_GATEWAY = 502, HTTP_CODE_SERVICE_UNAVAILABLE = 503,
    HTTP_CODE_GATEWAY_TIMEOUT = 504, HTTP_CODE_HTTP_VERSION_NOT_SUPPORTED = 505,
    HTTP_CODE_VARIANT_ALSO_NEGOTIATES = 506, HTTP_CODE_INSUFFICIENT_STORAGE = 507,
    HTTP_CODE_LOOP_DETECTED = 508, HTTP_CODE_NOT_EXTENDED = 510,
    HTTP_CODE_NETWORK_AUTHENTICATION_REQUIRED = 511
  transferEncoding_t* {.size: sizeof(cint), importcpp: "transferEncoding_t",
                       header: "ESP8266HTTPClient.h".} = enum
    HTTPC_TE_IDENTITY, HTTPC_TE_CHUNKED



discard "forward decl of TransportTraits"
type
  TransportTraitsPtr* = unique_ptr[TransportTraits]
  HTTPClient* {.importcpp: "HTTPClient", header: "ESP8266HTTPClient.h", bycopy.} = object
    ## / request handling
    ## / Response handling
  

proc constructHTTPClient*(): HTTPClient {.cdecl, constructor,
                                       importcpp: "HTTPClient(@)",
                                       header: "ESP8266HTTPClient.h".}
proc destroyHTTPClient*(this: var HTTPClient) {.cdecl, importcpp: "#.~HTTPClient()",
    header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; url: String): bool {.cdecl, importcpp: "begin",
    header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; host: String; port: uint16_t; uri: String = "/"): bool {.
    cdecl, importcpp: "begin", header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; url: String; httpsFingerprint: String): bool {.cdecl,
    importcpp: "begin", header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; host: String; port: uint16_t; uri: String;
           httpsFingerprint: String): bool {.cdecl, importcpp: "begin",
    header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; url: String; httpsFingerprint: array[20, uint8_t]): bool {.
    cdecl, importcpp: "begin", header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; host: String; port: uint16_t; uri: String;
           httpsFingerprint: array[20, uint8_t]): bool {.cdecl, importcpp: "begin",
    header: "ESP8266HTTPClient.h".}
proc begin*(this: var HTTPClient; host: String; port: uint16_t; uri: String; https: bool;
           httpsFingerprint: String): bool {.cdecl, importcpp: "begin",
    header: "ESP8266HTTPClient.h".}
proc `end`*(this: var HTTPClient) {.cdecl, importcpp: "end",
                                header: "ESP8266HTTPClient.h".}
proc connected*(this: var HTTPClient): bool {.cdecl, importcpp: "connected",
    header: "ESP8266HTTPClient.h".}
proc setReuse*(this: var HTTPClient; reuse: bool) {.cdecl, importcpp: "setReuse",
    header: "ESP8266HTTPClient.h".}
proc setUserAgent*(this: var HTTPClient; userAgent: String) {.cdecl,
    importcpp: "setUserAgent", header: "ESP8266HTTPClient.h".}
proc setAuthorization*(this: var HTTPClient; user: cstring; password: cstring) {.cdecl,
    importcpp: "setAuthorization", header: "ESP8266HTTPClient.h".}
proc setAuthorization*(this: var HTTPClient; auth: cstring) {.cdecl,
    importcpp: "setAuthorization", header: "ESP8266HTTPClient.h".}
proc setTimeout*(this: var HTTPClient; timeout: uint16_t) {.cdecl,
    importcpp: "setTimeout", header: "ESP8266HTTPClient.h".}
proc useHTTP10*(this: var HTTPClient; usehttp10: bool = true) {.cdecl,
    importcpp: "useHTTP10", header: "ESP8266HTTPClient.h".}
proc GET*(this: var HTTPClient): cint {.cdecl, importcpp: "GET",
                                   header: "ESP8266HTTPClient.h".}
proc POST*(this: var HTTPClient; payload: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "POST", header: "ESP8266HTTPClient.h".}
proc POST*(this: var HTTPClient; payload: String): cint {.cdecl, importcpp: "POST",
    header: "ESP8266HTTPClient.h".}
proc PUT*(this: var HTTPClient; payload: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "PUT", header: "ESP8266HTTPClient.h".}
proc PUT*(this: var HTTPClient; payload: String): cint {.cdecl, importcpp: "PUT",
    header: "ESP8266HTTPClient.h".}
proc PATCH*(this: var HTTPClient; payload: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "PATCH", header: "ESP8266HTTPClient.h".}
proc PATCH*(this: var HTTPClient; payload: String): cint {.cdecl, importcpp: "PATCH",
    header: "ESP8266HTTPClient.h".}
proc sendRequest*(this: var HTTPClient; `type`: cstring; payload: String): cint {.cdecl,
    importcpp: "sendRequest", header: "ESP8266HTTPClient.h".}
proc sendRequest*(this: var HTTPClient; `type`: cstring; payload: ptr uint8_t = nil;
                 size: csize = 0): cint {.cdecl, importcpp: "sendRequest",
                                     header: "ESP8266HTTPClient.h".}
proc sendRequest*(this: var HTTPClient; `type`: cstring; stream: ptr Stream;
                 size: csize = 0): cint {.cdecl, importcpp: "sendRequest",
                                     header: "ESP8266HTTPClient.h".}
proc addHeader*(this: var HTTPClient; name: String; value: String; first: bool = false;
               replace: bool = true) {.cdecl, importcpp: "addHeader",
                                   header: "ESP8266HTTPClient.h".}
proc collectHeaders*(this: var HTTPClient; headerKeys: ptr cstring;
                    headerKeysCount: csize) {.cdecl, importcpp: "collectHeaders",
    header: "ESP8266HTTPClient.h".}
proc header*(this: var HTTPClient; name: cstring): String {.cdecl, importcpp: "header",
    header: "ESP8266HTTPClient.h".}
proc header*(this: var HTTPClient; i: csize): String {.cdecl, importcpp: "header",
    header: "ESP8266HTTPClient.h".}
proc headerName*(this: var HTTPClient; i: csize): String {.cdecl,
    importcpp: "headerName", header: "ESP8266HTTPClient.h".}
proc headers*(this: var HTTPClient): cint {.cdecl, importcpp: "headers",
                                       header: "ESP8266HTTPClient.h".}
proc hasHeader*(this: var HTTPClient; name: cstring): bool {.cdecl,
    importcpp: "hasHeader", header: "ESP8266HTTPClient.h".}
proc getSize*(this: var HTTPClient): cint {.cdecl, importcpp: "getSize",
                                       header: "ESP8266HTTPClient.h".}
proc getStream*(this: var HTTPClient): var WiFiClient {.cdecl, importcpp: "getStream",
    header: "ESP8266HTTPClient.h".}
proc getStreamPtr*(this: var HTTPClient): ptr WiFiClient {.cdecl,
    importcpp: "getStreamPtr", header: "ESP8266HTTPClient.h".}
proc writeToStream*(this: var HTTPClient; stream: ptr Stream): cint {.cdecl,
    importcpp: "writeToStream", header: "ESP8266HTTPClient.h".}
proc getString*(this: var HTTPClient): String {.cdecl, importcpp: "getString",
    header: "ESP8266HTTPClient.h".}
proc errorToString*(error: cint): String {.cdecl, importcpp: "HTTPClient::errorToString(@)",
                                       header: "ESP8266HTTPClient.h".}