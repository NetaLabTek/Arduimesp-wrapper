discard "forward decl of ESP8266WebServer"
type
  ESP8266HTTPUpdateServer* {.importcpp: "ESP8266HTTPUpdateServer",
                            header: "ESP8266HTTPUpdateServer.h", bycopy.} = object
  

proc constructESP8266HTTPUpdateServer*(serial_debug: bool = false): ESP8266HTTPUpdateServer {.
    cdecl, constructor, importcpp: "ESP8266HTTPUpdateServer(@)",
    header: "ESP8266HTTPUpdateServer.h".}
proc setup*(this: var ESP8266HTTPUpdateServer; server: ptr ESP8266WebServer) {.cdecl,
    importcpp: "setup", header: "ESP8266HTTPUpdateServer.h".}
proc setup*(this: var ESP8266HTTPUpdateServer; server: ptr ESP8266WebServer;
           path: cstring) {.cdecl, importcpp: "setup",
                          header: "ESP8266HTTPUpdateServer.h".}
proc setup*(this: var ESP8266HTTPUpdateServer; server: ptr ESP8266WebServer;
           username: cstring; password: cstring) {.cdecl, importcpp: "setup",
    header: "ESP8266HTTPUpdateServer.h".}
proc setup*(this: var ESP8266HTTPUpdateServer; server: ptr ESP8266WebServer;
           path: cstring; username: cstring; password: cstring) {.cdecl,
    importcpp: "setup", header: "ESP8266HTTPUpdateServer.h".}
proc updateCredentials*(this: var ESP8266HTTPUpdateServer; username: cstring;
                       password: cstring) {.cdecl, importcpp: "updateCredentials",
    header: "ESP8266HTTPUpdateServer.h".}