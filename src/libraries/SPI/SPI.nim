##  
##   SPI.h - SPI library for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

const
  SPI_HAS_TRANSACTION* = true

##  This defines are not representing the real Divider of the ESP8266
##  the Defines match to an AVR Arduino on 16MHz for better compatibility

const
  SPI_CLOCK_DIV2* = 0x00101001
  SPI_CLOCK_DIV4* = 0x00241001
  SPI_CLOCK_DIV8* = 0x004C1001
  SPI_CLOCK_DIV16* = 0x009C1001
  SPI_CLOCK_DIV32* = 0x013C1001
  SPI_CLOCK_DIV64* = 0x027C1001
  SPI_CLOCK_DIV128* = 0x04FC1001

var SPI_MODE0* {.importcpp: "SPI_MODE0", header: "SPI.h".}: uint8_t

## /<  CPOL: 0  CPHA: 0

var SPI_MODE1* {.importcpp: "SPI_MODE1", header: "SPI.h".}: uint8_t

## /<  CPOL: 0  CPHA: 1

var SPI_MODE2* {.importcpp: "SPI_MODE2", header: "SPI.h".}: uint8_t

## /<  CPOL: 1  CPHA: 0

var SPI_MODE3* {.importcpp: "SPI_MODE3", header: "SPI.h".}: uint8_t

## /<  CPOL: 1  CPHA: 1

type
  SPISettings* {.importcpp: "SPISettings", header: "SPI.h", bycopy.} = object
    _clock* {.importc: "_clock".}: uint32_t
    _bitOrder* {.importc: "_bitOrder".}: uint8_t
    _dataMode* {.importc: "_dataMode".}: uint8_t


proc constructSPISettings*(): SPISettings {.cdecl, constructor,
    importcpp: "SPISettings(@)", header: "SPI.h".}
proc constructSPISettings*(clock: uint32_t; bitOrder: uint8_t; dataMode: uint8_t): SPISettings {.
    cdecl, constructor, importcpp: "SPISettings(@)", header: "SPI.h".}
type
  SPIClass* {.importcpp: "SPIClass", header: "SPI.h", bycopy.} = object
  

proc constructSPIClass*(): SPIClass {.cdecl, constructor, importcpp: "SPIClass(@)",
                                   header: "SPI.h".}
proc pins*(this: var SPIClass; sck: int8_t; miso: int8_t; mosi: int8_t; ss: int8_t): bool {.
    cdecl, importcpp: "pins", header: "SPI.h".}
proc begin*(this: var SPIClass) {.cdecl, importcpp: "begin", header: "SPI.h".}
proc `end`*(this: var SPIClass) {.cdecl, importcpp: "end", header: "SPI.h".}
proc setHwCs*(this: var SPIClass; use: bool) {.cdecl, importcpp: "setHwCs",
    header: "SPI.h".}
proc setBitOrder*(this: var SPIClass; bitOrder: uint8_t) {.cdecl,
    importcpp: "setBitOrder", header: "SPI.h".}
proc setDataMode*(this: var SPIClass; dataMode: uint8_t) {.cdecl,
    importcpp: "setDataMode", header: "SPI.h".}
proc setFrequency*(this: var SPIClass; freq: uint32_t) {.cdecl,
    importcpp: "setFrequency", header: "SPI.h".}
proc setClockDivider*(this: var SPIClass; clockDiv: uint32_t) {.cdecl,
    importcpp: "setClockDivider", header: "SPI.h".}
proc beginTransaction*(this: var SPIClass; settings: SPISettings) {.cdecl,
    importcpp: "beginTransaction", header: "SPI.h".}
proc transfer*(this: var SPIClass; data: uint8_t): uint8_t {.cdecl,
    importcpp: "transfer", header: "SPI.h".}
proc transfer16*(this: var SPIClass; data: uint16_t): uint16_t {.cdecl,
    importcpp: "transfer16", header: "SPI.h".}
proc write*(this: var SPIClass; data: uint8_t) {.cdecl, importcpp: "write",
    header: "SPI.h".}
proc write16*(this: var SPIClass; data: uint16_t) {.cdecl, importcpp: "write16",
    header: "SPI.h".}
proc write16*(this: var SPIClass; data: uint16_t; msb: bool) {.cdecl,
    importcpp: "write16", header: "SPI.h".}
proc write32*(this: var SPIClass; data: uint32_t) {.cdecl, importcpp: "write32",
    header: "SPI.h".}
proc write32*(this: var SPIClass; data: uint32_t; msb: bool) {.cdecl,
    importcpp: "write32", header: "SPI.h".}
proc writeBytes*(this: var SPIClass; data: ptr uint8_t; size: uint32_t) {.cdecl,
    importcpp: "writeBytes", header: "SPI.h".}
proc writePattern*(this: var SPIClass; data: ptr uint8_t; size: uint8_t; repeat: uint32_t) {.
    cdecl, importcpp: "writePattern", header: "SPI.h".}
proc transferBytes*(this: var SPIClass; `out`: ptr uint8_t; `in`: ptr uint8_t;
                   size: uint32_t) {.cdecl, importcpp: "transferBytes",
                                   header: "SPI.h".}
proc endTransaction*(this: var SPIClass) {.cdecl, importcpp: "endTransaction",
                                       header: "SPI.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_SPI):
  var SPI* {.importcpp: "SPI", header: "SPI.h".}: SPIClass