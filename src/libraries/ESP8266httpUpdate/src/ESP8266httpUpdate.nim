## *
## 
##  @file ESP8266HTTPUpdate.h
##  @date 21.06.2015
##  @author Markus Sattler
## 
##  Copyright (c) 2015 Markus Sattler. All rights reserved.
##  This file is part of the ESP8266 Http Updater.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
## 

when defined(DEBUG_ESP_HTTP_UPDATE):
  when defined(DEBUG_ESP_PORT):
    template DEBUG_HTTP_UPDATE*(): untyped =
      DEBUG_ESP_PORT.printf(__VA_ARGS__)

when not defined(DEBUG_HTTP_UPDATE):
  template DEBUG_HTTP_UPDATE*(): void =
    nil

## / note we use HTTP client errors too so we start at 100

const
  HTTP_UE_TOO_LESS_SPACE* = (-100)
  HTTP_UE_SERVER_NOT_REPORT_SIZE* = (-101)
  HTTP_UE_SERVER_FILE_NOT_FOUND* = (-102)
  HTTP_UE_SERVER_FORBIDDEN* = (-103)
  HTTP_UE_SERVER_WRONG_HTTP_CODE* = (-104)
  HTTP_UE_SERVER_FAULTY_MD5* = (-105)
  HTTP_UE_BIN_VERIFY_HEADER_FAILED* = (-106)
  HTTP_UE_BIN_FOR_WRONG_FLASH* = (-107)

type
  HTTPUpdateResult* {.size: sizeof(cint), importcpp: "HTTPUpdateResult",
                     header: "ESP8266httpUpdate.h".} = enum
    HTTP_UPDATE_FAILED, HTTP_UPDATE_NO_UPDATES, HTTP_UPDATE_OK


type
  t_httpUpdate_return* = HTTPUpdateResult

##  backward compatibility

type
  ESP8266HTTPUpdate* {.importcpp: "ESP8266HTTPUpdate",
                      header: "ESP8266httpUpdate.h", bycopy.} = object
  

proc constructESP8266HTTPUpdate*(): ESP8266HTTPUpdate {.cdecl, constructor,
    importcpp: "ESP8266HTTPUpdate(@)", header: "ESP8266httpUpdate.h".}
proc constructESP8266HTTPUpdate*(httpClientTimeout: cint): ESP8266HTTPUpdate {.
    cdecl, constructor, importcpp: "ESP8266HTTPUpdate(@)",
    header: "ESP8266httpUpdate.h".}
proc destroyESP8266HTTPUpdate*(this: var ESP8266HTTPUpdate) {.cdecl,
    importcpp: "#.~ESP8266HTTPUpdate()", header: "ESP8266httpUpdate.h".}
proc rebootOnUpdate*(this: var ESP8266HTTPUpdate; reboot: bool) {.cdecl,
    importcpp: "rebootOnUpdate", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String;
            httpsFingerprint: String; reboot: bool): t_httpUpdate_return {.cdecl,
    importcpp: "update", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String = ""): t_httpUpdate_return {.
    cdecl, importcpp: "update", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String;
            httpsFingerprint: String): t_httpUpdate_return {.cdecl,
    importcpp: "update", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String;
            httpsFingerprint: array[20, uint8_t]): t_httpUpdate_return {.cdecl,
    importcpp: "update", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; host: String; port: uint16_t; uri: String;
            currentVersion: String; https: bool; httpsFingerprint: String;
            reboot: bool): t_httpUpdate_return {.cdecl, importcpp: "update",
    header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; host: String; port: uint16_t;
            uri: String = "/"; currentVersion: String = ""): t_httpUpdate_return {.cdecl,
    importcpp: "update", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; host: String; port: uint16_t; url: String;
            currentVersion: String; httpsFingerprint: String): t_httpUpdate_return {.
    cdecl, importcpp: "update", header: "ESP8266httpUpdate.h".}
proc update*(this: var ESP8266HTTPUpdate; host: String; port: uint16_t; url: String;
            currentVersion: String; httpsFingerprint: array[20, uint8_t]): t_httpUpdate_return {.
    cdecl, importcpp: "update", header: "ESP8266httpUpdate.h".}
proc updateSpiffs*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String;
                  httpsFingerprint: String; reboot: bool): t_httpUpdate_return {.
    cdecl, importcpp: "updateSpiffs", header: "ESP8266httpUpdate.h".}
proc updateSpiffs*(this: var ESP8266HTTPUpdate; url: String;
                  currentVersion: String = ""): t_httpUpdate_return {.cdecl,
    importcpp: "updateSpiffs", header: "ESP8266httpUpdate.h".}
proc updateSpiffs*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String;
                  httpsFingerprint: String): t_httpUpdate_return {.cdecl,
    importcpp: "updateSpiffs", header: "ESP8266httpUpdate.h".}
proc updateSpiffs*(this: var ESP8266HTTPUpdate; url: String; currentVersion: String;
                  httpsFingerprint: array[20, uint8_t]): t_httpUpdate_return {.
    cdecl, importcpp: "updateSpiffs", header: "ESP8266httpUpdate.h".}
proc getLastError*(this: var ESP8266HTTPUpdate): cint {.cdecl,
    importcpp: "getLastError", header: "ESP8266httpUpdate.h".}
proc getLastErrorString*(this: var ESP8266HTTPUpdate): String {.cdecl,
    importcpp: "getLastErrorString", header: "ESP8266httpUpdate.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_HTTPUPDATE):
  var ESPhttpUpdate* {.importcpp: "ESPhttpUpdate", header: "ESP8266httpUpdate.h".}: ESP8266HTTPUpdate