discard "forward decl of UdpContext"
type
  ota_state_t* {.size: sizeof(cint), importcpp: "ota_state_t", header: "ArduinoOTA.h".} = enum
    OTA_IDLE, OTA_WAITAUTH, OTA_RUNUPDATE
  ota_error_t* {.size: sizeof(cint), importcpp: "ota_error_t", header: "ArduinoOTA.h".} = enum
    OTA_AUTH_ERROR, OTA_BEGIN_ERROR, OTA_CONNECT_ERROR, OTA_RECEIVE_ERROR,
    OTA_END_ERROR



type
  ArduinoOTAClass* {.importcpp: "ArduinoOTAClass", header: "ArduinoOTA.h", bycopy.} = object
  
  THandlerFunction* = function[nil]
  THandlerFunction_Error* = function[ota_error_t]
  THandlerFunction_Progress* = function[cuint, cuint]

proc constructArduinoOTAClass*(): ArduinoOTAClass {.cdecl, constructor,
    importcpp: "ArduinoOTAClass(@)", header: "ArduinoOTA.h".}
proc destroyArduinoOTAClass*(this: var ArduinoOTAClass) {.cdecl,
    importcpp: "#.~ArduinoOTAClass()", header: "ArduinoOTA.h".}
proc setPort*(this: var ArduinoOTAClass; port: uint16_t) {.cdecl, importcpp: "setPort",
    header: "ArduinoOTA.h".}
proc setHostname*(this: var ArduinoOTAClass; hostname: cstring) {.cdecl,
    importcpp: "setHostname", header: "ArduinoOTA.h".}
proc getHostname*(this: var ArduinoOTAClass): String {.cdecl,
    importcpp: "getHostname", header: "ArduinoOTA.h".}
proc setPassword*(this: var ArduinoOTAClass; password: cstring) {.cdecl,
    importcpp: "setPassword", header: "ArduinoOTA.h".}
proc setPasswordHash*(this: var ArduinoOTAClass; password: cstring) {.cdecl,
    importcpp: "setPasswordHash", header: "ArduinoOTA.h".}
proc setRebootOnSuccess*(this: var ArduinoOTAClass; reboot: bool) {.cdecl,
    importcpp: "setRebootOnSuccess", header: "ArduinoOTA.h".}
proc onStart*(this: var ArduinoOTAClass; fn: THandlerFunction) {.cdecl,
    importcpp: "onStart", header: "ArduinoOTA.h".}
proc onEnd*(this: var ArduinoOTAClass; fn: THandlerFunction) {.cdecl,
    importcpp: "onEnd", header: "ArduinoOTA.h".}
proc onError*(this: var ArduinoOTAClass; fn: THandlerFunction_Error) {.cdecl,
    importcpp: "onError", header: "ArduinoOTA.h".}
proc onProgress*(this: var ArduinoOTAClass; fn: THandlerFunction_Progress) {.cdecl,
    importcpp: "onProgress", header: "ArduinoOTA.h".}
proc begin*(this: var ArduinoOTAClass) {.cdecl, importcpp: "begin",
                                     header: "ArduinoOTA.h".}
proc handle*(this: var ArduinoOTAClass) {.cdecl, importcpp: "handle",
                                      header: "ArduinoOTA.h".}
proc getCommand*(this: var ArduinoOTAClass): cint {.cdecl, importcpp: "getCommand",
    header: "ArduinoOTA.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_ARDUINOOTA):
  var ArduinoOTA* {.importcpp: "ArduinoOTA", header: "ArduinoOTA.h".}: ArduinoOTAClass