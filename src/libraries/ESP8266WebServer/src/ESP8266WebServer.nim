## 
##   ESP8266WebServer.h - Dead simple web-server.
##   Supports only one simultaneous client, knows how to handle GET and POST.
## 
##   Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
##   Modified 8 May 2015 by Hristo Gochkov (proper post and file upload handling)
## 

type
  HTTPMethod* {.size: sizeof(cint), importcpp: "HTTPMethod",
               header: "ESP8266WebServer.h".} = enum
    HTTP_ANY, HTTP_GET, HTTP_POST, HTTP_PUT, HTTP_PATCH, HTTP_DELETE, HTTP_OPTIONS


type
  HTTPUploadStatus* {.size: sizeof(cint), importcpp: "HTTPUploadStatus",
                     header: "ESP8266WebServer.h".} = enum
    UPLOAD_FILE_START, UPLOAD_FILE_WRITE, UPLOAD_FILE_END, UPLOAD_FILE_ABORTED


type
  HTTPClientStatus* {.size: sizeof(cint), importcpp: "HTTPClientStatus",
                     header: "ESP8266WebServer.h".} = enum
    HC_NONE, HC_WAIT_READ, HC_WAIT_CLOSE


type
  HTTPAuthMethod* {.size: sizeof(cint), importcpp: "HTTPAuthMethod",
                   header: "ESP8266WebServer.h".} = enum
    BASIC_AUTH, DIGEST_AUTH


const
  HTTP_DOWNLOAD_UNIT_SIZE* = 1460

const
  HTTP_MAX_DATA_WAIT* = 5000
  HTTP_MAX_POST_WAIT* = 5000
  HTTP_MAX_SEND_WAIT* = 5000
  HTTP_MAX_CLOSE_WAIT* = 2000
  CONTENT_LENGTH_UNKNOWN* = ((size_t) - 1)
  CONTENT_LENGTH_NOT_SET* = ((size_t) - 2)

discard "forward decl of ESP8266WebServer"
type
  HTTPUpload* {.importcpp: "HTTPUpload", header: "ESP8266WebServer.h", bycopy.} = object
    status* {.importc: "status".}: HTTPUploadStatus
    filename* {.importc: "filename".}: String
    name* {.importc: "name".}: String
    `type`* {.importc: "type".}: String
    totalSize* {.importc: "totalSize".}: csize ##  file size
    currentSize* {.importc: "currentSize".}: csize ##  size of data currently in buf
    buf* {.importc: "buf".}: array[HTTP_UPLOAD_BUFLEN, uint8_t]


import
  detail/RequestHandler

discard "forward decl of FS"
type
  ESP8266WebServer* {.importcpp: "ESP8266WebServer", header: "ESP8266WebServer.h",
                     bycopy.} = object
    ##  Store noance and opaque for future comparison
    ##  Store the Auth realm between Calls
  

proc constructESP8266WebServer*(`addr`: IPAddress; port: cint = 80): ESP8266WebServer {.
    cdecl, constructor, importcpp: "ESP8266WebServer(@)",
    header: "ESP8266WebServer.h".}
proc constructESP8266WebServer*(port: cint = 80): ESP8266WebServer {.cdecl,
    constructor, importcpp: "ESP8266WebServer(@)", header: "ESP8266WebServer.h".}
proc destroyESP8266WebServer*(this: var ESP8266WebServer) {.cdecl,
    importcpp: "#.~ESP8266WebServer()", header: "ESP8266WebServer.h".}
proc begin*(this: var ESP8266WebServer) {.cdecl, importcpp: "begin",
                                      header: "ESP8266WebServer.h".}
proc begin*(this: var ESP8266WebServer; port: uint16_t) {.cdecl, importcpp: "begin",
    header: "ESP8266WebServer.h".}
proc handleClient*(this: var ESP8266WebServer) {.cdecl, importcpp: "handleClient",
    header: "ESP8266WebServer.h".}
proc close*(this: var ESP8266WebServer) {.cdecl, importcpp: "close",
                                      header: "ESP8266WebServer.h".}
proc stop*(this: var ESP8266WebServer) {.cdecl, importcpp: "stop",
                                     header: "ESP8266WebServer.h".}
proc authenticate*(this: var ESP8266WebServer; username: cstring; password: cstring): bool {.
    cdecl, importcpp: "authenticate", header: "ESP8266WebServer.h".}
proc requestAuthentication*(this: var ESP8266WebServer;
                           mode: HTTPAuthMethod = BASIC_AUTH; realm: cstring = nil;
                           authFailMsg: String = String("")) {.cdecl,
    importcpp: "requestAuthentication", header: "ESP8266WebServer.h".}
type
  THandlerFunction* = function[nil]

proc on*(this: var ESP8266WebServer; uri: String; handler: THandlerFunction) {.cdecl,
    importcpp: "on", header: "ESP8266WebServer.h".}
proc on*(this: var ESP8266WebServer; uri: String; `method`: HTTPMethod;
        fn: THandlerFunction) {.cdecl, importcpp: "on", header: "ESP8266WebServer.h".}
proc on*(this: var ESP8266WebServer; uri: String; `method`: HTTPMethod;
        fn: THandlerFunction; ufn: THandlerFunction) {.cdecl, importcpp: "on",
    header: "ESP8266WebServer.h".}
proc addHandler*(this: var ESP8266WebServer; handler: ptr RequestHandler) {.cdecl,
    importcpp: "addHandler", header: "ESP8266WebServer.h".}
proc serveStatic*(this: var ESP8266WebServer; uri: cstring; fs: var FS; path: cstring;
                 cache_header: cstring = nil) {.cdecl, importcpp: "serveStatic",
    header: "ESP8266WebServer.h".}
proc onNotFound*(this: var ESP8266WebServer; fn: THandlerFunction) {.cdecl,
    importcpp: "onNotFound", header: "ESP8266WebServer.h".}
proc onFileUpload*(this: var ESP8266WebServer; fn: THandlerFunction) {.cdecl,
    importcpp: "onFileUpload", header: "ESP8266WebServer.h".}
proc uri*(this: var ESP8266WebServer): String {.cdecl, importcpp: "uri",
    header: "ESP8266WebServer.h".}
proc `method`*(this: var ESP8266WebServer): HTTPMethod {.cdecl, importcpp: "method",
    header: "ESP8266WebServer.h".}
proc client*(this: var ESP8266WebServer): WiFiClient {.cdecl, importcpp: "client",
    header: "ESP8266WebServer.h".}
proc upload*(this: var ESP8266WebServer): var HTTPUpload {.cdecl, importcpp: "upload",
    header: "ESP8266WebServer.h".}
proc arg*(this: var ESP8266WebServer; name: String): String {.cdecl, importcpp: "arg",
    header: "ESP8266WebServer.h".}
proc arg*(this: var ESP8266WebServer; i: cint): String {.cdecl, importcpp: "arg",
    header: "ESP8266WebServer.h".}
proc argName*(this: var ESP8266WebServer; i: cint): String {.cdecl,
    importcpp: "argName", header: "ESP8266WebServer.h".}
proc args*(this: var ESP8266WebServer): cint {.cdecl, importcpp: "args",
    header: "ESP8266WebServer.h".}
proc hasArg*(this: var ESP8266WebServer; name: String): bool {.cdecl,
    importcpp: "hasArg", header: "ESP8266WebServer.h".}
proc collectHeaders*(this: var ESP8266WebServer; headerKeys: ptr cstring;
                    headerKeysCount: csize) {.cdecl, importcpp: "collectHeaders",
    header: "ESP8266WebServer.h".}
proc header*(this: var ESP8266WebServer; name: String): String {.cdecl,
    importcpp: "header", header: "ESP8266WebServer.h".}
proc header*(this: var ESP8266WebServer; i: cint): String {.cdecl, importcpp: "header",
    header: "ESP8266WebServer.h".}
proc headerName*(this: var ESP8266WebServer; i: cint): String {.cdecl,
    importcpp: "headerName", header: "ESP8266WebServer.h".}
proc headers*(this: var ESP8266WebServer): cint {.cdecl, importcpp: "headers",
    header: "ESP8266WebServer.h".}
proc hasHeader*(this: var ESP8266WebServer; name: String): bool {.cdecl,
    importcpp: "hasHeader", header: "ESP8266WebServer.h".}
proc hostHeader*(this: var ESP8266WebServer): String {.cdecl, importcpp: "hostHeader",
    header: "ESP8266WebServer.h".}
proc send*(this: var ESP8266WebServer; code: cint; content_type: cstring = nil;
          content: String = String("")) {.cdecl, importcpp: "send",
                                      header: "ESP8266WebServer.h".}
proc send*(this: var ESP8266WebServer; code: cint; content_type: cstring;
          content: String) {.cdecl, importcpp: "send", header: "ESP8266WebServer.h".}
proc send*(this: var ESP8266WebServer; code: cint; content_type: String; content: String) {.
    cdecl, importcpp: "send", header: "ESP8266WebServer.h".}
proc send_P*(this: var ESP8266WebServer; code: cint; content_type: PGM_P; content: PGM_P) {.
    cdecl, importcpp: "send_P", header: "ESP8266WebServer.h".}
proc send_P*(this: var ESP8266WebServer; code: cint; content_type: PGM_P;
            content: PGM_P; contentLength: csize) {.cdecl, importcpp: "send_P",
    header: "ESP8266WebServer.h".}
proc setContentLength*(this: var ESP8266WebServer; contentLength: csize) {.cdecl,
    importcpp: "setContentLength", header: "ESP8266WebServer.h".}
proc sendHeader*(this: var ESP8266WebServer; name: String; value: String;
                first: bool = false) {.cdecl, importcpp: "sendHeader",
                                   header: "ESP8266WebServer.h".}
proc sendContent*(this: var ESP8266WebServer; content: String) {.cdecl,
    importcpp: "sendContent", header: "ESP8266WebServer.h".}
proc sendContent_P*(this: var ESP8266WebServer; content: PGM_P) {.cdecl,
    importcpp: "sendContent_P", header: "ESP8266WebServer.h".}
proc sendContent_P*(this: var ESP8266WebServer; content: PGM_P; size: csize) {.cdecl,
    importcpp: "sendContent_P", header: "ESP8266WebServer.h".}
proc urlDecode*(text: String): String {.cdecl, importcpp: "ESP8266WebServer::urlDecode(@)",
                                    header: "ESP8266WebServer.h".}
proc streamFile*[T](this: var ESP8266WebServer; file: var T; contentType: String): csize {.
    cdecl, importcpp: "streamFile", header: "ESP8266WebServer.h".}