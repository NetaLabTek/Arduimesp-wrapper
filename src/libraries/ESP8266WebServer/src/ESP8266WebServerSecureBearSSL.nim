## 
##   ESP8266WebServerSecure.h - Dead simple HTTPS web-server.
##   Supports only one simultaneous client, knows how to handle GET and POST.
## 
##   Copyright (c) 2017 Earle F. Philhower, III. All rights reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  ESP8266WebServerSecure* {.importcpp: "BearSSL::ESP8266WebServerSecure",
                           header: "ESP8266WebServerSecureBearSSL.h", bycopy.} = object of ESP8266WebServer
  

proc constructESP8266WebServerSecure*(`addr`: IPAddress; port: cint = 443): ESP8266WebServerSecure {.
    cdecl, constructor, importcpp: "BearSSL::ESP8266WebServerSecure(@)",
    header: "ESP8266WebServerSecureBearSSL.h".}
proc constructESP8266WebServerSecure*(port: cint = 443): ESP8266WebServerSecure {.
    cdecl, constructor, importcpp: "BearSSL::ESP8266WebServerSecure(@)",
    header: "ESP8266WebServerSecureBearSSL.h".}
proc destroyESP8266WebServerSecure*(this: var ESP8266WebServerSecure) {.cdecl,
    importcpp: "#.~ESP8266WebServerSecure()",
    header: "ESP8266WebServerSecureBearSSL.h".}
proc setBufferSizes*(this: var ESP8266WebServerSecure; recv: cint; xmit: cint) {.cdecl,
    importcpp: "setBufferSizes", header: "ESP8266WebServerSecureBearSSL.h".}
proc setRSACert*(this: var ESP8266WebServerSecure; chain: ptr BearSSLX509List;
                sk: ptr BearSSLPrivateKey) {.cdecl, importcpp: "setRSACert",
    header: "ESP8266WebServerSecureBearSSL.h".}
proc setECCert*(this: var ESP8266WebServerSecure; chain: ptr BearSSLX509List;
               cert_issuer_key_type: cuint; sk: ptr BearSSLPrivateKey) {.cdecl,
    importcpp: "setECCert", header: "ESP8266WebServerSecureBearSSL.h".}
proc client*(this: var ESP8266WebServerSecure): WiFiClient {.cdecl,
    importcpp: "client", header: "ESP8266WebServerSecureBearSSL.h".}
proc begin*(this: var ESP8266WebServerSecure) {.cdecl, importcpp: "begin",
    header: "ESP8266WebServerSecureBearSSL.h".}
proc handleClient*(this: var ESP8266WebServerSecure) {.cdecl,
    importcpp: "handleClient", header: "ESP8266WebServerSecureBearSSL.h".}
proc close*(this: var ESP8266WebServerSecure) {.cdecl, importcpp: "close",
    header: "ESP8266WebServerSecureBearSSL.h".}
proc streamFile*[T](this: var ESP8266WebServerSecure; file: var T; contentType: String): csize {.
    cdecl, importcpp: "streamFile", header: "ESP8266WebServerSecureBearSSL.h".}
proc setServerKeyAndCert_P*(this: var ESP8266WebServerSecure; key: ptr uint8_t;
                           keyLen: cint; cert: ptr uint8_t; certLen: cint) {.cdecl,
    importcpp: "setServerKeyAndCert_P", header: "ESP8266WebServerSecureBearSSL.h".}
proc setServerKeyAndCert*(this: var ESP8266WebServerSecure; key: ptr uint8_t;
                         keyLen: cint; cert: ptr uint8_t; certLen: cint) {.cdecl,
    importcpp: "setServerKeyAndCert", header: "ESP8266WebServerSecureBearSSL.h".}
nil