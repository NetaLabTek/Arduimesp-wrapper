## 
##   ESP8266WiFiMesh.h - Mesh network node
##   Sets up a Mesh Node which acts as a router, creating a Mesh Network with other nodes. All information
##   is passed in both directions, but it is up to the user what the data sent is and how it is dealt with.
##  
##   Copyright (c) 2015 Julian Fell. All rights reserved.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  ESP8266WiFiMesh* {.importcpp: "ESP8266WiFiMesh", header: "ESP8266WiFiMesh.h",
                    bycopy.} = object ## *
                                   ##  WiFiMesh Constructor method. Creates a WiFi Mesh Node, ready to be initialised.
                                   ## 
                                   ##  @chip_id A unique identifier number for the node.
                                   ##  @handler The callback handler for dealing with received messages. Takes a string as an argument which
                                   ##           is the string received from another node and returns the string to send back.
                                   ##  
                                   ## 
  

proc constructESP8266WiFiMesh*(chip_id: uint32_t; handler: function[String]): ESP8266WiFiMesh {.
    cdecl, constructor, importcpp: "ESP8266WiFiMesh(@)", header: "ESP8266WiFiMesh.h".}
proc begin*(this: var ESP8266WiFiMesh) {.cdecl, importcpp: "begin",
                                     header: "ESP8266WiFiMesh.h".}
proc attemptScan*(this: var ESP8266WiFiMesh; message: String) {.cdecl,
    importcpp: "attemptScan", header: "ESP8266WiFiMesh.h".}
proc acceptRequest*(this: var ESP8266WiFiMesh) {.cdecl, importcpp: "acceptRequest",
    header: "ESP8266WiFiMesh.h".}