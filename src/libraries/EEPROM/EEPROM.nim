##  
##   EEPROM.cpp - esp8266 EEPROM emulation
## 
##   Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  EEPROMClass* {.importcpp: "EEPROMClass", header: "EEPROM.h", bycopy.} = object
  

proc constructEEPROMClass*(sector: uint32_t): EEPROMClass {.cdecl, constructor,
    importcpp: "EEPROMClass(@)", header: "EEPROM.h".}
proc constructEEPROMClass*(): EEPROMClass {.cdecl, constructor,
    importcpp: "EEPROMClass(@)", header: "EEPROM.h".}
proc begin*(this: var EEPROMClass; size: csize) {.cdecl, importcpp: "begin",
    header: "EEPROM.h".}
proc read*(this: var EEPROMClass; address: cint): uint8_t {.cdecl, importcpp: "read",
    header: "EEPROM.h".}
proc write*(this: var EEPROMClass; address: cint; val: uint8_t) {.cdecl,
    importcpp: "write", header: "EEPROM.h".}
proc commit*(this: var EEPROMClass): bool {.cdecl, importcpp: "commit",
                                       header: "EEPROM.h".}
proc `end`*(this: var EEPROMClass) {.cdecl, importcpp: "end", header: "EEPROM.h".}
proc getDataPtr*(this: var EEPROMClass): ptr uint8_t {.cdecl, importcpp: "getDataPtr",
    header: "EEPROM.h".}
proc getConstDataPtr*(this: EEPROMClass): ptr uint8_t {.noSideEffect, cdecl,
    importcpp: "getConstDataPtr", header: "EEPROM.h".}
proc get*[T](this: var EEPROMClass; address: cint; t: var T): var T {.cdecl,
    importcpp: "get", header: "EEPROM.h".}
proc put*[T](this: var EEPROMClass; address: cint; t: T): T {.cdecl, importcpp: "put",
    header: "EEPROM.h".}
proc length*(this: var EEPROMClass): csize {.cdecl, importcpp: "length",
                                        header: "EEPROM.h".}
proc `[]`*(this: var EEPROMClass; address: cint): var uint8_t {.cdecl, importcpp: "#[@]",
    header: "EEPROM.h".}
proc `[]`*(this: EEPROMClass; address: cint): uint8_t {.noSideEffect, cdecl,
    importcpp: "#[@]", header: "EEPROM.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_EEPROM):
  var EEPROM* {.importcpp: "EEPROM", header: "EEPROM.h".}: EEPROMClass