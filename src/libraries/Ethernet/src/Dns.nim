##  Arduino DNS client for WizNet5100-based Ethernet shield
##  (c) Copyright 2009-2010 MCQN Ltd.
##  Released under Apache License, version 2.0

type
  DNSClient* {.importcpp: "DNSClient", header: "Dns.h", bycopy.} = object ##  ctor
  

proc begin*(this: var DNSClient; aDNSServer: IPAddress) {.cdecl, importcpp: "begin",
    header: "Dns.h".}
proc inet_aton*(this: var DNSClient; aIPAddrString: cstring; aResult: var IPAddress): cint {.
    cdecl, importcpp: "inet_aton", header: "Dns.h".}
proc getHostByName*(this: var DNSClient; aHostname: cstring; aResult: var IPAddress): cint {.
    cdecl, importcpp: "getHostByName", header: "Dns.h".}