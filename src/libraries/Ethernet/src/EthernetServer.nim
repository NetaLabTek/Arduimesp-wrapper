import
  Server

discard "forward decl of EthernetClient"
type
  EthernetServer* {.importcpp: "EthernetServer", header: "EthernetServer.h", bycopy.} = object of Server
  

proc constructEthernetServer*(a2: uint16_t): EthernetServer {.cdecl, constructor,
    importcpp: "EthernetServer(@)", header: "EthernetServer.h".}
proc available*(this: var EthernetServer): EthernetClient {.cdecl,
    importcpp: "available", header: "EthernetServer.h".}
proc begin*(this: var EthernetServer) {.cdecl, importcpp: "begin",
                                    header: "EthernetServer.h".}
proc write*(this: var EthernetServer; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "EthernetServer.h".}
proc write*(this: var EthernetServer; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "EthernetServer.h".}