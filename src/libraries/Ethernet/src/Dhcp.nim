##  DHCP Library v0.3 - April 25, 2009
##  Author: Jordan Terrell - blog.jordanterrell.com

import
  EthernetUdp

##  DHCP state machine.

const
  STATE_DHCP_START* = 0
  STATE_DHCP_DISCOVER* = 1
  STATE_DHCP_REQUEST* = 2
  STATE_DHCP_LEASED* = 3
  STATE_DHCP_REREQUEST* = 4
  STATE_DHCP_RELEASE* = 5
  DHCP_FLAGSBROADCAST* = 0x00008000

##  UDP port numbers for DHCP

const
  DHCP_SERVER_PORT* = 67
  DHCP_CLIENT_PORT* = 68

##  DHCP message OP code

const
  DHCP_BOOTREQUEST* = 1
  DHCP_BOOTREPLY* = 2

##  DHCP message type

const
  DHCP_DISCOVER* = 1
  DHCP_OFFER* = 2
  DHCP_REQUEST* = 3
  DHCP_DECLINE* = 4
  DHCP_ACK* = 5
  DHCP_NAK* = 6
  DHCP_RELEASE* = 7
  DHCP_INFORM* = 8
  DHCP_HTYPE10MB* = 1
  DHCP_HTYPE100MB* = 2
  DHCP_HLENETHERNET* = 6
  DHCP_HOPS* = 0
  DHCP_SECS* = 0
  MAGIC_COOKIE* = 0x63825363
  MAX_DHCP_OPT* = 16
  HOST_NAME* = "WIZnet"
  DEFAULT_LEASE* = (900)        ## default lease time in seconds
  DHCP_CHECK_NONE* = (0)
  DHCP_CHECK_RENEW_FAIL* = (1)
  DHCP_CHECK_RENEW_OK* = (2)
  DHCP_CHECK_REBIND_FAIL* = (3)
  DHCP_CHECK_REBIND_OK* = (4)

const
  padOption* = 0
  subnetMask* = 1
  timerOffset* = 2
  routersOnSubnet* = 3          ##  timeServer		=	4,
                    ## 	nameServer		=	5,
  dns* = 6                      ## logServer		=	7,
        ## 	cookieServer		=	8,
        ## 	lprServer		=	9,
        ## 	impressServer		=	10,
        ## 	resourceLocationServer	=	11,
  hostName* = 12                ## bootFileSize		=	13,
              ## 	meritDumpFile		=	14,
  domainName* = 15              ## swapServer		=	16,
                ## 	rootPath		=	17,
                ## 	extentionsPath		=	18,
                ## 	IPforwarding		=	19,
                ## 	nonLocalSourceRouting	=	20,
                ## 	policyFilter		=	21,
                ## 	maxDgramReasmSize	=	22,
                ## 	defaultIPTTL		=	23,
                ## 	pathMTUagingTimeout	=	24,
                ## 	pathMTUplateauTable	=	25,
                ## 	ifMTU			=	26,
                ## 	allSubnetsLocal		=	27,
                ## 	broadcastAddr		=	28,
                ## 	performMaskDiscovery	=	29,
                ## 	maskSupplier		=	30,
                ## 	performRouterDiscovery	=	31,
                ## 	routerSolicitationAddr	=	32,
                ## 	staticRoute		=	33,
                ## 	trailerEncapsulation	=	34,
                ## 	arpCacheTimeout		=	35,
                ## 	ethernetEncapsulation	=	36,
                ## 	tcpDefaultTTL		=	37,
                ## 	tcpKeepaliveInterval	=	38,
                ## 	tcpKeepaliveGarbage	=	39,
                ## 	nisDomainName		=	40,
                ## 	nisServers		=	41,
                ## 	ntpServers		=	42,
                ## 	vendorSpecificInfo	=	43,
                ## 	netBIOSnameServer	=	44,
                ## 	netBIOSdgramDistServer	=	45,
                ## 	netBIOSnodeType		=	46,
                ## 	netBIOSscope		=	47,
                ## 	xFontServer		=	48,
                ## 	xDisplayManager		=	49,
  dhcpRequestedIPaddr* = 50
  dhcpIPaddrLeaseTime* = 51     ## dhcpOptionOverload	=	52,
  dhcpMessageType* = 53
  dhcpServerIdentifier* = 54
  dhcpParamRequest* = 55        ## dhcpMsg			=	56,
                      ## 	dhcpMaxMsgSize		=	57,
  dhcpT1value* = 58
  dhcpT2value* = 59             ## dhcpClassIdentifier	=	60,
  dhcpClientIdentifier* = 61
  endOption* = 255

type
  RIP_MSG_FIXED* {.importcpp: "RIP_MSG_FIXED", header: "Dhcp.h", bycopy.} = object
    op* {.importc: "op".}: uint8_t
    htype* {.importc: "htype".}: uint8_t
    hlen* {.importc: "hlen".}: uint8_t
    hops* {.importc: "hops".}: uint8_t
    xid* {.importc: "xid".}: uint32_t
    secs* {.importc: "secs".}: uint16_t
    flags* {.importc: "flags".}: uint16_t
    ciaddr* {.importc: "ciaddr".}: array[4, uint8_t]
    yiaddr* {.importc: "yiaddr".}: array[4, uint8_t]
    siaddr* {.importc: "siaddr".}: array[4, uint8_t]
    giaddr* {.importc: "giaddr".}: array[4, uint8_t]
    chaddr* {.importc: "chaddr".}: array[6, uint8_t]

  DhcpClass* {.importcpp: "DhcpClass", header: "Dhcp.h", bycopy.} = object
  

proc getLocalIp*(this: var DhcpClass): IPAddress {.cdecl, importcpp: "getLocalIp",
    header: "Dhcp.h".}
proc getSubnetMask*(this: var DhcpClass): IPAddress {.cdecl,
    importcpp: "getSubnetMask", header: "Dhcp.h".}
proc getGatewayIp*(this: var DhcpClass): IPAddress {.cdecl, importcpp: "getGatewayIp",
    header: "Dhcp.h".}
proc getDhcpServerIp*(this: var DhcpClass): IPAddress {.cdecl,
    importcpp: "getDhcpServerIp", header: "Dhcp.h".}
proc getDnsServerIp*(this: var DhcpClass): IPAddress {.cdecl,
    importcpp: "getDnsServerIp", header: "Dhcp.h".}
proc beginWithDHCP*(this: var DhcpClass; a3: ptr uint8_t; timeout: culong = 60000;
                   responseTimeout: culong = 4000): cint {.cdecl,
    importcpp: "beginWithDHCP", header: "Dhcp.h".}
proc checkLease*(this: var DhcpClass): cint {.cdecl, importcpp: "checkLease",
    header: "Dhcp.h".}