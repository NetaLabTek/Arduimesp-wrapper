## 
##   Udp.cpp: Library to send/receive UDP packets with the Arduino ethernet shield.
##   This version only offers minimal wrapping of socket.c/socket.h
##   Drop Udp.h/.cpp into the Ethernet library directory at hardware/libraries/Ethernet/ 
## 
##  NOTE: UDP is fast, but has some important limitations (thanks to Warren Gray for mentioning these)
##  1) UDP does not guarantee the order in which assembled UDP packets are received. This
##  might not happen often in practice, but in larger network topologies, a UDP
##  packet can be received out of sequence. 
##  2) UDP does not guard against lost packets - so packets *can* disappear without the sender being
##  aware of it. Again, this may not be a concern in practice on small local networks.
##  For more information, see http://www.cafeaulait.org/course/week12/35.html
## 
##  MIT License:
##  Copyright (c) 2008 Bjoern Hartmann
##  Permission is hereby granted, free of charge, to any person obtaining a copy
##  of this software and associated documentation files (the "Software"), to deal
##  in the Software without restriction, including without limitation the rights
##  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##  copies of the Software, and to permit persons to whom the Software is
##  furnished to do so, subject to the following conditions:
##  
##  The above copyright notice and this permission notice shall be included in
##  all copies or substantial portions of the Software.
##  
##  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##  THE SOFTWARE.
## 
##  bjoern@cs.stanford.edu 12/30/2008
## 

const
  UDP_TX_PACKET_MAX_SIZE* = 24

type
  EthernetUDP* {.importcpp: "EthernetUDP", header: "EthernetUdp.h", bycopy.} = object of UDP
    ##  socket ID for Wiz5100
    ##  local port to listen on
    ##  remote IP address for the incoming packet whilst it's being processed
    ##  remote port for the incoming packet whilst it's being processed
    ##  offset into the packet being sent
    ##  remaining bytes of incoming packet yet to be processed
    ##  Return the next byte from the current packet without moving on to the next byte
    ##  Return the port of the host who sent the current incoming packet
  

proc constructEthernetUDP*(): EthernetUDP {.cdecl, constructor,
    importcpp: "EthernetUDP(@)", header: "EthernetUdp.h".}
proc begin*(this: var EthernetUDP; a3: uint16_t): uint8_t {.cdecl, importcpp: "begin",
    header: "EthernetUdp.h".}
proc stop*(this: var EthernetUDP) {.cdecl, importcpp: "stop", header: "EthernetUdp.h".}
proc beginPacket*(this: var EthernetUDP; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "beginPacket", header: "EthernetUdp.h".}
proc beginPacket*(this: var EthernetUDP; host: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "beginPacket", header: "EthernetUdp.h".}
proc endPacket*(this: var EthernetUDP): cint {.cdecl, importcpp: "endPacket",
    header: "EthernetUdp.h".}
proc write*(this: var EthernetUDP; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "EthernetUdp.h".}
proc write*(this: var EthernetUDP; buffer: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "EthernetUdp.h".}
proc parsePacket*(this: var EthernetUDP): cint {.cdecl, importcpp: "parsePacket",
    header: "EthernetUdp.h".}
proc available*(this: var EthernetUDP): cint {.cdecl, importcpp: "available",
    header: "EthernetUdp.h".}
proc read*(this: var EthernetUDP): cint {.cdecl, importcpp: "read",
                                     header: "EthernetUdp.h".}
proc read*(this: var EthernetUDP; buffer: ptr cuchar; len: csize): cint {.cdecl,
    importcpp: "read", header: "EthernetUdp.h".}
proc read*(this: var EthernetUDP; buffer: cstring; len: csize): cint {.cdecl,
    importcpp: "read", header: "EthernetUdp.h".}
proc peek*(this: var EthernetUDP): cint {.cdecl, importcpp: "peek",
                                     header: "EthernetUdp.h".}
proc flush*(this: var EthernetUDP) {.cdecl, importcpp: "flush", header: "EthernetUdp.h".}
proc remoteIP*(this: var EthernetUDP): IPAddress {.cdecl, importcpp: "remoteIP",
    header: "EthernetUdp.h".}
proc remotePort*(this: var EthernetUDP): uint16_t {.cdecl, importcpp: "remotePort",
    header: "EthernetUdp.h".}