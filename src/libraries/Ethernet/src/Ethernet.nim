## #include "w5100.h"

import
  IPAddress, EthernetClient, EthernetServer, Dhcp

const
  MAX_SOCK_NUM* = 4

type
  EthernetClass* {.importcpp: "EthernetClass", header: "Ethernet.h", bycopy.} = object
    ##  Initialise the Ethernet shield to use the provided MAC address and gain the rest of the
    ##  configuration through DHCP.
    ##  Returns 0 if the DHCP configuration failed, and 1 if it succeeded
  

proc begin*(this: var EthernetClass; mac_address: ptr uint8_t): cint {.cdecl,
    importcpp: "begin", header: "Ethernet.h".}
proc begin*(this: var EthernetClass; mac_address: ptr uint8_t; local_ip: IPAddress) {.
    cdecl, importcpp: "begin", header: "Ethernet.h".}
proc begin*(this: var EthernetClass; mac_address: ptr uint8_t; local_ip: IPAddress;
           dns_server: IPAddress) {.cdecl, importcpp: "begin", header: "Ethernet.h".}
proc begin*(this: var EthernetClass; mac_address: ptr uint8_t; local_ip: IPAddress;
           dns_server: IPAddress; gateway: IPAddress) {.cdecl, importcpp: "begin",
    header: "Ethernet.h".}
proc begin*(this: var EthernetClass; mac_address: ptr uint8_t; local_ip: IPAddress;
           dns_server: IPAddress; gateway: IPAddress; subnet: IPAddress) {.cdecl,
    importcpp: "begin", header: "Ethernet.h".}
proc maintain*(this: var EthernetClass): cint {.cdecl, importcpp: "maintain",
    header: "Ethernet.h".}
proc localIP*(this: var EthernetClass): IPAddress {.cdecl, importcpp: "localIP",
    header: "Ethernet.h".}
proc subnetMask*(this: var EthernetClass): IPAddress {.cdecl, importcpp: "subnetMask",
    header: "Ethernet.h".}
proc gatewayIP*(this: var EthernetClass): IPAddress {.cdecl, importcpp: "gatewayIP",
    header: "Ethernet.h".}
proc dnsServerIP*(this: var EthernetClass): IPAddress {.cdecl,
    importcpp: "dnsServerIP", header: "Ethernet.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_ETHERNET):
  var Ethernet* {.importcpp: "Ethernet", header: "Ethernet.h".}: EthernetClass