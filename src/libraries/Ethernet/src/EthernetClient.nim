import
  Arduino, Print, Client, IPAddress

type
  EthernetClient* {.importcpp: "EthernetClient", header: "EthernetClient.h", bycopy.} = object of Client
  

proc constructEthernetClient*(): EthernetClient {.cdecl, constructor,
    importcpp: "EthernetClient(@)", header: "EthernetClient.h".}
proc constructEthernetClient*(sock: uint8_t): EthernetClient {.cdecl, constructor,
    importcpp: "EthernetClient(@)", header: "EthernetClient.h".}
proc status*(this: var EthernetClient): uint8_t {.cdecl, importcpp: "status",
    header: "EthernetClient.h".}
proc connect*(this: var EthernetClient; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "EthernetClient.h".}
proc connect*(this: var EthernetClient; host: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "EthernetClient.h".}
proc write*(this: var EthernetClient; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "EthernetClient.h".}
proc write*(this: var EthernetClient; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "EthernetClient.h".}
proc available*(this: var EthernetClient): cint {.cdecl, importcpp: "available",
    header: "EthernetClient.h".}
proc read*(this: var EthernetClient): cint {.cdecl, importcpp: "read",
                                        header: "EthernetClient.h".}
proc read*(this: var EthernetClient; buf: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "read", header: "EthernetClient.h".}
proc peek*(this: var EthernetClient): cint {.cdecl, importcpp: "peek",
                                        header: "EthernetClient.h".}
proc flush*(this: var EthernetClient) {.cdecl, importcpp: "flush",
                                    header: "EthernetClient.h".}
proc stop*(this: var EthernetClient) {.cdecl, importcpp: "stop",
                                   header: "EthernetClient.h".}
proc connected*(this: var EthernetClient): uint8_t {.cdecl, importcpp: "connected",
    header: "EthernetClient.h".}
converter `bool`*(this: var EthernetClient): bool {.cdecl,
    importcpp: "EthernetClient::operator bool", header: "EthernetClient.h".}
proc `==`*(this: var EthernetClient; value: bool): bool {.cdecl, importcpp: "(# == #)",
    header: "EthernetClient.h".}
proc `==`*(this: var EthernetClient; a3: EthernetClient): bool {.cdecl,
    importcpp: "(# == #)", header: "EthernetClient.h".}