## 
##   TwoWire.h - TWI/I2C library for Arduino & Wiring
##   Copyright (c) 2006 Nicholas Zambetti.  All right reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##   Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts
##   Modified December 2014 by Ivan Grokhotkov (ivan@esp8266.com) - esp8266 support
##   Modified April 2015 by Hrsto Gochkov (ficeto@ficeto.com) - alternative esp8266 support
## 

import
  Stream

const
  BUFFER_LENGTH* = 32

type
  TwoWire* {.importcpp: "TwoWire", header: "Wire.h", bycopy.} = object of Stream
  

proc constructTwoWire*(): TwoWire {.cdecl, constructor, importcpp: "TwoWire(@)",
                                 header: "Wire.h".}
proc begin*(this: var TwoWire; sda: cint; scl: cint) {.cdecl, importcpp: "begin",
    header: "Wire.h".}
proc pins*(this: var TwoWire; sda: cint; scl: cint) {.cdecl, importcpp: "pins",
    header: "Wire.h".}
proc begin*(this: var TwoWire) {.cdecl, importcpp: "begin", header: "Wire.h".}
proc begin*(this: var TwoWire; a3: uint8_t) {.cdecl, importcpp: "begin", header: "Wire.h".}
proc begin*(this: var TwoWire; a3: cint) {.cdecl, importcpp: "begin", header: "Wire.h".}
proc setClock*(this: var TwoWire; a3: uint32_t) {.cdecl, importcpp: "setClock",
    header: "Wire.h".}
proc setClockStretchLimit*(this: var TwoWire; a3: uint32_t) {.cdecl,
    importcpp: "setClockStretchLimit", header: "Wire.h".}
proc beginTransmission*(this: var TwoWire; a3: uint8_t) {.cdecl,
    importcpp: "beginTransmission", header: "Wire.h".}
proc beginTransmission*(this: var TwoWire; a3: cint) {.cdecl,
    importcpp: "beginTransmission", header: "Wire.h".}
proc endTransmission*(this: var TwoWire): uint8_t {.cdecl,
    importcpp: "endTransmission", header: "Wire.h".}
proc endTransmission*(this: var TwoWire; a3: uint8_t): uint8_t {.cdecl,
    importcpp: "endTransmission", header: "Wire.h".}
proc requestFrom*(this: var TwoWire; address: uint8_t; size: csize; sendStop: bool): csize {.
    cdecl, importcpp: "requestFrom", header: "Wire.h".}
proc status*(this: var TwoWire): uint8_t {.cdecl, importcpp: "status", header: "Wire.h".}
proc requestFrom*(this: var TwoWire; a3: uint8_t; a4: uint8_t): uint8_t {.cdecl,
    importcpp: "requestFrom", header: "Wire.h".}
proc requestFrom*(this: var TwoWire; a3: uint8_t; a4: uint8_t; a5: uint8_t): uint8_t {.
    cdecl, importcpp: "requestFrom", header: "Wire.h".}
proc requestFrom*(this: var TwoWire; a3: cint; a4: cint): uint8_t {.cdecl,
    importcpp: "requestFrom", header: "Wire.h".}
proc requestFrom*(this: var TwoWire; a3: cint; a4: cint; a5: cint): uint8_t {.cdecl,
    importcpp: "requestFrom", header: "Wire.h".}
proc write*(this: var TwoWire; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "Wire.h".}
proc write*(this: var TwoWire; a3: ptr uint8_t; a4: csize): csize {.cdecl,
    importcpp: "write", header: "Wire.h".}
proc available*(this: var TwoWire): cint {.cdecl, importcpp: "available",
                                      header: "Wire.h".}
proc read*(this: var TwoWire): cint {.cdecl, importcpp: "read", header: "Wire.h".}
proc peek*(this: var TwoWire): cint {.cdecl, importcpp: "peek", header: "Wire.h".}
proc flush*(this: var TwoWire) {.cdecl, importcpp: "flush", header: "Wire.h".}
proc onReceive*(this: var TwoWire; a3: proc (a2: cint) {.cdecl.}) {.cdecl,
    importcpp: "onReceive", header: "Wire.h".}
proc onRequest*(this: var TwoWire; a3: proc () {.cdecl.}) {.cdecl,
    importcpp: "onRequest", header: "Wire.h".}
proc write*(this: var TwoWire; n: culong): csize {.cdecl, importcpp: "write",
    header: "Wire.h".}
proc write*(this: var TwoWire; n: clong): csize {.cdecl, importcpp: "write",
    header: "Wire.h".}
proc write*(this: var TwoWire; n: cuint): csize {.cdecl, importcpp: "write",
    header: "Wire.h".}
proc write*(this: var TwoWire; n: cint): csize {.cdecl, importcpp: "write",
    header: "Wire.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_TWOWIRE):
  var Wire* {.importcpp: "Wire", header: "Wire.h".}: TwoWire