## 
## AVR In-System Programming over WiFi for ESP8266
## Copyright (c) Kiril Zyapkov <kiril@robotev.com>
## 
## Original version:
##     ArduinoISP version 04m3
##     Copyright (c) 2008-2011 Randall Bohn
##     If you require a license, see
##         http://www.opensource.org/licenses/bsd-license.php
## 

##  uncomment if you use an n-mos to level-shift the reset line
##  #define AVRISP_ACTIVE_HIGH_RESET
##  SPI clock frequency in Hz

const
  AVRISP_SPI_FREQ* = 300000.0

##  programmer states

type
  AVRISPState_t* {.size: sizeof(cint), importcpp: "AVRISPState_t",
                  header: "ESP8266AVRISP.h".} = enum
    AVRISP_STATE_IDLE = 0,      ##  no active TCP session
    AVRISP_STATE_PENDING,     ##  TCP connected, pending SPI activation
    AVRISP_STATE_ACTIVE       ##  programmer is active and owns the SPI bus


##  stk500 parameters

type
  AVRISP_parameter_t* {.importcpp: "AVRISP_parameter_t", header: "ESP8266AVRISP.h",
                       bycopy.} = object
    devicecode* {.importc: "devicecode".}: uint8_t
    revision* {.importc: "revision".}: uint8_t
    progtype* {.importc: "progtype".}: uint8_t
    parmode* {.importc: "parmode".}: uint8_t
    polling* {.importc: "polling".}: uint8_t
    selftimed* {.importc: "selftimed".}: uint8_t
    lockbytes* {.importc: "lockbytes".}: uint8_t
    fusebytes* {.importc: "fusebytes".}: uint8_t
    flashpoll* {.importc: "flashpoll".}: cint
    eeprompoll* {.importc: "eeprompoll".}: cint
    pagesize* {.importc: "pagesize".}: cint
    eepromsize* {.importc: "eepromsize".}: cint
    flashsize* {.importc: "flashsize".}: cint

  ESP8266AVRISP* {.importcpp: "ESP8266AVRISP", header: "ESP8266AVRISP.h", bycopy.} = object
    ##  programmer settings, set by remote end
    ##  page buffer
    ##  address for reading and writing, set by 'U' command
  

proc constructESP8266AVRISP*(port: uint16_t; reset_pin: uint8_t;
                            spi_freq: uint32_t = AVRISP_SPI_FREQ;
                            reset_state: bool = false;
                            reset_activehigh: bool = false): ESP8266AVRISP {.cdecl,
    constructor, importcpp: "ESP8266AVRISP(@)", header: "ESP8266AVRISP.h".}
proc begin*(this: var ESP8266AVRISP) {.cdecl, importcpp: "begin",
                                   header: "ESP8266AVRISP.h".}
proc setSpiFrequency*(this: var ESP8266AVRISP; a3: uint32_t) {.cdecl,
    importcpp: "setSpiFrequency", header: "ESP8266AVRISP.h".}
proc setReset*(this: var ESP8266AVRISP; a3: bool) {.cdecl, importcpp: "setReset",
    header: "ESP8266AVRISP.h".}
proc update*(this: var ESP8266AVRISP): AVRISPState_t {.cdecl, importcpp: "update",
    header: "ESP8266AVRISP.h".}
proc serve*(this: var ESP8266AVRISP): AVRISPState_t {.cdecl, importcpp: "serve",
    header: "ESP8266AVRISP.h".}