## 
##   WiFiClient.h - Library for Arduino Wifi shield.
##   Copyright (c) 2011-2014 Arduino.  All right reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##   Modified by Ivan Grokhotkov, December 2014 - esp8266 support
## 

import
  Arduino, Print, Client, IPAddress, include/slist

const
  WIFICLIENT_MAX_PACKET_SIZE* = 1460
  TCP_DEFAULT_KEEPALIVE_IDLE_SEC* = 7200
  TCP_DEFAULT_KEEPALIVE_INTERVAL_SEC* = 75
  TCP_DEFAULT_KEEPALIVE_COUNT* = 9

discard "forward decl of ClientContext"
discard "forward decl of WiFiServer"
type
  WiFiClient* {.importcpp: "WiFiClient", header: "WiFiClient.h", bycopy.} = object of Client
  

proc constructWiFiClient*(): WiFiClient {.cdecl, constructor,
                                       importcpp: "WiFiClient(@)",
                                       header: "WiFiClient.h".}
proc destroyWiFiClient*(this: var WiFiClient) {.cdecl, importcpp: "#.~WiFiClient()",
    header: "WiFiClient.h".}
proc constructWiFiClient*(a2: WiFiClient): WiFiClient {.cdecl, constructor,
    importcpp: "WiFiClient(@)", header: "WiFiClient.h".}
proc status*(this: var WiFiClient): uint8_t {.cdecl, importcpp: "status",
    header: "WiFiClient.h".}
proc connect*(this: var WiFiClient; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClient.h".}
proc connect*(this: var WiFiClient; host: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClient.h".}
proc connect*(this: var WiFiClient; host: String; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClient.h".}
proc write*(this: var WiFiClient; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "WiFiClient.h".}
proc write*(this: var WiFiClient; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "WiFiClient.h".}
proc write_P*(this: var WiFiClient; buf: PGM_P; size: csize): csize {.cdecl,
    importcpp: "write_P", header: "WiFiClient.h".}
proc write*(this: var WiFiClient; stream: var Stream): csize {.cdecl, importcpp: "write",
    header: "WiFiClient.h".}
proc write*(this: var WiFiClient; stream: var Stream; unitSize: csize): csize {.cdecl,
    importcpp: "write", header: "WiFiClient.h".}
proc available*(this: var WiFiClient): cint {.cdecl, importcpp: "available",
    header: "WiFiClient.h".}
proc read*(this: var WiFiClient): cint {.cdecl, importcpp: "read",
                                    header: "WiFiClient.h".}
proc read*(this: var WiFiClient; buf: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "read", header: "WiFiClient.h".}
proc peek*(this: var WiFiClient): cint {.cdecl, importcpp: "peek",
                                    header: "WiFiClient.h".}
proc peekBytes*(this: var WiFiClient; buffer: ptr uint8_t; length: csize): csize {.cdecl,
    importcpp: "peekBytes", header: "WiFiClient.h".}
proc peekBytes*(this: var WiFiClient; buffer: cstring; length: csize): csize {.cdecl,
    importcpp: "peekBytes", header: "WiFiClient.h".}
proc flush*(this: var WiFiClient) {.cdecl, importcpp: "flush", header: "WiFiClient.h".}
proc stop*(this: var WiFiClient) {.cdecl, importcpp: "stop", header: "WiFiClient.h".}
proc connected*(this: var WiFiClient): uint8_t {.cdecl, importcpp: "connected",
    header: "WiFiClient.h".}
converter `bool`*(this: var WiFiClient): bool {.cdecl,
    importcpp: "WiFiClient::operator bool", header: "WiFiClient.h".}
proc remoteIP*(this: var WiFiClient): IPAddress {.cdecl, importcpp: "remoteIP",
    header: "WiFiClient.h".}
proc remotePort*(this: var WiFiClient): uint16_t {.cdecl, importcpp: "remotePort",
    header: "WiFiClient.h".}
proc localIP*(this: var WiFiClient): IPAddress {.cdecl, importcpp: "localIP",
    header: "WiFiClient.h".}
proc localPort*(this: var WiFiClient): uint16_t {.cdecl, importcpp: "localPort",
    header: "WiFiClient.h".}
proc getNoDelay*(this: var WiFiClient): bool {.cdecl, importcpp: "getNoDelay",
    header: "WiFiClient.h".}
proc setNoDelay*(this: var WiFiClient; nodelay: bool) {.cdecl, importcpp: "setNoDelay",
    header: "WiFiClient.h".}
proc setLocalPortStart*(port: uint16_t) {.cdecl, importcpp: "WiFiClient::setLocalPortStart(@)",
                                       header: "WiFiClient.h".}
proc availableForWrite*(this: var WiFiClient): csize {.cdecl,
    importcpp: "availableForWrite", header: "WiFiClient.h".}
proc stopAll*() {.cdecl, importcpp: "WiFiClient::stopAll(@)", header: "WiFiClient.h".}
proc stopAllExcept*(c: ptr WiFiClient) {.cdecl,
                                     importcpp: "WiFiClient::stopAllExcept(@)",
                                     header: "WiFiClient.h".}
proc keepAlive*(this: var WiFiClient;
               idle_sec: uint16_t = TCP_DEFAULT_KEEPALIVE_IDLE_SEC;
               intv_sec: uint16_t = TCP_DEFAULT_KEEPALIVE_INTERVAL_SEC;
               count: uint8_t = TCP_DEFAULT_KEEPALIVE_COUNT) {.cdecl,
    importcpp: "keepAlive", header: "WiFiClient.h".}
proc isKeepAliveEnabled*(this: WiFiClient): bool {.noSideEffect, cdecl,
    importcpp: "isKeepAliveEnabled", header: "WiFiClient.h".}
proc getKeepAliveIdle*(this: WiFiClient): uint16_t {.noSideEffect, cdecl,
    importcpp: "getKeepAliveIdle", header: "WiFiClient.h".}
proc getKeepAliveInterval*(this: WiFiClient): uint16_t {.noSideEffect, cdecl,
    importcpp: "getKeepAliveInterval", header: "WiFiClient.h".}
proc getKeepAliveCount*(this: WiFiClient): uint8_t {.noSideEffect, cdecl,
    importcpp: "getKeepAliveCount", header: "WiFiClient.h".}
proc disableKeepAlive*(this: var WiFiClient) {.cdecl, importcpp: "disableKeepAlive",
    header: "WiFiClient.h".}