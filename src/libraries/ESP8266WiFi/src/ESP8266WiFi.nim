## 
##  ESP8266WiFi.h - esp8266 Wifi support.
##  Based on WiFi.h from Arduino WiFi shield library.
##  Copyright (c) 2011-2014 Arduino.  All right reserved.
##  Modified by Ivan Grokhotkov, December 2014
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  include/wl_definitions, IPAddress, ESP8266WiFiType, ESP8266WiFiSTA, ESP8266WiFiAP,
  ESP8266WiFiScan, ESP8266WiFiGeneric, WiFiClient, WiFiServer, WiFiServerSecure,
  WiFiClientSecure, BearSSLHelpers, CertStoreBearSSL

when defined(DEBUG_ESP_WIFI):
  when defined(DEBUG_ESP_PORT):
    template DEBUG_WIFI*(): untyped =
      DEBUG_ESP_PORT.printf(__VA_ARGS__)

when not defined(DEBUG_WIFI):
  template DEBUG_WIFI*(): void =
    nil

type
  ESP8266WiFiClass* {.importcpp: "ESP8266WiFiClass", header: "ESP8266WiFi.h", bycopy.} = object of ESP8266WiFiGenericClass ##  workaround same function name with different signature
  

proc printDiag*(this: var ESP8266WiFiClass; dest: var Print) {.cdecl,
    importcpp: "printDiag", header: "ESP8266WiFi.h".}
var WiFi* {.importcpp: "WiFi", header: "ESP8266WiFi.h".}: ESP8266WiFiClass
