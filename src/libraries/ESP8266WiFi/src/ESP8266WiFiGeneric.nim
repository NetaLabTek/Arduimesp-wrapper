## 
##  ESP8266WiFiGeneric.h - esp8266 Wifi support.
##  Based on WiFi.h from Ardiono WiFi shield library.
##  Copyright (c) 2011-2014 Arduino.  All right reserved.
##  Modified by Ivan Grokhotkov, December 2014
##  Reworked by Markus Sattler, December 2015
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  ESP8266WiFiType

when defined(DEBUG_ESP_WIFI):
  when defined(DEBUG_ESP_PORT):
    template DEBUG_WIFI_GENERIC*(): untyped =
      DEBUG_ESP_PORT.printf(__VA_ARGS__)

when not defined(DEBUG_WIFI_GENERIC):
  template DEBUG_WIFI_GENERIC*(): void =
    nil

discard "forward decl of WiFiEventHandlerOpaque"
type
  WiFiEventHandler* = shared_ptr[WiFiEventHandlerOpaque]
  WiFiEventCb* = proc (a2: WiFiEvent_t) {.cdecl.}
  ESP8266WiFiGenericClass* {.importcpp: "ESP8266WiFiGenericClass",
                            header: "ESP8266WiFiGeneric.h", bycopy.} = object ##  ----------------------------------------------------------------------------------------------
                                                                         ##  -------------------------------------- Generic WiFi function ---------------------------------
                                                                         ##  ----------------------------------------------------------------------------------------------
  

proc constructESP8266WiFiGenericClass*(): ESP8266WiFiGenericClass {.cdecl,
    constructor, importcpp: "ESP8266WiFiGenericClass(@)",
    header: "ESP8266WiFiGeneric.h".}
proc onEvent*(this: var ESP8266WiFiGenericClass; cb: WiFiEventCb;
             event: WiFiEvent_t = WIFI_EVENT_ANY) {.cdecl, importcpp: "onEvent",
    header: "ESP8266WiFiGeneric.h".}
proc onStationModeConnected*(this: var ESP8266WiFiGenericClass;
                            a3: function[WiFiEventStationModeConnected]): WiFiEventHandler {.
    cdecl, importcpp: "onStationModeConnected", header: "ESP8266WiFiGeneric.h".}
proc onStationModeDisconnected*(this: var ESP8266WiFiGenericClass;
                               a3: function[WiFiEventStationModeDisconnected]): WiFiEventHandler {.
    cdecl, importcpp: "onStationModeDisconnected", header: "ESP8266WiFiGeneric.h".}
proc onStationModeAuthModeChanged*(this: var ESP8266WiFiGenericClass; a3: function[
    WiFiEventStationModeAuthModeChanged]): WiFiEventHandler {.cdecl,
    importcpp: "onStationModeAuthModeChanged", header: "ESP8266WiFiGeneric.h".}
proc onStationModeGotIP*(this: var ESP8266WiFiGenericClass;
                        a3: function[WiFiEventStationModeGotIP]): WiFiEventHandler {.
    cdecl, importcpp: "onStationModeGotIP", header: "ESP8266WiFiGeneric.h".}
proc onStationModeDHCPTimeout*(this: var ESP8266WiFiGenericClass; a3: function[nil]): WiFiEventHandler {.
    cdecl, importcpp: "onStationModeDHCPTimeout", header: "ESP8266WiFiGeneric.h".}
proc onSoftAPModeStationConnected*(this: var ESP8266WiFiGenericClass; a3: function[
    WiFiEventSoftAPModeStationConnected]): WiFiEventHandler {.cdecl,
    importcpp: "onSoftAPModeStationConnected", header: "ESP8266WiFiGeneric.h".}
proc onSoftAPModeStationDisconnected*(this: var ESP8266WiFiGenericClass; a3: function[
    WiFiEventSoftAPModeStationDisconnected]): WiFiEventHandler {.cdecl,
    importcpp: "onSoftAPModeStationDisconnected", header: "ESP8266WiFiGeneric.h".}
proc onSoftAPModeProbeRequestReceived*(this: var ESP8266WiFiGenericClass; a3: function[
    WiFiEventSoftAPModeProbeRequestReceived]): WiFiEventHandler {.cdecl,
    importcpp: "onSoftAPModeProbeRequestReceived", header: "ESP8266WiFiGeneric.h".}
proc channel*(this: var ESP8266WiFiGenericClass): int32_t {.cdecl,
    importcpp: "channel", header: "ESP8266WiFiGeneric.h".}
proc setSleepMode*(this: var ESP8266WiFiGenericClass; `type`: WiFiSleepType_t): bool {.
    cdecl, importcpp: "setSleepMode", header: "ESP8266WiFiGeneric.h".}
proc getSleepMode*(this: var ESP8266WiFiGenericClass): WiFiSleepType_t {.cdecl,
    importcpp: "getSleepMode", header: "ESP8266WiFiGeneric.h".}
proc setPhyMode*(this: var ESP8266WiFiGenericClass; mode: WiFiPhyMode_t): bool {.cdecl,
    importcpp: "setPhyMode", header: "ESP8266WiFiGeneric.h".}
proc getPhyMode*(this: var ESP8266WiFiGenericClass): WiFiPhyMode_t {.cdecl,
    importcpp: "getPhyMode", header: "ESP8266WiFiGeneric.h".}
proc setOutputPower*(this: var ESP8266WiFiGenericClass; dBm: cfloat) {.cdecl,
    importcpp: "setOutputPower", header: "ESP8266WiFiGeneric.h".}
proc persistent*(this: var ESP8266WiFiGenericClass; persistent: bool) {.cdecl,
    importcpp: "persistent", header: "ESP8266WiFiGeneric.h".}
proc mode*(this: var ESP8266WiFiGenericClass; a3: WiFiMode_t): bool {.cdecl,
    importcpp: "mode", header: "ESP8266WiFiGeneric.h".}
proc getMode*(this: var ESP8266WiFiGenericClass): WiFiMode_t {.cdecl,
    importcpp: "getMode", header: "ESP8266WiFiGeneric.h".}
proc enableSTA*(this: var ESP8266WiFiGenericClass; enable: bool): bool {.cdecl,
    importcpp: "enableSTA", header: "ESP8266WiFiGeneric.h".}
proc enableAP*(this: var ESP8266WiFiGenericClass; enable: bool): bool {.cdecl,
    importcpp: "enableAP", header: "ESP8266WiFiGeneric.h".}
proc forceSleepBegin*(this: var ESP8266WiFiGenericClass; sleepUs: uint32 = 0): bool {.
    cdecl, importcpp: "forceSleepBegin", header: "ESP8266WiFiGeneric.h".}
proc forceSleepWake*(this: var ESP8266WiFiGenericClass): bool {.cdecl,
    importcpp: "forceSleepWake", header: "ESP8266WiFiGeneric.h".}
proc hostByName*(this: var ESP8266WiFiGenericClass; aHostname: cstring;
                aResult: var IPAddress): cint {.cdecl, importcpp: "hostByName",
    header: "ESP8266WiFiGeneric.h".}
proc hostByName*(this: var ESP8266WiFiGenericClass; aHostname: cstring;
                aResult: var IPAddress; timeout_ms: uint32_t): cint {.cdecl,
    importcpp: "hostByName", header: "ESP8266WiFiGeneric.h".}
proc getPersistent*(this: var ESP8266WiFiGenericClass): bool {.cdecl,
    importcpp: "getPersistent", header: "ESP8266WiFiGeneric.h".}