## 
##  ESP8266WiFiScan.h - esp8266 Wifi support.
##  Based on WiFi.h from Ardiono WiFi shield library.
##  Copyright (c) 2011-2014 Arduino.  All right reserved.
##  Modified by Ivan Grokhotkov, December 2014
##  Reworked by Markus Sattler, December 2015
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  ESP8266WiFiType, ESP8266WiFiGeneric

type
  ESP8266WiFiScanClass* {.importcpp: "ESP8266WiFiScanClass",
                         header: "ESP8266WiFiScan.h", bycopy.} = object ##  ----------------------------------------------------------------------------------------------
                                                                   ##  ----------------------------------------- scan function --------------------------------------
                                                                   ##  ----------------------------------------------------------------------------------------------
  

proc scanNetworks*(this: var ESP8266WiFiScanClass; async: bool = false;
                  show_hidden: bool = false; channel: uint8 = 0; ssid: ptr uint8 = nil): int8_t {.
    cdecl, importcpp: "scanNetworks", header: "ESP8266WiFiScan.h".}
proc scanNetworksAsync*(this: var ESP8266WiFiScanClass; onComplete: function[cint];
                       show_hidden: bool = false) {.cdecl,
    importcpp: "scanNetworksAsync", header: "ESP8266WiFiScan.h".}
proc scanComplete*(this: var ESP8266WiFiScanClass): int8_t {.cdecl,
    importcpp: "scanComplete", header: "ESP8266WiFiScan.h".}
proc scanDelete*(this: var ESP8266WiFiScanClass) {.cdecl, importcpp: "scanDelete",
    header: "ESP8266WiFiScan.h".}
proc getNetworkInfo*(this: var ESP8266WiFiScanClass; networkItem: uint8_t;
                    ssid: var String; encryptionType: var uint8_t; RSSI: var int32_t;
                    BSSID: var ptr uint8_t; channel: var int32_t; isHidden: var bool): bool {.
    cdecl, importcpp: "getNetworkInfo", header: "ESP8266WiFiScan.h".}
proc SSID*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): String {.cdecl,
    importcpp: "SSID", header: "ESP8266WiFiScan.h".}
proc encryptionType*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): uint8_t {.
    cdecl, importcpp: "encryptionType", header: "ESP8266WiFiScan.h".}
proc RSSI*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): int32_t {.cdecl,
    importcpp: "RSSI", header: "ESP8266WiFiScan.h".}
proc BSSID*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): ptr uint8_t {.cdecl,
    importcpp: "BSSID", header: "ESP8266WiFiScan.h".}
proc BSSIDstr*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): String {.cdecl,
    importcpp: "BSSIDstr", header: "ESP8266WiFiScan.h".}
proc channel*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): int32_t {.cdecl,
    importcpp: "channel", header: "ESP8266WiFiScan.h".}
proc isHidden*(this: var ESP8266WiFiScanClass; networkItem: uint8_t): bool {.cdecl,
    importcpp: "isHidden", header: "ESP8266WiFiScan.h".}