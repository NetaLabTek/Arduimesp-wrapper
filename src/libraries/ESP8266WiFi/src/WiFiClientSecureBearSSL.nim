## 
##   WiFiClientBearSSL- SSL client/server for esp8266 using BearSSL libraries
##   - Mostly compatible with Arduino WiFi shield library and standard
##     WiFiClient/ServerSecure (except for certificate handling).
## 
##   Copyright (c) 2018 Earle F. Philhower, III
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  WiFiClient, BearSSLHelpers, CertStoreBearSSL

type
  WiFiClientSecure* {.importcpp: "BearSSL::WiFiClientSecure",
                     header: "WiFiClientSecureBearSSL.h", bycopy.} = object of WiFiClient ##  Single memory buffer used for BearSSL auxilliary stack, insead of growing main Arduino stack for all apps
    ##  &_sc->eng, to allow for client or server contexts
    ##  Methods for handling server.available() call which returns a client connection.
    ##  The local copy, only used to enable a reference count
  

proc constructWiFiClientSecure*(): WiFiClientSecure {.cdecl, constructor,
    importcpp: "BearSSL::WiFiClientSecure(@)", header: "WiFiClientSecureBearSSL.h".}
proc destroyWiFiClientSecure*(this: var WiFiClientSecure) {.cdecl,
    importcpp: "#.~WiFiClientSecure()", header: "WiFiClientSecureBearSSL.h".}
proc connect*(this: var WiFiClientSecure; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClientSecureBearSSL.h".}
proc connect*(this: var WiFiClientSecure; host: String; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClientSecureBearSSL.h".}
proc connect*(this: var WiFiClientSecure; name: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClientSecureBearSSL.h".}
proc connected*(this: var WiFiClientSecure): uint8_t {.cdecl, importcpp: "connected",
    header: "WiFiClientSecureBearSSL.h".}
proc write*(this: var WiFiClientSecure; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "WiFiClientSecureBearSSL.h".}
proc write_P*(this: var WiFiClientSecure; buf: PGM_P; size: csize): csize {.cdecl,
    importcpp: "write_P", header: "WiFiClientSecureBearSSL.h".}
proc write*(this: var WiFiClientSecure; buf: cstring): csize {.cdecl,
    importcpp: "write", header: "WiFiClientSecureBearSSL.h".}
proc write_P*(this: var WiFiClientSecure; buf: cstring): csize {.cdecl,
    importcpp: "write_P", header: "WiFiClientSecureBearSSL.h".}
proc write*(this: var WiFiClientSecure; stream: var Stream): csize {.cdecl,
    importcpp: "write", header: "WiFiClientSecureBearSSL.h".}
proc read*(this: var WiFiClientSecure; buf: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "read", header: "WiFiClientSecureBearSSL.h".}
proc available*(this: var WiFiClientSecure): cint {.cdecl, importcpp: "available",
    header: "WiFiClientSecureBearSSL.h".}
proc read*(this: var WiFiClientSecure): cint {.cdecl, importcpp: "read",
    header: "WiFiClientSecureBearSSL.h".}
proc peek*(this: var WiFiClientSecure): cint {.cdecl, importcpp: "peek",
    header: "WiFiClientSecureBearSSL.h".}
proc peekBytes*(this: var WiFiClientSecure; buffer: ptr uint8_t; length: csize): csize {.
    cdecl, importcpp: "peekBytes", header: "WiFiClientSecureBearSSL.h".}
proc stop*(this: var WiFiClientSecure) {.cdecl, importcpp: "stop",
                                     header: "WiFiClientSecureBearSSL.h".}
proc flush*(this: var WiFiClientSecure) {.cdecl, importcpp: "flush",
                                      header: "WiFiClientSecureBearSSL.h".}
proc setInsecure*(this: var WiFiClientSecure) {.cdecl, importcpp: "setInsecure",
    header: "WiFiClientSecureBearSSL.h".}
proc setKnownKey*(this: var WiFiClientSecure; pk: ptr BearSSLPublicKey;
                 usages: cuint = BR_KEYTYPE_KEYX or BR_KEYTYPE_SIGN) {.cdecl,
    importcpp: "setKnownKey", header: "WiFiClientSecureBearSSL.h".}
proc setFingerprint*(this: var WiFiClientSecure; fingerprint: array[20, uint8_t]) {.
    cdecl, importcpp: "setFingerprint", header: "WiFiClientSecureBearSSL.h".}
proc allowSelfSignedCerts*(this: var WiFiClientSecure) {.cdecl,
    importcpp: "allowSelfSignedCerts", header: "WiFiClientSecureBearSSL.h".}
proc setTrustAnchors*(this: var WiFiClientSecure; ta: ptr BearSSLX509List) {.cdecl,
    importcpp: "setTrustAnchors", header: "WiFiClientSecureBearSSL.h".}
proc setX509Time*(this: var WiFiClientSecure; now: time_t) {.cdecl,
    importcpp: "setX509Time", header: "WiFiClientSecureBearSSL.h".}
proc setClientRSACert*(this: var WiFiClientSecure; cert: ptr BearSSLX509List;
                      sk: ptr BearSSLPrivateKey) {.cdecl,
    importcpp: "setClientRSACert", header: "WiFiClientSecureBearSSL.h".}
proc setClientECCert*(this: var WiFiClientSecure; cert: ptr BearSSLX509List;
                     sk: ptr BearSSLPrivateKey; allowed_usages: cuint;
                     cert_issuer_key_type: cuint) {.cdecl,
    importcpp: "setClientECCert", header: "WiFiClientSecureBearSSL.h".}
proc setBufferSizes*(this: var WiFiClientSecure; recv: cint; xmit: cint) {.cdecl,
    importcpp: "setBufferSizes", header: "WiFiClientSecureBearSSL.h".}
proc getLastSSLError*(this: var WiFiClientSecure; dest: cstring = nil; len: csize = 0): cint {.
    cdecl, importcpp: "getLastSSLError", header: "WiFiClientSecureBearSSL.h".}
proc setCertStore*(this: var WiFiClientSecure; certStore: ptr CertStore) {.cdecl,
    importcpp: "setCertStore", header: "WiFiClientSecureBearSSL.h".}
proc probeMaxFragmentLength*(ip: IPAddress; port: uint16_t; len: uint16_t): bool {.
    cdecl, importcpp: "BearSSL::WiFiClientSecure::probeMaxFragmentLength(@)",
    header: "WiFiClientSecureBearSSL.h".}
proc probeMaxFragmentLength*(hostname: cstring; port: uint16_t; len: uint16_t): bool {.
    cdecl, importcpp: "BearSSL::WiFiClientSecure::probeMaxFragmentLength(@)",
    header: "WiFiClientSecureBearSSL.h".}
proc probeMaxFragmentLength*(host: String; port: uint16_t; len: uint16_t): bool {.cdecl,
    importcpp: "BearSSL::WiFiClientSecure::probeMaxFragmentLength(@)",
    header: "WiFiClientSecureBearSSL.h".}
proc verify*(this: var WiFiClientSecure; fingerprint: cstring; domain_name: cstring): bool {.
    cdecl, importcpp: "verify", header: "WiFiClientSecureBearSSL.h".}
proc verifyCertChain*(this: var WiFiClientSecure; domain_name: cstring): bool {.cdecl,
    importcpp: "verifyCertChain", header: "WiFiClientSecureBearSSL.h".}
proc setCACert*(this: var WiFiClientSecure; pk: ptr uint8_t; size: csize): bool {.cdecl,
    importcpp: "setCACert", header: "WiFiClientSecureBearSSL.h".}
proc setCertificate*(this: var WiFiClientSecure; pk: ptr uint8_t; size: csize): bool {.
    cdecl, importcpp: "setCertificate", header: "WiFiClientSecureBearSSL.h".}
proc setPrivateKey*(this: var WiFiClientSecure; pk: ptr uint8_t; size: csize): bool {.
    cdecl, importcpp: "setPrivateKey", header: "WiFiClientSecureBearSSL.h".}
proc setCACert_P*(this: var WiFiClientSecure; pk: PGM_VOID_P; size: csize): bool {.cdecl,
    importcpp: "setCACert_P", header: "WiFiClientSecureBearSSL.h".}
proc setCertificate_P*(this: var WiFiClientSecure; pk: PGM_VOID_P; size: csize): bool {.
    cdecl, importcpp: "setCertificate_P", header: "WiFiClientSecureBearSSL.h".}
proc setPrivateKey_P*(this: var WiFiClientSecure; pk: PGM_VOID_P; size: csize): bool {.
    cdecl, importcpp: "setPrivateKey_P", header: "WiFiClientSecureBearSSL.h".}
proc loadCACert*(this: var WiFiClientSecure; stream: var Stream; size: csize): bool {.
    cdecl, importcpp: "loadCACert", header: "WiFiClientSecureBearSSL.h".}
proc loadCertificate*(this: var WiFiClientSecure; stream: var Stream; size: csize): bool {.
    cdecl, importcpp: "loadCertificate", header: "WiFiClientSecureBearSSL.h".}
proc loadPrivateKey*(this: var WiFiClientSecure; stream: var Stream; size: csize): bool {.
    cdecl, importcpp: "loadPrivateKey", header: "WiFiClientSecureBearSSL.h".}
proc loadCertificate*[TFile](this: var WiFiClientSecure; file: var TFile): bool {.cdecl,
    importcpp: "loadCertificate", header: "WiFiClientSecureBearSSL.h".}
proc loadPrivateKey*[TFile](this: var WiFiClientSecure; file: var TFile): bool {.cdecl,
    importcpp: "loadPrivateKey", header: "WiFiClientSecureBearSSL.h".}
proc loadCACert*[TFile](this: var WiFiClientSecure; file: var TFile): bool {.cdecl,
    importcpp: "loadCACert", header: "WiFiClientSecureBearSSL.h".}
nil