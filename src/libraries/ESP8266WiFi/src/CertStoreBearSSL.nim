## 
##   CertStoreBearSSL.h - Library for Arduino ESP8266
##   Copyright (c) 2018 Earle F. Philhower, III
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

##  Base class for the certificate stores, which allow use
##  of a large set of certificates stored on SPIFFS of SD card to
##  be dynamically used when validating a X509 certificate

##  Subclass this and provide virtual functions appropriate for your storage.
##  Required because there are conflicting definitions for a "File" in the
##  Arduino setup, and there is no simple way to work around the minor
##  differences.
##  See the examples for implementations to use in your own code.
## 
##  NOTE: This virtual class may migrate to a templated model in a future
##  release.  Expect some changes to the interface, no matter what, as the
##  SD and SPIFFS filesystem get unified.

type
  CertStoreFile* {.importcpp: "BearSSL::CertStoreFile",
                  header: "CertStoreBearSSL.h", bycopy.} = object
  

proc constructCertStoreFile*(): CertStoreFile {.cdecl, constructor,
    importcpp: "BearSSL::CertStoreFile(@)", header: "CertStoreBearSSL.h".}
proc destroyCertStoreFile*(this: var CertStoreFile) {.cdecl,
    importcpp: "#.~CertStoreFile()", header: "CertStoreBearSSL.h".}
proc open*(this: var CertStoreFile; write: bool = false): bool {.cdecl, importcpp: "open",
    header: "CertStoreBearSSL.h".}
proc seek*(this: var CertStoreFile; absolute_pos: csize): bool {.cdecl,
    importcpp: "seek", header: "CertStoreBearSSL.h".}
proc read*(this: var CertStoreFile; dest: pointer; bytes: csize): ssize_t {.cdecl,
    importcpp: "read", header: "CertStoreBearSSL.h".}
proc write*(this: var CertStoreFile; dest: pointer; bytes: csize): ssize_t {.cdecl,
    importcpp: "write", header: "CertStoreBearSSL.h".}
proc close*(this: var CertStoreFile) {.cdecl, importcpp: "close",
                                   header: "CertStoreBearSSL.h".}
type
  CertStore* {.importcpp: "BearSSL::CertStore", header: "CertStoreBearSSL.h", bycopy.} = object
    ##  These need to be static as they are callbacks from BearSSL C code
  

proc constructCertStore*(): CertStore {.cdecl, constructor,
                                     importcpp: "BearSSL::CertStore(@)",
                                     header: "CertStoreBearSSL.h".}
proc destroyCertStore*(this: var CertStore) {.cdecl, importcpp: "#.~CertStore()",
    header: "CertStoreBearSSL.h".}
proc initCertStore*(this: var CertStore; index: ptr CertStoreFile;
                   data: ptr CertStoreFile): cint {.cdecl,
    importcpp: "initCertStore", header: "CertStoreBearSSL.h".}
proc installCertStore*(this: var CertStore; ctx: ptr br_x509_minimal_context) {.cdecl,
    importcpp: "installCertStore", header: "CertStoreBearSSL.h".}
nil