## 
##   WiFiUdp.h - Library for Arduino Wifi shield.
##   Copyright (c) 2011-2014 Arduino LLC.  All right reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##   Modified by Ivan Grokhotkov, January 2015 - esp8266 support
## 

const
  UDP_TX_PACKET_MAX_SIZE* = 8192

discard "forward decl of UdpContext"
type
  WiFiUDP* {.importcpp: "WiFiUDP", header: "WiFiUdp.h", bycopy.} = object of UDP
    ##  Return the next byte from the current packet without moving on to the next byte
  

proc constructWiFiUDP*(): WiFiUDP {.cdecl, constructor, importcpp: "WiFiUDP(@)",
                                 header: "WiFiUdp.h".}
proc constructWiFiUDP*(other: WiFiUDP): WiFiUDP {.cdecl, constructor,
    importcpp: "WiFiUDP(@)", header: "WiFiUdp.h".}
proc destroyWiFiUDP*(this: var WiFiUDP) {.cdecl, importcpp: "#.~WiFiUDP()",
                                      header: "WiFiUdp.h".}
converter `bool`*(this: WiFiUDP): bool {.noSideEffect, cdecl,
                                     importcpp: "WiFiUDP::operator bool",
                                     header: "WiFiUdp.h".}
proc begin*(this: var WiFiUDP; port: uint16_t): uint8_t {.cdecl, importcpp: "begin",
    header: "WiFiUdp.h".}
proc stop*(this: var WiFiUDP) {.cdecl, importcpp: "stop", header: "WiFiUdp.h".}
proc beginMulticast*(this: var WiFiUDP; interfaceAddr: IPAddress;
                    multicast: IPAddress; port: uint16_t): uint8_t {.cdecl,
    importcpp: "beginMulticast", header: "WiFiUdp.h".}
proc beginPacket*(this: var WiFiUDP; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "beginPacket", header: "WiFiUdp.h".}
proc beginPacket*(this: var WiFiUDP; host: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "beginPacket", header: "WiFiUdp.h".}
proc beginPacketMulticast*(this: var WiFiUDP; multicastAddress: IPAddress;
                          port: uint16_t; interfaceAddress: IPAddress; ttl: cint = 1): cint {.
    cdecl, importcpp: "beginPacketMulticast", header: "WiFiUdp.h".}
proc endPacket*(this: var WiFiUDP): cint {.cdecl, importcpp: "endPacket",
                                      header: "WiFiUdp.h".}
proc write*(this: var WiFiUDP; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "WiFiUdp.h".}
proc write*(this: var WiFiUDP; buffer: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "WiFiUdp.h".}
proc parsePacket*(this: var WiFiUDP): cint {.cdecl, importcpp: "parsePacket",
                                        header: "WiFiUdp.h".}
proc available*(this: var WiFiUDP): cint {.cdecl, importcpp: "available",
                                      header: "WiFiUdp.h".}
proc read*(this: var WiFiUDP): cint {.cdecl, importcpp: "read", header: "WiFiUdp.h".}
proc read*(this: var WiFiUDP; buffer: ptr cuchar; len: csize): cint {.cdecl,
    importcpp: "read", header: "WiFiUdp.h".}
proc read*(this: var WiFiUDP; buffer: cstring; len: csize): cint {.cdecl,
    importcpp: "read", header: "WiFiUdp.h".}
proc peek*(this: var WiFiUDP): cint {.cdecl, importcpp: "peek", header: "WiFiUdp.h".}
proc flush*(this: var WiFiUDP) {.cdecl, importcpp: "flush", header: "WiFiUdp.h".}
proc remoteIP*(this: var WiFiUDP): IPAddress {.cdecl, importcpp: "remoteIP",
    header: "WiFiUdp.h".}
proc remotePort*(this: var WiFiUDP): uint16_t {.cdecl, importcpp: "remotePort",
    header: "WiFiUdp.h".}
proc destinationIP*(this: var WiFiUDP): IPAddress {.cdecl, importcpp: "destinationIP",
    header: "WiFiUdp.h".}
proc localPort*(this: var WiFiUDP): uint16_t {.cdecl, importcpp: "localPort",
    header: "WiFiUdp.h".}
proc stopAll*() {.cdecl, importcpp: "WiFiUDP::stopAll(@)", header: "WiFiUdp.h".}
proc stopAllExcept*(exC: ptr WiFiUDP) {.cdecl,
                                    importcpp: "WiFiUDP::stopAllExcept(@)",
                                    header: "WiFiUdp.h".}