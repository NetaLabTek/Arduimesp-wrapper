## 
##   WiFiClientBearSSL- SSL client/server for esp8266 using BearSSL libraries
##   - Mostly compatible with Arduino WiFi shield library and standard
##     WiFiClient/ServerSecure (except for certificate handling).
## 
##   Copyright (c) 2018 Earle F. Philhower, III
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

##  Internal opaque structures, not needed by user applications

discard "forward decl of public_key"
discard "forward decl of private_key"
##  Holds either a single public RSA or EC key for use when BearSSL wants a pubkey.
##  Copies all associated data so no need to keep input PEM/DER keys.
##  All inputs can be either in RAM or PROGMEM.

type
  BearSSLPublicKey* {.importcpp: "BearSSLPublicKey", header: "BearSSLHelpers.h",
                     bycopy.} = object
  

proc constructBearSSLPublicKey*(): BearSSLPublicKey {.cdecl, constructor,
    importcpp: "BearSSLPublicKey(@)", header: "BearSSLHelpers.h".}
proc constructBearSSLPublicKey*(pemKey: cstring): BearSSLPublicKey {.cdecl,
    constructor, importcpp: "BearSSLPublicKey(@)", header: "BearSSLHelpers.h".}
proc constructBearSSLPublicKey*(derKey: ptr uint8_t; derLen: csize): BearSSLPublicKey {.
    cdecl, constructor, importcpp: "BearSSLPublicKey(@)", header: "BearSSLHelpers.h".}
proc destroyBearSSLPublicKey*(this: var BearSSLPublicKey) {.cdecl,
    importcpp: "#.~BearSSLPublicKey()", header: "BearSSLHelpers.h".}
proc parse*(this: var BearSSLPublicKey; pemKey: cstring): bool {.cdecl,
    importcpp: "parse", header: "BearSSLHelpers.h".}
proc parse*(this: var BearSSLPublicKey; derKey: ptr uint8_t; derLen: csize): bool {.cdecl,
    importcpp: "parse", header: "BearSSLHelpers.h".}
proc isRSA*(this: BearSSLPublicKey): bool {.noSideEffect, cdecl, importcpp: "isRSA",
                                        header: "BearSSLHelpers.h".}
proc isEC*(this: BearSSLPublicKey): bool {.noSideEffect, cdecl, importcpp: "isEC",
                                       header: "BearSSLHelpers.h".}
proc getRSA*(this: BearSSLPublicKey): ptr br_rsa_public_key {.noSideEffect, cdecl,
    importcpp: "getRSA", header: "BearSSLHelpers.h".}
proc getEC*(this: BearSSLPublicKey): ptr br_ec_public_key {.noSideEffect, cdecl,
    importcpp: "getEC", header: "BearSSLHelpers.h".}
##  Holds either a single private RSA or EC key for use when BearSSL wants a secretkey.
##  Copies all associated data so no need to keep input PEM/DER keys.
##  All inputs can be either in RAM or PROGMEM.

type
  BearSSLPrivateKey* {.importcpp: "BearSSLPrivateKey", header: "BearSSLHelpers.h",
                      bycopy.} = object
  

proc constructBearSSLPrivateKey*(): BearSSLPrivateKey {.cdecl, constructor,
    importcpp: "BearSSLPrivateKey(@)", header: "BearSSLHelpers.h".}
proc constructBearSSLPrivateKey*(pemKey: cstring): BearSSLPrivateKey {.cdecl,
    constructor, importcpp: "BearSSLPrivateKey(@)", header: "BearSSLHelpers.h".}
proc constructBearSSLPrivateKey*(derKey: ptr uint8_t; derLen: csize): BearSSLPrivateKey {.
    cdecl, constructor, importcpp: "BearSSLPrivateKey(@)",
    header: "BearSSLHelpers.h".}
proc destroyBearSSLPrivateKey*(this: var BearSSLPrivateKey) {.cdecl,
    importcpp: "#.~BearSSLPrivateKey()", header: "BearSSLHelpers.h".}
proc parse*(this: var BearSSLPrivateKey; pemKey: cstring): bool {.cdecl,
    importcpp: "parse", header: "BearSSLHelpers.h".}
proc parse*(this: var BearSSLPrivateKey; derKey: ptr uint8_t; derLen: csize): bool {.
    cdecl, importcpp: "parse", header: "BearSSLHelpers.h".}
proc isRSA*(this: BearSSLPrivateKey): bool {.noSideEffect, cdecl, importcpp: "isRSA",
    header: "BearSSLHelpers.h".}
proc isEC*(this: BearSSLPrivateKey): bool {.noSideEffect, cdecl, importcpp: "isEC",
                                        header: "BearSSLHelpers.h".}
proc getRSA*(this: BearSSLPrivateKey): ptr br_rsa_private_key {.noSideEffect, cdecl,
    importcpp: "getRSA", header: "BearSSLHelpers.h".}
proc getEC*(this: BearSSLPrivateKey): ptr br_ec_private_key {.noSideEffect, cdecl,
    importcpp: "getEC", header: "BearSSLHelpers.h".}
##  Holds one or more X.509 certificates and associated trust anchors for
##  use whenever BearSSL needs a cert or TA.  May want to have multiple
##  certs for things like a series of trusted CAs (but check the CertStore class
##  for a more memory efficient way).
##  Copies all associated data so no need to keep input PEM/DER certs.
##  All inputs can be either in RAM or PROGMEM.

type
  BearSSLX509List* {.importcpp: "BearSSLX509List", header: "BearSSLHelpers.h", bycopy.} = object
  

proc constructBearSSLX509List*(): BearSSLX509List {.cdecl, constructor,
    importcpp: "BearSSLX509List(@)", header: "BearSSLHelpers.h".}
proc constructBearSSLX509List*(pemCert: cstring): BearSSLX509List {.cdecl,
    constructor, importcpp: "BearSSLX509List(@)", header: "BearSSLHelpers.h".}
proc constructBearSSLX509List*(derCert: ptr uint8_t; derLen: csize): BearSSLX509List {.
    cdecl, constructor, importcpp: "BearSSLX509List(@)", header: "BearSSLHelpers.h".}
proc destroyBearSSLX509List*(this: var BearSSLX509List) {.cdecl,
    importcpp: "#.~BearSSLX509List()", header: "BearSSLHelpers.h".}
proc append*(this: var BearSSLX509List; pemCert: cstring): bool {.cdecl,
    importcpp: "append", header: "BearSSLHelpers.h".}
proc append*(this: var BearSSLX509List; derCert: ptr uint8_t; derLen: csize): bool {.
    cdecl, importcpp: "append", header: "BearSSLHelpers.h".}
proc getCount*(this: BearSSLX509List): csize {.noSideEffect, cdecl,
    importcpp: "getCount", header: "BearSSLHelpers.h".}
proc getX509Certs*(this: BearSSLX509List): ptr br_x509_certificate {.noSideEffect,
    cdecl, importcpp: "getX509Certs", header: "BearSSLHelpers.h".}
proc getTrustAnchors*(this: BearSSLX509List): ptr br_x509_trust_anchor {.
    noSideEffect, cdecl, importcpp: "getTrustAnchors", header: "BearSSLHelpers.h".}