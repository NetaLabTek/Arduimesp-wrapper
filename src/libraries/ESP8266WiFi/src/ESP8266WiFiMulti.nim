## *
## 
##  @file ESP8266WiFiMulti.h
##  @date 16.05.2015
##  @author Markus Sattler
## 
##  Copyright (c) 2015 Markus Sattler. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
## 

import
  ESP8266WiFi

when defined(DEBUG_ESP_WIFI):
  when defined(DEBUG_ESP_PORT):
    template DEBUG_WIFI_MULTI*(): untyped =
      DEBUG_ESP_PORT.printf(__VA_ARGS__)

when not defined(DEBUG_WIFI_MULTI):
  template DEBUG_WIFI_MULTI*(): void =
    nil

type
  WifiAPEntry* {.importcpp: "WifiAPEntry", header: "ESP8266WiFiMulti.h", bycopy.} = object
    ssid* {.importc: "ssid".}: cstring
    passphrase* {.importc: "passphrase".}: cstring

  WifiAPlist* = vector[WifiAPEntry]
  ESP8266WiFiMulti* {.importcpp: "ESP8266WiFiMulti", header: "ESP8266WiFiMulti.h",
                     bycopy.} = object
  

proc constructESP8266WiFiMulti*(): ESP8266WiFiMulti {.cdecl, constructor,
    importcpp: "ESP8266WiFiMulti(@)", header: "ESP8266WiFiMulti.h".}
proc destroyESP8266WiFiMulti*(this: var ESP8266WiFiMulti) {.cdecl,
    importcpp: "#.~ESP8266WiFiMulti()", header: "ESP8266WiFiMulti.h".}
proc addAP*(this: var ESP8266WiFiMulti; ssid: cstring; passphrase: cstring = nil): bool {.
    cdecl, importcpp: "addAP", header: "ESP8266WiFiMulti.h".}
proc run*(this: var ESP8266WiFiMulti): wl_status_t {.cdecl, importcpp: "run",
    header: "ESP8266WiFiMulti.h".}