## 
##  ESP8266WiFiSTA.h - esp8266 Wifi support.
##  Based on WiFi.h from Ardiono WiFi shield library.
##  Copyright (c) 2011-2014 Arduino.  All right reserved.
##  Modified by Ivan Grokhotkov, December 2014
##  Reworked by Markus Sattler, December 2015
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  ESP8266WiFiType, ESP8266WiFiGeneric

type
  ESP8266WiFiSTAClass* {.importcpp: "ESP8266WiFiSTAClass",
                        header: "ESP8266WiFiSTA.h", bycopy.} = object ##  ----------------------------------------------------------------------------------------------
                                                                 ##  ---------------------------------------- STA function ----------------------------------------
                                                                 ##  ----------------------------------------------------------------------------------------------
  
  ##  ----------------------------------------------------------------------------------------------
  ##  ------------------------------------ STA remote configure  -----------------------------------
  ##  ----------------------------------------------------------------------------------------------

proc begin*(this: var ESP8266WiFiSTAClass; ssid: cstring; passphrase: cstring = nil;
           channel: int32_t = 0; bssid: ptr uint8_t = nil; connect: bool = true): wl_status_t {.
    cdecl, importcpp: "begin", header: "ESP8266WiFiSTA.h".}
proc begin*(this: var ESP8266WiFiSTAClass; ssid: cstring; passphrase: cstring = nil;
           channel: int32_t = 0; bssid: ptr uint8_t = nil; connect: bool = true): wl_status_t {.
    cdecl, importcpp: "begin", header: "ESP8266WiFiSTA.h".}
proc begin*(this: var ESP8266WiFiSTAClass): wl_status_t {.cdecl, importcpp: "begin",
    header: "ESP8266WiFiSTA.h".}
proc config*(this: var ESP8266WiFiSTAClass; local_ip: IPAddress; gateway: IPAddress;
            subnet: IPAddress; dns1: IPAddress = cast[uint32_t](0x00000000);
            dns2: IPAddress = cast[uint32_t](0x00000000)): bool {.cdecl,
    importcpp: "config", header: "ESP8266WiFiSTA.h".}
proc reconnect*(this: var ESP8266WiFiSTAClass): bool {.cdecl, importcpp: "reconnect",
    header: "ESP8266WiFiSTA.h".}
proc disconnect*(this: var ESP8266WiFiSTAClass; wifioff: bool = false): bool {.cdecl,
    importcpp: "disconnect", header: "ESP8266WiFiSTA.h".}
proc isConnected*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "isConnected", header: "ESP8266WiFiSTA.h".}
proc setAutoConnect*(this: var ESP8266WiFiSTAClass; autoConnect: bool): bool {.cdecl,
    importcpp: "setAutoConnect", header: "ESP8266WiFiSTA.h".}
proc getAutoConnect*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "getAutoConnect", header: "ESP8266WiFiSTA.h".}
proc setAutoReconnect*(this: var ESP8266WiFiSTAClass; autoReconnect: bool): bool {.
    cdecl, importcpp: "setAutoReconnect", header: "ESP8266WiFiSTA.h".}
proc getAutoReconnect*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "getAutoReconnect", header: "ESP8266WiFiSTA.h".}
proc waitForConnectResult*(this: var ESP8266WiFiSTAClass): uint8_t {.cdecl,
    importcpp: "waitForConnectResult", header: "ESP8266WiFiSTA.h".}
proc localIP*(this: var ESP8266WiFiSTAClass): IPAddress {.cdecl, importcpp: "localIP",
    header: "ESP8266WiFiSTA.h".}
proc macAddress*(this: var ESP8266WiFiSTAClass; mac: ptr uint8_t): ptr uint8_t {.cdecl,
    importcpp: "macAddress", header: "ESP8266WiFiSTA.h".}
proc macAddress*(this: var ESP8266WiFiSTAClass): String {.cdecl,
    importcpp: "macAddress", header: "ESP8266WiFiSTA.h".}
proc subnetMask*(this: var ESP8266WiFiSTAClass): IPAddress {.cdecl,
    importcpp: "subnetMask", header: "ESP8266WiFiSTA.h".}
proc gatewayIP*(this: var ESP8266WiFiSTAClass): IPAddress {.cdecl,
    importcpp: "gatewayIP", header: "ESP8266WiFiSTA.h".}
proc dnsIP*(this: var ESP8266WiFiSTAClass; dns_no: uint8_t = 0): IPAddress {.cdecl,
    importcpp: "dnsIP", header: "ESP8266WiFiSTA.h".}
proc hostname*(this: var ESP8266WiFiSTAClass): String {.cdecl, importcpp: "hostname",
    header: "ESP8266WiFiSTA.h".}
proc hostname*(this: var ESP8266WiFiSTAClass; aHostname: cstring): bool {.cdecl,
    importcpp: "hostname", header: "ESP8266WiFiSTA.h".}
proc hostname*(this: var ESP8266WiFiSTAClass; aHostname: cstring): bool {.cdecl,
    importcpp: "hostname", header: "ESP8266WiFiSTA.h".}
proc hostname*(this: var ESP8266WiFiSTAClass; aHostname: String): bool {.cdecl,
    importcpp: "hostname", header: "ESP8266WiFiSTA.h".}
proc status*(this: var ESP8266WiFiSTAClass): wl_status_t {.cdecl, importcpp: "status",
    header: "ESP8266WiFiSTA.h".}
proc SSID*(this: ESP8266WiFiSTAClass): String {.noSideEffect, cdecl,
    importcpp: "SSID", header: "ESP8266WiFiSTA.h".}
proc psk*(this: ESP8266WiFiSTAClass): String {.noSideEffect, cdecl, importcpp: "psk",
    header: "ESP8266WiFiSTA.h".}
proc BSSID*(this: var ESP8266WiFiSTAClass): ptr uint8_t {.cdecl, importcpp: "BSSID",
    header: "ESP8266WiFiSTA.h".}
proc BSSIDstr*(this: var ESP8266WiFiSTAClass): String {.cdecl, importcpp: "BSSIDstr",
    header: "ESP8266WiFiSTA.h".}
proc RSSI*(this: var ESP8266WiFiSTAClass): int32_t {.cdecl, importcpp: "RSSI",
    header: "ESP8266WiFiSTA.h".}
proc beginWPSConfig*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "beginWPSConfig", header: "ESP8266WiFiSTA.h".}
proc beginSmartConfig*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "beginSmartConfig", header: "ESP8266WiFiSTA.h".}
proc stopSmartConfig*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "stopSmartConfig", header: "ESP8266WiFiSTA.h".}
proc smartConfigDone*(this: var ESP8266WiFiSTAClass): bool {.cdecl,
    importcpp: "smartConfigDone", header: "ESP8266WiFiSTA.h".}