## 
##   WiFiServerBearSSL.h - Library for Arduino ESP8266
##   Copyright (c) 2018 Earle F. Philhower, III
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  WiFiServer, WiFiClientSecureBearSSL, BearSSLHelpers

discard "forward decl of WiFiClientSecure"
type
  WiFiServerSecure* {.importcpp: "BearSSL::WiFiServerSecure",
                     header: "WiFiServerSecureBearSSL.h", bycopy.} = object of WiFiServer
  

proc constructWiFiServerSecure*(`addr`: IPAddress; port: uint16_t): WiFiServerSecure {.
    cdecl, constructor, importcpp: "BearSSL::WiFiServerSecure(@)",
    header: "WiFiServerSecureBearSSL.h".}
proc constructWiFiServerSecure*(port: uint16_t): WiFiServerSecure {.cdecl,
    constructor, importcpp: "BearSSL::WiFiServerSecure(@)",
    header: "WiFiServerSecureBearSSL.h".}
proc destroyWiFiServerSecure*(this: var WiFiServerSecure) {.cdecl,
    importcpp: "#.~WiFiServerSecure()", header: "WiFiServerSecureBearSSL.h".}
proc setBufferSizes*(this: var WiFiServerSecure; recv: cint; xmit: cint) {.cdecl,
    importcpp: "setBufferSizes", header: "WiFiServerSecureBearSSL.h".}
proc setRSACert*(this: var WiFiServerSecure; chain: ptr BearSSLX509List;
                sk: ptr BearSSLPrivateKey) {.cdecl, importcpp: "setRSACert",
    header: "WiFiServerSecureBearSSL.h".}
proc setECCert*(this: var WiFiServerSecure; chain: ptr BearSSLX509List;
               cert_issuer_key_type: cuint; sk: ptr BearSSLPrivateKey) {.cdecl,
    importcpp: "setECCert", header: "WiFiServerSecureBearSSL.h".}
proc setClientTrustAnchor*(this: var WiFiServerSecure;
                          client_CA_ta: ptr BearSSLX509List) {.cdecl,
    importcpp: "setClientTrustAnchor", header: "WiFiServerSecureBearSSL.h".}
proc available*(this: var WiFiServerSecure; status: ptr uint8_t = nil): WiFiClientSecure {.
    cdecl, importcpp: "available", header: "WiFiServerSecureBearSSL.h".}
proc setServerKeyAndCert*(this: var WiFiServerSecure; key: ptr uint8_t; keyLen: cint;
                         cert: ptr uint8_t; certLen: cint) {.cdecl,
    importcpp: "setServerKeyAndCert", header: "WiFiServerSecureBearSSL.h".}
proc setServerKeyAndCert_P*(this: var WiFiServerSecure; key: ptr uint8_t; keyLen: cint;
                           cert: ptr uint8_t; certLen: cint) {.cdecl,
    importcpp: "setServerKeyAndCert_P", header: "WiFiServerSecureBearSSL.h".}
nil