## 
##   WiFiClientSecure.h - Variant of WiFiClient with TLS support
##   Copyright (c) 2015 Ivan Grokhotkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
## 
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
## 

import
  WiFiClient, include/ssl

discard "forward decl of SSLContext"
type
  WiFiClientSecure* {.importcpp: "axTLS::WiFiClientSecure",
                     header: "WiFiClientSecureAxTLS.h", bycopy.} = object of WiFiClient ##  Only called by WiFiServerSecure
  

proc constructWiFiClientSecure*(): WiFiClientSecure {.cdecl, constructor,
    importcpp: "axTLS::WiFiClientSecure(@)", header: "WiFiClientSecureAxTLS.h".}
proc destroyWiFiClientSecure*(this: var WiFiClientSecure) {.cdecl,
    importcpp: "#.~WiFiClientSecure()", header: "WiFiClientSecureAxTLS.h".}
proc connect*(this: var WiFiClientSecure; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClientSecureAxTLS.h".}
proc connect*(this: var WiFiClientSecure; host: String; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClientSecureAxTLS.h".}
proc connect*(this: var WiFiClientSecure; name: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "WiFiClientSecureAxTLS.h".}
proc verify*(this: var WiFiClientSecure; fingerprint: cstring; domain_name: cstring): bool {.
    cdecl, importcpp: "verify", header: "WiFiClientSecureAxTLS.h".}
proc verifyCertChain*(this: var WiFiClientSecure; domain_name: cstring): bool {.cdecl,
    importcpp: "verifyCertChain", header: "WiFiClientSecureAxTLS.h".}
proc connected*(this: var WiFiClientSecure): uint8_t {.cdecl, importcpp: "connected",
    header: "WiFiClientSecureAxTLS.h".}
proc write*(this: var WiFiClientSecure; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "WiFiClientSecureAxTLS.h".}
proc write_P*(this: var WiFiClientSecure; buf: PGM_P; size: csize): csize {.cdecl,
    importcpp: "write_P", header: "WiFiClientSecureAxTLS.h".}
proc write*(this: var WiFiClientSecure; stream: var Stream): csize {.cdecl,
    importcpp: "write", header: "WiFiClientSecureAxTLS.h".}
proc read*(this: var WiFiClientSecure; buf: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "read", header: "WiFiClientSecureAxTLS.h".}
proc available*(this: var WiFiClientSecure): cint {.cdecl, importcpp: "available",
    header: "WiFiClientSecureAxTLS.h".}
proc read*(this: var WiFiClientSecure): cint {.cdecl, importcpp: "read",
    header: "WiFiClientSecureAxTLS.h".}
proc peek*(this: var WiFiClientSecure): cint {.cdecl, importcpp: "peek",
    header: "WiFiClientSecureAxTLS.h".}
proc peekBytes*(this: var WiFiClientSecure; buffer: ptr uint8_t; length: csize): csize {.
    cdecl, importcpp: "peekBytes", header: "WiFiClientSecureAxTLS.h".}
proc stop*(this: var WiFiClientSecure) {.cdecl, importcpp: "stop",
                                     header: "WiFiClientSecureAxTLS.h".}
proc setCACert*(this: var WiFiClientSecure; pk: ptr uint8_t; size: csize): bool {.cdecl,
    importcpp: "setCACert", header: "WiFiClientSecureAxTLS.h".}
proc setCertificate*(this: var WiFiClientSecure; pk: ptr uint8_t; size: csize): bool {.
    cdecl, importcpp: "setCertificate", header: "WiFiClientSecureAxTLS.h".}
proc setPrivateKey*(this: var WiFiClientSecure; pk: ptr uint8_t; size: csize): bool {.
    cdecl, importcpp: "setPrivateKey", header: "WiFiClientSecureAxTLS.h".}
proc setCACert_P*(this: var WiFiClientSecure; pk: PGM_VOID_P; size: csize): bool {.cdecl,
    importcpp: "setCACert_P", header: "WiFiClientSecureAxTLS.h".}
proc setCertificate_P*(this: var WiFiClientSecure; pk: PGM_VOID_P; size: csize): bool {.
    cdecl, importcpp: "setCertificate_P", header: "WiFiClientSecureAxTLS.h".}
proc setPrivateKey_P*(this: var WiFiClientSecure; pk: PGM_VOID_P; size: csize): bool {.
    cdecl, importcpp: "setPrivateKey_P", header: "WiFiClientSecureAxTLS.h".}
proc loadCACert*(this: var WiFiClientSecure; stream: var Stream; size: csize): bool {.
    cdecl, importcpp: "loadCACert", header: "WiFiClientSecureAxTLS.h".}
proc loadCertificate*(this: var WiFiClientSecure; stream: var Stream; size: csize): bool {.
    cdecl, importcpp: "loadCertificate", header: "WiFiClientSecureAxTLS.h".}
proc loadPrivateKey*(this: var WiFiClientSecure; stream: var Stream; size: csize): bool {.
    cdecl, importcpp: "loadPrivateKey", header: "WiFiClientSecureAxTLS.h".}
proc allowSelfSignedCerts*(this: var WiFiClientSecure) {.cdecl,
    importcpp: "allowSelfSignedCerts", header: "WiFiClientSecureAxTLS.h".}
proc loadCertificate*[TFile](this: var WiFiClientSecure; file: var TFile): bool {.cdecl,
    importcpp: "loadCertificate", header: "WiFiClientSecureAxTLS.h".}
proc loadPrivateKey*[TFile](this: var WiFiClientSecure; file: var TFile): bool {.cdecl,
    importcpp: "loadPrivateKey", header: "WiFiClientSecureAxTLS.h".}
proc loadCACert*[TFile](this: var WiFiClientSecure; file: var TFile): bool {.cdecl,
    importcpp: "loadCACert", header: "WiFiClientSecureAxTLS.h".}
nil