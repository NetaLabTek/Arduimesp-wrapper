## 
##   WiFiServerSecure.h - Library for Arduino ESP8266
##   Copyright (c) 2017 Earle F. Philhower, III
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  WiFiServer

discard "forward decl of WiFiClientSecure"
type
  WiFiServerSecure* {.importcpp: "axTLS::WiFiServerSecure",
                     header: "WiFiServerSecureAxTLS.h", bycopy.} = object of WiFiServer
  

proc constructWiFiServerSecure*(`addr`: IPAddress; port: uint16_t): WiFiServerSecure {.
    cdecl, constructor, importcpp: "axTLS::WiFiServerSecure(@)",
    header: "WiFiServerSecureAxTLS.h".}
proc constructWiFiServerSecure*(port: uint16_t): WiFiServerSecure {.cdecl,
    constructor, importcpp: "axTLS::WiFiServerSecure(@)",
    header: "WiFiServerSecureAxTLS.h".}
proc setServerKeyAndCert*(this: var WiFiServerSecure; key: ptr uint8_t; keyLen: cint;
                         cert: ptr uint8_t; certLen: cint) {.cdecl,
    importcpp: "setServerKeyAndCert", header: "WiFiServerSecureAxTLS.h".}
proc setServerKeyAndCert_P*(this: var WiFiServerSecure; key: ptr uint8_t; keyLen: cint;
                           cert: ptr uint8_t; certLen: cint) {.cdecl,
    importcpp: "setServerKeyAndCert_P", header: "WiFiServerSecureAxTLS.h".}
proc destroyWiFiServerSecure*(this: var WiFiServerSecure) {.cdecl,
    importcpp: "#.~WiFiServerSecure()", header: "WiFiServerSecureAxTLS.h".}
proc available*(this: var WiFiServerSecure; status: ptr uint8_t = nil): WiFiClientSecure {.
    cdecl, importcpp: "available", header: "WiFiServerSecureAxTLS.h".}
nil