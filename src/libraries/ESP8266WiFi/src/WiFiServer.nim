## 
##   WiFiServer.h - Library for Arduino Wifi shield.
##   Copyright (c) 2011-2014 Arduino LLC.  All right reserved.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##   Modified by Ivan Grokhotkov, December 2014 - esp8266 support
## 

import
  include/wl_definitions

discard "forward decl of tcp_pcb"
import
  Server, IPAddress

discard "forward decl of ClientContext"
discard "forward decl of WiFiClient"
type
  WiFiServer* {.importcpp: "WiFiServer", header: "WiFiServer.h", bycopy.} = object of Server ##  Secure server needs access to all the private entries here
  

proc constructWiFiServer*(`addr`: IPAddress; port: uint16_t): WiFiServer {.cdecl,
    constructor, importcpp: "WiFiServer(@)", header: "WiFiServer.h".}
proc constructWiFiServer*(port: uint16_t): WiFiServer {.cdecl, constructor,
    importcpp: "WiFiServer(@)", header: "WiFiServer.h".}
proc destroyWiFiServer*(this: var WiFiServer) {.cdecl, importcpp: "#.~WiFiServer()",
    header: "WiFiServer.h".}
proc available*(this: var WiFiServer; status: ptr uint8_t = nil): WiFiClient {.cdecl,
    importcpp: "available", header: "WiFiServer.h".}
proc hasClient*(this: var WiFiServer): bool {.cdecl, importcpp: "hasClient",
    header: "WiFiServer.h".}
proc begin*(this: var WiFiServer) {.cdecl, importcpp: "begin", header: "WiFiServer.h".}
proc begin*(this: var WiFiServer; port: uint16_t) {.cdecl, importcpp: "begin",
    header: "WiFiServer.h".}
proc setNoDelay*(this: var WiFiServer; nodelay: bool) {.cdecl, importcpp: "setNoDelay",
    header: "WiFiServer.h".}
proc getNoDelay*(this: var WiFiServer): bool {.cdecl, importcpp: "getNoDelay",
    header: "WiFiServer.h".}
proc write*(this: var WiFiServer; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "WiFiServer.h".}
proc write*(this: var WiFiServer; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "WiFiServer.h".}
proc status*(this: var WiFiServer): uint8_t {.cdecl, importcpp: "status",
    header: "WiFiServer.h".}
proc close*(this: var WiFiServer) {.cdecl, importcpp: "close", header: "WiFiServer.h".}
proc stop*(this: var WiFiServer) {.cdecl, importcpp: "stop", header: "WiFiServer.h".}