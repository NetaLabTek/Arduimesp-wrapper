## 
##  ESP8266WiFiAP.h - esp8266 Wifi support.
##  Based on WiFi.h from Arduino WiFi shield library.
##  Copyright (c) 2011-2014 Arduino.  All right reserved.
##  Modified by Ivan Grokhotkov, December 2014
##  Reworked by Markus Sattler, December 2015
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  ESP8266WiFiType, ESP8266WiFiGeneric

type
  ESP8266WiFiAPClass* {.importcpp: "ESP8266WiFiAPClass", header: "ESP8266WiFiAP.h",
                       bycopy.} = object ##  ----------------------------------------------------------------------------------------------
                                      ##  ----------------------------------------- AP function ----------------------------------------
                                      ##  ----------------------------------------------------------------------------------------------
  

proc softAP*(this: var ESP8266WiFiAPClass; ssid: cstring; passphrase: cstring = nil;
            channel: cint = 1; ssid_hidden: cint = 0; max_connection: cint = 4): bool {.
    cdecl, importcpp: "softAP", header: "ESP8266WiFiAP.h".}
proc softAPConfig*(this: var ESP8266WiFiAPClass; local_ip: IPAddress;
                  gateway: IPAddress; subnet: IPAddress): bool {.cdecl,
    importcpp: "softAPConfig", header: "ESP8266WiFiAP.h".}
proc softAPdisconnect*(this: var ESP8266WiFiAPClass; wifioff: bool = false): bool {.
    cdecl, importcpp: "softAPdisconnect", header: "ESP8266WiFiAP.h".}
proc softAPgetStationNum*(this: var ESP8266WiFiAPClass): uint8_t {.cdecl,
    importcpp: "softAPgetStationNum", header: "ESP8266WiFiAP.h".}
proc softAPIP*(this: var ESP8266WiFiAPClass): IPAddress {.cdecl,
    importcpp: "softAPIP", header: "ESP8266WiFiAP.h".}
proc softAPmacAddress*(this: var ESP8266WiFiAPClass; mac: ptr uint8_t): ptr uint8_t {.
    cdecl, importcpp: "softAPmacAddress", header: "ESP8266WiFiAP.h".}
proc softAPmacAddress*(this: var ESP8266WiFiAPClass): String {.cdecl,
    importcpp: "softAPmacAddress", header: "ESP8266WiFiAP.h".}
proc softAPSSID*(this: ESP8266WiFiAPClass): String {.noSideEffect, cdecl,
    importcpp: "softAPSSID", header: "ESP8266WiFiAP.h".}
proc softAPPSK*(this: ESP8266WiFiAPClass): String {.noSideEffect, cdecl,
    importcpp: "softAPPSK", header: "ESP8266WiFiAP.h".}