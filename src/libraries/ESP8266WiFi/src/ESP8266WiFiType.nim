## 
##  ESP8266WiFiType.h - esp8266 Wifi support.
##  Copyright (c) 2011-2014 Arduino.  All right reserved.
##  Modified by Ivan Grokhotkov, December 2014
##  Reworked by Markus Sattler, December 2015
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

const
  WIFI_SCAN_RUNNING* = (-1)
  WIFI_SCAN_FAILED* = (-2)

##  Note: these enums need to be in sync with the SDK!
##  TODO: replace/deprecate/remove enum typedefs ending with _t below

type
  WiFiMode_t* {.size: sizeof(cint), importcpp: "WiFiMode_t",
               header: "ESP8266WiFiType.h".} = enum
    WIFI_OFF = 0, WIFI_STA = 1, WIFI_AP = 2, WIFI_AP_STA = 3
  WiFiPhyMode_t* {.size: sizeof(cint), importcpp: "WiFiPhyMode_t",
                  header: "ESP8266WiFiType.h".} = enum
    WIFI_PHY_MODE_11B = 1, WIFI_PHY_MODE_11G = 2, WIFI_PHY_MODE_11N = 3
  WiFiSleepType_t* {.size: sizeof(cint), importcpp: "WiFiSleepType_t",
                    header: "ESP8266WiFiType.h".} = enum
    WIFI_NONE_SLEEP = 0, WIFI_LIGHT_SLEEP = 1, WIFI_MODEM_SLEEP = 2
  WiFiEvent_t* {.size: sizeof(cint), importcpp: "WiFiEvent_t",
                header: "ESP8266WiFiType.h".} = enum
    WIFI_EVENT_STAMODE_CONNECTED = 0, WIFI_EVENT_STAMODE_DISCONNECTED,
    WIFI_EVENT_STAMODE_AUTHMODE_CHANGE, WIFI_EVENT_STAMODE_GOT_IP,
    WIFI_EVENT_STAMODE_DHCP_TIMEOUT, WIFI_EVENT_SOFTAPMODE_STACONNECTED,
    WIFI_EVENT_SOFTAPMODE_STADISCONNECTED, WIFI_EVENT_SOFTAPMODE_PROBEREQRECVED,
    WIFI_EVENT_MAX, WIFI_EVENT_MODE_CHANGE




const
  WIFI_EVENT_ANY = WIFI_EVENT_MAX

type
  WiFiDisconnectReason* {.size: sizeof(cint), importcpp: "WiFiDisconnectReason",
                         header: "ESP8266WiFiType.h".} = enum
    WIFI_DISCONNECT_REASON_UNSPECIFIED = 1, WIFI_DISCONNECT_REASON_AUTH_EXPIRE = 2,
    WIFI_DISCONNECT_REASON_AUTH_LEAVE = 3, WIFI_DISCONNECT_REASON_ASSOC_EXPIRE = 4,
    WIFI_DISCONNECT_REASON_ASSOC_TOOMANY = 5,
    WIFI_DISCONNECT_REASON_NOT_AUTHED = 6, WIFI_DISCONNECT_REASON_NOT_ASSOCED = 7,
    WIFI_DISCONNECT_REASON_ASSOC_LEAVE = 8,
    WIFI_DISCONNECT_REASON_ASSOC_NOT_AUTHED = 9, WIFI_DISCONNECT_REASON_DISASSOC_PWRCAP_BAD = 10, ##  11h
    WIFI_DISCONNECT_REASON_DISASSOC_SUPCHAN_BAD = 11, ##  11h
    WIFI_DISCONNECT_REASON_IE_INVALID = 13, ##  11i
    WIFI_DISCONNECT_REASON_MIC_FAILURE = 14, ##  11i
    WIFI_DISCONNECT_REASON_4WAY_HANDSHAKE_TIMEOUT = 15, ##  11i
    WIFI_DISCONNECT_REASON_GROUP_KEY_UPDATE_TIMEOUT = 16, ##  11i
    WIFI_DISCONNECT_REASON_IE_IN_4WAY_DIFFERS = 17, ##  11i
    WIFI_DISCONNECT_REASON_GROUP_CIPHER_INVALID = 18, ##  11i
    WIFI_DISCONNECT_REASON_PAIRWISE_CIPHER_INVALID = 19, ##  11i
    WIFI_DISCONNECT_REASON_AKMP_INVALID = 20, ##  11i
    WIFI_DISCONNECT_REASON_UNSUPP_RSN_IE_VERSION = 21, ##  11i
    WIFI_DISCONNECT_REASON_INVALID_RSN_IE_CAP = 22, ##  11i
    WIFI_DISCONNECT_REASON_802_1X_AUTH_FAILED = 23, ##  11i
    WIFI_DISCONNECT_REASON_CIPHER_SUITE_REJECTED = 24, ##  11i
    WIFI_DISCONNECT_REASON_BEACON_TIMEOUT = 200,
    WIFI_DISCONNECT_REASON_NO_AP_FOUND = 201,
    WIFI_DISCONNECT_REASON_AUTH_FAIL = 202,
    WIFI_DISCONNECT_REASON_ASSOC_FAIL = 203,
    WIFI_DISCONNECT_REASON_HANDSHAKE_TIMEOUT = 204


type
  WiFiEventModeChange* {.importcpp: "WiFiEventModeChange",
                        header: "ESP8266WiFiType.h", bycopy.} = object
    oldMode* {.importc: "oldMode".}: WiFiMode
    newMode* {.importc: "newMode".}: WiFiMode

  WiFiEventStationModeConnected* {.importcpp: "WiFiEventStationModeConnected",
                                  header: "ESP8266WiFiType.h", bycopy.} = object
    ssid* {.importc: "ssid".}: String
    bssid* {.importc: "bssid".}: array[6, uint8]
    channel* {.importc: "channel".}: uint8

  WiFiEventStationModeDisconnected* {.importcpp: "WiFiEventStationModeDisconnected",
                                     header: "ESP8266WiFiType.h", bycopy.} = object
    ssid* {.importc: "ssid".}: String
    bssid* {.importc: "bssid".}: array[6, uint8]
    reason* {.importc: "reason".}: WiFiDisconnectReason

  WiFiEventStationModeAuthModeChanged* {.importcpp: "WiFiEventStationModeAuthModeChanged",
                                        header: "ESP8266WiFiType.h", bycopy.} = object
    oldMode* {.importc: "oldMode".}: uint8
    newMode* {.importc: "newMode".}: uint8

  WiFiEventStationModeGotIP* {.importcpp: "WiFiEventStationModeGotIP",
                              header: "ESP8266WiFiType.h", bycopy.} = object
    ip* {.importc: "ip".}: IPAddress
    mask* {.importc: "mask".}: IPAddress
    gw* {.importc: "gw".}: IPAddress

  WiFiEventSoftAPModeStationConnected* {.importcpp: "WiFiEventSoftAPModeStationConnected",
                                        header: "ESP8266WiFiType.h", bycopy.} = object
    mac* {.importc: "mac".}: array[6, uint8]
    aid* {.importc: "aid".}: uint8

  WiFiEventSoftAPModeStationDisconnected* {.
      importcpp: "WiFiEventSoftAPModeStationDisconnected",
      header: "ESP8266WiFiType.h", bycopy.} = object
    mac* {.importc: "mac".}: array[6, uint8]
    aid* {.importc: "aid".}: uint8

  WiFiEventSoftAPModeProbeRequestReceived* {.
      importcpp: "WiFiEventSoftAPModeProbeRequestReceived",
      header: "ESP8266WiFiType.h", bycopy.} = object
    rssi* {.importc: "rssi".}: cint
    mac* {.importc: "mac".}: array[6, uint8]

