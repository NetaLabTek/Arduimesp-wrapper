## 
## ESP8266 Simple Service Discovery
## Copyright (c) 2015 Hristo Gochkov
## 
## Original (Arduino) version by Filippo Sallemi, July 23, 2014.
## Can be found at: https://github.com/nomadnt/uSSDP
## 
## License (MIT license):
##   Permission is hereby granted, free of charge, to any person obtaining a copy
##   of this software and associated documentation files (the "Software"), to deal
##   in the Software without restriction, including without limitation the rights
##   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##   copies of the Software, and to permit persons to whom the Software is
##   furnished to do so, subject to the following conditions:
## 
##   The above copyright notice and this permission notice shall be included in
##   all copies or substantial portions of the Software.
## 
##   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##   THE SOFTWARE.
## 
## 

discard "forward decl of UdpContext"
const
  SSDP_UUID_SIZE* = 37
  SSDP_SCHEMA_URL_SIZE* = 64
  SSDP_DEVICE_TYPE_SIZE* = 64
  SSDP_FRIENDLY_NAME_SIZE* = 64
  SSDP_SERIAL_NUMBER_SIZE* = 32
  SSDP_PRESENTATION_URL_SIZE* = 128
  SSDP_MODEL_NAME_SIZE* = 64
  SSDP_MODEL_URL_SIZE* = 128
  SSDP_MODEL_VERSION_SIZE* = 32
  SSDP_MANUFACTURER_SIZE* = 64
  SSDP_MANUFACTURER_URL_SIZE* = 128

type
  ssdp_method_t* {.size: sizeof(cint), importcpp: "ssdp_method_t",
                  header: "ESP8266SSDP.h".} = enum
    NONE, SEARCH, NOTIFY


discard "forward decl of SSDPTimer"
type
  SSDPClass* {.importcpp: "SSDPClass", header: "ESP8266SSDP.h", bycopy.} = object
  

proc constructSSDPClass*(): SSDPClass {.cdecl, constructor,
                                     importcpp: "SSDPClass(@)",
                                     header: "ESP8266SSDP.h".}
proc destroySSDPClass*(this: var SSDPClass) {.cdecl, importcpp: "#.~SSDPClass()",
    header: "ESP8266SSDP.h".}
proc begin*(this: var SSDPClass): bool {.cdecl, importcpp: "begin",
                                    header: "ESP8266SSDP.h".}
proc schema*(this: var SSDPClass; client: WiFiClient) {.cdecl, importcpp: "schema",
    header: "ESP8266SSDP.h".}
proc setDeviceType*(this: var SSDPClass; deviceType: String) {.cdecl,
    importcpp: "setDeviceType", header: "ESP8266SSDP.h".}
proc setDeviceType*(this: var SSDPClass; deviceType: cstring) {.cdecl,
    importcpp: "setDeviceType", header: "ESP8266SSDP.h".}
proc setName*(this: var SSDPClass; name: String) {.cdecl, importcpp: "setName",
    header: "ESP8266SSDP.h".}
proc setName*(this: var SSDPClass; name: cstring) {.cdecl, importcpp: "setName",
    header: "ESP8266SSDP.h".}
proc setURL*(this: var SSDPClass; url: String) {.cdecl, importcpp: "setURL",
    header: "ESP8266SSDP.h".}
proc setURL*(this: var SSDPClass; url: cstring) {.cdecl, importcpp: "setURL",
    header: "ESP8266SSDP.h".}
proc setSchemaURL*(this: var SSDPClass; url: String) {.cdecl,
    importcpp: "setSchemaURL", header: "ESP8266SSDP.h".}
proc setSchemaURL*(this: var SSDPClass; url: cstring) {.cdecl,
    importcpp: "setSchemaURL", header: "ESP8266SSDP.h".}
proc setSerialNumber*(this: var SSDPClass; serialNumber: String) {.cdecl,
    importcpp: "setSerialNumber", header: "ESP8266SSDP.h".}
proc setSerialNumber*(this: var SSDPClass; serialNumber: cstring) {.cdecl,
    importcpp: "setSerialNumber", header: "ESP8266SSDP.h".}
proc setSerialNumber*(this: var SSDPClass; serialNumber: uint32_t) {.cdecl,
    importcpp: "setSerialNumber", header: "ESP8266SSDP.h".}
proc setModelName*(this: var SSDPClass; name: String) {.cdecl,
    importcpp: "setModelName", header: "ESP8266SSDP.h".}
proc setModelName*(this: var SSDPClass; name: cstring) {.cdecl,
    importcpp: "setModelName", header: "ESP8266SSDP.h".}
proc setModelNumber*(this: var SSDPClass; num: String) {.cdecl,
    importcpp: "setModelNumber", header: "ESP8266SSDP.h".}
proc setModelNumber*(this: var SSDPClass; num: cstring) {.cdecl,
    importcpp: "setModelNumber", header: "ESP8266SSDP.h".}
proc setModelURL*(this: var SSDPClass; url: String) {.cdecl, importcpp: "setModelURL",
    header: "ESP8266SSDP.h".}
proc setModelURL*(this: var SSDPClass; url: cstring) {.cdecl, importcpp: "setModelURL",
    header: "ESP8266SSDP.h".}
proc setManufacturer*(this: var SSDPClass; name: String) {.cdecl,
    importcpp: "setManufacturer", header: "ESP8266SSDP.h".}
proc setManufacturer*(this: var SSDPClass; name: cstring) {.cdecl,
    importcpp: "setManufacturer", header: "ESP8266SSDP.h".}
proc setManufacturerURL*(this: var SSDPClass; url: String) {.cdecl,
    importcpp: "setManufacturerURL", header: "ESP8266SSDP.h".}
proc setManufacturerURL*(this: var SSDPClass; url: cstring) {.cdecl,
    importcpp: "setManufacturerURL", header: "ESP8266SSDP.h".}
proc setHTTPPort*(this: var SSDPClass; port: uint16_t) {.cdecl,
    importcpp: "setHTTPPort", header: "ESP8266SSDP.h".}
proc setTTL*(this: var SSDPClass; ttl: uint8_t) {.cdecl, importcpp: "setTTL",
    header: "ESP8266SSDP.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_SSDP):
  var SSDP* {.importcpp: "SSDP", header: "ESP8266SSDP.h".}: SSDPClass