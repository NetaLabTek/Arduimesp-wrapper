##  
##  xtensa/config/core-isa.h -- HAL definitions that are dependent on Xtensa
## 				processor CORE configuration
## 
##   See <xtensa/config/core.h>, which includes this file, for more details.
## 
##  Xtensa processor core configuration information.
## 
##    Customer ID=7011; Build=0x2b6f6; Copyright (c) 1999-2010 Tensilica Inc.
## 
##    Permission is hereby granted, free of charge, to any person obtaining
##    a copy of this software and associated documentation files (the
##    "Software"), to deal in the Software without restriction, including
##    without limitation the rights to use, copy, modify, merge, publish,
##    distribute, sublicense, and/or sell copies of the Software, and to
##    permit persons to whom the Software is furnished to do so, subject to
##    the following conditions:
## 
##    The above copyright notice and this permission notice shall be included
##    in all copies or substantial portions of the Software.
## 
##    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
##    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
##    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
##    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
##    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
##    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
##    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## ***************************************************************************
## 	    Parameters Useful for Any Code, USER or PRIVILEGED
## **************************************************************************
## 
##   Note:  Macros of the form XCHAL_HAVE_*** have a value of 1 if the option is
##   configured, and a value of 0 otherwise.  These macros are always defined.
## 
## ----------------------------------------------------------------------
## 				ISA
##   ----------------------------------------------------------------------

const
  XCHAL_HAVE_BE* = 0
  XCHAL_HAVE_WINDOWED* = 0
  XCHAL_NUM_AREGS* = 16
  XCHAL_NUM_AREGS_LOG2* = 4
  XCHAL_MAX_INSTRUCTION_SIZE* = 3
  XCHAL_HAVE_DEBUG* = 1
  XCHAL_HAVE_DENSITY* = 1
  XCHAL_HAVE_LOOPS* = 0
  XCHAL_HAVE_NSA* = 1
  XCHAL_HAVE_MINMAX* = 0
  XCHAL_HAVE_SEXT* = 0
  XCHAL_HAVE_CLAMPS* = 0
  XCHAL_HAVE_MUL16* = 1
  XCHAL_HAVE_MUL32* = 1
  XCHAL_HAVE_MUL32_HIGH* = 0
  XCHAL_HAVE_DIV32* = 0
  XCHAL_HAVE_L32R* = 1
  XCHAL_HAVE_ABSOLUTE_LITERALS* = 1
  XCHAL_HAVE_CONST16* = 0
  XCHAL_HAVE_ADDX* = 1
  XCHAL_HAVE_WIDE_BRANCHES* = 0
  XCHAL_HAVE_PREDICTED_BRANCHES* = 0
  XCHAL_HAVE_CALL4AND12* = 0
  XCHAL_HAVE_ABS* = 1

## #define XCHAL_HAVE_POPC		0
##  POPC instruction
## #define XCHAL_HAVE_CRC		0
##  CRC instruction

const
  XCHAL_HAVE_RELEASE_SYNC* = 0
  XCHAL_HAVE_S32C1I* = 0
  XCHAL_HAVE_SPECULATION* = 0
  XCHAL_HAVE_FULL_RESET* = 1
  XCHAL_NUM_CONTEXTS* = 1
  XCHAL_NUM_MISC_REGS* = 0
  XCHAL_HAVE_TAP_MASTER* = 0
  XCHAL_HAVE_PRID* = 1
  XCHAL_HAVE_EXTERN_REGS* = 1
  XCHAL_HAVE_MP_INTERRUPTS* = 0
  XCHAL_HAVE_MP_RUNSTALL* = 0
  XCHAL_HAVE_THREADPTR* = 0
  XCHAL_HAVE_BOOLEANS* = 0
  XCHAL_HAVE_CP* = 0
  XCHAL_CP_MAXCFG* = 0
  XCHAL_HAVE_MAC16* = 0
  XCHAL_HAVE_VECTORFPU2005* = 0
  XCHAL_HAVE_FP* = 0
  XCHAL_HAVE_DFP* = 0
  XCHAL_HAVE_DFP_accel* = 0
  XCHAL_HAVE_VECTRA1* = 0
  XCHAL_HAVE_VECTRALX* = 0
  XCHAL_HAVE_HIFIPRO* = 0
  XCHAL_HAVE_HIFI2* = 0
  XCHAL_HAVE_CONNXD2* = 0

## ----------------------------------------------------------------------
## 				MISC
##   ----------------------------------------------------------------------

const
  XCHAL_NUM_WRITEBUFFER_ENTRIES* = 1
  XCHAL_INST_FETCH_WIDTH* = 4
  XCHAL_DATA_WIDTH* = 4

##   In T1050, applies to selected core load and store instructions (see ISA):

const
  XCHAL_UNALIGNED_LOAD_EXCEPTION* = 1
  XCHAL_UNALIGNED_STORE_EXCEPTION* = 1
  XCHAL_UNALIGNED_LOAD_HW* = 0
  XCHAL_UNALIGNED_STORE_HW* = 0
  XCHAL_SW_VERSION* = 800001
  XCHAL_CORE_ID* = "lx106"
  XCHAL_BUILD_UNIQUE_ID* = 0x0002B6F6

## 
##   These definitions describe the hardware targeted by this software.
## 

const
  XCHAL_HW_CONFIGID0* = 0xC28CDAFA
  XCHAL_HW_CONFIGID1* = 0x1082B6F6
  XCHAL_HW_VERSION_NAME* = "LX3.0.1"
  XCHAL_HW_VERSION_MAJOR* = 2300
  XCHAL_HW_VERSION_MINOR* = 1
  XCHAL_HW_VERSION* = 230001
  XCHAL_HW_REL_LX3* = 1
  XCHAL_HW_REL_LX3_0* = 1
  XCHAL_HW_REL_LX3_0_1* = 1
  XCHAL_HW_CONFIGID_RELIABLE* = 1

##   If software targets a *range* of hardware versions, these are the bounds:

const
  XCHAL_HW_MIN_VERSION_MAJOR* = 2300
  XCHAL_HW_MIN_VERSION_MINOR* = 1
  XCHAL_HW_MIN_VERSION* = 230001
  XCHAL_HW_MAX_VERSION_MAJOR* = 2300
  XCHAL_HW_MAX_VERSION_MINOR* = 1
  XCHAL_HW_MAX_VERSION* = 230001

## ----------------------------------------------------------------------
## 				CACHE
##   ----------------------------------------------------------------------

const
  XCHAL_ICACHE_LINESIZE* = 4
  XCHAL_DCACHE_LINESIZE* = 4
  XCHAL_ICACHE_LINEWIDTH* = 2
  XCHAL_DCACHE_LINEWIDTH* = 2
  XCHAL_ICACHE_SIZE* = 0
  XCHAL_DCACHE_SIZE* = 0
  XCHAL_DCACHE_IS_WRITEBACK* = 0
  XCHAL_DCACHE_IS_COHERENT* = 0
  XCHAL_HAVE_PREFETCH* = 0

## ***************************************************************************
##     Parameters Useful for PRIVILEGED (Supervisory or Non-Virtualized) Code
## **************************************************************************

when not defined(XTENSA_HAL_NON_PRIVILEGED_ONLY):
  ## ----------------------------------------------------------------------
  ## 				CACHE
  ##   ----------------------------------------------------------------------
  const
    XCHAL_HAVE_PIF* = 1
  ##   If present, cache size in bytes == (ways * 2^(linewidth + setwidth)).
  ##   Number of cache sets in log2(lines per way):
  const
    XCHAL_ICACHE_SETWIDTH* = 0
    XCHAL_DCACHE_SETWIDTH* = 0
  ##   Cache set associativity (number of ways):
  const
    XCHAL_ICACHE_WAYS* = 1
    XCHAL_DCACHE_WAYS* = 1
  ##   Cache features:
  const
    XCHAL_ICACHE_LINE_LOCKABLE* = 0
    XCHAL_DCACHE_LINE_LOCKABLE* = 0
    XCHAL_ICACHE_ECC_PARITY* = 0
    XCHAL_DCACHE_ECC_PARITY* = 0
  ##   Cache access size in bytes (affects operation of SICW instruction):
  const
    XCHAL_ICACHE_ACCESS_SIZE* = 1
    XCHAL_DCACHE_ACCESS_SIZE* = 1
  ##   Number of encoded cache attr bits (see <xtensa/hal.h> for decoded bits):
  const
    XCHAL_CA_BITS* = 4

  ## ----------------------------------------------------------------------
  ## 			INTERNAL I/D RAM/ROMs and XLMI
  ##   ----------------------------------------------------------------------
  const
    XCHAL_NUM_INSTROM* = 1
    XCHAL_NUM_INSTRAM* = 2
    XCHAL_NUM_DATAROM* = 1
    XCHAL_NUM_DATARAM* = 2
    XCHAL_NUM_URAM* = 0
    XCHAL_NUM_XLMI* = 1
  ##   Instruction ROM 0:
  const
    XCHAL_INSTROM0_VADDR* = 0x40200000
    XCHAL_INSTROM0_PADDR* = 0x40200000
    XCHAL_INSTROM0_SIZE* = 1048576
    XCHAL_INSTROM0_ECC_PARITY* = 0
  ##   Instruction RAM 0:
  const
    XCHAL_INSTRAM0_VADDR* = 0x40000000
    XCHAL_INSTRAM0_PADDR* = 0x40000000
    XCHAL_INSTRAM0_SIZE* = 1048576
    XCHAL_INSTRAM0_ECC_PARITY* = 0
  ##   Instruction RAM 1:
  const
    XCHAL_INSTRAM1_VADDR* = 0x40100000
    XCHAL_INSTRAM1_PADDR* = 0x40100000
    XCHAL_INSTRAM1_SIZE* = 1048576
    XCHAL_INSTRAM1_ECC_PARITY* = 0
  ##   Data ROM 0:
  const
    XCHAL_DATAROM0_VADDR* = 0x3FF40000
    XCHAL_DATAROM0_PADDR* = 0x3FF40000
    XCHAL_DATAROM0_SIZE* = 262144
    XCHAL_DATAROM0_ECC_PARITY* = 0
  ##   Data RAM 0:
  const
    XCHAL_DATARAM0_VADDR* = 0x3FFC0000
    XCHAL_DATARAM0_PADDR* = 0x3FFC0000
    XCHAL_DATARAM0_SIZE* = 262144
    XCHAL_DATARAM0_ECC_PARITY* = 0
  ##   Data RAM 1:
  const
    XCHAL_DATARAM1_VADDR* = 0x3FF80000
    XCHAL_DATARAM1_PADDR* = 0x3FF80000
    XCHAL_DATARAM1_SIZE* = 262144
    XCHAL_DATARAM1_ECC_PARITY* = 0
  ##   XLMI Port 0:
  const
    XCHAL_XLMI0_VADDR* = 0x3FF00000
    XCHAL_XLMI0_PADDR* = 0x3FF00000
    XCHAL_XLMI0_SIZE* = 262144
    XCHAL_XLMI0_ECC_PARITY* = 0

  ## ----------------------------------------------------------------------
  ## 			INTERRUPTS and TIMERS
  ##   ----------------------------------------------------------------------
  const
    XCHAL_HAVE_INTERRUPTS* = 1
    XCHAL_HAVE_HIGHPRI_INTERRUPTS* = 1
    XCHAL_HAVE_NMI* = 1
    XCHAL_HAVE_CCOUNT* = 1
    XCHAL_NUM_TIMERS* = 1
    XCHAL_NUM_INTERRUPTS* = 15
    XCHAL_NUM_INTERRUPTS_LOG2* = 4
    XCHAL_NUM_EXTINTERRUPTS* = 13
    XCHAL_NUM_INTLEVELS* = 2
    XCHAL_EXCM_LEVEL* = 1
  ##  (always 1 in XEA1; levels 2 .. EXCM_LEVEL are "medium priority")
  ##   Masks of interrupts at each interrupt level:
  const
    XCHAL_INTLEVEL1_MASK* = 0x00003FFF
    XCHAL_INTLEVEL2_MASK* = 0x00000000
    XCHAL_INTLEVEL3_MASK* = 0x00004000
    XCHAL_INTLEVEL4_MASK* = 0x00000000
    XCHAL_INTLEVEL5_MASK* = 0x00000000
    XCHAL_INTLEVEL6_MASK* = 0x00000000
    XCHAL_INTLEVEL7_MASK* = 0x00000000
  ##   Masks of interrupts at each range 1..n of interrupt levels:
  const
    XCHAL_INTLEVEL1_ANDBELOW_MASK* = 0x00003FFF
    XCHAL_INTLEVEL2_ANDBELOW_MASK* = 0x00003FFF
    XCHAL_INTLEVEL3_ANDBELOW_MASK* = 0x00007FFF
    XCHAL_INTLEVEL4_ANDBELOW_MASK* = 0x00007FFF
    XCHAL_INTLEVEL5_ANDBELOW_MASK* = 0x00007FFF
    XCHAL_INTLEVEL6_ANDBELOW_MASK* = 0x00007FFF
    XCHAL_INTLEVEL7_ANDBELOW_MASK* = 0x00007FFF
  ##   Level of each interrupt:
  const
    XCHAL_INT0_LEVEL* = 1
    XCHAL_INT1_LEVEL* = 1
    XCHAL_INT2_LEVEL* = 1
    XCHAL_INT3_LEVEL* = 1
    XCHAL_INT4_LEVEL* = 1
    XCHAL_INT5_LEVEL* = 1
    XCHAL_INT6_LEVEL* = 1
    XCHAL_INT7_LEVEL* = 1
    XCHAL_INT8_LEVEL* = 1
    XCHAL_INT9_LEVEL* = 1
    XCHAL_INT10_LEVEL* = 1
    XCHAL_INT11_LEVEL* = 1
    XCHAL_INT12_LEVEL* = 1
    XCHAL_INT13_LEVEL* = 1
    XCHAL_INT14_LEVEL* = 3
    XCHAL_DEBUGLEVEL* = 2
    XCHAL_HAVE_DEBUG_EXTERN_INT* = 1
    XCHAL_NMILEVEL* = 3
  ##   Type of each interrupt:
  const
    XCHAL_INT0_TYPE* = XTHAL_INTTYPE_EXTERN_LEVEL
    XCHAL_INT1_TYPE* = XTHAL_INTTYPE_EXTERN_LEVEL
    XCHAL_INT2_TYPE* = XTHAL_INTTYPE_EXTERN_LEVEL
    XCHAL_INT3_TYPE* = XTHAL_INTTYPE_EXTERN_LEVEL
    XCHAL_INT4_TYPE* = XTHAL_INTTYPE_EXTERN_LEVEL
    XCHAL_INT5_TYPE* = XTHAL_INTTYPE_EXTERN_LEVEL
    XCHAL_INT6_TYPE* = XTHAL_INTTYPE_TIMER
    XCHAL_INT7_TYPE* = XTHAL_INTTYPE_SOFTWARE
    XCHAL_INT8_TYPE* = XTHAL_INTTYPE_EXTERN_EDGE
    XCHAL_INT9_TYPE* = XTHAL_INTTYPE_EXTERN_EDGE
    XCHAL_INT10_TYPE* = XTHAL_INTTYPE_EXTERN_EDGE
    XCHAL_INT11_TYPE* = XTHAL_INTTYPE_EXTERN_EDGE
    XCHAL_INT12_TYPE* = XTHAL_INTTYPE_EXTERN_EDGE
    XCHAL_INT13_TYPE* = XTHAL_INTTYPE_EXTERN_EDGE
    XCHAL_INT14_TYPE* = XTHAL_INTTYPE_NMI
  ##   Masks of interrupts for each type of interrupt:
  const
    XCHAL_INTTYPE_MASK_UNCONFIGURED* = 0xFFFF8000
    XCHAL_INTTYPE_MASK_SOFTWARE* = 0x00000080
    XCHAL_INTTYPE_MASK_EXTERN_EDGE* = 0x00003F00
    XCHAL_INTTYPE_MASK_EXTERN_LEVEL* = 0x0000003F
    XCHAL_INTTYPE_MASK_TIMER* = 0x00000040
    XCHAL_INTTYPE_MASK_NMI* = 0x00004000
    XCHAL_INTTYPE_MASK_WRITE_ERROR* = 0x00000000
  ##   Interrupt numbers assigned to specific interrupt sources:
  const
    XCHAL_TIMER0_INTERRUPT* = 6
    XCHAL_TIMER1_INTERRUPT* = XTHAL_TIMER_UNCONFIGURED
    XCHAL_TIMER2_INTERRUPT* = XTHAL_TIMER_UNCONFIGURED
    XCHAL_TIMER3_INTERRUPT* = XTHAL_TIMER_UNCONFIGURED
    XCHAL_NMI_INTERRUPT* = 14
  ##   Interrupt numbers for levels at which only one interrupt is configured:
  const
    XCHAL_INTLEVEL3_NUM* = 14
  ##   (There are many interrupts each at level(s) 1.)
  ## 
  ##   External interrupt vectors/levels.
  ##   These macros describe how Xtensa processor interrupt numbers
  ##   (as numbered internally, eg. in INTERRUPT and INTENABLE registers)
  ##   map to external BInterrupt<n> pins, for those interrupts
  ##   configured as external (level-triggered, edge-triggered, or NMI).
  ##   See the Xtensa processor databook for more details.
  ## 
  ##   Core interrupt numbers mapped to each EXTERNAL interrupt number:
  const
    XCHAL_EXTINT0_NUM* = 0
    XCHAL_EXTINT1_NUM* = 1
    XCHAL_EXTINT2_NUM* = 2
    XCHAL_EXTINT3_NUM* = 3
    XCHAL_EXTINT4_NUM* = 4
    XCHAL_EXTINT5_NUM* = 5
    XCHAL_EXTINT6_NUM* = 8
    XCHAL_EXTINT7_NUM* = 9
    XCHAL_EXTINT8_NUM* = 10
    XCHAL_EXTINT9_NUM* = 11
    XCHAL_EXTINT10_NUM* = 12
    XCHAL_EXTINT11_NUM* = 13
    XCHAL_EXTINT12_NUM* = 14

  ## ----------------------------------------------------------------------
  ## 			EXCEPTIONS and VECTORS
  ##   ----------------------------------------------------------------------
  const
    XCHAL_XEA_VERSION* = 2
    XCHAL_HAVE_XEA1* = 0
    XCHAL_HAVE_XEA2* = 1
    XCHAL_HAVE_XEAX* = 0
    XCHAL_HAVE_EXCEPTIONS* = 1
    XCHAL_HAVE_MEM_ECC_PARITY* = 0
    XCHAL_HAVE_VECTOR_SELECT* = 1
    XCHAL_HAVE_VECBASE* = 1
    XCHAL_VECBASE_RESET_VADDR* = 0x40000000
    XCHAL_VECBASE_RESET_PADDR* = 0x40000000
    XCHAL_RESET_VECBASE_OVERLAP* = 0
    XCHAL_RESET_VECTOR0_VADDR* = 0x50000000
    XCHAL_RESET_VECTOR0_PADDR* = 0x50000000
    XCHAL_RESET_VECTOR1_VADDR* = 0x40000080
    XCHAL_RESET_VECTOR1_PADDR* = 0x40000080
    XCHAL_RESET_VECTOR_VADDR* = 0x50000000
    XCHAL_RESET_VECTOR_PADDR* = 0x50000000
    XCHAL_USER_VECOFS* = 0x00000050
    XCHAL_USER_VECTOR_VADDR* = 0x40000050
    XCHAL_USER_VECTOR_PADDR* = 0x40000050
    XCHAL_KERNEL_VECOFS* = 0x00000030
    XCHAL_KERNEL_VECTOR_VADDR* = 0x40000030
    XCHAL_KERNEL_VECTOR_PADDR* = 0x40000030
    XCHAL_DOUBLEEXC_VECOFS* = 0x00000070
    XCHAL_DOUBLEEXC_VECTOR_VADDR* = 0x40000070
    XCHAL_DOUBLEEXC_VECTOR_PADDR* = 0x40000070
    XCHAL_INTLEVEL2_VECOFS* = 0x00000010
    XCHAL_INTLEVEL2_VECTOR_VADDR* = 0x40000010
    XCHAL_INTLEVEL2_VECTOR_PADDR* = 0x40000010
    XCHAL_DEBUG_VECOFS* = XCHAL_INTLEVEL2_VECOFS
    XCHAL_DEBUG_VECTOR_VADDR* = XCHAL_INTLEVEL2_VECTOR_VADDR
    XCHAL_DEBUG_VECTOR_PADDR* = XCHAL_INTLEVEL2_VECTOR_PADDR
    XCHAL_NMI_VECOFS* = 0x00000020
    XCHAL_NMI_VECTOR_VADDR* = 0x40000020
    XCHAL_NMI_VECTOR_PADDR* = 0x40000020
    XCHAL_INTLEVEL3_VECOFS* = XCHAL_NMI_VECOFS
    XCHAL_INTLEVEL3_VECTOR_VADDR* = XCHAL_NMI_VECTOR_VADDR
    XCHAL_INTLEVEL3_VECTOR_PADDR* = XCHAL_NMI_VECTOR_PADDR

  ## ----------------------------------------------------------------------
  ## 				DEBUG
  ##   ----------------------------------------------------------------------
  const
    XCHAL_HAVE_OCD* = 1
    XCHAL_NUM_IBREAK* = 1
    XCHAL_NUM_DBREAK* = 1
    XCHAL_HAVE_OCD_DIR_ARRAY* = 0

  ## ----------------------------------------------------------------------
  ## 				MMU
  ##   ----------------------------------------------------------------------
  ##   See core-matmap.h header file for more details.
  const
    XCHAL_HAVE_TLBS* = 1
    XCHAL_HAVE_SPANNING_WAY* = 1
    XCHAL_SPANNING_WAY* = 0
    XCHAL_HAVE_IDENTITY_MAP* = 1
    XCHAL_HAVE_CACHEATTR* = 0
    XCHAL_HAVE_MIMIC_CACHEATTR* = 1
    XCHAL_HAVE_XLT_CACHEATTR* = 0
    XCHAL_HAVE_PTP_MMU* = 0
  ##   If none of the above last 4 are set, it's a custom TLB configuration.
  const
    XCHAL_MMU_ASID_BITS* = 0
    XCHAL_MMU_RINGS* = 1
    XCHAL_MMU_RING_BITS* = 0