## 
##  Xtensa Special Register symbolic names
## 
##  $Id: //depot/rel/Boreal/Xtensa/SWConfig/hal/specreg.h.tpp#2 $
##  Customer ID=7011; Build=0x2b6f6; Copyright (c) 1998-2002 Tensilica Inc.
## 
##    Permission is hereby granted, free of charge, to any person obtaining
##    a copy of this software and associated documentation files (the
##    "Software"), to deal in the Software without restriction, including
##    without limitation the rights to use, copy, modify, merge, publish,
##    distribute, sublicense, and/or sell copies of the Software, and to
##    permit persons to whom the Software is furnished to do so, subject to
##    the following conditions:
## 
##    The above copyright notice and this permission notice shall be included
##    in all copies or substantial portions of the Software.
## 
##    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
##    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
##    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
##    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
##    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
##    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
##    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

##   Include these special register bitfield definitions, for historical reasons:

##   Special registers:

const
  SAR* = 3
  LITBASE* = 5
  IBREAKENABLE* = 96
  DDR* = 104
  IBREAKA_0* = 128
  DBREAKA_0* = 144
  DBREAKC_0* = 160
  EPC_1* = 177
  EPC_2* = 178
  EPC_3* = 179
  DEPC* = 192
  EPS_2* = 194
  EPS_3* = 195
  EXCSAVE_1* = 209
  EXCSAVE_2* = 210
  EXCSAVE_3* = 211
  INTERRUPT* = 226
  INTENABLE* = 228
  PS* = 230
  VECBASE* = 231
  EXCCAUSE* = 232
  DEBUGCAUSE* = 233
  CCOUNT* = 234
  PRID* = 235
  ICOUNT* = 236
  ICOUNTLEVEL* = 237
  EXCVADDR* = 238
  CCOMPARE_0* = 240

##   Special cases (bases of special register series):

const
  IBREAKA* = 128
  DBREAKA* = 144
  DBREAKC* = 160
  EPC* = 176
  EPS* = 192
  EXCSAVE* = 208
  CCOMPARE* = 240

##   Special names for read-only and write-only interrupt registers:

const
  INTREAD* = 226
  INTSET* = 226
  INTCLEAR* = 227
