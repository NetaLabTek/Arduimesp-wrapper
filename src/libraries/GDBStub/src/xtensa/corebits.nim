## 
##  xtensa/corebits.h - Xtensa Special Register field positions, masks, values.
## 
##  (In previous releases, these were defined in specreg.h, a generated file.
##   This file is not generated, ie. it is processor configuration independent.)
## 
##  $Id: //depot/rel/Boreal/Xtensa/OS/include/xtensa/corebits.h#2 $
## 
##  Copyright (c) 2005-2007 Tensilica Inc.
## 
##  Permission is hereby granted, free of charge, to any person obtaining
##  a copy of this software and associated documentation files (the
##  "Software"), to deal in the Software without restriction, including
##  without limitation the rights to use, copy, modify, merge, publish,
##  distribute, sublicense, and/or sell copies of the Software, and to
##  permit persons to whom the Software is furnished to do so, subject to
##  the following conditions:
## 
##  The above copyright notice and this permission notice shall be included
##  in all copies or substantial portions of the Software.
## 
##  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
##  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
##  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
##  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
##  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
##  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
##  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
## 

##   EXCCAUSE register fields:

const
  EXCCAUSE_EXCCAUSE_SHIFT* = 0
  EXCCAUSE_EXCCAUSE_MASK* = 0x0000003F

##   EXCCAUSE register values:
## 
##   General Exception Causes
##   (values of EXCCAUSE special register set by general exceptions,
##    which vector to the user, kernel, or double-exception vectors).
## 

const
  EXCCAUSE_ILLEGAL* = 0
  EXCCAUSE_SYSCALL* = 1
  EXCCAUSE_INSTR_ERROR* = 2
  EXCCAUSE_IFETCHERROR* = 2
  EXCCAUSE_LOAD_STORE_ERROR* = 3
  EXCCAUSE_LOADSTOREERROR* = 3
  EXCCAUSE_LEVEL1_INTERRUPT* = 4
  EXCCAUSE_LEVEL1INTERRUPT* = 4
  EXCCAUSE_ALLOCA* = 5
  EXCCAUSE_DIVIDE_BY_ZERO* = 6
  EXCCAUSE_SPECULATION* = 7
  EXCCAUSE_PRIVILEGED* = 8
  EXCCAUSE_UNALIGNED* = 9

##  Reserved				10..11

const
  EXCCAUSE_INSTR_DATA_ERROR* = 12
  EXCCAUSE_LOAD_STORE_DATA_ERROR* = 13
  EXCCAUSE_INSTR_ADDR_ERROR* = 14
  EXCCAUSE_LOAD_STORE_ADDR_ERROR* = 15
  EXCCAUSE_ITLB_MISS* = 16
  EXCCAUSE_ITLB_MULTIHIT* = 17
  EXCCAUSE_INSTR_RING* = 18

##  Reserved				19
##  Size Restriction on IFetch (not implemented)

const
  EXCCAUSE_INSTR_PROHIBITED* = 20

##  Reserved				21..23

const
  EXCCAUSE_DTLB_MISS* = 24
  EXCCAUSE_DTLB_MULTIHIT* = 25
  EXCCAUSE_LOAD_STORE_RING* = 26

##  Reserved				27
##  Size Restriction on Load/Store (not implemented)

const
  EXCCAUSE_LOAD_PROHIBITED* = 28
  EXCCAUSE_STORE_PROHIBITED* = 29

##  Reserved				30..31

template EXCCAUSE_CP_DISABLED*(n: untyped): untyped =
  (32 + (n))                    ##  Access to Coprocessor 'n' when disabled
  
const
  EXCCAUSE_CP0_DISABLED* = 32
  EXCCAUSE_CP1_DISABLED* = 33
  EXCCAUSE_CP2_DISABLED* = 34
  EXCCAUSE_CP3_DISABLED* = 35
  EXCCAUSE_CP4_DISABLED* = 36
  EXCCAUSE_CP5_DISABLED* = 37
  EXCCAUSE_CP6_DISABLED* = 38
  EXCCAUSE_CP7_DISABLED* = 39

## #define EXCCAUSE_FLOATING_POINT	40
##  Floating Point Exception (not implemented)
##  Reserved				40..63
##   PS register fields:

const
  PS_WOE_SHIFT* = 18
  PS_WOE_MASK* = 0x00040000
  PS_WOE* = PS_WOE_MASK
  PS_CALLINC_SHIFT* = 16
  PS_CALLINC_MASK* = 0x00030000

template PS_CALLINC*(n: untyped): untyped =
  (((n) and 3) shl PS_CALLINC_SHIFT) ##  n = 0..3
  
const
  PS_OWB_SHIFT* = 8
  PS_OWB_MASK* = 0x00000F00

template PS_OWB*(n: untyped): untyped =
  (((n) and 15) shl PS_OWB_SHIFT) ##  n = 0..15 (or 0..7)
  
const
  PS_RING_SHIFT* = 6
  PS_RING_MASK* = 0x000000C0

template PS_RING*(n: untyped): untyped =
  (((n) and 3) shl PS_RING_SHIFT) ##  n = 0..3
  
const
  PS_UM_SHIFT* = 5
  PS_UM_MASK* = 0x00000020
  PS_UM* = PS_UM_MASK
  PS_EXCM_SHIFT* = 4
  PS_EXCM_MASK* = 0x00000010
  PS_EXCM* = PS_EXCM_MASK
  PS_INTLEVEL_SHIFT* = 0
  PS_INTLEVEL_MASK* = 0x0000000F

template PS_INTLEVEL*(n: untyped): untyped =
  ((n) and PS_INTLEVEL_MASK)    ##  n = 0..15
  
##   Backward compatibility (deprecated):

const
  PS_PROGSTACK_SHIFT* = PS_UM_SHIFT
  PS_PROGSTACK_MASK* = PS_UM_MASK
  PS_PROG_SHIFT* = PS_UM_SHIFT
  PS_PROG_MASK* = PS_UM_MASK
  PS_PROG* = PS_UM

##   DBREAKCn register fields:

const
  DBREAKC_MASK_SHIFT* = 0
  DBREAKC_MASK_MASK* = 0x0000003F
  DBREAKC_LOADBREAK_SHIFT* = 30
  DBREAKC_LOADBREAK_MASK* = 0x40000000
  DBREAKC_STOREBREAK_SHIFT* = 31
  DBREAKC_STOREBREAK_MASK* = 0x80000000

##   DEBUGCAUSE register fields:

const
  DEBUGCAUSE_DEBUGINT_SHIFT* = 5
  DEBUGCAUSE_DEBUGINT_MASK* = 0x00000020
  DEBUGCAUSE_BREAKN_SHIFT* = 4
  DEBUGCAUSE_BREAKN_MASK* = 0x00000010
  DEBUGCAUSE_BREAK_SHIFT* = 3
  DEBUGCAUSE_BREAK_MASK* = 0x00000008
  DEBUGCAUSE_DBREAK_SHIFT* = 2
  DEBUGCAUSE_DBREAK_MASK* = 0x00000004
  DEBUGCAUSE_IBREAK_SHIFT* = 1
  DEBUGCAUSE_IBREAK_MASK* = 0x00000002
  DEBUGCAUSE_ICOUNT_SHIFT* = 0
  DEBUGCAUSE_ICOUNT_MASK* = 0x00000001

##   MESR register fields:

const
  MESR_MEME* = 0x00000001
  MESR_MEME_SHIFT* = 0
  MESR_DME* = 0x00000002
  MESR_DME_SHIFT* = 1
  MESR_RCE* = 0x00000010
  MESR_RCE_SHIFT* = 4
  MESR_LCE* = true
  MESR_LCE_L* = true
  MESR_ERRENAB* = 0x00000100
  MESR_ERRENAB_SHIFT* = 8
  MESR_ERRTEST* = 0x00000200
  MESR_ERRTEST_SHIFT* = 9
  MESR_DATEXC* = 0x00000400
  MESR_DATEXC_SHIFT* = 10
  MESR_INSEXC* = 0x00000800
  MESR_INSEXC_SHIFT* = 11
  MESR_WAYNUM_SHIFT* = 16
  MESR_ACCTYPE_SHIFT* = 20
  MESR_MEMTYPE_SHIFT* = 24
  MESR_ERRTYPE_SHIFT* = 30
