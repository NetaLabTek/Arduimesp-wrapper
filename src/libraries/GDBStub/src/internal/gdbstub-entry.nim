proc gdbstub_init_debug_entry*() {.cdecl, importcpp: "gdbstub_init_debug_entry(@)",
                                 header: "gdbstub-entry.h".}
proc gdbstub_do_break*() {.cdecl, importcpp: "gdbstub_do_break(@)",
                         header: "gdbstub-entry.h".}
proc gdbstub_icount_ena_single_step*() {.cdecl, importcpp: "gdbstub_icount_ena_single_step(@)",
                                       header: "gdbstub-entry.h".}
proc gdbstub_save_extra_sfrs_for_exception*() {.cdecl,
    importcpp: "gdbstub_save_extra_sfrs_for_exception(@)",
    header: "gdbstub-entry.h".}
proc gdbstub_uart_entry*() {.cdecl, importcpp: "gdbstub_uart_entry(@)",
                           header: "gdbstub-entry.h".}
proc gdbstub_set_hw_breakpoint*(`addr`: cint; len: cint): cint {.cdecl,
    importcpp: "gdbstub_set_hw_breakpoint(@)", header: "gdbstub-entry.h".}
proc gdbstub_set_hw_watchpoint*(`addr`: cint; len: cint; `type`: cint): cint {.cdecl,
    importcpp: "gdbstub_set_hw_watchpoint(@)", header: "gdbstub-entry.h".}
proc gdbstub_del_hw_breakpoint*(`addr`: cint): cint {.cdecl,
    importcpp: "gdbstub_del_hw_breakpoint(@)", header: "gdbstub-entry.h".}
proc gdbstub_del_hw_watchpoint*(`addr`: cint): cint {.cdecl,
    importcpp: "gdbstub_del_hw_watchpoint(@)", header: "gdbstub-entry.h".}
var gdbstub_do_break_breakpoint_addr* {.importcpp: "gdbstub_do_break_breakpoint_addr",
                                      header: "gdbstub-entry.h".}: pointer
