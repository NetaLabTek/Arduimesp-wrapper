## *
##  @file sha1.h
##  @date 20.05.2015
##  @author Steve Reid <steve@edmweb.com>
## 
##  from: http://www.virtualbox.org/svn/vbox/trunk/src/recompiler/tests/sha1.c
## 
##  ================ sha1.h ================
## 
##     SHA-1 in C
##     By Steve Reid <steve@edmweb.com>
##     100% Public Domain
## 

type
  SHA1_CTX* {.importcpp: "SHA1_CTX", header: "sha1.h", bycopy.} = object
    state* {.importc: "state".}: array[5, uint32_t]
    count* {.importc: "count".}: array[2, uint32_t]
    buffer* {.importc: "buffer".}: array[64, cuchar]


proc SHA1Transform*(state: array[5, uint32_t]; buffer: array[64, uint8_t]) {.cdecl,
    importcpp: "SHA1Transform(@)", header: "sha1.h".}
proc SHA1Init*(context: ptr SHA1_CTX) {.cdecl, importcpp: "SHA1Init(@)",
                                    header: "sha1.h".}
proc SHA1Update*(context: ptr SHA1_CTX; data: ptr uint8_t; len: uint32_t) {.cdecl,
    importcpp: "SHA1Update(@)", header: "sha1.h".}
proc SHA1Final*(digest: array[20, cuchar]; context: ptr SHA1_CTX) {.cdecl,
    importcpp: "SHA1Final(@)", header: "sha1.h".}
##  ================ end of sha1.h ================
