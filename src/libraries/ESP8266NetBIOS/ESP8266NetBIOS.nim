## 

import
  lwip/init

const
  NBNS_PORT* = 137

## *
##  @def NBNS_MAX_HOSTNAME_LEN
##  @brief maximalni delka NBNS jmena zarizeni
##  @remarks
##  Jmeno zarizeni musi byt uvedeno VELKYMI pismenami a nesmi obsahovat mezery (whitespaces).
## 

const
  NBNS_MAX_HOSTNAME_LEN* = 16

discard "forward decl of udp_pcb"
discard "forward decl of pbuf"
type
  ESP8266NetBIOS* {.importcpp: "ESP8266NetBIOS", header: "ESP8266NetBIOS.h", bycopy.} = object
  

proc constructESP8266NetBIOS*(): ESP8266NetBIOS {.cdecl, constructor,
    importcpp: "ESP8266NetBIOS(@)", header: "ESP8266NetBIOS.h".}
proc destroyESP8266NetBIOS*(this: var ESP8266NetBIOS) {.cdecl,
    importcpp: "#.~ESP8266NetBIOS()", header: "ESP8266NetBIOS.h".}
proc begin*(this: var ESP8266NetBIOS; name: cstring): bool {.cdecl, importcpp: "begin",
    header: "ESP8266NetBIOS.h".}
proc `end`*(this: var ESP8266NetBIOS) {.cdecl, importcpp: "end",
                                    header: "ESP8266NetBIOS.h".}
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_NETBIOS):
  var NBNS* {.importcpp: "NBNS", header: "ESP8266NetBIOS.h".}: ESP8266NetBIOS