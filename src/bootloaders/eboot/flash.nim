##  Copyright (c) 2015 Ivan Grokhotkov. All rights reserved. 
##  This file is part of eboot bootloader.
## 
##  Redistribution and use is permitted according to the conditions of the
##  3-clause BSD license to be found in the LICENSE file.
## 

proc SPIEraseBlock*(`block`: uint32_t): cint {.cdecl, importcpp: "SPIEraseBlock(@)",
    header: "flash.h".}
proc SPIEraseSector*(sector: uint32_t): cint {.cdecl, importcpp: "SPIEraseSector(@)",
    header: "flash.h".}
proc SPIRead*(`addr`: uint32_t; dest: pointer; size: csize): cint {.cdecl,
    importcpp: "SPIRead(@)", header: "flash.h".}
proc SPIWrite*(`addr`: uint32_t; src: pointer; size: csize): cint {.cdecl,
    importcpp: "SPIWrite(@)", header: "flash.h".}
proc SPIEraseAreaEx*(start: uint32_t; size: uint32_t): cint {.cdecl,
    importcpp: "SPIEraseAreaEx(@)", header: "flash.h".}
const
  FLASH_SECTOR_SIZE* = 0x00001000
  FLASH_BLOCK_SIZE* = 0x00010000
  APP_START_OFFSET* = 0x00001000

type
  image_header_t* {.importcpp: "image_header_t", header: "flash.h", bycopy.} = object
    magic* {.importc: "magic".}: cuchar
    num_segments* {.importc: "num_segments".}: cuchar ##  SPI Flash Interface (0 = QIO, 1 = QOUT, 2 = DIO, 0x3 = DOUT)
    flash_mode* {.importc: "flash_mode".}: cuchar ##  High four bits: 0 = 512K, 1 = 256K, 2 = 1M, 3 = 2M, 4 = 4M, 
                                              ##        Low four bits:  0 = 40MHz, 1= 26MHz, 2 = 20MHz, 0xf = 80MHz
    flash_size_freq* {.importc: "flash_size_freq".}: cuchar
    entry* {.importc: "entry".}: uint32_t

  section_header_t* {.importcpp: "section_header_t", header: "flash.h", bycopy.} = object
    address* {.importc: "address".}: uint32_t
    size* {.importc: "size".}: uint32_t

