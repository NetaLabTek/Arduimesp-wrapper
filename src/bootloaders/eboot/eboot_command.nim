##  Copyright (c) 2015 Ivan Grokhotkov. All rights reserved. 
##  This file is part of eboot bootloader.
## 
##  Redistribution and use is permitted according to the conditions of the
##  3-clause BSD license to be found in the LICENSE file.
## 

const
  RTC_MEM* = (cast[ptr uint32_t](0x60001200))

type
  action_t* {.size: sizeof(cint), importcpp: "action_t", header: "eboot_command.h".} = enum
    ACTION_COPY_RAW = 0x00000001, ACTION_LOAD_APP = 0xFFFFFFFF


const
  EBOOT_MAGIC* = 0xEB001000
  EBOOT_MAGIC_MASK* = 0xFFFFF000

type
  eboot_command* {.importcpp: "eboot_command", header: "eboot_command.h", bycopy.} = object
    magic* {.importc: "magic".}: uint32_t
    args* {.importc: "args".}: array[29, uint32_t]
    crc32* {.importc: "crc32".}: uint32_t


var action* {.importcpp: "action", header: "eboot_command.h".}: action_t

proc eboot_command_read*(cmd: ptr eboot_command): cint {.cdecl,
    importcpp: "eboot_command_read(@)", header: "eboot_command.h".}
proc eboot_command_write*(cmd: ptr eboot_command) {.cdecl,
    importcpp: "eboot_command_write(@)", header: "eboot_command.h".}
proc eboot_command_clear*() {.cdecl, importcpp: "eboot_command_clear(@)",
                            header: "eboot_command.h".}