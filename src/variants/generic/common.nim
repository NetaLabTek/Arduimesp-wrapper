## 
##   common.h - Commoon pin definition functions for ESP8266 boards
##   Part of Arduino - http://www.arduino.cc/
## 
##   Copyright (c) 2007 David A. Mellis
##   Modified for ESP8266 platform by Ivan Grokhotkov, 2014-2016.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General
##   Public License along with this library; if not, write to the
##   Free Software Foundation, Inc., 59 Temple Place, Suite 330,
##   Boston, MA  02111-1307  USA
## 
##   $Id: wiring.h 249 2007-02-03 16:52:51Z mellis $
## 

const
  EXTERNAL_NUM_INTERRUPTS* = 16
  NUM_DIGITAL_PINS* = 17
  NUM_ANALOG_INPUTS* = 1

##  TODO: this should be <= 9 if flash is in DIO mode

template isFlashInterfacePin*(p: untyped): untyped =
  ((p) >= 6 and (p) <= 11)

template analogInputToDigitalPin*(p: untyped): untyped =
  (if (p > 0): NOT_A_PIN else: 0)

template digitalPinToInterrupt*(p: untyped): untyped =
  (if ((p) < EXTERNAL_NUM_INTERRUPTS): (p) else: NOT_AN_INTERRUPT)

template digitalPinHasPWM*(p: untyped): untyped =
  (if ((p) < NUM_DIGITAL_PINS and not isFlashInterfacePin(p)): 1 else: 0)

const
  PIN_SPI_SS* = (15)
  PIN_SPI_MOSI* = (13)
  PIN_SPI_MISO* = (12)
  PIN_SPI_SCK* = (14)

var SS* {.importcpp: "SS", header: "common.h".}: uint8

var MOSI* {.importcpp: "MOSI", header: "common.h".}: uint8

var MISO* {.importcpp: "MISO", header: "common.h".}: uint8

var SCK* {.importcpp: "SCK", header: "common.h".}: uint8

var A0* {.importcpp: "A0", header: "common.h".}: uint8

##  These serial port names are intended to allow libraries and architecture-neutral
##  sketches to automatically default to the correct port name for a particular type
##  of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
##  the first hardware serial port whose RX/TX pins are not dedicated to another use.
## 
##  SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
## 
##  SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
## 
##  SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
## 
##  SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
## 
##  SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
##                             pins are NOT connected to anything by default.

#const
#  SERIAL_PORT_MONITOR* = Serial
#  SERIAL_PORT_USBVIRTUAL* = Serial
#  SERIAL_PORT_HARDWARE* = Serial
#  SERIAL_PORT_HARDWARE_OPEN* = Serial1

when defined(LED_BUILTIN):
  var BUILTIN_LED* {.importcpp: "BUILTIN_LED", header: "common.h".}: cint
