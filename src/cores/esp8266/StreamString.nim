## *
##  StreamString.h
## 
##  Copyright (c) 2015 Markus Sattler. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
## 

type
  StreamString* {.importcpp: "StreamString", header: "StreamString.h", bycopy.} = object of Stream
  

proc write*(this: var StreamString; buffer: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "StreamString.h".}
proc write*(this: var StreamString; data: uint8_t): csize {.cdecl, importcpp: "write",
    header: "StreamString.h".}
proc available*(this: var StreamString): cint {.cdecl, importcpp: "available",
    header: "StreamString.h".}
proc read*(this: var StreamString): cint {.cdecl, importcpp: "read",
                                      header: "StreamString.h".}
proc peek*(this: var StreamString): cint {.cdecl, importcpp: "peek",
                                      header: "StreamString.h".}
proc flush*(this: var StreamString) {.cdecl, importcpp: "flush",
                                  header: "StreamString.h".}