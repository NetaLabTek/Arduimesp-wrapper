##  
##   twi.h - Software I2C library for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  Arduino

const
  I2C_OK* = 0
  I2C_SCL_HELD_LOW* = 1
  I2C_SCL_HELD_LOW_AFTER_READ* = 2
  I2C_SDA_HELD_LOW* = 3
  I2C_SDA_HELD_LOW_AFTER_INIT* = 4

proc twi_init*(sda: cuchar; scl: cuchar) {.cdecl, importcpp: "twi_init(@)",
                                      header: "twi.h".}
proc twi_stop*() {.cdecl, importcpp: "twi_stop(@)", header: "twi.h".}
proc twi_setClock*(freq: cuint) {.cdecl, importcpp: "twi_setClock(@)", header: "twi.h".}
proc twi_setClockStretchLimit*(limit: uint32) {.cdecl,
    importcpp: "twi_setClockStretchLimit(@)", header: "twi.h".}
proc twi_writeTo*(address: cuchar; buf: ptr cuchar; len: cuint; sendStop: cuchar): uint8 {.
    cdecl, importcpp: "twi_writeTo(@)", header: "twi.h".}
proc twi_readFrom*(address: cuchar; buf: ptr cuchar; len: cuint; sendStop: cuchar): uint8 {.
    cdecl, importcpp: "twi_readFrom(@)", header: "twi.h".}
proc twi_status*(): uint8 {.cdecl, importcpp: "twi_status(@)", header: "twi.h".}
