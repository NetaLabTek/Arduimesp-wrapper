when defined(DEBUG_ESP_CORE):
when not defined(DEBUGV):
proc __panic_func*(file: cstring; line: cint; `func`: cstring) {.cdecl,
    importcpp: "__panic_func(@)", header: "debug.h".}
template panic*(): untyped =
  __panic_func(__FILE__, __LINE__, __func__)
