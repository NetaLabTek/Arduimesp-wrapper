## 
##  cont.h - continuations support for Xtensa call0 ABI
##  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  cont_t* {.importcpp: "cont_t", header: "cont.h", bycopy.} = object
    pc_ret* {.importc: "pc_ret".}: proc () {.cdecl.}
    sp_ret* {.importc: "sp_ret".}: ptr cuint
    pc_yield* {.importc: "pc_yield".}: proc () {.cdecl.}
    sp_yield* {.importc: "sp_yield".}: ptr cuint
    stack_end* {.importc: "stack_end".}: ptr cuint
    unused1* {.importc: "unused1".}: cuint
    unused2* {.importc: "unused2".}: cuint
    stack_guard1* {.importc: "stack_guard1".}: cuint
    stack* {.importc: "stack".}: array[CONT_STACKSIZE div 4, cuint]
    stack_guard2* {.importc: "stack_guard2".}: cuint
    struct_start* {.importc: "struct_start".}: ptr cuint


##  Initialize the cont_t structure before calling cont_run

proc cont_init*(a2: ptr cont_t) {.cdecl, importcpp: "cont_init(@)", header: "cont.h".}
##  Run function pfn in a separate stack, or continue execution
##  at the point where cont_yield was called

proc cont_run*(a2: ptr cont_t; pfn: proc () {.cdecl.}) {.cdecl, importcpp: "cont_run(@)",
    header: "cont.h".}
##  Return to the point where cont_run was called, saving the
##  execution state (registers and stack)

proc cont_yield*(a2: ptr cont_t) {.cdecl, importcpp: "cont_yield(@)", header: "cont.h".}
##  Check guard bytes around the stack. Return 0 in case everything is ok,
##  return 1 if guard bytes were overwritten.

proc cont_check*(cont: ptr cont_t): cint {.cdecl, importcpp: "cont_check(@)",
                                      header: "cont.h".}
##  Go through stack and check how many bytes are most probably still unchanged
##  and thus weren't used by the user code. i.e. that stack space is free. (high water mark)

proc cont_get_free_stack*(cont: ptr cont_t): cint {.cdecl,
    importcpp: "cont_get_free_stack(@)", header: "cont.h".}
##  Check if yield() may be called. Returns true if we are running inside
##  continuation stack

proc cont_can_yield*(cont: ptr cont_t): bool {.cdecl, importcpp: "cont_can_yield(@)",
    header: "cont.h".}