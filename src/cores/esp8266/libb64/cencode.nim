## 
## cencode.h - c header for a base64 encoding algorithm
## 
## This is part of the libb64 project, and has been placed in the public domain.
## For details, see http://sourceforge.net/projects/libb64
## 

const
  BASE64_CHARS_PER_LINE* = 72

template base64_encode_expected_len_nonewlines*(n: untyped): untyped =
  ((((4 * (n)) div 3) + 3) and not 3)

template base64_encode_expected_len*(n: untyped): untyped =
  (base64_encode_expected_len_nonewlines(n) +
      ((n div ((BASE64_CHARS_PER_LINE * 3) div 4)) + 1))

type
  base64_encodestep* {.size: sizeof(cint), importcpp: "base64_encodestep",
                      header: "cencode.h".} = enum
    step_A, step_B, step_C
  base64_encodestate* {.importcpp: "base64_encodestate", header: "cencode.h", bycopy.} = object
    step* {.importc: "step".}: base64_encodestep
    result* {.importc: "result".}: char
    stepcount* {.importc: "stepcount".}: cint
    stepsnewline* {.importc: "stepsnewline".}: cint



proc base64_init_encodestate*(state_in: ptr base64_encodestate) {.cdecl,
    importcpp: "base64_init_encodestate(@)", header: "cencode.h".}
proc base64_init_encodestate_nonewlines*(state_in: ptr base64_encodestate) {.cdecl,
    importcpp: "base64_init_encodestate_nonewlines(@)", header: "cencode.h".}
proc base64_encode_value*(value_in: char): char {.cdecl,
    importcpp: "base64_encode_value(@)", header: "cencode.h".}
proc base64_encode_block*(plaintext_in: cstring; length_in: cint; code_out: cstring;
                         state_in: ptr base64_encodestate): cint {.cdecl,
    importcpp: "base64_encode_block(@)", header: "cencode.h".}
proc base64_encode_blockend*(code_out: cstring; state_in: ptr base64_encodestate): cint {.
    cdecl, importcpp: "base64_encode_blockend(@)", header: "cencode.h".}
proc base64_encode_chars*(plaintext_in: cstring; length_in: cint; code_out: cstring): cint {.
    cdecl, importcpp: "base64_encode_chars(@)", header: "cencode.h".}