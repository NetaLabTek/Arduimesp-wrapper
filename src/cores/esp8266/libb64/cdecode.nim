## 
## cdecode.h - c header for a base64 decoding algorithm
## 
## This is part of the libb64 project, and has been placed in the public domain.
## For details, see http://sourceforge.net/projects/libb64
## 

template base64_decode_expected_len*(n: untyped): untyped =
  ((n * 3) div 4)

type
  base64_decodestep* {.size: sizeof(cint), importcpp: "base64_decodestep",
                      header: "cdecode.h".} = enum
    step_a, step_b, step_c, step_d
  base64_decodestate* {.importcpp: "base64_decodestate", header: "cdecode.h", bycopy.} = object
    step* {.importc: "step".}: base64_decodestep
    plainchar* {.importc: "plainchar".}: char



proc base64_init_decodestate*(state_in: ptr base64_decodestate) {.cdecl,
    importcpp: "base64_init_decodestate(@)", header: "cdecode.h".}
proc base64_decode_value*(value_in: char): cint {.cdecl,
    importcpp: "base64_decode_value(@)", header: "cdecode.h".}
proc base64_decode_block*(code_in: cstring; length_in: cint; plaintext_out: cstring;
                         state_in: ptr base64_decodestate): cint {.cdecl,
    importcpp: "base64_decode_block(@)", header: "cdecode.h".}
proc base64_decode_chars*(code_in: cstring; length_in: cint; plaintext_out: cstring): cint {.
    cdecl, importcpp: "base64_decode_chars(@)", header: "cdecode.h".}