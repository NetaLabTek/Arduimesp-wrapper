const
  SCHEDULED_FN_MAX_COUNT* = 32
  SCHEDULED_FN_INITIAL_COUNT* = 4

##  Warning 
##  This API is not considered stable. 
##  Function signatures will change.
##  You have been warned.
##  Run given function next time `loop` function returns, 
##  or `run_scheduled_functions` is called.
##  Use std::bind to pass arguments to a function, or call a class member function.
##  Note: there is no mechanism for cancelling scheduled functions.
##  Keep that in mind when binding functions to objects which may have short lifetime.
##  Returns false if the number of scheduled functions exceeds SCHEDULED_FN_MAX_COUNT.

proc schedule_function*(fn: function[nil]): bool {.cdecl,
    importcpp: "schedule_function(@)", header: "Schedule.h".}
##  Run all scheduled functions. 
##  Use this function if your are not using `loop`, or `loop` does not return
##  on a regular basis.

proc run_scheduled_functions*() {.cdecl, importcpp: "run_scheduled_functions(@)",
                                header: "Schedule.h".}