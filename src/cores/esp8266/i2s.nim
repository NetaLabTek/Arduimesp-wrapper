##  
##   i2s.h - Software I2S library for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

## 
## How does this work? Basically, to get sound, you need to:
## - Connect an I2S codec to the I2S pins on the ESP.
## - Start up a thread that's going to do the sound output
## - Call i2s_begin()
## - Call i2s_set_rate() with the sample rate you want.
## - Generate sound and call i2s_write_sample() with 32-bit samples.
## The 32bit samples basically are 2 16-bit signed values (the analog values for
## the left and right channel) concatenated as (Rout<<16)+Lout
## 
## i2s_write_sample will block when you're sending data too quickly, so you can just
## generate and push data as fast as you can and i2s_write_sample will regulate the
## speed.
## 

proc i2s_begin*() {.cdecl, importcpp: "i2s_begin(@)", header: "i2s.h".}
##  Enable TX only, for compatibility

proc i2s_rxtx_begin*(enableRx: bool; enableTx: bool): bool {.cdecl,
    importcpp: "i2s_rxtx_begin(@)", header: "i2s.h".}
##  Allow TX and/or RX, returns false on OOM error

proc i2s_end*() {.cdecl, importcpp: "i2s_end(@)", header: "i2s.h".}
proc i2s_set_rate*(rate: uint32_t) {.cdecl, importcpp: "i2s_set_rate(@)",
                                  header: "i2s.h".}
## Sample Rate in Hz (ex 44100, 48000)

proc i2s_set_dividers*(div1: uint8_t; div2: uint8_t) {.cdecl,
    importcpp: "i2s_set_dividers(@)", header: "i2s.h".}
## Direct control over output rate

proc i2s_get_real_rate*(): cfloat {.cdecl, importcpp: "i2s_get_real_rate(@)",
                                 header: "i2s.h".}
## The actual Sample Rate on output

proc i2s_write_sample*(sample: uint32_t): bool {.cdecl,
    importcpp: "i2s_write_sample(@)", header: "i2s.h".}
## 32bit sample with channels being upper and lower 16 bits (blocking when DMA is full)

proc i2s_write_sample_nb*(sample: uint32_t): bool {.cdecl,
    importcpp: "i2s_write_sample_nb(@)", header: "i2s.h".}
## same as above but does not block when DMA is full and returns false instead

proc i2s_write_lr*(left: int16_t; right: int16_t): bool {.cdecl,
    importcpp: "i2s_write_lr(@)", header: "i2s.h".}
## combines both channels and calls i2s_write_sample with the result

proc i2s_read_sample*(left: ptr int16_t; right: ptr int16_t; blocking: bool): bool {.
    cdecl, importcpp: "i2s_read_sample(@)", header: "i2s.h".}
##  RX data returned in both 16-bit outputs.

proc i2s_is_full*(): bool {.cdecl, importcpp: "i2s_is_full(@)", header: "i2s.h".}
## returns true if DMA is full and can not take more bytes (overflow)

proc i2s_is_empty*(): bool {.cdecl, importcpp: "i2s_is_empty(@)", header: "i2s.h".}
## returns true if DMA is empty (underflow)

proc i2s_rx_is_full*(): bool {.cdecl, importcpp: "i2s_rx_is_full(@)", header: "i2s.h".}
proc i2s_rx_is_empty*(): bool {.cdecl, importcpp: "i2s_rx_is_empty(@)",
                             header: "i2s.h".}
proc i2s_available*(): int16_t {.cdecl, importcpp: "i2s_available(@)", header: "i2s.h".}
##  returns the number of samples than can be written before blocking

proc i2s_rx_available*(): int16_t {.cdecl, importcpp: "i2s_rx_available(@)",
                                 header: "i2s.h".}
##  returns the number of samples than can be written before blocking

proc i2s_set_callback*(callback: proc () {.cdecl.}) {.cdecl,
    importcpp: "i2s_set_callback(@)", header: "i2s.h".}
proc i2s_rx_set_callback*(callback: proc () {.cdecl.}) {.cdecl,
    importcpp: "i2s_rx_set_callback(@)", header: "i2s.h".}