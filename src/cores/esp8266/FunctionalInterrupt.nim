import
  c_types, ets_sys

proc attachInterrupt*(pin: uint8_t; intRoutine: function[nil]; mode: cint) {.cdecl,
    importcpp: "attachInterrupt(@)", header: "FunctionalInterrupt.h".}