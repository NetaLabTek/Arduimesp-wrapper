## 
##  FS.h - file system wrapper
##  Copyright (c) 2015 Ivan Grokhotkov. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

discard "forward decl of File"
discard "forward decl of Dir"
discard "forward decl of FileImpl"
type
  FileImplPtr* = shared_ptr[FileImpl]

discard "forward decl of FSImpl"
type
  FSImplPtr* = shared_ptr[FSImpl]

discard "forward decl of DirImpl"
type
  DirImplPtr* = shared_ptr[DirImpl]

proc mount*[Tfs](fs: var Tfs; mountPoint: cstring): bool {.cdecl,
    importcpp: "fs::mount(@)", header: "FS.h".}
type
  SeekMode* {.size: sizeof(cint), importcpp: "fs::SeekMode", header: "FS.h".} = enum
    SeekSet = 0, SeekCur = 1, SeekEnd = 2


type
  File* {.importcpp: "fs::File", header: "FS.h", bycopy.} = object of Stream
  

proc constructFile*(p: FileImplPtr = FileImplPtr()): File {.cdecl, constructor,
    importcpp: "fs::File(@)", header: "FS.h".}
proc write*(this: var File; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "FS.h".}
proc write*(this: var File; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "FS.h".}
proc available*(this: var File): cint {.cdecl, importcpp: "available", header: "FS.h".}
proc read*(this: var File): cint {.cdecl, importcpp: "read", header: "FS.h".}
proc peek*(this: var File): cint {.cdecl, importcpp: "peek", header: "FS.h".}
proc flush*(this: var File) {.cdecl, importcpp: "flush", header: "FS.h".}
proc readBytes*(this: var File; buffer: cstring; length: csize): csize {.cdecl,
    importcpp: "readBytes", header: "FS.h".}
proc read*(this: var File; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "read", header: "FS.h".}
proc seek*(this: var File; pos: uint32_t; mode: SeekMode): bool {.cdecl,
    importcpp: "seek", header: "FS.h".}
proc seek*(this: var File; pos: uint32_t): bool {.cdecl, importcpp: "seek", header: "FS.h".}
proc position*(this: File): csize {.noSideEffect, cdecl, importcpp: "position",
                                header: "FS.h".}
proc size*(this: File): csize {.noSideEffect, cdecl, importcpp: "size", header: "FS.h".}
proc close*(this: var File) {.cdecl, importcpp: "close", header: "FS.h".}
converter `bool`*(this: File): bool {.noSideEffect, cdecl,
                                  importcpp: "File::operator bool", header: "FS.h".}
proc name*(this: File): cstring {.noSideEffect, cdecl, importcpp: "name", header: "FS.h".}
type
  Dir* {.importcpp: "fs::Dir", header: "FS.h", bycopy.} = object
  

proc constructDir*(impl: DirImplPtr = DirImplPtr()): Dir {.cdecl, constructor,
    importcpp: "fs::Dir(@)", header: "FS.h".}
proc openFile*(this: var Dir; mode: cstring): File {.cdecl, importcpp: "openFile",
    header: "FS.h".}
proc fileName*(this: var Dir): String {.cdecl, importcpp: "fileName", header: "FS.h".}
proc fileSize*(this: var Dir): csize {.cdecl, importcpp: "fileSize", header: "FS.h".}
proc next*(this: var Dir): bool {.cdecl, importcpp: "next", header: "FS.h".}
type
  FSInfo* {.importcpp: "fs::FSInfo", header: "FS.h", bycopy.} = object
    totalBytes* {.importc: "totalBytes".}: csize
    usedBytes* {.importc: "usedBytes".}: csize
    blockSize* {.importc: "blockSize".}: csize
    pageSize* {.importc: "pageSize".}: csize
    maxOpenFiles* {.importc: "maxOpenFiles".}: csize
    maxPathLength* {.importc: "maxPathLength".}: csize

  FS* {.importcpp: "fs::FS", header: "FS.h", bycopy.} = object
  

proc constructFS*(impl: FSImplPtr): FS {.cdecl, constructor, importcpp: "fs::FS(@)",
                                     header: "FS.h".}
proc begin*(this: var FS): bool {.cdecl, importcpp: "begin", header: "FS.h".}
proc `end`*(this: var FS) {.cdecl, importcpp: "end", header: "FS.h".}
proc format*(this: var FS): bool {.cdecl, importcpp: "format", header: "FS.h".}
proc info*(this: var FS; info: var FSInfo): bool {.cdecl, importcpp: "info", header: "FS.h".}
proc open*(this: var FS; path: cstring; mode: cstring): File {.cdecl, importcpp: "open",
    header: "FS.h".}
proc open*(this: var FS; path: String; mode: cstring): File {.cdecl, importcpp: "open",
    header: "FS.h".}
proc exists*(this: var FS; path: cstring): bool {.cdecl, importcpp: "exists",
    header: "FS.h".}
proc exists*(this: var FS; path: String): bool {.cdecl, importcpp: "exists",
    header: "FS.h".}
proc openDir*(this: var FS; path: cstring): Dir {.cdecl, importcpp: "openDir",
    header: "FS.h".}
proc openDir*(this: var FS; path: String): Dir {.cdecl, importcpp: "openDir",
    header: "FS.h".}
proc remove*(this: var FS; path: cstring): bool {.cdecl, importcpp: "remove",
    header: "FS.h".}
proc remove*(this: var FS; path: String): bool {.cdecl, importcpp: "remove",
    header: "FS.h".}
proc rename*(this: var FS; pathFrom: cstring; pathTo: cstring): bool {.cdecl,
    importcpp: "rename", header: "FS.h".}
proc rename*(this: var FS; pathFrom: String; pathTo: String): bool {.cdecl,
    importcpp: "rename", header: "FS.h".}
##  namespace fs

when not defined(FS_NO_GLOBALS):
  nil
  nil
  nil
  nil
  nil
  nil
  nil
  nil
when not defined(NO_GLOBAL_INSTANCES) and not defined(NO_GLOBAL_SPIFFS):
  var SPIFFS* {.importcpp: "SPIFFS", header: "FS.h".}: FS