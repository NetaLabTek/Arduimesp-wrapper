##  
##  cbuf.h - Circular buffer implementation
##  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
##  
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  cbuf* {.importcpp: "cbuf", header: "cbuf.h", bycopy.} = object
    next* {.importc: "next".}: ptr cbuf
  

proc constructcbuf*(size: csize): cbuf {.cdecl, constructor, importcpp: "cbuf(@)",
                                     header: "cbuf.h".}
proc destroycbuf*(this: var cbuf) {.cdecl, importcpp: "#.~cbuf()", header: "cbuf.h".}
proc resizeAdd*(this: var cbuf; addSize: csize): csize {.cdecl, importcpp: "resizeAdd",
    header: "cbuf.h".}
proc resize*(this: var cbuf; newSize: csize): csize {.cdecl, importcpp: "resize",
    header: "cbuf.h".}
proc available*(this: cbuf): csize {.noSideEffect, cdecl, importcpp: "available",
                                 header: "cbuf.h".}
proc size*(this: var cbuf): csize {.cdecl, importcpp: "size", header: "cbuf.h".}
proc room*(this: cbuf): csize {.noSideEffect, cdecl, importcpp: "room", header: "cbuf.h".}
proc empty*(this: cbuf): bool {.noSideEffect, cdecl, importcpp: "empty",
                            header: "cbuf.h".}
proc full*(this: cbuf): bool {.noSideEffect, cdecl, importcpp: "full", header: "cbuf.h".}
proc peek*(this: var cbuf): cint {.cdecl, importcpp: "peek", header: "cbuf.h".}
proc peek*(this: var cbuf; dst: cstring; size: csize): csize {.cdecl, importcpp: "peek",
    header: "cbuf.h".}
proc read*(this: var cbuf): cint {.cdecl, importcpp: "read", header: "cbuf.h".}
proc read*(this: var cbuf; dst: cstring; size: csize): csize {.cdecl, importcpp: "read",
    header: "cbuf.h".}
proc write*(this: var cbuf; c: char): csize {.cdecl, importcpp: "write", header: "cbuf.h".}
proc write*(this: var cbuf; src: cstring; size: csize): csize {.cdecl, importcpp: "write",
    header: "cbuf.h".}
proc flush*(this: var cbuf) {.cdecl, importcpp: "flush", header: "cbuf.h".}
proc remove*(this: var cbuf; size: csize): csize {.cdecl, importcpp: "remove",
    header: "cbuf.h".}