## 
##   md5.h - exposed md5 ROM functions for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
## 
##   original C source from https://github.com/morrissinger/ESP8266-Websocket/raw/master/MD5.h
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  md5_context_t* {.importcpp: "md5_context_t", header: "md5.h", bycopy.} = object
    state* {.importc: "state".}: array[4, uint32_t]
    count* {.importc: "count".}: array[2, uint32_t]
    buffer* {.importc: "buffer".}: array[64, uint8_t]


proc MD5Init*(a2: ptr md5_context_t) {.cdecl, importcpp: "MD5Init(@)", header: "md5.h".}
proc MD5Update*(a2: ptr md5_context_t; a3: ptr uint8_t; a4: uint16_t) {.cdecl,
    importcpp: "MD5Update(@)", header: "md5.h".}
proc MD5Final*(a2: array[16, uint8_t]; a3: ptr md5_context_t) {.cdecl,
    importcpp: "MD5Final(@)", header: "md5.h".}