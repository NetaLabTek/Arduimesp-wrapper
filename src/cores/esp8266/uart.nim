## 
##  uart.h - UART HAL
## 
##  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
## 

when not defined(ESP_UART_H):
  const
    UART0* = 0
    UART1* = 1
    UART_NO* = -1
  ##  Options for `config` argument of uart_init
  const
    UART_NB_BIT_MASK* = 0b00000000000000000000000000001100
    UART_NB_BIT_5* = 0b00000000000000000000000000000000
    UART_NB_BIT_6* = 0b00000000000000000000000000000100
    UART_NB_BIT_7* = 0b00000000000000000000000000001000
    UART_NB_BIT_8* = 0b00000000000000000000000000001100
    UART_PARITY_MASK* = 0b00000000000000000000000000000011
    UART_PARITY_NONE* = 0b00000000000000000000000000000000
    UART_PARITY_EVEN* = 0b00000000000000000000000000000010
    UART_PARITY_ODD* = 0b00000000000000000000000000000011
    UART_NB_STOP_BIT_MASK* = 0b00000000000000000000000000110000
    UART_NB_STOP_BIT_0* = 0b00000000000000000000000000000000
    UART_NB_STOP_BIT_1* = 0b00000000000000000000000000010000
    UART_NB_STOP_BIT_15* = 0b00000000000000000000000000100000
    UART_NB_STOP_BIT_2* = 0b00000000000000000000000000110000
    UART_5N1* = (UART_NB_BIT_5 or UART_PARITY_NONE or UART_NB_STOP_BIT_1)
    UART_6N1* = (UART_NB_BIT_6 or UART_PARITY_NONE or UART_NB_STOP_BIT_1)
    UART_7N1* = (UART_NB_BIT_7 or UART_PARITY_NONE or UART_NB_STOP_BIT_1)
    UART_8N1* = (UART_NB_BIT_8 or UART_PARITY_NONE or UART_NB_STOP_BIT_1)
    UART_5N2* = (UART_NB_BIT_5 or UART_PARITY_NONE or UART_NB_STOP_BIT_2)
    UART_6N2* = (UART_NB_BIT_6 or UART_PARITY_NONE or UART_NB_STOP_BIT_2)
    UART_7N2* = (UART_NB_BIT_7 or UART_PARITY_NONE or UART_NB_STOP_BIT_2)
    UART_8N2* = (UART_NB_BIT_8 or UART_PARITY_NONE or UART_NB_STOP_BIT_2)
    UART_5E1* = (UART_NB_BIT_5 or UART_PARITY_EVEN or UART_NB_STOP_BIT_1)
    UART_6E1* = (UART_NB_BIT_6 or UART_PARITY_EVEN or UART_NB_STOP_BIT_1)
    UART_7E1* = (UART_NB_BIT_7 or UART_PARITY_EVEN or UART_NB_STOP_BIT_1)
    UART_8E1* = (UART_NB_BIT_8 or UART_PARITY_EVEN or UART_NB_STOP_BIT_1)
    UART_5E2* = (UART_NB_BIT_5 or UART_PARITY_EVEN or UART_NB_STOP_BIT_2)
    UART_6E2* = (UART_NB_BIT_6 or UART_PARITY_EVEN or UART_NB_STOP_BIT_2)
    UART_7E2* = (UART_NB_BIT_7 or UART_PARITY_EVEN or UART_NB_STOP_BIT_2)
    UART_8E2* = (UART_NB_BIT_8 or UART_PARITY_EVEN or UART_NB_STOP_BIT_2)
    UART_5O1* = (UART_NB_BIT_5 or UART_PARITY_ODD or UART_NB_STOP_BIT_1)
    UART_6O1* = (UART_NB_BIT_6 or UART_PARITY_ODD or UART_NB_STOP_BIT_1)
    UART_7O1* = (UART_NB_BIT_7 or UART_PARITY_ODD or UART_NB_STOP_BIT_1)
    UART_8O1* = (UART_NB_BIT_8 or UART_PARITY_ODD or UART_NB_STOP_BIT_1)
    UART_5O2* = (UART_NB_BIT_5 or UART_PARITY_ODD or UART_NB_STOP_BIT_2)
    UART_6O2* = (UART_NB_BIT_6 or UART_PARITY_ODD or UART_NB_STOP_BIT_2)
    UART_7O2* = (UART_NB_BIT_7 or UART_PARITY_ODD or UART_NB_STOP_BIT_2)
    UART_8O2* = (UART_NB_BIT_8 or UART_PARITY_ODD or UART_NB_STOP_BIT_2)


  ## 
  ## #define UART_5N1 0x10
  ## #define UART_6N1 0x14
  ## #define UART_7N1 0x18
  ## #define UART_8N1 0x1c
  ## #define UART_5N2 0x30
  ## #define UART_6N2 0x34
  ## #define UART_7N2 0x38
  ## #define UART_8N2 0x3c
  ## #define UART_5E1 0x12
  ## #define UART_6E1 0x16
  ## #define UART_7E1 0x1a
  ## #define UART_8E1 0x1e
  ## #define UART_5E2 0x32
  ## #define UART_6E2 0x36
  ## #define UART_7E2 0x3a
  ## #define UART_8E2 0x3e
  ## #define UART_5O1 0x13
  ## #define UART_6O1 0x17
  ## #define UART_7O1 0x1b
  ## #define UART_8O1 0x1f
  ## #define UART_5O2 0x33
  ## #define UART_6O2 0x37
  ## #define UART_7O2 0x3b
  ## #define UART_8O2 0x3f
  ## 
  ##  Options for `mode` argument of uart_init
  const
    UART_FULL* = 0
    UART_RX_ONLY* = 1
    UART_TX_ONLY* = 2
    UART_TX_FIFO_SIZE* = 0x00000080
  discard "forward decl of uart_"
  type
    uart_t* = uart_
  proc uart_init*(uart_nr: cint; baudrate: cint; config: cint; mode: cint; tx_pin: cint;
                 rx_size: csize): ptr uart_t {.cdecl, importcpp: "uart_init(@)",
      header: "uart.h".}
  proc uart_uninit*(uart: ptr uart_t) {.cdecl, importcpp: "uart_uninit(@)",
                                    header: "uart.h".}
  proc uart_swap*(uart: ptr uart_t; tx_pin: cint) {.cdecl, importcpp: "uart_swap(@)",
      header: "uart.h".}
  proc uart_set_tx*(uart: ptr uart_t; tx_pin: cint) {.cdecl,
      importcpp: "uart_set_tx(@)", header: "uart.h".}
  proc uart_set_pins*(uart: ptr uart_t; tx: cint; rx: cint) {.cdecl,
      importcpp: "uart_set_pins(@)", header: "uart.h".}
  proc uart_tx_enabled*(uart: ptr uart_t): bool {.cdecl,
      importcpp: "uart_tx_enabled(@)", header: "uart.h".}
  proc uart_rx_enabled*(uart: ptr uart_t): bool {.cdecl,
      importcpp: "uart_rx_enabled(@)", header: "uart.h".}
  proc uart_set_baudrate*(uart: ptr uart_t; baud_rate: cint) {.cdecl,
      importcpp: "uart_set_baudrate(@)", header: "uart.h".}
  proc uart_get_baudrate*(uart: ptr uart_t): cint {.cdecl,
      importcpp: "uart_get_baudrate(@)", header: "uart.h".}
  proc uart_resize_rx_buffer*(uart: ptr uart_t; new_size: csize): csize {.cdecl,
      importcpp: "uart_resize_rx_buffer(@)", header: "uart.h".}
  proc uart_write_char*(uart: ptr uart_t; c: char): csize {.cdecl,
      importcpp: "uart_write_char(@)", header: "uart.h".}
  proc uart_write*(uart: ptr uart_t; buf: cstring; size: csize): csize {.cdecl,
      importcpp: "uart_write(@)", header: "uart.h".}
  proc uart_read_char*(uart: ptr uart_t): cint {.cdecl,
      importcpp: "uart_read_char(@)", header: "uart.h".}
  proc uart_peek_char*(uart: ptr uart_t): cint {.cdecl,
      importcpp: "uart_peek_char(@)", header: "uart.h".}
  proc uart_rx_available*(uart: ptr uart_t): csize {.cdecl,
      importcpp: "uart_rx_available(@)", header: "uart.h".}
  proc uart_tx_free*(uart: ptr uart_t): csize {.cdecl, importcpp: "uart_tx_free(@)",
      header: "uart.h".}
  proc uart_wait_tx_empty*(uart: ptr uart_t) {.cdecl,
      importcpp: "uart_wait_tx_empty(@)", header: "uart.h".}
  proc uart_flush*(uart: ptr uart_t) {.cdecl, importcpp: "uart_flush(@)",
                                   header: "uart.h".}
  proc uart_has_overrun*(uart: ptr uart_t): bool {.cdecl,
      importcpp: "uart_has_overrun(@)", header: "uart.h".}
  ##  returns then clear overrun flag
  proc uart_set_debug*(uart_nr: cint) {.cdecl, importcpp: "uart_set_debug(@)",
                                     header: "uart.h".}
  proc uart_get_debug*(): cint {.cdecl, importcpp: "uart_get_debug(@)",
                              header: "uart.h".}