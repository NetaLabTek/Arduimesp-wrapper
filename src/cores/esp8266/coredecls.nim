##  TODO: put declarations here, get rid of -Wno-implicit-function-declaration

var timeshift64_is_set* {.importcpp: "timeshift64_is_set", header: "coredecls.h".}: bool

proc tune_timeshift64*(now_us: uint64_t) {.cdecl, importcpp: "tune_timeshift64(@)",
                                        header: "coredecls.h".}
proc settimeofday_cb*(cb: proc () {.cdecl.}) {.cdecl, importcpp: "settimeofday_cb(@)",
    header: "coredecls.h".}