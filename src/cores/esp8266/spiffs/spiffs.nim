## 
##  spiffs.h
## 
##   Created on: May 26, 2013
##       Author: petera
## 

import
  spiffs_config

const
  SPIFFS_OK* = 0
  SPIFFS_ERR_NOT_MOUNTED* = -10000
  SPIFFS_ERR_FULL* = -10001
  SPIFFS_ERR_NOT_FOUND* = -10002
  SPIFFS_ERR_END_OF_OBJECT* = -10003
  SPIFFS_ERR_DELETED* = -10004
  SPIFFS_ERR_NOT_FINALIZED* = -10005
  SPIFFS_ERR_NOT_INDEX* = -10006
  SPIFFS_ERR_OUT_OF_FILE_DESCS* = -10007
  SPIFFS_ERR_FILE_CLOSED* = -10008
  SPIFFS_ERR_FILE_DELETED* = -10009
  SPIFFS_ERR_BAD_DESCRIPTOR* = -10010
  SPIFFS_ERR_IS_INDEX* = -10011
  SPIFFS_ERR_IS_FREE* = -10012
  SPIFFS_ERR_INDEX_SPAN_MISMATCH* = -10013
  SPIFFS_ERR_DATA_SPAN_MISMATCH* = -10014
  SPIFFS_ERR_INDEX_REF_FREE* = -10015
  SPIFFS_ERR_INDEX_REF_LU* = -10016
  SPIFFS_ERR_INDEX_REF_INVALID* = -10017
  SPIFFS_ERR_INDEX_FREE* = -10018
  SPIFFS_ERR_INDEX_LU* = -10019
  SPIFFS_ERR_INDEX_INVALID* = -10020
  SPIFFS_ERR_NOT_WRITABLE* = -10021
  SPIFFS_ERR_NOT_READABLE* = -10022
  SPIFFS_ERR_CONFLICTING_NAME* = -10023
  SPIFFS_ERR_NOT_CONFIGURED* = -10024
  SPIFFS_ERR_NOT_A_FS* = -10025
  SPIFFS_ERR_MOUNTED* = -10026
  SPIFFS_ERR_ERASE_FAIL* = -10027
  SPIFFS_ERR_MAGIC_NOT_POSSIBLE* = -10028
  SPIFFS_ERR_NO_DELETED_BLOCKS* = -10029
  SPIFFS_ERR_FILE_EXISTS* = -10030
  SPIFFS_ERR_NOT_A_FILE* = -10031
  SPIFFS_ERR_RO_NOT_IMPL* = -10032
  SPIFFS_ERR_RO_ABORTED_OPERATION* = -10033
  SPIFFS_ERR_PROBE_TOO_FEW_BLOCKS* = -10034
  SPIFFS_ERR_PROBE_NOT_A_FS* = -10035
  SPIFFS_ERR_NAME_TOO_LONG* = -10036
  SPIFFS_ERR_IX_MAP_UNMAPPED* = -10037
  SPIFFS_ERR_IX_MAP_MAPPED* = -10038
  SPIFFS_ERR_IX_MAP_BAD_RANGE* = -10039
  SPIFFS_ERR_SEEK_BOUNDS* = -10040
  SPIFFS_ERR_INTERNAL* = -10050
  SPIFFS_ERR_TEST* = -10100

##  spiffs file descriptor index type. must be signed

type
  spiffs_file* = s16_t

##  spiffs file descriptor flags

type
  spiffs_flags* = u16_t

##  spiffs file mode

type
  spiffs_mode* = u16_t

##  object type

type
  spiffs_obj_type* = u8_t

discard "forward decl of spiffs_t"
when SPIFFS_HAL_CALLBACK_EXTRA:
  ##  spi read call function type
  type
    spiffs_read* = proc (fs: ptr spiffs_t; `addr`: u32_t; size: u32_t; dst: ptr u8_t): s32_t {.
        cdecl.}
  ##  spi write call function type
  type
    spiffs_write* = proc (fs: ptr spiffs_t; `addr`: u32_t; size: u32_t; src: ptr u8_t): s32_t {.
        cdecl.}
  ##  spi erase call function type
  type
    spiffs_erase* = proc (fs: ptr spiffs_t; `addr`: u32_t; size: u32_t): s32_t {.cdecl.}
else:
  ##  spi read call function type
  type
    spiffs_read* = proc (`addr`: u32_t; size: u32_t; dst: ptr u8_t): s32_t {.cdecl.}
  ##  spi write call function type
  type
    spiffs_write* = proc (`addr`: u32_t; size: u32_t; src: ptr u8_t): s32_t {.cdecl.}
  ##  spi erase call function type
  type
    spiffs_erase* = proc (`addr`: u32_t; size: u32_t): s32_t {.cdecl.}
##  file system check callback report operation

type
  spiffs_check_type* {.size: sizeof(cint), importcpp: "spiffs_check_type",
                      header: "spiffs.h".} = enum
    SPIFFS_CHECK_LOOKUP = 0, SPIFFS_CHECK_INDEX, SPIFFS_CHECK_PAGE


##  file system check callback report type

type
  spiffs_check_report* {.size: sizeof(cint), importcpp: "spiffs_check_report",
                        header: "spiffs.h".} = enum
    SPIFFS_CHECK_PROGRESS = 0, SPIFFS_CHECK_ERROR, SPIFFS_CHECK_FIX_INDEX,
    SPIFFS_CHECK_FIX_LOOKUP, SPIFFS_CHECK_DELETE_ORPHANED_INDEX,
    SPIFFS_CHECK_DELETE_PAGE, SPIFFS_CHECK_DELETE_BAD_FILE


##  file system check callback function
##  file system listener callback operation

type                          ##  the file has been created
  spiffs_fileop_type* {.size: sizeof(cint), importcpp: "spiffs_fileop_type",
                       header: "spiffs.h".} = enum
    SPIFFS_CB_CREATED = 0,      ##  the file has been updated or moved to another page
    SPIFFS_CB_UPDATED,        ##  the file has been deleted
    SPIFFS_CB_DELETED


##  file system listener callback function

type
  spiffs_file_callback* = proc (fs: ptr spiffs_t; op: spiffs_fileop_type;
                             obj_id: spiffs_obj_id; pix: spiffs_page_ix) {.cdecl.}

##  Any write to the filehandle is appended to end of the file

const
  SPIFFS_APPEND* = (1 shl 0)
  SPIFFS_O_APPEND* = SPIFFS_APPEND

##  If the opened file exists, it will be truncated to zero length before opened

const
  SPIFFS_TRUNC* = (1 shl 1)
  SPIFFS_O_TRUNC* = SPIFFS_TRUNC

##  If the opened file does not exist, it will be created before opened

const
  SPIFFS_CREAT* = (1 shl 2)
  SPIFFS_O_CREAT* = SPIFFS_CREAT

##  The opened file may only be read

const
  SPIFFS_RDONLY* = (1 shl 3)
  SPIFFS_O_RDONLY* = SPIFFS_RDONLY

##  The opened file may only be written

const
  SPIFFS_WRONLY* = (1 shl 4)
  SPIFFS_O_WRONLY* = SPIFFS_WRONLY

##  The opened file may be both read and written

const
  SPIFFS_RDWR* = (SPIFFS_RDONLY or SPIFFS_WRONLY)
  SPIFFS_O_RDWR* = SPIFFS_RDWR

##  Any writes to the filehandle will never be cached but flushed directly

const
  SPIFFS_DIRECT* = (1 shl 5)
  SPIFFS_O_DIRECT* = SPIFFS_DIRECT

##  If SPIFFS_O_CREAT and SPIFFS_O_EXCL are set, SPIFFS_open() shall fail if the file exists

const
  SPIFFS_EXCL* = (1 shl 6)
  SPIFFS_O_EXCL* = SPIFFS_EXCL
  SPIFFS_SEEK_SET* = (0)
  SPIFFS_SEEK_CUR* = (1)
  SPIFFS_SEEK_END* = (2)
  SPIFFS_TYPE_FILE* = (1)
  SPIFFS_TYPE_DIR* = (2)
  SPIFFS_TYPE_HARD_LINK* = (3)
  SPIFFS_TYPE_SOFT_LINK* = (4)

when not defined(SPIFFS_LOCK):
  template SPIFFS_LOCK*(fs: untyped): void =
    nil

when not defined(SPIFFS_UNLOCK):
  template SPIFFS_UNLOCK*(fs: untyped): void =
    nil

##  phys structs
##  spiffs spi configuration struct

type
  spiffs_config* {.importcpp: "spiffs_config", header: "spiffs.h", bycopy.} = object
    hal_read_f* {.importc: "hal_read_f".}: spiffs_read ##  physical read function
    ##  physical write function
    hal_write_f* {.importc: "hal_write_f".}: spiffs_write ##  physical erase function
    hal_erase_f* {.importc: "hal_erase_f".}: spiffs_erase ##  physical size of the spi flash
    phys_size* {.importc: "phys_size".}: u32_t ##  physical offset in spi flash used for spiffs,
                                           ##  must be on block boundary
    phys_addr* {.importc: "phys_addr".}: u32_t ##  physical size when erasing a block
    phys_erase_block* {.importc: "phys_erase_block".}: u32_t ##  logical size of a block, must be on physical
                                                         ##  block size boundary and must never be less than
                                                         ##  a physical block
    log_block_size* {.importc: "log_block_size".}: u32_t ##  logical size of a page, must be at least
                                                     ##  log_block_size / 8
    log_page_size* {.importc: "log_page_size".}: u32_t ##  an integer offset added to each file handle
    fh_ix_offset* {.importc: "fh_ix_offset".}: u16_t

  spiffs* {.importcpp: "spiffs", header: "spiffs.h", bycopy.} = object
    cfg* {.importc: "cfg".}: spiffs_config ##  file system configuration
    ##  number of logical blocks
    block_count* {.importc: "block_count".}: u32_t ##  cursor for free blocks, block index
    free_cursor_block_ix* {.importc: "free_cursor_block_ix".}: spiffs_block_ix ##  cursor for free blocks, entry index
    free_cursor_obj_lu_entry* {.importc: "free_cursor_obj_lu_entry".}: cint ##  cursor when searching, block index
    cursor_block_ix* {.importc: "cursor_block_ix".}: spiffs_block_ix ##  cursor when searching, entry index
    cursor_obj_lu_entry* {.importc: "cursor_obj_lu_entry".}: cint ##  primary work buffer, size of a logical page
    lu_work* {.importc: "lu_work".}: ptr u8_t ##  secondary work buffer, size of a logical page
    work* {.importc: "work".}: ptr u8_t ##  file descriptor memory area
    fd_space* {.importc: "fd_space".}: ptr u8_t ##  available file descriptors
    fd_count* {.importc: "fd_count".}: u32_t ##  last error
    err_code* {.importc: "err_code".}: s32_t ##  current number of free blocks
    free_blocks* {.importc: "free_blocks".}: u32_t ##  current number of busy pages
    stats_p_allocated* {.importc: "stats_p_allocated".}: u32_t ##  current number of deleted pages
    stats_p_deleted* {.importc: "stats_p_deleted".}: u32_t ##  flag indicating that garbage collector is cleaning
    cleaning* {.importc: "cleaning".}: u8_t ##  max erase count amongst all blocks
    max_erase_count* {.importc: "max_erase_count".}: spiffs_obj_id
    stats_gc_runs* {.importc: "stats_gc_runs".}: u32_t ##  cache memory
    cache* {.importc: "cache".}: pointer ##  cache size
    cache_size* {.importc: "cache_size".}: u32_t
    cache_hits* {.importc: "cache_hits".}: u32_t
    cache_misses* {.importc: "cache_misses".}: u32_t ##  check callback function
    check_cb_f* {.importc: "check_cb_f".}: spiffs_check_callback ##  file callback function
    file_cb_f* {.importc: "file_cb_f".}: spiffs_file_callback ##  mounted flag
    mounted* {.importc: "mounted".}: u8_t ##  user data
    user_data* {.importc: "user_data".}: pointer ##  config magic
    config_magic* {.importc: "config_magic".}: u32_t


##  spiffs file status struct

type
  spiffs_stat* {.importcpp: "spiffs_stat", header: "spiffs.h", bycopy.} = object
    obj_id* {.importc: "obj_id".}: spiffs_obj_id
    size* {.importc: "size".}: u32_t
    `type`* {.importc: "type".}: spiffs_obj_type
    pix* {.importc: "pix".}: spiffs_page_ix
    name* {.importc: "name".}: array[SPIFFS_OBJ_NAME_LEN, u8_t]
    meta* {.importc: "meta".}: array[SPIFFS_OBJ_META_LEN, u8_t]

  spiffs_dirent* {.importcpp: "spiffs_dirent", header: "spiffs.h", bycopy.} = object
    obj_id* {.importc: "obj_id".}: spiffs_obj_id
    name* {.importc: "name".}: array[SPIFFS_OBJ_NAME_LEN, u8_t]
    `type`* {.importc: "type".}: spiffs_obj_type
    size* {.importc: "size".}: u32_t
    pix* {.importc: "pix".}: spiffs_page_ix
    meta* {.importc: "meta".}: array[SPIFFS_OBJ_META_LEN, u8_t]

  spiffs_DIR* {.importcpp: "spiffs_DIR", header: "spiffs.h", bycopy.} = object
    fs* {.importc: "fs".}: ptr spiffs
    `block`* {.importc: "block".}: spiffs_block_ix
    entry* {.importc: "entry".}: cint

  spiffs_ix_map* {.importcpp: "spiffs_ix_map", header: "spiffs.h", bycopy.} = object
    map_buf* {.importc: "map_buf".}: ptr spiffs_page_ix ##  buffer with looked up data pixes
    ##  precise file byte offset
    offset* {.importc: "offset".}: u32_t ##  start data span index of lookup buffer
    start_spix* {.importc: "start_spix".}: spiffs_span_ix ##  end data span index of lookup buffer
    end_spix* {.importc: "end_spix".}: spiffs_span_ix


##  functions
## *
##  Special function. This takes a spiffs config struct and returns the number
##  of blocks this file system was formatted with. This function relies on
##  that following info is set correctly in given config struct:
## 
##  phys_addr, log_page_size, and log_block_size.
## 
##  Also, hal_read_f must be set in the config struct.
## 
##  One must be sure of the correct page size and that the physical address is
##  correct in the probed file system when calling this function. It is not
##  checked if the phys_addr actually points to the start of the file system,
##  so one might get a false positive if entering a phys_addr somewhere in the
##  middle of the file system at block boundary. In addition, it is not checked
##  if the page size is actually correct. If it is not, weird file system sizes
##  will be returned.
## 
##  If this function detects a file system it returns the assumed file system
##  size, which can be used to set the phys_size.
## 
##  Otherwise, it returns an error indicating why it is not regarded as a file
##  system.
## 
##  Note: this function is not protected with SPIFFS_LOCK and SPIFFS_UNLOCK
##  macros. It returns the error code directly, instead of as read by
##  SPIFFS_errno.
## 
##  @param config        essential parts of the physical and logical
##                       configuration of the file system.
## 

proc SPIFFS_probe_fs*(config: ptr spiffs_config): s32_t {.cdecl,
    importcpp: "SPIFFS_probe_fs(@)", header: "spiffs.h".}
##  SPIFFS_USE_MAGIC && SPIFFS_USE_MAGIC_LENGTH && SPIFFS_SINGLETON==0
## *
##  Initializes the file system dynamic parameters and mounts the filesystem.
##  If SPIFFS_USE_MAGIC is enabled the mounting may fail with SPIFFS_ERR_NOT_A_FS
##  if the flash does not contain a recognizable file system.
##  In this case, SPIFFS_format must be called prior to remounting.
##  @param fs            the file system struct
##  @param config        the physical and logical configuration of the file system
##  @param work          a memory work buffer comprising 2*config->log_page_size
##                       bytes used throughout all file system operations
##  @param fd_space      memory for file descriptors
##  @param fd_space_size memory size of file descriptors
##  @param cache         memory for cache, may be null
##  @param cache_size    memory size of cache
##  @param check_cb_f    callback function for reporting during consistency checks
## 

proc SPIFFS_mount*(fs: ptr spiffs; config: ptr spiffs_config; work: ptr u8_t;
                  fd_space: ptr u8_t; fd_space_size: u32_t; cache: pointer;
                  cache_size: u32_t; check_cb_f: spiffs_check_callback): s32_t {.
    cdecl, importcpp: "SPIFFS_mount(@)", header: "spiffs.h".}
## *
##  Unmounts the file system. All file handles will be flushed of any
##  cached writes and closed.
##  @param fs            the file system struct
## 

proc SPIFFS_unmount*(fs: ptr spiffs) {.cdecl, importcpp: "SPIFFS_unmount(@)",
                                   header: "spiffs.h".}
## *
##  Creates a new file.
##  @param fs            the file system struct
##  @param path          the path of the new file
##  @param mode          ignored, for posix compliance
## 

proc SPIFFS_creat*(fs: ptr spiffs; path: cstring; mode: spiffs_mode): s32_t {.cdecl,
    importcpp: "SPIFFS_creat(@)", header: "spiffs.h".}
## *
##  Opens/creates a file.
##  @param fs            the file system struct
##  @param path          the path of the new file
##  @param flags         the flags for the open command, can be combinations of
##                       SPIFFS_O_APPEND, SPIFFS_O_TRUNC, SPIFFS_O_CREAT, SPIFFS_O_RDONLY,
##                       SPIFFS_O_WRONLY, SPIFFS_O_RDWR, SPIFFS_O_DIRECT, SPIFFS_O_EXCL
##  @param mode          ignored, for posix compliance
## 

proc SPIFFS_open*(fs: ptr spiffs; path: cstring; flags: spiffs_flags; mode: spiffs_mode): spiffs_file {.
    cdecl, importcpp: "SPIFFS_open(@)", header: "spiffs.h".}
## *
##  Opens a file by given dir entry.
##  Optimization purposes, when traversing a file system with SPIFFS_readdir
##  a normal SPIFFS_open would need to traverse the filesystem again to find
##  the file, whilst SPIFFS_open_by_dirent already knows where the file resides.
##  @param fs            the file system struct
##  @param e             the dir entry to the file
##  @param flags         the flags for the open command, can be combinations of
##                       SPIFFS_APPEND, SPIFFS_TRUNC, SPIFFS_CREAT, SPIFFS_RD_ONLY,
##                       SPIFFS_WR_ONLY, SPIFFS_RDWR, SPIFFS_DIRECT.
##                       SPIFFS_CREAT will have no effect in this case.
##  @param mode          ignored, for posix compliance
## 

proc SPIFFS_open_by_dirent*(fs: ptr spiffs; e: ptr spiffs_dirent; flags: spiffs_flags;
                           mode: spiffs_mode): spiffs_file {.cdecl,
    importcpp: "SPIFFS_open_by_dirent(@)", header: "spiffs.h".}
## *
##  Opens a file by given page index.
##  Optimization purposes, opens a file by directly pointing to the page
##  index in the spi flash.
##  If the page index does not point to a file header SPIFFS_ERR_NOT_A_FILE
##  is returned.
##  @param fs            the file system struct
##  @param page_ix       the page index
##  @param flags         the flags for the open command, can be combinations of
##                       SPIFFS_APPEND, SPIFFS_TRUNC, SPIFFS_CREAT, SPIFFS_RD_ONLY,
##                       SPIFFS_WR_ONLY, SPIFFS_RDWR, SPIFFS_DIRECT.
##                       SPIFFS_CREAT will have no effect in this case.
##  @param mode          ignored, for posix compliance
## 

proc SPIFFS_open_by_page*(fs: ptr spiffs; page_ix: spiffs_page_ix;
                         flags: spiffs_flags; mode: spiffs_mode): spiffs_file {.
    cdecl, importcpp: "SPIFFS_open_by_page(@)", header: "spiffs.h".}
## *
##  Reads from given filehandle.
##  @param fs            the file system struct
##  @param fh            the filehandle
##  @param buf           where to put read data
##  @param len           how much to read
##  @returns number of bytes read, or -1 if error
## 

proc SPIFFS_read*(fs: ptr spiffs; fh: spiffs_file; buf: pointer; len: s32_t): s32_t {.
    cdecl, importcpp: "SPIFFS_read(@)", header: "spiffs.h".}
## *
##  Writes to given filehandle.
##  @param fs            the file system struct
##  @param fh            the filehandle
##  @param buf           the data to write
##  @param len           how much to write
##  @returns number of bytes written, or -1 if error
## 

proc SPIFFS_write*(fs: ptr spiffs; fh: spiffs_file; buf: pointer; len: s32_t): s32_t {.
    cdecl, importcpp: "SPIFFS_write(@)", header: "spiffs.h".}
## *
##  Moves the read/write file offset. Resulting offset is returned or negative if error.
##  lseek(fs, fd, 0, SPIFFS_SEEK_CUR) will thus return current offset.
##  @param fs            the file system struct
##  @param fh            the filehandle
##  @param offs          how much/where to move the offset
##  @param whence        if SPIFFS_SEEK_SET, the file offset shall be set to offset bytes
##                       if SPIFFS_SEEK_CUR, the file offset shall be set to its current location plus offset
##                       if SPIFFS_SEEK_END, the file offset shall be set to the size of the file plus offse, which should be negative
## 

proc SPIFFS_lseek*(fs: ptr spiffs; fh: spiffs_file; offs: s32_t; whence: cint): s32_t {.
    cdecl, importcpp: "SPIFFS_lseek(@)", header: "spiffs.h".}
## *
##  Removes a file by path
##  @param fs            the file system struct
##  @param path          the path of the file to remove
## 

proc SPIFFS_remove*(fs: ptr spiffs; path: cstring): s32_t {.cdecl,
    importcpp: "SPIFFS_remove(@)", header: "spiffs.h".}
## *
##  Removes a file by filehandle
##  @param fs            the file system struct
##  @param fh            the filehandle of the file to remove
## 

proc SPIFFS_fremove*(fs: ptr spiffs; fh: spiffs_file): s32_t {.cdecl,
    importcpp: "SPIFFS_fremove(@)", header: "spiffs.h".}
## *
##  Gets file status by path
##  @param fs            the file system struct
##  @param path          the path of the file to stat
##  @param s             the stat struct to populate
## 

proc SPIFFS_stat*(fs: ptr spiffs; path: cstring; s: ptr spiffs_stat): s32_t {.cdecl,
    importcpp: "SPIFFS_stat(@)", header: "spiffs.h".}
## *
##  Gets file status by filehandle
##  @param fs            the file system struct
##  @param fh            the filehandle of the file to stat
##  @param s             the stat struct to populate
## 

proc SPIFFS_fstat*(fs: ptr spiffs; fh: spiffs_file; s: ptr spiffs_stat): s32_t {.cdecl,
    importcpp: "SPIFFS_fstat(@)", header: "spiffs.h".}
## *
##  Flushes all pending write operations from cache for given file
##  @param fs            the file system struct
##  @param fh            the filehandle of the file to flush
## 

proc SPIFFS_fflush*(fs: ptr spiffs; fh: spiffs_file): s32_t {.cdecl,
    importcpp: "SPIFFS_fflush(@)", header: "spiffs.h".}
## *
##  Closes a filehandle. If there are pending write operations, these are finalized before closing.
##  @param fs            the file system struct
##  @param fh            the filehandle of the file to close
## 

proc SPIFFS_close*(fs: ptr spiffs; fh: spiffs_file): s32_t {.cdecl,
    importcpp: "SPIFFS_close(@)", header: "spiffs.h".}
## *
##  Renames a file
##  @param fs            the file system struct
##  @param old           path of file to rename
##  @param newPath       new path of file
## 

proc SPIFFS_rename*(fs: ptr spiffs; old: cstring; newPath: cstring): s32_t {.cdecl,
    importcpp: "SPIFFS_rename(@)", header: "spiffs.h".}
when SPIFFS_OBJ_META_LEN:
  ## *
  ##  Updates file's metadata
  ##  @param fs            the file system struct
  ##  @param path          path to the file
  ##  @param meta          new metadata. must be SPIFFS_OBJ_META_LEN bytes long.
  ## 
  proc SPIFFS_update_meta*(fs: ptr spiffs; name: cstring; meta: pointer): s32_t {.cdecl,
      importcpp: "SPIFFS_update_meta(@)", header: "spiffs.h".}
  ## *
  ##  Updates file's metadata
  ##  @param fs            the file system struct
  ##  @param fh            file handle of the file
  ##  @param meta          new metadata. must be SPIFFS_OBJ_META_LEN bytes long.
  ## 
  proc SPIFFS_fupdate_meta*(fs: ptr spiffs; fh: spiffs_file; meta: pointer): s32_t {.
      cdecl, importcpp: "SPIFFS_fupdate_meta(@)", header: "spiffs.h".}
## *
##  Returns last error of last file operation.
##  @param fs            the file system struct
## 

proc SPIFFS_errno*(fs: ptr spiffs): s32_t {.cdecl, importcpp: "SPIFFS_errno(@)",
                                       header: "spiffs.h".}
## *
##  Clears last error.
##  @param fs            the file system struct
## 

proc SPIFFS_clearerr*(fs: ptr spiffs) {.cdecl, importcpp: "SPIFFS_clearerr(@)",
                                    header: "spiffs.h".}
## *
##  Opens a directory stream corresponding to the given name.
##  The stream is positioned at the first entry in the directory.
##  On hydrogen builds the name argument is ignored as hydrogen builds always correspond
##  to a flat file structure - no directories.
##  @param fs            the file system struct
##  @param name          the name of the directory
##  @param d             pointer the directory stream to be populated
## 

proc SPIFFS_opendir*(fs: ptr spiffs; name: cstring; d: ptr spiffs_DIR): ptr spiffs_DIR {.
    cdecl, importcpp: "SPIFFS_opendir(@)", header: "spiffs.h".}
## *
##  Closes a directory stream
##  @param d             the directory stream to close
## 

proc SPIFFS_closedir*(d: ptr spiffs_DIR): s32_t {.cdecl,
    importcpp: "SPIFFS_closedir(@)", header: "spiffs.h".}
## *
##  Reads a directory into given spifs_dirent struct.
##  @param d             pointer to the directory stream
##  @param e             the dirent struct to be populated
##  @returns null if error or end of stream, else given dirent is returned
## 

proc SPIFFS_readdir*(d: ptr spiffs_DIR; e: ptr spiffs_dirent): ptr spiffs_dirent {.cdecl,
    importcpp: "SPIFFS_readdir(@)", header: "spiffs.h".}
## *
##  Runs a consistency check on given filesystem.
##  @param fs            the file system struct
## 

proc SPIFFS_check*(fs: ptr spiffs): s32_t {.cdecl, importcpp: "SPIFFS_check(@)",
                                       header: "spiffs.h".}
## *
##  Returns number of total bytes available and number of used bytes.
##  This is an estimation, and depends on if there a many files with little
##  data or few files with much data.
##  NB: If used number of bytes exceeds total bytes, a SPIFFS_check should
##  run. This indicates a power loss in midst of things. In worst case
##  (repeated powerlosses in mending or gc) you might have to delete some files.
## 
##  @param fs            the file system struct
##  @param total         total number of bytes in filesystem
##  @param used          used number of bytes in filesystem
## 

proc SPIFFS_info*(fs: ptr spiffs; total: ptr u32_t; used: ptr u32_t): s32_t {.cdecl,
    importcpp: "SPIFFS_info(@)", header: "spiffs.h".}
## *
##  Formats the entire file system. All data will be lost.
##  The filesystem must not be mounted when calling this.
## 
##  NB: formatting is awkward. Due to backwards compatibility, SPIFFS_mount
##  MUST be called prior to formatting in order to configure the filesystem.
##  If SPIFFS_mount succeeds, SPIFFS_unmount must be called before calling
##  SPIFFS_format.
##  If SPIFFS_mount fails, SPIFFS_format can be called directly without calling
##  SPIFFS_unmount first.
## 
##  @param fs            the file system struct
## 

proc SPIFFS_format*(fs: ptr spiffs): s32_t {.cdecl, importcpp: "SPIFFS_format(@)",
                                        header: "spiffs.h".}
## *
##  Returns nonzero if spiffs is mounted, or zero if unmounted.
##  @param fs            the file system struct
## 

proc SPIFFS_mounted*(fs: ptr spiffs): u8_t {.cdecl, importcpp: "SPIFFS_mounted(@)",
                                        header: "spiffs.h".}
## *
##  Tries to find a block where most or all pages are deleted, and erase that
##  block if found. Does not care for wear levelling. Will not move pages
##  around.
##  If parameter max_free_pages are set to 0, only blocks with only deleted
##  pages will be selected.
## 
##  NB: the garbage collector is automatically called when spiffs needs free
##  pages. The reason for this function is to give possibility to do background
##  tidying when user knows the system is idle.
## 
##  Use with care.
## 
##  Setting max_free_pages to anything larger than zero will eventually wear
##  flash more as a block containing free pages can be erased.
## 
##  Will set err_no to SPIFFS_OK if a block was found and erased,
##  SPIFFS_ERR_NO_DELETED_BLOCK if no matching block was found,
##  or other error.
## 
##  @param fs             the file system struct
##  @param max_free_pages maximum number allowed free pages in block
## 

proc SPIFFS_gc_quick*(fs: ptr spiffs; max_free_pages: u16_t): s32_t {.cdecl,
    importcpp: "SPIFFS_gc_quick(@)", header: "spiffs.h".}
## *
##  Will try to make room for given amount of bytes in the filesystem by moving
##  pages and erasing blocks.
##  If it is physically impossible, err_no will be set to SPIFFS_ERR_FULL. If
##  there already is this amount (or more) of free space, SPIFFS_gc will
##  silently return. It is recommended to call SPIFFS_info before invoking
##  this method in order to determine what amount of bytes to give.
## 
##  NB: the garbage collector is automatically called when spiffs needs free
##  pages. The reason for this function is to give possibility to do background
##  tidying when user knows the system is idle.
## 
##  Use with care.
## 
##  @param fs            the file system struct
##  @param size          amount of bytes that should be freed
## 

proc SPIFFS_gc*(fs: ptr spiffs; size: u32_t): s32_t {.cdecl, importcpp: "SPIFFS_gc(@)",
    header: "spiffs.h".}
## *
##  Check if EOF reached.
##  @param fs            the file system struct
##  @param fh            the filehandle of the file to check
## 

proc SPIFFS_eof*(fs: ptr spiffs; fh: spiffs_file): s32_t {.cdecl,
    importcpp: "SPIFFS_eof(@)", header: "spiffs.h".}
## *
##  Get position in file.
##  @param fs            the file system struct
##  @param fh            the filehandle of the file to check
## 

proc SPIFFS_tell*(fs: ptr spiffs; fh: spiffs_file): s32_t {.cdecl,
    importcpp: "SPIFFS_tell(@)", header: "spiffs.h".}
## *
##  Registers a callback function that keeps track on operations on file
##  headers. Do note, that this callback is called from within internal spiffs
##  mechanisms. Any operations on the actual file system being callbacked from
##  in this callback will mess things up for sure - do not do this.
##  This can be used to track where files are and move around during garbage
##  collection, which in turn can be used to build location tables in ram.
##  Used in conjuction with SPIFFS_open_by_page this may improve performance
##  when opening a lot of files.
##  Must be invoked after mount.
## 
##  @param fs            the file system struct
##  @param cb_func       the callback on file operations
## 

proc SPIFFS_set_file_callback_func*(fs: ptr spiffs; cb_func: spiffs_file_callback): s32_t {.
    cdecl, importcpp: "SPIFFS_set_file_callback_func(@)", header: "spiffs.h".}
when SPIFFS_IX_MAP:
  ## *
  ##  Maps the first level index lookup to a given memory map.
  ##  This will make reading big files faster, as the memory map will be used for
  ##  looking up data pages instead of searching for the indices on the physical
  ##  medium. When mapping, all affected indicies are found and the information is
  ##  copied to the array.
  ##  Whole file or only parts of it may be mapped. The index map will cover file
  ##  contents from argument offset until and including arguments (offset+len).
  ##  It is valid to map a longer range than the current file size. The map will
  ##  then be populated when the file grows.
  ##  On garbage collections and file data page movements, the map array will be
  ##  automatically updated. Do not tamper with the map array, as this contains
  ##  the references to the data pages. Modifying it from outside will corrupt any
  ##  future readings using this file descriptor.
  ##  The map will no longer be used when the file descriptor closed or the file
  ##  is unmapped.
  ##  This can be useful to get faster and more deterministic timing when reading
  ##  large files, or when seeking and reading a lot within a file.
  ##  @param fs      the file system struct
  ##  @param fh      the file handle of the file to map
  ##  @param map     a spiffs_ix_map struct, describing the index map
  ##  @param offset  absolute file offset where to start the index map
  ##  @param len     length of the mapping in actual file bytes
  ##  @param map_buf the array buffer for the look up data - number of required
  ##                 elements in the array can be derived from function
  ##                 SPIFFS_bytes_to_ix_map_entries given the length
  ## 
  proc SPIFFS_ix_map*(fs: ptr spiffs; fh: spiffs_file; map: ptr spiffs_ix_map;
                     offset: u32_t; len: u32_t; map_buf: ptr spiffs_page_ix): s32_t {.
      cdecl, importcpp: "SPIFFS_ix_map(@)", header: "spiffs.h".}
  ## *
  ##  Unmaps the index lookup from this filehandle. All future readings will
  ##  proceed as normal, requiring reading of the first level indices from
  ##  physical media.
  ##  The map and map buffer given in function SPIFFS_ix_map will no longer be
  ##  referenced by spiffs.
  ##  It is not strictly necessary to unmap a file before closing it, as closing
  ##  a file will automatically unmap it.
  ##  @param fs      the file system struct
  ##  @param fh      the file handle of the file to unmap
  ## 
  proc SPIFFS_ix_unmap*(fs: ptr spiffs; fh: spiffs_file): s32_t {.cdecl,
      importcpp: "SPIFFS_ix_unmap(@)", header: "spiffs.h".}
  ## *
  ##  Moves the offset for the index map given in function SPIFFS_ix_map. Parts or
  ##  all of the map buffer will repopulated.
  ##  @param fs      the file system struct
  ##  @param fh      the mapped file handle of the file to remap
  ##  @param offset  new absolute file offset where to start the index map
  ## 
  proc SPIFFS_ix_remap*(fs: ptr spiffs; fh: spiffs_file; offs: u32_t): s32_t {.cdecl,
      importcpp: "SPIFFS_ix_remap(@)", header: "spiffs.h".}
  ## *
  ##  Utility function to get number of spiffs_page_ix entries a map buffer must
  ##  contain on order to map given amount of file data in bytes.
  ##  See function SPIFFS_ix_map and SPIFFS_ix_map_entries_to_bytes.
  ##  @param fs      the file system struct
  ##  @param bytes   number of file data bytes to map
  ##  @return        needed number of elements in a spiffs_page_ix array needed to
  ##                 map given amount of bytes in a file
  ## 
  proc SPIFFS_bytes_to_ix_map_entries*(fs: ptr spiffs; bytes: u32_t): s32_t {.cdecl,
      importcpp: "SPIFFS_bytes_to_ix_map_entries(@)", header: "spiffs.h".}
  ## *
  ##  Utility function to amount of file data bytes that can be mapped when
  ##  mapping a file with buffer having given number of spiffs_page_ix entries.
  ##  See function SPIFFS_ix_map and SPIFFS_bytes_to_ix_map_entries.
  ##  @param fs      the file system struct
  ##  @param map_page_ix_entries   number of entries in a spiffs_page_ix array
  ##  @return        amount of file data in bytes that can be mapped given a map
  ##                 buffer having given amount of spiffs_page_ix entries
  ## 
  proc SPIFFS_ix_map_entries_to_bytes*(fs: ptr spiffs; map_page_ix_entries: u32_t): s32_t {.
      cdecl, importcpp: "SPIFFS_ix_map_entries_to_bytes(@)", header: "spiffs.h".}
## *
##  Prints out a visualization of the filesystem.
##  @param fs            the file system struct
## 

proc SPIFFS_vis*(fs: ptr spiffs): s32_t {.cdecl, importcpp: "SPIFFS_vis(@)",
                                     header: "spiffs.h".}
## *
##  Returns number of bytes needed for the filedescriptor buffer given
##  amount of file descriptors.
## 

proc SPIFFS_buffer_bytes_for_filedescs*(fs: ptr spiffs; num_descs: u32_t): u32_t {.
    cdecl, importcpp: "SPIFFS_buffer_bytes_for_filedescs(@)", header: "spiffs.h".}
## *
##  Returns number of bytes needed for the cache buffer given
##  amount of cache pages.
## 

proc SPIFFS_buffer_bytes_for_cache*(fs: ptr spiffs; num_pages: u32_t): u32_t {.cdecl,
    importcpp: "SPIFFS_buffer_bytes_for_cache(@)", header: "spiffs.h".}