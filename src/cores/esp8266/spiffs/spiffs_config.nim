## 
##  spiffs_config.h
## 
##   Created on: Jul 3, 2013
##       Author: petera
## 

const
  c_memcpy* = memcpy
  c_printf* = printf
  c_memset* = memset

type
  file_t* = int16_t
  s32_t* = int32_t
  u32_t* = uint32_t
  s16_t* = int16_t
  u16_t* = uint16_t
  s8_t* = int8_t
  u8_t* = uint8_t

##  compile time switches
##  Set generic spiffs debug output call.

when not defined(SPIFFS_DBG):
  template SPIFFS_DBG*(): void =
    ## printf(__VA_ARGS__)
    nil

##  Set spiffs debug output call for garbage collecting.

when not defined(SPIFFS_GC_DBG):
  template SPIFFS_GC_DBG*(): void =
    ## printf(__VA_ARGS__)
    nil

##  Set spiffs debug output call for caching.

when not defined(SPIFFS_CACHE_DBG):
  template SPIFFS_CACHE_DBG*(): void =
    ## printf(__VA_ARGS__)
    nil

##  Set spiffs debug output call for system consistency checks.

when not defined(SPIFFS_CHECK_DBG):
  template SPIFFS_CHECK_DBG*(): void =
    ## printf(__VA_ARGS__)
    nil

##  Set spiffs debug output call for all api invocations.

when not defined(SPIFFS_API_DBG):
  template SPIFFS_API_DBG*(): void =
    ## printf(__VA_ARGS__)
    nil

##  Defines spiffs debug print formatters
##  some general signed number

##  address

##  block

##  page

##  span index

##  file descriptor

##  file object id

##  file flags

##  Enable/disable API functions to determine exact number of bytes
##  for filedescriptor and cache buffers. Once decided for a configuration,
##  this can be disabled to reduce flash.

##  Enables/disable memory read caching of nucleus file system operations.
##  If enabled, memory area must be provided for cache in SPIFFS_mount.

when SPIFFS_CACHE:
  ##  Enables memory write caching for file descriptors in hydrogen


  ##  Enable/disable statistics on caching. Debug/test purpose only.
##  Always check header of each accessed page to ensure consistent state.
##  If enabled it will increase number of reads, will increase flash.

##  Define maximum number of gc runs to perform to reach desired free pages.

##  Enable/disable statistics on gc. Debug/test purpose only.

##  Garbage collecting examines all pages in a block which and sums up
##  to a block score. Deleted pages normally gives positive score and
##  used pages normally gives a negative score (as these must be moved).
##  To have a fair wear-leveling, the erase age is also included in score,
##  whose factor normally is the most positive.
##  The larger the score, the more likely it is that the block will
##  picked for garbage collection.
##  Garbage collecting heuristics - weight used for deleted pages.

##  Garbage collecting heuristics - weight used for used pages.

##  Garbage collecting heuristics - weight used for time between
##  last erased and erase of this block.

##  Object name maximum length.

##  Maximum length of the metadata associated with an object.
##  Setting to non-zero value enables metadata-related API but also
##  changes the on-disk format, so the change is not backward-compatible.
## 
##  Do note: the meta length must never exceed
##  logical_page_size - (SPIFFS_OBJ_NAME_LEN + 64)
## 
##  This is derived from following:
##  logical_page_size - (SPIFFS_OBJ_NAME_LEN + sizeof(spiffs_page_header) +
##  spiffs_object_ix_header fields + at least some LUT entries)

##  Size of buffer allocated on stack used when copying data.
##  Lower value generates more read/writes. No meaning having it bigger
##  than logical page size.

##  Enable this to have an identifiable spiffs filesystem. This will look for
##  a magic in all sectors to determine if this is a valid spiffs system or
##  not on mount point. If not, SPIFFS_format must be called prior to mounting
##  again.

when SPIFFS_USE_MAGIC:
  ##  Only valid when SPIFFS_USE_MAGIC is enabled. If SPIFFS_USE_MAGIC_LENGTH is
  ##  enabled, the magic will also be dependent on the length of the filesystem.
  ##  For example, a filesystem configured and formatted for 4 megabytes will not
  ##  be accepted for mounting with a configuration defining the filesystem as 2
  ##  megabytes.
##  SPIFFS_LOCK and SPIFFS_UNLOCK protects spiffs from reentrancy on api level
##  These should be defined on a multithreaded system
##  define this to enter a mutex if you're running on a multithreaded system

when not defined(SPIFFS_LOCK):
  template SPIFFS_LOCK*(fs: untyped): void =
    nil

##  define this to exit a mutex if you're running on a multithreaded system

when not defined(SPIFFS_UNLOCK):
  template SPIFFS_UNLOCK*(fs: untyped): void =
    nil

##  Enable if only one spiffs instance with constant configuration will exist
##  on the target. This will reduce calculations, flash and memory accesses.
##  Parts of configuration must be defined below instead of at time of mount.

when SPIFFS_SINGLETON:
  ##  Instead of giving parameters in config struct, singleton build must
  ##  give parameters in defines below.
  when not defined(SPIFFS_CFG_PHYS_SZ):
    template SPIFFS_CFG_PHYS_SZ*(ignore: untyped): untyped =
      (1024 * 1024 * 2)

  when not defined(SPIFFS_CFG_PHYS_ERASE_SZ):
    template SPIFFS_CFG_PHYS_ERASE_SZ*(ignore: untyped): untyped =
      (65536)

  when not defined(SPIFFS_CFG_PHYS_ADDR):
    template SPIFFS_CFG_PHYS_ADDR*(ignore: untyped): untyped =
      (0)

  when not defined(SPIFFS_CFG_LOG_PAGE_SZ):
    template SPIFFS_CFG_LOG_PAGE_SZ*(ignore: untyped): untyped =
      (256)

  when not defined(SPIFFS_CFG_LOG_BLOCK_SZ):
    template SPIFFS_CFG_LOG_BLOCK_SZ*(ignore: untyped): untyped =
      (65536)

##  Enable this if your target needs aligned data for index tables

##  Enable this if you want the HAL callbacks to be called with the spiffs struct

##  Enable this if you want to add an integer offset to all file handles
##  (spiffs_file). This is useful if running multiple instances of spiffs on
##  same target, in order to recognise to what spiffs instance a file handle
##  belongs.
##  NB: This adds config field fh_ix_offset in the configuration struct when
##  mounting, which must be defined.

##  Enable this to compile a read only version of spiffs.
##  This will reduce binary size of spiffs. All code comprising modification
##  of the file system will not be compiled. Some config will be ignored.
##  HAL functions for erasing and writing to spi-flash may be null. Cache
##  can be disabled for even further binary size reduction (and ram savings).
##  Functions modifying the fs will return SPIFFS_ERR_RO_NOT_IMPL.
##  If the file system cannot be mounted due to aborted erase operation and
##  SPIFFS_USE_MAGIC is enabled, SPIFFS_ERR_RO_ABORTED_OPERATION will be
##  returned.
##  Might be useful for e.g. bootloaders and such.

##  Enable this to add a temporal file cache using the fd buffer.
##  The effects of the cache is that SPIFFS_open will find the file faster in
##  certain cases. It will make it a lot easier for spiffs to find files
##  opened frequently, reducing number of readings from the spi flash for
##  finding those files.
##  This will grow each fd by 6 bytes. If your files are opened in patterns
##  with a degree of temporal locality, the system is optimized.
##  Examples can be letting spiffs serve web content, where one file is the css.
##  The css is accessed for each html file that is opened, meaning it is
##  accessed almost every second time a file is opened. Another example could be
##  a log file that is often opened, written, and closed.
##  The size of the cache is number of given file descriptors, as it piggybacks
##  on the fd update mechanism. The cache lives in the closed file descriptors.
##  When closed, the fd know the whereabouts of the file. Instead of forgetting
##  this, the temporal cache will keep handling updates to that file even if the
##  fd is closed. If the file is opened again, the location of the file is found
##  directly. If all available descriptors become opened, all cache memory is
##  lost.

##  Temporal file cache hit score. Each time a file is opened, all cached files
##  will lose one point. If the opened file is found in cache, that entry will
##  gain SPIFFS_TEMPORAL_CACHE_HIT_SCORE points. One can experiment with this
##  value for the specific access patterns of the application. However, it must
##  be between 1 (no gain for hitting a cached entry often) and 255.

##  Enable to be able to map object indices to memory.
##  This allows for faster and more deterministic reading if cases of reading
##  large files and when changing file offset by seeking around a lot.
##  When mapping a file's index, the file system will be scanned for index pages
##  and the info will be put in memory provided by user. When reading, the
##  memory map can be looked up instead of searching for index pages on the
##  medium. This way, user can trade memory against performance.
##  Whole, parts of, or future parts not being written yet can be mapped. The
##  memory array will be owned by spiffs and updated accordingly during garbage
##  collecting or when modifying the indices. The latter is invoked by when the
##  file is modified in some way. The index buffer is tied to the file
##  descriptor.

##  Set SPIFFS_TEST_VISUALISATION to non-zero to enable SPIFFS_vis function
##  in the api. This function will visualize all filesystem using given printf
##  function.

when SPIFFS_TEST_VISUALISATION:
  when not defined(spiffs_printf):
    template spiffs_printf*(): untyped =
      c_printf(__VA_ARGS__)


  ##  spiffs_printf argument for a free page

  ##  spiffs_printf argument for a deleted page

  ##  spiffs_printf argument for an index page for given object id
  when not defined(SPIFFS_TEST_VIS_INDX_STR):
    template SPIFFS_TEST_VIS_INDX_STR*(id: untyped): untyped =
      "i"


  ##  spiffs_printf argument for a data page for given object id
  when not defined(SPIFFS_TEST_VIS_DATA_STR):
    template SPIFFS_TEST_VIS_DATA_STR*(id: untyped): untyped =
      "d"

##  Types depending on configuration such as the amount of flash bytes
##  given to spiffs file system in total (spiffs_file_system_size),
##  the logical block size (log_block_size), and the logical page size
##  (log_page_size)
##  Block index type. Make sure the size of this type can hold
##  the highest number of all blocks - i.e. spiffs_file_system_size / log_block_size

type
  spiffs_block_ix* = u16_t

##  Page index type. Make sure the size of this type can hold
##  the highest page number of all pages - i.e. spiffs_file_system_size / log_page_size

type
  spiffs_page_ix* = u16_t

##  Object id type - most significant bit is reserved for index flag. Make sure the
##  size of this type can hold the highest object id on a full system,
##  i.e. 2 + (spiffs_file_system_size / (2*log_page_size))*2

type
  spiffs_obj_id* = u16_t

##  Object span index type. Make sure the size of this type can
##  hold the largest possible span index on the system -
##  i.e. (spiffs_file_system_size / log_page_size) - 1

type
  spiffs_span_ix* = u16_t
