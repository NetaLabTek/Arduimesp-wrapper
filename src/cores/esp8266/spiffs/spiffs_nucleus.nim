## 
##  spiffs_nucleus.h
## 
##   Created on: Jun 15, 2013
##       Author: petera
## 
##  SPIFFS layout
## 
##  spiffs is designed for following spi flash characteristics:
##    - only big areas of data (blocks) can be erased
##    - erasing resets all bits in a block to ones
##    - writing pulls ones to zeroes
##    - zeroes cannot be pulled to ones, without erase
##    - wear leveling
## 
##  spiffs is also meant to be run on embedded, memory constraint devices.
## 
##  Entire area is divided in blocks. Entire area is also divided in pages.
##  Each block contains same number of pages. A page cannot be erased, but a
##  block can be erased.
## 
##  Entire area must be block_size * x
##  page_size must be block_size / (2^y) where y > 2
## 
##  ex: area = 1024*1024 bytes, block size = 65536 bytes, page size = 256 bytes
## 
##  BLOCK 0  PAGE 0       object lookup 1
##           PAGE 1       object lookup 2
##           ...
##           PAGE n-1     object lookup n
##           PAGE n       object data 1
##           PAGE n+1     object data 2
##           ...
##           PAGE n+m-1   object data m
## 
##  BLOCK 1  PAGE n+m     object lookup 1
##           PAGE n+m+1   object lookup 2
##           ...
##           PAGE 2n+m-1  object lookup n
##           PAGE 2n+m    object data 1
##           PAGE 2n+m    object data 2
##           ...
##           PAGE 2n+2m-1 object data m
##  ...
## 
##  n is number of object lookup pages, which is number of pages needed to index all pages
##  in a block by object id
##    : block_size / page_size * sizeof(obj_id) / page_size
##  m is number data pages, which is number of pages in block minus number of lookup pages
##    : block_size / page_size - block_size / page_size * sizeof(obj_id) / page_size
##  thus, n+m is total number of pages in a block
##    : block_size / page_size
## 
##  ex: n = 65536/256*2/256 = 2, m = 65536/256 - 2 = 254 => n+m = 65536/256 = 256
## 
##  Object lookup pages contain object id entries. Each entry represent the corresponding
##  data page.
##  Assuming a 16 bit object id, an object id being 0xffff represents a free page.
##  An object id being 0x0000 represents a deleted page.
## 
##  ex: page 0 : lookup : 0008 0001 0aaa ffff ffff ffff ffff ffff ..
##      page 1 : lookup : ffff ffff ffff ffff ffff ffff ffff ffff ..
##      page 2 : data   : data for object id 0008
##      page 3 : data   : data for object id 0001
##      page 4 : data   : data for object id 0aaa
##      ...
## 
## 
##  Object data pages can be either object index pages or object content.
##  All object data pages contains a data page header, containing object id and span index.
##  The span index denotes the object page ordering amongst data pages with same object id.
##  This applies to both object index pages (when index spans more than one page of entries),
##  and object data pages.
##  An object index page contains page entries pointing to object content page. The entry index
##  in a object index page correlates to the span index in the actual object data page.
##  The first object index page (span index 0) is called object index header page, and also
##  contains object flags (directory/file), size, object name etc.
## 
##  ex:
##   BLOCK 1
##     PAGE 256: objectl lookup page 1
##       [*123] [ 123] [ 123] [ 123]
##       [ 123] [*123] [ 123] [ 123]
##       [free] [free] [free] [free] ...
##     PAGE 257: objectl lookup page 2
##       [free] [free] [free] [free] ...
##     PAGE 258: object index page (header)
##       obj.id:0123 span.ix:0000 flags:INDEX
##       size:1600 name:ex.txt type:file
##       [259] [260] [261] [262]
##     PAGE 259: object data page
##       obj.id:0123 span.ix:0000 flags:DATA
##     PAGE 260: object data page
##       obj.id:0123 span.ix:0001 flags:DATA
##     PAGE 261: object data page
##       obj.id:0123 span.ix:0002 flags:DATA
##     PAGE 262: object data page
##       obj.id:0123 span.ix:0003 flags:DATA
##     PAGE 263: object index page
##       obj.id:0123 span.ix:0001 flags:INDEX
##       [264] [265] [fre] [fre]
##       [fre] [fre] [fre] [fre]
##     PAGE 264: object data page
##       obj.id:0123 span.ix:0004 flags:DATA
##     PAGE 265: object data page
##       obj.id:0123 span.ix:0005 flags:DATA
## 
## 

const
  _SPIFFS_ERR_CHECK_FIRST* = (SPIFFS_ERR_INTERNAL - 1)
  SPIFFS_ERR_CHECK_OBJ_ID_MISM* = (SPIFFS_ERR_INTERNAL - 1)
  SPIFFS_ERR_CHECK_SPIX_MISM* = (SPIFFS_ERR_INTERNAL - 2)
  SPIFFS_ERR_CHECK_FLAGS_BAD* = (SPIFFS_ERR_INTERNAL - 3)
  _SPIFFS_ERR_CHECK_LAST* = (SPIFFS_ERR_INTERNAL - 4)

##  visitor result, continue searching

const
  SPIFFS_VIS_COUNTINUE* = (SPIFFS_ERR_INTERNAL - 20)

##  visitor result, continue searching after reloading lu buffer

const
  SPIFFS_VIS_COUNTINUE_RELOAD* = (SPIFFS_ERR_INTERNAL - 21)

##  visitor result, stop searching

const
  SPIFFS_VIS_END* = (SPIFFS_ERR_INTERNAL - 22)

##  updating an object index contents

const
  SPIFFS_EV_IX_UPD* = (0)

##  creating a new object index

const
  SPIFFS_EV_IX_NEW* = (1)

##  deleting an object index

const
  SPIFFS_EV_IX_DEL* = (2)

##  moving an object index without updating contents

const
  SPIFFS_EV_IX_MOV* = (3)

##  updating an object index header data only, not the table itself

const
  SPIFFS_EV_IX_UPD_HDR* = (4)
  SPIFFS_OBJ_ID_IX_FLAG* = (
    (spiffs_obj_id)(1 shl (8 * sizeof((spiffs_obj_id)) - 1)))
  SPIFFS_UNDEFINED_LEN* = (u32_t)(-1)
  SPIFFS_OBJ_ID_DELETED* = (cast[spiffs_obj_id](0))
  SPIFFS_OBJ_ID_FREE* = ((spiffs_obj_id) - 1)

when defined(__GNUC__) or defined(__clang__):
  ##  For GCC and clang
  const
    SPIFFS_PACKED* = __attribute__((packed))
elif defined(__ICCARM__) or defined(__CC_ARM):
  ##  For IAR ARM and Keil MDK-ARM compilers
  const
    SPIFFS_PACKED* = true
else:
  ##  Unknown compiler
  const
    SPIFFS_PACKED* = true
when SPIFFS_USE_MAGIC:
  when not SPIFFS_USE_MAGIC_LENGTH:
    template SPIFFS_MAGIC*(fs, bix: untyped): untyped =
      ((spiffs_obj_id)(0x20140529 xor SPIFFS_CFG_LOG_PAGE_SZ(fs)))

  else:
    template SPIFFS_MAGIC*(fs, bix: untyped): untyped =
      ((spiffs_obj_id)(0x20140529 xor SPIFFS_CFG_LOG_PAGE_SZ(fs) xor
          ((fs).block_count - (bix))))

const
  SPIFFS_CONFIG_MAGIC* = (0x20090315)

when SPIFFS_SINGLETON == 0:
  template SPIFFS_CFG_LOG_PAGE_SZ*(fs: untyped): untyped =
    ((fs).cfg.log_page_size)

  template SPIFFS_CFG_LOG_BLOCK_SZ*(fs: untyped): untyped =
    ((fs).cfg.log_block_size)

  template SPIFFS_CFG_PHYS_SZ*(fs: untyped): untyped =
    ((fs).cfg.phys_size)

  template SPIFFS_CFG_PHYS_ERASE_SZ*(fs: untyped): untyped =
    ((fs).cfg.phys_erase_block)

  template SPIFFS_CFG_PHYS_ADDR*(fs: untyped): untyped =
    ((fs).cfg.phys_addr)

##  total number of pages

template SPIFFS_MAX_PAGES*(fs: untyped): untyped =
  (SPIFFS_CFG_PHYS_SZ(fs) div SPIFFS_CFG_LOG_PAGE_SZ(fs))

##  total number of pages per block, including object lookup pages

template SPIFFS_PAGES_PER_BLOCK*(fs: untyped): untyped =
  (SPIFFS_CFG_LOG_BLOCK_SZ(fs) div SPIFFS_CFG_LOG_PAGE_SZ(fs))

##  number of object lookup pages per block

template SPIFFS_OBJ_LOOKUP_PAGES*(fs: untyped): untyped =
  (MAX(1, (SPIFFS_PAGES_PER_BLOCK(fs) * sizeof((spiffs_obj_id))) div
      SPIFFS_CFG_LOG_PAGE_SZ(fs)))

##  checks if page index belongs to object lookup

template SPIFFS_IS_LOOKUP_PAGE*(fs, pix: untyped): untyped =
  (((pix) mod SPIFFS_PAGES_PER_BLOCK(fs)) < SPIFFS_OBJ_LOOKUP_PAGES(fs))

##  number of object lookup entries in all object lookup pages

template SPIFFS_OBJ_LOOKUP_MAX_ENTRIES*(fs: untyped): untyped =
  (SPIFFS_PAGES_PER_BLOCK(fs) - SPIFFS_OBJ_LOOKUP_PAGES(fs))

##  converts a block to physical address

template SPIFFS_BLOCK_TO_PADDR*(fs, `block`: untyped): untyped =
  (SPIFFS_CFG_PHYS_ADDR(fs) + (`block`) * SPIFFS_CFG_LOG_BLOCK_SZ(fs))

##  converts a object lookup entry to page index

template SPIFFS_OBJ_LOOKUP_ENTRY_TO_PIX*(fs, `block`, entry: untyped): untyped =
  ((`block`) * SPIFFS_PAGES_PER_BLOCK(fs) + (SPIFFS_OBJ_LOOKUP_PAGES(fs) + entry))

##  converts a object lookup entry to physical address of corresponding page

template SPIFFS_OBJ_LOOKUP_ENTRY_TO_PADDR*(fs, `block`, entry: untyped): untyped =
  (SPIFFS_BLOCK_TO_PADDR(fs, `block`) +
      (SPIFFS_OBJ_LOOKUP_PAGES(fs) + entry) * SPIFFS_CFG_LOG_PAGE_SZ(fs))

##  converts a page to physical address

template SPIFFS_PAGE_TO_PADDR*(fs, page: untyped): untyped =
  (SPIFFS_CFG_PHYS_ADDR(fs) + (page) * SPIFFS_CFG_LOG_PAGE_SZ(fs))

##  converts a physical address to page

template SPIFFS_PADDR_TO_PAGE*(fs, `addr`: untyped): untyped =
  (((`addr`) - SPIFFS_CFG_PHYS_ADDR(fs)) div SPIFFS_CFG_LOG_PAGE_SZ(fs))

##  gives index in page for a physical address

template SPIFFS_PADDR_TO_PAGE_OFFSET*(fs, `addr`: untyped): untyped =
  (((`addr`) - SPIFFS_CFG_PHYS_ADDR(fs)) mod SPIFFS_CFG_LOG_PAGE_SZ(fs))

##  returns containing block for given page

template SPIFFS_BLOCK_FOR_PAGE*(fs, page: untyped): untyped =
  ((page) div SPIFFS_PAGES_PER_BLOCK(fs))

##  returns starting page for block

template SPIFFS_PAGE_FOR_BLOCK*(fs, `block`: untyped): untyped =
  ((`block`) * SPIFFS_PAGES_PER_BLOCK(fs))

##  converts page to entry in object lookup page

template SPIFFS_OBJ_LOOKUP_ENTRY_FOR_PAGE*(fs, page: untyped): untyped =
  ((page) mod SPIFFS_PAGES_PER_BLOCK(fs) - SPIFFS_OBJ_LOOKUP_PAGES(fs))

##  returns data size in a data page

template SPIFFS_DATA_PAGE_SIZE*(fs: untyped): untyped =
  (SPIFFS_CFG_LOG_PAGE_SZ(fs) - sizeof((spiffs_page_header)))

##  returns physical address for block's erase count,
##  always in the physical last entry of the last object lookup page

template SPIFFS_ERASE_COUNT_PADDR*(fs, bix: untyped): untyped =
  (SPIFFS_BLOCK_TO_PADDR(fs, bix) +
      SPIFFS_OBJ_LOOKUP_PAGES(fs) * SPIFFS_CFG_LOG_PAGE_SZ(fs) -
      sizeof((spiffs_obj_id)))

##  returns physical address for block's magic,
##  always in the physical second last entry of the last object lookup page

template SPIFFS_MAGIC_PADDR*(fs, bix: untyped): untyped =
  (SPIFFS_BLOCK_TO_PADDR(fs, bix) +
      SPIFFS_OBJ_LOOKUP_PAGES(fs) * SPIFFS_CFG_LOG_PAGE_SZ(fs) -
      sizeof((spiffs_obj_id) * 2))

##  checks if there is any room for magic in the object luts

template SPIFFS_CHECK_MAGIC_POSSIBLE*(fs: untyped): untyped =
  ((SPIFFS_OBJ_LOOKUP_MAX_ENTRIES(fs) mod
      (SPIFFS_CFG_LOG_PAGE_SZ(fs) div sizeof((spiffs_obj_id)))) *
      sizeof((spiffs_obj_id)) <=
      (SPIFFS_CFG_LOG_PAGE_SZ(fs) - sizeof((spiffs_obj_id) * 2)))

##  define helpers object
##  entries in an object header page index

template SPIFFS_OBJ_HDR_IX_LEN*(fs: untyped): untyped =
  ((SPIFFS_CFG_LOG_PAGE_SZ(fs) - sizeof((spiffs_page_object_ix_header))) div
      sizeof((spiffs_page_ix)))

##  entries in an object page index

template SPIFFS_OBJ_IX_LEN*(fs: untyped): untyped =
  ((SPIFFS_CFG_LOG_PAGE_SZ(fs) - sizeof((spiffs_page_object_ix))) div
      sizeof((spiffs_page_ix)))

##  object index entry for given data span index

template SPIFFS_OBJ_IX_ENTRY*(fs, spix: untyped): untyped =
  (if (spix) < SPIFFS_OBJ_HDR_IX_LEN(fs): (spix) else: (
      ((spix) - SPIFFS_OBJ_HDR_IX_LEN(fs)) mod SPIFFS_OBJ_IX_LEN(fs)))

##  object index span index number for given data span index or entry

template SPIFFS_OBJ_IX_ENTRY_SPAN_IX*(fs, spix: untyped): untyped =
  (if (spix) < SPIFFS_OBJ_HDR_IX_LEN(fs): 0 else: (
      1 + ((spix) - SPIFFS_OBJ_HDR_IX_LEN(fs)) div SPIFFS_OBJ_IX_LEN(fs)))

##  get data span index for object index span index

template SPIFFS_DATA_SPAN_IX_FOR_OBJ_IX_SPAN_IX*(fs, spix: untyped): untyped =
  (if (spix) == 0: 0 else: (SPIFFS_OBJ_HDR_IX_LEN(fs) +
      (((spix) - 1) * SPIFFS_OBJ_IX_LEN(fs))))

when SPIFFS_FILEHDL_OFFSET:
  template SPIFFS_FH_OFFS*(fs, fh: untyped): untyped =
    (if (fh) != 0: ((fh) + (fs).cfg.fh_ix_offset) else: 0)

  template SPIFFS_FH_UNOFFS*(fs, fh: untyped): untyped =
    (if (fh) != 0: ((fh) - (fs).cfg.fh_ix_offset) else: 0)

else:
  template SPIFFS_FH_OFFS*(fs, fh: untyped): untyped =
    (fh)

  template SPIFFS_FH_UNOFFS*(fs, fh: untyped): untyped =
    (fh)

const
  SPIFFS_OP_T_OBJ_LU* = (0 shl 0)
  SPIFFS_OP_T_OBJ_LU2* = (1 shl 0)
  SPIFFS_OP_T_OBJ_IX* = (2 shl 0)
  SPIFFS_OP_T_OBJ_DA* = (3 shl 0)
  SPIFFS_OP_C_DELE* = (0 shl 2)
  SPIFFS_OP_C_UPDT* = (1 shl 2)
  SPIFFS_OP_C_MOVS* = (2 shl 2)
  SPIFFS_OP_C_MOVD* = (3 shl 2)
  SPIFFS_OP_C_FLSH* = (4 shl 2)
  SPIFFS_OP_C_READ* = (5 shl 2)
  SPIFFS_OP_C_WRTHRU* = (6 shl 2)
  SPIFFS_OP_TYPE_MASK* = (3 shl 0)
  SPIFFS_OP_COM_MASK* = (7 shl 2)

##  if 0, this page is written to, else clean

const
  SPIFFS_PH_FLAG_USED* = (1 shl 0)

##  if 0, writing is finalized, else under modification

const
  SPIFFS_PH_FLAG_FINAL* = (1 shl 1)

##  if 0, this is an index page, else a data page

const
  SPIFFS_PH_FLAG_INDEX* = (1 shl 2)

##  if 0, page is deleted, else valid

const
  SPIFFS_PH_FLAG_DELET* = (1 shl 7)

##  if 0, this index header is being deleted

const
  SPIFFS_PH_FLAG_IXDELE* = (1 shl 6)

template SPIFFS_CHECK_MOUNT*(fs: untyped): untyped =
  ((fs).mounted != 0)

template SPIFFS_CHECK_CFG*(fs: untyped): untyped =
  ((fs).config_magic == SPIFFS_CONFIG_MAGIC)

template SPIFFS_CHECK_RES*(res: untyped): void =
  while true:
  if (res) < SPIFFS_OK: return res
  if not 0: break

template SPIFFS_API_CHECK_MOUNT*(fs: untyped): void =
  if not SPIFFS_CHECK_MOUNT((fs)):
  (fs).err_code = SPIFFS_ERR_NOT_MOUNTED
  return SPIFFS_ERR_NOT_MOUNTED

template SPIFFS_API_CHECK_CFG*(fs: untyped): void =
  if not SPIFFS_CHECK_CFG((fs)):
  (fs).err_code = SPIFFS_ERR_NOT_CONFIGURED
  return SPIFFS_ERR_NOT_CONFIGURED

template SPIFFS_API_CHECK_RES*(fs, res: untyped): void =
  if (res) < SPIFFS_OK:
  (fs).err_code = (res)
  return res

template SPIFFS_API_CHECK_RES_UNLOCK*(fs, res: untyped): void =
  if (res) < SPIFFS_OK:
  (fs).err_code = (res)
  SPIFFS_UNLOCK(fs)
  return res

##  check id, only visit matching objec ids

const
  SPIFFS_VIS_CHECK_ID* = (1 shl 0)

##  report argument object id to visitor - else object lookup id is reported

const
  SPIFFS_VIS_CHECK_PH* = (1 shl 1)

##  stop searching at end of all look up pages

const
  SPIFFS_VIS_NO_WRAP* = (1 shl 2)

when SPIFFS_HAL_CALLBACK_EXTRA:
  template SPIFFS_HAL_WRITE*(_fs, _paddr, _len, _src: untyped): untyped =
    (_fs).cfg.hal_write_f((_fs), (_paddr), (_len), (_src))

  template SPIFFS_HAL_READ*(_fs, _paddr, _len, _dst: untyped): untyped =
    (_fs).cfg.hal_read_f((_fs), (_paddr), (_len), (_dst))

  template SPIFFS_HAL_ERASE*(_fs, _paddr, _len: untyped): untyped =
    (_fs).cfg.hal_erase_f((_fs), (_paddr), (_len))

else:
  template SPIFFS_HAL_WRITE*(_fs, _paddr, _len, _src: untyped): untyped =
    (_fs).cfg.hal_write_f((_paddr), (_len), (_src))

  template SPIFFS_HAL_READ*(_fs, _paddr, _len, _dst: untyped): untyped =
    (_fs).cfg.hal_read_f((_paddr), (_len), (_dst))

  template SPIFFS_HAL_ERASE*(_fs, _paddr, _len: untyped): untyped =
    (_fs).cfg.hal_erase_f((_paddr), (_len))

when SPIFFS_CACHE:
  const
    SPIFFS_CACHE_FLAG_DIRTY* = (1 shl 0)
    SPIFFS_CACHE_FLAG_WRTHRU* = (1 shl 1)
    SPIFFS_CACHE_FLAG_OBJLU* = (1 shl 2)
    SPIFFS_CACHE_FLAG_OBJIX* = (1 shl 3)
    SPIFFS_CACHE_FLAG_DATA* = (1 shl 4)
    SPIFFS_CACHE_FLAG_TYPE_WR* = (1 shl 7)
  template SPIFFS_CACHE_PAGE_SIZE*(fs: untyped): untyped =
    (sizeof((spiffs_cache_page)) + SPIFFS_CFG_LOG_PAGE_SZ(fs))

  template spiffs_get_cache*(fs: untyped): untyped =
    (cast[ptr spiffs_cache](((fs).cache)))

  template spiffs_get_cache_page_hdr*(fs, c, ix: untyped): untyped =
    (cast[ptr spiffs_cache_page]((addr(((c).cpages[
        (ix) * SPIFFS_CACHE_PAGE_SIZE(fs)])))))

  template spiffs_get_cache_page*(fs, c, ix: untyped): untyped =
    (cast[ptr u8_t]((addr(((c).cpages[(ix) * SPIFFS_CACHE_PAGE_SIZE(fs)])))) +
        sizeof((spiffs_cache_page)))

  ##  cache page struct
  type
    INNER_C_STRUCT_1509216849* {.importcpp: "no_name", header: "spiffs_nucleus.h",
                                bycopy.} = object
      pix* {.importc: "pix".}: spiffs_page_ix ##  read cache page index
    
  type
    INNER_C_STRUCT_1387632981* {.importcpp: "no_name", header: "spiffs_nucleus.h",
                                bycopy.} = object
      obj_id* {.importc: "obj_id".}: spiffs_obj_id ##  write cache
      ##  offset in cache page
      offset* {.importc: "offset".}: u32_t ##  size of cache page
      size* {.importc: "size".}: u16_t

  type
    INNER_C_UNION_4242944148* {.importcpp: "no_name", header: "spiffs_nucleus.h",
                               bycopy.} = object {.union.}
      ano_537765156* {.importc: "ano_537765156".}: INNER_C_STRUCT_1509216849 ##  type read cache
      ano_3241979246* {.importc: "ano_3241979246".}: INNER_C_STRUCT_1387632981

  type
    spiffs_cache_page* {.importcpp: "spiffs_cache_page",
                        header: "spiffs_nucleus.h", bycopy.} = object
      flags* {.importc: "flags".}: u8_t ##  cache flags
      ##  cache page index
      ix* {.importc: "ix".}: u8_t ##  last access of this cache page
      last_access* {.importc: "last_access".}: u32_t
      ano_3258502254* {.importc: "ano_3258502254".}: INNER_C_UNION_4242944148



  ##  cache struct
  type
    spiffs_cache* {.importcpp: "spiffs_cache", header: "spiffs_nucleus.h", bycopy.} = object
      cpage_count* {.importc: "cpage_count".}: u8_t
      last_access* {.importc: "last_access".}: u32_t
      cpage_use_map* {.importc: "cpage_use_map".}: u32_t
      cpage_use_mask* {.importc: "cpage_use_mask".}: u32_t
      cpages* {.importc: "cpages".}: ptr u8_t

##  spiffs nucleus file descriptor

type
  spiffs_fd* {.importcpp: "spiffs_fd", header: "spiffs_nucleus.h", bycopy.} = object
    fs* {.importc: "fs".}: ptr spiffs ##  the filesystem of this descriptor
    ##  number of file descriptor - if 0, the file descriptor is closed
    file_nbr* {.importc: "file_nbr".}: spiffs_file ##  object id - if SPIFFS_OBJ_ID_ERASED, the file was deleted
    obj_id* {.importc: "obj_id".}: spiffs_obj_id ##  size of the file
    size* {.importc: "size".}: u32_t ##  cached object index header page index
    objix_hdr_pix* {.importc: "objix_hdr_pix".}: spiffs_page_ix ##  cached offset object index page index
    cursor_objix_pix* {.importc: "cursor_objix_pix".}: spiffs_page_ix ##  cached offset object index span index
    cursor_objix_spix* {.importc: "cursor_objix_spix".}: spiffs_span_ix ##  current absolute offset
    offset* {.importc: "offset".}: u32_t ##  current file descriptor offset (cached)
    fdoffset* {.importc: "fdoffset".}: u32_t ##  fd flags
    flags* {.importc: "flags".}: spiffs_flags
    cache_page* {.importc: "cache_page".}: ptr spiffs_cache_page ##  djb2 hash of filename
    name_hash* {.importc: "name_hash".}: u32_t ##  hit score (score == 0 indicates never used fd)
    score* {.importc: "score".}: u16_t ##  spiffs index map, if 0 it means unmapped
    ix_map* {.importc: "ix_map".}: ptr spiffs_ix_map


##  object structs
##  page header, part of each page except object lookup pages
##  NB: this is always aligned when the data page is an object index,
##  as in this case struct spiffs_page_object_ix is used

type
  spiffs_page_header* {.importcpp: "spiffs_page_header",
                       header: "spiffs_nucleus.h", bycopy.} = object
    obj_id* {.importc: "obj_id".}: spiffs_obj_id ##  object id
    ##  object span index
    span_ix* {.importc: "span_ix".}: spiffs_span_ix ##  flags
    flags* {.importc: "flags".}: u8_t


##  object index header page header

type
  spiffs_page_object_ix_header* {.importcpp: "spiffs_page_object_ix_header",
                                 header: "spiffs_nucleus.h", bycopy.} = object
    p_hdr* {.importc: "p_hdr".}: spiffs_page_header ##  common page header
    ##  alignment
    _align* {.importc: "_align".}: array[4 -
        (if (sizeof((spiffs_page_header)) and 3) == 0: 4 else: (
        sizeof((spiffs_page_header)) and 3)), u8_t] ##  size of object
    size* {.importc: "size".}: u32_t ##  type of object
    `type`* {.importc: "type".}: spiffs_obj_type ##  name of object
    name* {.importc: "name".}: array[SPIFFS_OBJ_NAME_LEN, u8_t] ##  metadata. not interpreted by SPIFFS in any way.
    meta* {.importc: "meta".}: array[SPIFFS_OBJ_META_LEN, u8_t]


##  object index page header

type
  spiffs_page_object_ix* {.importcpp: "spiffs_page_object_ix",
                          header: "spiffs_nucleus.h", bycopy.} = object
    p_hdr* {.importc: "p_hdr".}: spiffs_page_header
    _align* {.importc: "_align".}: array[4 -
        (if (sizeof((spiffs_page_header)) and 3) == 0: 4 else: (
        sizeof((spiffs_page_header)) and 3)), u8_t]


##  callback func for object lookup visitor

type
  spiffs_visitor_f* = proc (fs: ptr spiffs; id: spiffs_obj_id; bix: spiffs_block_ix;
                         ix_entry: cint; user_const_p: pointer; user_var_p: pointer): s32_t {.
      cdecl.}

when SPIFFS_CACHE:
  template _spiffs_rd*(fs, op, fh, `addr`, len, dst: untyped): untyped =
    spiffs_phys_rd((fs), (op), (fh), (`addr`), (len), (dst))

  template _spiffs_wr*(fs, op, fh, `addr`, len, src: untyped): untyped =
    spiffs_phys_wr((fs), (op), (fh), (`addr`), (len), (src))

else:
  template _spiffs_rd*(fs, op, fh, `addr`, len, dst: untyped): untyped =
    spiffs_phys_rd((fs), (`addr`), (len), (dst))

  template _spiffs_wr*(fs, op, fh, `addr`, len, src: untyped): untyped =
    spiffs_phys_wr((fs), (`addr`), (len), (src))

when not defined(MIN):
  template MIN*(a, b: untyped): untyped =
    (if (a) < (b): (a) else: (b))

when not defined(MAX):
  template MAX*(a, b: untyped): untyped =
    (if (a) > (b): (a) else: (b))

##  ---------------

proc spiffs_phys_rd*(fs: ptr spiffs; op: u8_t; fh: spiffs_file; `addr`: u32_t; len: u32_t;
                    dst: ptr u8_t): s32_t {.cdecl, importcpp: "spiffs_phys_rd(@)",
                                        header: "spiffs_nucleus.h".}
proc spiffs_phys_wr*(fs: ptr spiffs; op: u8_t; fh: spiffs_file; `addr`: u32_t; len: u32_t;
                    src: ptr u8_t): s32_t {.cdecl, importcpp: "spiffs_phys_wr(@)",
                                        header: "spiffs_nucleus.h".}
proc spiffs_phys_cpy*(fs: ptr spiffs; fh: spiffs_file; dst: u32_t; src: u32_t; len: u32_t): s32_t {.
    cdecl, importcpp: "spiffs_phys_cpy(@)", header: "spiffs_nucleus.h".}
proc spiffs_phys_count_free_blocks*(fs: ptr spiffs): s32_t {.cdecl,
    importcpp: "spiffs_phys_count_free_blocks(@)", header: "spiffs_nucleus.h".}
proc spiffs_obj_lu_find_entry_visitor*(fs: ptr spiffs;
                                      starting_block: spiffs_block_ix;
                                      starting_lu_entry: cint; flags: u8_t;
                                      obj_id: spiffs_obj_id; v: spiffs_visitor_f;
                                      user_const_p: pointer; user_var_p: pointer;
                                      block_ix: ptr spiffs_block_ix;
                                      lu_entry: ptr cint): s32_t {.cdecl,
    importcpp: "spiffs_obj_lu_find_entry_visitor(@)", header: "spiffs_nucleus.h".}
proc spiffs_erase_block*(fs: ptr spiffs; bix: spiffs_block_ix): s32_t {.cdecl,
    importcpp: "spiffs_erase_block(@)", header: "spiffs_nucleus.h".}
proc spiffs_probe*(cfg: ptr spiffs_config): s32_t {.cdecl,
    importcpp: "spiffs_probe(@)", header: "spiffs_nucleus.h".}
##  SPIFFS_USE_MAGIC && SPIFFS_USE_MAGIC_LENGTH
##  ---------------

proc spiffs_obj_lu_scan*(fs: ptr spiffs): s32_t {.cdecl,
    importcpp: "spiffs_obj_lu_scan(@)", header: "spiffs_nucleus.h".}
proc spiffs_obj_lu_find_free_obj_id*(fs: ptr spiffs; obj_id: ptr spiffs_obj_id;
                                    conflicting_name: ptr u8_t): s32_t {.cdecl,
    importcpp: "spiffs_obj_lu_find_free_obj_id(@)", header: "spiffs_nucleus.h".}
proc spiffs_obj_lu_find_free*(fs: ptr spiffs; starting_block: spiffs_block_ix;
                             starting_lu_entry: cint;
                             block_ix: ptr spiffs_block_ix; lu_entry: ptr cint): s32_t {.
    cdecl, importcpp: "spiffs_obj_lu_find_free(@)", header: "spiffs_nucleus.h".}
proc spiffs_obj_lu_find_id*(fs: ptr spiffs; starting_block: spiffs_block_ix;
                           starting_lu_entry: cint; obj_id: spiffs_obj_id;
                           block_ix: ptr spiffs_block_ix; lu_entry: ptr cint): s32_t {.
    cdecl, importcpp: "spiffs_obj_lu_find_id(@)", header: "spiffs_nucleus.h".}
proc spiffs_obj_lu_find_id_and_span*(fs: ptr spiffs; obj_id: spiffs_obj_id;
                                    spix: spiffs_span_ix;
                                    exclusion_pix: spiffs_page_ix;
                                    pix: ptr spiffs_page_ix): s32_t {.cdecl,
    importcpp: "spiffs_obj_lu_find_id_and_span(@)", header: "spiffs_nucleus.h".}
proc spiffs_obj_lu_find_id_and_span_by_phdr*(fs: ptr spiffs; obj_id: spiffs_obj_id;
    spix: spiffs_span_ix; exclusion_pix: spiffs_page_ix; pix: ptr spiffs_page_ix): s32_t {.
    cdecl, importcpp: "spiffs_obj_lu_find_id_and_span_by_phdr(@)",
    header: "spiffs_nucleus.h".}
##  ---------------

proc spiffs_page_allocate_data*(fs: ptr spiffs; obj_id: spiffs_obj_id;
                               ph: ptr spiffs_page_header; data: ptr u8_t; len: u32_t;
                               page_offs: u32_t; finalize: u8_t;
                               pix: ptr spiffs_page_ix): s32_t {.cdecl,
    importcpp: "spiffs_page_allocate_data(@)", header: "spiffs_nucleus.h".}
proc spiffs_page_move*(fs: ptr spiffs; fh: spiffs_file; page_data: ptr u8_t;
                      obj_id: spiffs_obj_id; page_hdr: ptr spiffs_page_header;
                      src_pix: spiffs_page_ix; dst_pix: ptr spiffs_page_ix): s32_t {.
    cdecl, importcpp: "spiffs_page_move(@)", header: "spiffs_nucleus.h".}
proc spiffs_page_delete*(fs: ptr spiffs; pix: spiffs_page_ix): s32_t {.cdecl,
    importcpp: "spiffs_page_delete(@)", header: "spiffs_nucleus.h".}
##  ---------------

proc spiffs_object_create*(fs: ptr spiffs; obj_id: spiffs_obj_id; name: ptr u8_t;
                          meta: ptr u8_t; `type`: spiffs_obj_type;
                          objix_hdr_pix: ptr spiffs_page_ix): s32_t {.cdecl,
    importcpp: "spiffs_object_create(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_update_index_hdr*(fs: ptr spiffs; fd: ptr spiffs_fd;
                                    obj_id: spiffs_obj_id;
                                    objix_hdr_pix: spiffs_page_ix;
                                    new_objix_hdr_data: ptr u8_t; name: ptr u8_t;
                                    meta: ptr u8_t; size: u32_t;
                                    new_pix: ptr spiffs_page_ix): s32_t {.cdecl,
    importcpp: "spiffs_object_update_index_hdr(@)", header: "spiffs_nucleus.h".}
proc spiffs_populate_ix_map*(fs: ptr spiffs; fd: ptr spiffs_fd; vec_entry_start: u32_t;
                            vec_entry_end: u32_t): s32_t {.cdecl,
    importcpp: "spiffs_populate_ix_map(@)", header: "spiffs_nucleus.h".}
proc spiffs_cb_object_event*(fs: ptr spiffs; objix: ptr spiffs_page_object_ix;
                            ev: cint; obj_id: spiffs_obj_id; spix: spiffs_span_ix;
                            new_pix: spiffs_page_ix; new_size: u32_t) {.cdecl,
    importcpp: "spiffs_cb_object_event(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_open_by_id*(fs: ptr spiffs; obj_id: spiffs_obj_id;
                              f: ptr spiffs_fd; flags: spiffs_flags;
                              mode: spiffs_mode): s32_t {.cdecl,
    importcpp: "spiffs_object_open_by_id(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_open_by_page*(fs: ptr spiffs; pix: spiffs_page_ix;
                                f: ptr spiffs_fd; flags: spiffs_flags;
                                mode: spiffs_mode): s32_t {.cdecl,
    importcpp: "spiffs_object_open_by_page(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_append*(fd: ptr spiffs_fd; offset: u32_t; data: ptr u8_t; len: u32_t): s32_t {.
    cdecl, importcpp: "spiffs_object_append(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_modify*(fd: ptr spiffs_fd; offset: u32_t; data: ptr u8_t; len: u32_t): s32_t {.
    cdecl, importcpp: "spiffs_object_modify(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_read*(fd: ptr spiffs_fd; offset: u32_t; len: u32_t; dst: ptr u8_t): s32_t {.
    cdecl, importcpp: "spiffs_object_read(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_truncate*(fd: ptr spiffs_fd; new_len: u32_t; remove_object: u8_t): s32_t {.
    cdecl, importcpp: "spiffs_object_truncate(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_find_object_index_header_by_name*(fs: ptr spiffs;
    name: array[SPIFFS_OBJ_NAME_LEN, u8_t]; pix: ptr spiffs_page_ix): s32_t {.cdecl,
    importcpp: "spiffs_object_find_object_index_header_by_name(@)",
    header: "spiffs_nucleus.h".}
##  ---------------

proc spiffs_gc_check*(fs: ptr spiffs; len: u32_t): s32_t {.cdecl,
    importcpp: "spiffs_gc_check(@)", header: "spiffs_nucleus.h".}
proc spiffs_gc_erase_page_stats*(fs: ptr spiffs; bix: spiffs_block_ix): s32_t {.cdecl,
    importcpp: "spiffs_gc_erase_page_stats(@)", header: "spiffs_nucleus.h".}
proc spiffs_gc_find_candidate*(fs: ptr spiffs;
                              block_candidate: ptr ptr spiffs_block_ix;
                              candidate_count: ptr cint; fs_crammed: char): s32_t {.
    cdecl, importcpp: "spiffs_gc_find_candidate(@)", header: "spiffs_nucleus.h".}
proc spiffs_gc_clean*(fs: ptr spiffs; bix: spiffs_block_ix): s32_t {.cdecl,
    importcpp: "spiffs_gc_clean(@)", header: "spiffs_nucleus.h".}
proc spiffs_gc_quick*(fs: ptr spiffs; max_free_pages: u16_t): s32_t {.cdecl,
    importcpp: "spiffs_gc_quick(@)", header: "spiffs_nucleus.h".}
##  ---------------

proc spiffs_fd_find_new*(fs: ptr spiffs; fd: ptr ptr spiffs_fd; name: cstring): s32_t {.
    cdecl, importcpp: "spiffs_fd_find_new(@)", header: "spiffs_nucleus.h".}
proc spiffs_fd_return*(fs: ptr spiffs; f: spiffs_file): s32_t {.cdecl,
    importcpp: "spiffs_fd_return(@)", header: "spiffs_nucleus.h".}
proc spiffs_fd_get*(fs: ptr spiffs; f: spiffs_file; fd: ptr ptr spiffs_fd): s32_t {.cdecl,
    importcpp: "spiffs_fd_get(@)", header: "spiffs_nucleus.h".}
proc spiffs_fd_temporal_cache_rehash*(fs: ptr spiffs; old_path: cstring;
                                     new_path: cstring) {.cdecl,
    importcpp: "spiffs_fd_temporal_cache_rehash(@)", header: "spiffs_nucleus.h".}
proc spiffs_cache_init*(fs: ptr spiffs) {.cdecl, importcpp: "spiffs_cache_init(@)",
                                      header: "spiffs_nucleus.h".}
proc spiffs_cache_drop_page*(fs: ptr spiffs; pix: spiffs_page_ix) {.cdecl,
    importcpp: "spiffs_cache_drop_page(@)", header: "spiffs_nucleus.h".}
proc spiffs_cache_page_allocate_by_fd*(fs: ptr spiffs; fd: ptr spiffs_fd): ptr spiffs_cache_page {.
    cdecl, importcpp: "spiffs_cache_page_allocate_by_fd(@)",
    header: "spiffs_nucleus.h".}
proc spiffs_cache_fd_release*(fs: ptr spiffs; cp: ptr spiffs_cache_page) {.cdecl,
    importcpp: "spiffs_cache_fd_release(@)", header: "spiffs_nucleus.h".}
proc spiffs_cache_page_get_by_fd*(fs: ptr spiffs; fd: ptr spiffs_fd): ptr spiffs_cache_page {.
    cdecl, importcpp: "spiffs_cache_page_get_by_fd(@)", header: "spiffs_nucleus.h".}
proc spiffs_lookup_consistency_check*(fs: ptr spiffs; check_all_objects: u8_t): s32_t {.
    cdecl, importcpp: "spiffs_lookup_consistency_check(@)",
    header: "spiffs_nucleus.h".}
proc spiffs_page_consistency_check*(fs: ptr spiffs): s32_t {.cdecl,
    importcpp: "spiffs_page_consistency_check(@)", header: "spiffs_nucleus.h".}
proc spiffs_object_index_consistency_check*(fs: ptr spiffs): s32_t {.cdecl,
    importcpp: "spiffs_object_index_consistency_check(@)",
    header: "spiffs_nucleus.h".}
##  memcpy macro,
##  checked in test builds, otherwise plain memcpy (unless already defined)

when defined(_SPIFFS_TEST):
  template _SPIFFS_MEMCPY*(__d, __s, __l: untyped): void =
  while true:
    var __a1: intptr_t
    var __a2: intptr_t
    var __b1: intptr_t
    var __b2: intptr_t
    if __a1 <= __b2 and __b1 <= __a2:
      printf("FATAL OVERLAP: memcpy from %lx..%lx to %lx..%lx\x0A", __a1, __a2,
             __b1, __b2)
      ERREXIT()

    memcpy((__d), (__s), (__l))
    if not 0: break
  
else:
  when not defined(_SPIFFS_MEMCPY):
    template _SPIFFS_MEMCPY*(__d, __s, __l: untyped): void =
    while true:
      memcpy((__d), (__s), (__l))
      if not 0: break
    