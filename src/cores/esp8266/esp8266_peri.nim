##  
##   esp8266_peri.h - Peripheral registers exposed in more AVR style for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

template ESP8266_REG*(`addr`: untyped): untyped =
  (cast[ptr uint32]((0x60000000 + (`addr`))))[]

template ESP8266_DREG*(`addr`: untyped): untyped =
  (cast[ptr uint32]((0x3FF00000 + (`addr`))))[]

const
  ESP8266_CLOCK* = 80000000

template i2c_readReg_Mask*(`block`, host_id, reg_add, Msb, Lsb: untyped): untyped =
  rom_i2c_readReg_Mask(`block`, host_id, reg_add, Msb, Lsb)

template i2c_readReg_Mask_def*(`block`, reg_add: untyped): untyped =
  i2c_readReg_Mask(`block`, block_hostid, reg_add, reg_add_msb, reg_add_lsb)

template i2c_writeReg_Mask*(`block`, host_id, reg_add, Msb, Lsb, indata: untyped): untyped =
  rom_i2c_writeReg_Mask(`block`, host_id, reg_add, Msb, Lsb, indata)

template i2c_writeReg_Mask_def*(`block`, reg_add, indata: untyped): untyped =
  i2c_writeReg_Mask(`block`, block_hostid, reg_add, reg_add_msb, reg_add_lsb, indata)

## CPU Register

const
  CPU2X* = ESP8266_DREG(0x00000014) ## when bit 0 is set, F_CPU = 160MHz

## OTP Registers

const
  MAC0* = ESP8266_DREG(0x00000050)
  MAC1* = ESP8266_DREG(0x00000054)
  CHIPID* = ESP8266_DREG(0x00000058)

## GPIO (0-15) Control Registers

const
  GPO* = ESP8266_REG(0x00000300) ## GPIO_OUT R/W (Output Level)
  GPOS* = ESP8266_REG(0x00000304) ## GPIO_OUT_SET WO
  GPOC* = ESP8266_REG(0x00000308) ## GPIO_OUT_CLR WO
  GPE* = ESP8266_REG(0x0000030C) ## GPIO_ENABLE R/W (Enable)
  GPES* = ESP8266_REG(0x00000310) ## GPIO_ENABLE_SET WO
  GPEC* = ESP8266_REG(0x00000314) ## GPIO_ENABLE_CLR WO
  GPI* = ESP8266_REG(0x00000318) ## GPIO_IN RO (Read Input Level)
  GPIE* = ESP8266_REG(0x0000031C) ## GPIO_STATUS R/W (Interrupt Enable)
  GPIES* = ESP8266_REG(0x00000320) ## GPIO_STATUS_SET WO
  GPIEC* = ESP8266_REG(0x00000324) ## GPIO_STATUS_CLR WO

template GPOP*(p: untyped): untyped =
  ((GPO and (1 shl ((p) and 0x0000000F))) != 0)

template GPEP*(p: untyped): untyped =
  ((GPE and (1 shl ((p) and 0x0000000F))) != 0)

template GPIP*(p: untyped): untyped =
  ((GPI and (1 shl ((p) and 0x0000000F))) != 0)

template GPIEP*(p: untyped): untyped =
  ((GPIE and (1 shl ((p) and 0x0000000F))) != 0)

## GPIO (0-15) PIN Control Registers

template GPC*(p: untyped): untyped =
  ESP8266_REG(0x00000328 + ((p and 0x0000000F) * 4))

const
  GPC0* = ESP8266_REG(0x00000328) ## GPIO_PIN0
  GPC1* = ESP8266_REG(0x0000032C) ## GPIO_PIN1
  GPC2* = ESP8266_REG(0x00000330) ## GPIO_PIN2
  GPC3* = ESP8266_REG(0x00000334) ## GPIO_PIN3
  GPC4* = ESP8266_REG(0x00000338) ## GPIO_PIN4
  GPC5* = ESP8266_REG(0x0000033C) ## GPIO_PIN5
  GPC6* = ESP8266_REG(0x00000340) ## GPIO_PIN6
  GPC7* = ESP8266_REG(0x00000344) ## GPIO_PIN7
  GPC8* = ESP8266_REG(0x00000348) ## GPIO_PIN8
  GPC9* = ESP8266_REG(0x0000034C) ## GPIO_PIN9
  GPC10* = ESP8266_REG(0x00000350) ## GPIO_PIN10
  GPC11* = ESP8266_REG(0x00000354) ## GPIO_PIN11
  GPC12* = ESP8266_REG(0x00000358) ## GPIO_PIN12
  GPC13* = ESP8266_REG(0x0000035C) ## GPIO_PIN13
  GPC14* = ESP8266_REG(0x00000360) ## GPIO_PIN14
  GPC15* = ESP8266_REG(0x00000364) ## GPIO_PIN15

## GPIO (0-15) PIN Control Bits

const
  GPCWE* = 10
  GPCI* = 7
  GPCD* = 2
  GPCS* = 0
  GPMUX* = ESP8266_REG(0x00000800)

## GPIO (0-15) PIN Function Registers

const
  GPF0* = ESP8266_REG(0x00000834)
  GPF1* = ESP8266_REG(0x00000818)
  GPF2* = ESP8266_REG(0x00000838)
  GPF3* = ESP8266_REG(0x00000814)
  GPF4* = ESP8266_REG(0x0000083C)
  GPF5* = ESP8266_REG(0x00000840)
  GPF6* = ESP8266_REG(0x0000081C)
  GPF7* = ESP8266_REG(0x00000820)
  GPF8* = ESP8266_REG(0x00000824)
  GPF9* = ESP8266_REG(0x00000828)
  GPF10* = ESP8266_REG(0x0000082C)
  GPF11* = ESP8266_REG(0x00000830)
  GPF12* = ESP8266_REG(0x00000804)
  GPF13* = ESP8266_REG(0x00000808)
  GPF14* = ESP8266_REG(0x0000080C)
  GPF15* = ESP8266_REG(0x00000810)

var esp8266_gpioToFn* {.importcpp: "esp8266_gpioToFn", header: "esp8266_peri.h".}: array[
    16, uint8]

template GPF*(p: untyped): untyped =
  ESP8266_REG(0x00000800 + esp8266_gpioToFn[(p and 0x0000000F)])

## GPIO (0-15) PIN Function Bits

const
  GPFSOE* = 0
  GPFSS* = 1
  GPFSPD* = 2
  GPFSPU* = 3
  GPFFS0* = 4
  GPFFS1* = 5
  GPFPD* = 6
  GPFPU* = 7
  GPFFS2* = 8

template GPFFS*(f: untyped): untyped =
  (((((f) and 4) != 0) shl GPFFS2) or ((((f) and 2) != 0) shl GPFFS1) or
      ((((f) and 1) != 0) shl GPFFS0))

template GPFFS_GPIO*(p: untyped): untyped =
  if ((p) == 0 or (p) == 2 or (p) == 4 or (p) == 5):
    0 
  else:
    if ((p) == 16):
        1 
    else: 
        3

template GPFFS_BUS*(p: untyped): untyped =
  if ((p) == 1 or (p) == 3):
    0 
  else:
    if ((p) == 2 or (p) == 12 or (p) == 13 or (p) == 14 or (p) == 15):
        2 
    else:
        if ((p) == 0):
            4 
        else:
            1

## GPIO 16 Control Registers

const
  GP16O* = ESP8266_REG(0x00000768)
  GP16E* = ESP8266_REG(0x00000774)
  GP16I* = ESP8266_REG(0x0000078C)

## GPIO 16 PIN Control Register

const
  GP16C* = ESP8266_REG(0x00000790)
  GPC16* = GP16C

## GPIO 16 PIN Function Register

const
  GP16F* = ESP8266_REG(0x000007A0)
  GPF16* = GP16F

## GPIO 16 PIN Function Bits

const
  GP16FFS0* = 0
  GP16FFS1* = 1
  GP16FPD* = 3
  GP16FSPD* = 5
  GP16FFS2* = 6

template GP16FFS*(f: untyped): untyped =
  (((f) and 0x00000003) or (((f) and 0x00000004) shl 4))

## Timer 1 Registers (23bit CountDown Timer)

const
  T1L* = ESP8266_REG(0x00000600) ## Load Value (Starting Value of Counter) 23bit (0-8388607)
  T1V* = ESP8266_REG(0x00000604) ## (RO) Current Value
  T1C* = ESP8266_REG(0x00000608) ## Control Register
  T1I* = ESP8266_REG(0x0000060C) ## Interrupt Status Register (1bit) write to clear
                              ## edge interrupt enable register
  TEIE* = ESP8266_DREG(0x00000004)
  TEIE1* = 0x00000002

## Timer 2 Registers (32bit CountUp Timer)

const
  T2L* = ESP8266_REG(0x00000620) ## Load Value (Starting Value of Counter)
  T2V* = ESP8266_REG(0x00000624) ## (RO) Current Value
  T2C* = ESP8266_REG(0x00000628) ## Control Register
  T2I* = ESP8266_REG(0x0000062C) ## Interrupt Status Register (1bit) write to clear
  T2A* = ESP8266_REG(0x00000630) ## Alarm Value

## Timer Control Bits

const
  TCIS* = 8
  TCTE* = 7
  TCAR* = 6
  TCPD* = 2
  TCIT* = 0

## RTC Registers

const
  RTCSV* = ESP8266_REG(0x00000704) ## RTC SLEEP COUNTER Target Value
  RTCCV* = ESP8266_REG(0x0000071C) ## RTC SLEEP COUNTER Value
  RTCIS* = ESP8266_REG(0x00000720) ## RTC INT Status
  RTCIC* = ESP8266_REG(0x00000724) ## RTC INT Clear
  RTCIE* = ESP8266_REG(0x00000728) ## RTC INT Enable
  RTC_USER_MEM* = (cast[ptr uint32](0x60001200))

## IO SWAP Register

const
  IOSWAP* = ESP8266_DREG(0x00000028)
  IOSWAPU* = 0
  IOSWAPS* = 1
  IOSWAPU0* = 2
  IOSWAPU1* = 3
  IOSWAPHS* = 5
  IOSWAP2HS* = 6
  IOSWAP2CS* = 7

## UART INT Status

const
  UIS* = ESP8266_DREG(0x00020020)
  UIS0* = 0
  UIS1* = 2

## UART 0 Registers

const
  U0F* = ESP8266_REG(0x00000000) ## UART FIFO
  U0IR* = ESP8266_REG(0x00000004) ## INT_RAW
  U0IS* = ESP8266_REG(0x00000008) ## INT_STATUS
  U0IE* = ESP8266_REG(0x0000000C) ## INT_ENABLE
  U0IC* = ESP8266_REG(0x00000010) ## INT_CLEAR
  U0D* = ESP8266_REG(0x00000014) ## CLKDIV
  U0A* = ESP8266_REG(0x00000018) ## AUTOBAUD
  U0S* = ESP8266_REG(0x0000001C) ## STATUS
  U0C0* = ESP8266_REG(0x00000020) ## CONF0
  U0C1* = ESP8266_REG(0x00000024) ## CONF1
  U0LP* = ESP8266_REG(0x00000028) ## LOW_PULSE
  U0HP* = ESP8266_REG(0x0000002C) ## HIGH_PULSE
  U0PN* = ESP8266_REG(0x00000030) ## PULSE_NUM
  U0DT* = ESP8266_REG(0x00000078) ## DATE
  U0ID* = ESP8266_REG(0x0000007C) ## ID

## UART 1 Registers

const
  U1F* = ESP8266_REG(0x00000F00) ## UART FIFO
  U1IR* = ESP8266_REG(0x00000F04) ## INT_RAW
  U1IS* = ESP8266_REG(0x00000F08) ## INT_STATUS
  U1IE* = ESP8266_REG(0x00000F0C) ## INT_ENABLE
  U1IC* = ESP8266_REG(0x00000F10) ## INT_CLEAR
  U1D* = ESP8266_REG(0x00000F14) ## CLKDIV
  U1A* = ESP8266_REG(0x00000F18) ## AUTOBAUD
  U1S* = ESP8266_REG(0x00000F1C) ## STATUS
  U1C0* = ESP8266_REG(0x00000F20) ## CONF0
  U1C1* = ESP8266_REG(0x00000F24) ## CONF1
  U1LP* = ESP8266_REG(0x00000F28) ## LOW_PULSE
  U1HP* = ESP8266_REG(0x00000F2C) ## HIGH_PULSE
  U1PN* = ESP8266_REG(0x00000F30) ## PULSE_NUM
  U1DT* = ESP8266_REG(0x00000F78) ## DATE
  U1ID* = ESP8266_REG(0x00000F7C) ## ID

## UART(uart) Registers

template USF*(u: untyped): untyped =
  ESP8266_REG(0x00000000 + (0x00000F00 * (u and 1))) ## UART FIFO
  
template USIR*(u: untyped): untyped =
  ESP8266_REG(0x00000004 + (0x00000F00 * (u and 1))) ## INT_RAW
  
template USIS*(u: untyped): untyped =
  ESP8266_REG(0x00000008 + (0x00000F00 * (u and 1))) ## INT_STATUS
  
template USIE*(u: untyped): untyped =
  ESP8266_REG(0x0000000C + (0x00000F00 * (u and 1))) ## INT_ENABLE
  
template USIC*(u: untyped): untyped =
  ESP8266_REG(0x00000010 + (0x00000F00 * (u and 1))) ## INT_CLEAR
  
template USD*(u: untyped): untyped =
  ESP8266_REG(0x00000014 + (0x00000F00 * (u and 1))) ## CLKDIV
  
template USA*(u: untyped): untyped =
  ESP8266_REG(0x00000018 + (0x00000F00 * (u and 1))) ## AUTOBAUD
  
template USS*(u: untyped): untyped =
  ESP8266_REG(0x0000001C + (0x00000F00 * (u and 1))) ## STATUS
  
template USC0*(u: untyped): untyped =
  ESP8266_REG(0x00000020 + (0x00000F00 * (u and 1))) ## CONF0
  
template USC1*(u: untyped): untyped =
  ESP8266_REG(0x00000024 + (0x00000F00 * (u and 1))) ## CONF1
  
template USLP*(u: untyped): untyped =
  ESP8266_REG(0x00000028 + (0x00000F00 * (u and 1))) ## LOW_PULSE
  
template USHP*(u: untyped): untyped =
  ESP8266_REG(0x0000002C + (0x00000F00 * (u and 1))) ## HIGH_PULSE
  
template USPN*(u: untyped): untyped =
  ESP8266_REG(0x00000030 + (0x00000F00 * (u and 1))) ## PULSE_NUM
  
template USDT*(u: untyped): untyped =
  ESP8266_REG(0x00000078 + (0x00000F00 * (u and 1))) ## DATE
  
template USID*(u: untyped): untyped =
  ESP8266_REG(0x0000007C + (0x00000F00 * (u and 1))) ## ID
  
## UART INT Registers Bits

const
  UITO* = 8
  UIBD* = 7
  UICTS* = 6
  UIDSR* = 5
  UIOF* = 4
  UIFR* = 3
  UIPE* = 2
  UIFE* = 1
  UIFF* = 0

## UART STATUS Registers Bits

const
  USTX* = 31
  USRTS* = 30
  USDTR* = 39
  USTXC* = 16
  USRXD* = 15
  USCTS* = 14
  USDSR* = 13
  USRXC* = 0

## UART CONF0 Registers Bits

const
  UCDTRI* = 24
  UCRTSI* = 23
  UCTXI* = 22
  UCDSRI* = 21
  UCCTSI* = 20
  UCRXI* = 19
  UCTXRST* = 18
  UCRXRST* = 17
  UCTXHFE* = 15
  UCLBE* = 14
  UCBRK* = 8
  UCSWDTR* = 7
  UCSWRTS* = 6
  UCSBN* = 4
  UCBN* = 2
  UCPAE* = 1
  UCPA* = 0

## UART CONF1 Registers Bits

const
  UCTOE* = 31
  UCTOT* = 24
  UCRXHFE* = 23
  UCRXHFT* = 16
  UCFET* = 8
  UCFFT* = 0

## WDT Feed (the dog) Register

const
  WDTFEED* = ESP8266_REG(0x00000914)

#template WDT_FEED*(): untyped =
#  (WDTFEED = 0x00000073)

## SPI_READY

const
  SPIRDY* = ESP8266_DREG(0x0000000C)
  SPI_BUSY* = 9

## SPI0 Registers (SPI0 is used for the flash)

const
  SPI0CMD* = ESP8266_REG(0x00000200)
  SPI0A* = ESP8266_REG(0x00000204)
  SPI0C* = ESP8266_REG(0x00000208)
  SPI0C1* = ESP8266_REG(0x0000020C)
  SPI0RS* = ESP8266_REG(0x00000210)
  SPI0C2* = ESP8266_REG(0x00000214)
  SPI0CLK* = ESP8266_REG(0x00000218)
  SPI0U* = ESP8266_REG(0x0000021C)
  SPI0U1* = ESP8266_REG(0x00000220)
  SPI0U2* = ESP8266_REG(0x00000224)
  SPI0WS* = ESP8266_REG(0x00000228)
  SPI0P* = ESP8266_REG(0x0000022C)
  SPI0S* = ESP8266_REG(0x00000230)
  SPI0S1* = ESP8266_REG(0x00000234)
  SPI0S2* = ESP8266_REG(0x00000238)
  SPI0S3* = ESP8266_REG(0x0000023C)
  SPI0W0* = ESP8266_REG(0x00000240)
  SPI0W1* = ESP8266_REG(0x00000244)
  SPI0W2* = ESP8266_REG(0x00000248)
  SPI0W3* = ESP8266_REG(0x0000024C)
  SPI0W4* = ESP8266_REG(0x00000250)
  SPI0W5* = ESP8266_REG(0x00000254)
  SPI0W6* = ESP8266_REG(0x00000258)
  SPI0W7* = ESP8266_REG(0x0000025C)
  SPI0W8* = ESP8266_REG(0x00000260)
  SPI0W9* = ESP8266_REG(0x00000264)
  SPI0W10* = ESP8266_REG(0x00000268)
  SPI0W11* = ESP8266_REG(0x0000026C)
  SPI0W12* = ESP8266_REG(0x00000270)
  SPI0W13* = ESP8266_REG(0x00000274)
  SPI0W14* = ESP8266_REG(0x00000278)
  SPI0W15* = ESP8266_REG(0x0000027C)
  SPI0E3* = ESP8266_REG(0x000002FC)

template SPI0W*(p: untyped): untyped =
  ESP8266_REG(0x00000240 + ((p and 0x0000000F) * 4))

## SPI1 Registers

const
  SPI1CMD* = ESP8266_REG(0x00000100)
  SPI1A* = ESP8266_REG(0x00000104)
  SPI1C* = ESP8266_REG(0x00000108)
  SPI1C1* = ESP8266_REG(0x0000010C)
  SPI1RS* = ESP8266_REG(0x00000110)
  SPI1C2* = ESP8266_REG(0x00000114)
  SPI1CLK* = ESP8266_REG(0x00000118)
  SPI1U* = ESP8266_REG(0x0000011C)
  SPI1U1* = ESP8266_REG(0x00000120)
  SPI1U2* = ESP8266_REG(0x00000124)
  SPI1WS* = ESP8266_REG(0x00000128)
  SPI1P* = ESP8266_REG(0x0000012C)
  SPI1S* = ESP8266_REG(0x00000130)
  SPI1S1* = ESP8266_REG(0x00000134)
  SPI1S2* = ESP8266_REG(0x00000138)
  SPI1S3* = ESP8266_REG(0x0000013C)
  SPI1W0* = ESP8266_REG(0x00000140)
  SPI1W1* = ESP8266_REG(0x00000144)
  SPI1W2* = ESP8266_REG(0x00000148)
  SPI1W3* = ESP8266_REG(0x0000014C)
  SPI1W4* = ESP8266_REG(0x00000150)
  SPI1W5* = ESP8266_REG(0x00000154)
  SPI1W6* = ESP8266_REG(0x00000158)
  SPI1W7* = ESP8266_REG(0x0000015C)
  SPI1W8* = ESP8266_REG(0x00000160)
  SPI1W9* = ESP8266_REG(0x00000164)
  SPI1W10* = ESP8266_REG(0x00000168)
  SPI1W11* = ESP8266_REG(0x0000016C)
  SPI1W12* = ESP8266_REG(0x00000170)
  SPI1W13* = ESP8266_REG(0x00000174)
  SPI1W14* = ESP8266_REG(0x00000178)
  SPI1W15* = ESP8266_REG(0x0000017C)
  SPI1E0* = ESP8266_REG(0x000001F0)
  SPI1E1* = ESP8266_REG(0x000001F4)
  SPI1E2* = ESP8266_REG(0x000001F8)
  SPI1E3* = ESP8266_REG(0x000001FC)

template SPI1W*(p: untyped): untyped =
  ESP8266_REG(0x00000140 + ((p and 0x0000000F) * 4))

## SPI0, SPI1 & I2S Interupt Register

const
  SPIIR* = ESP8266_DREG(0x00000020)
  SPII0* = 4
  SPII1* = 7
  SPII2* = 9

## SPI CMD

const
  SPICMDREAD* = (1 shl 31)        ## SPI_FLASH_READ
  SPICMDWREN* = (1 shl 30)        ## SPI_FLASH_WREN
  SPICMDWRDI* = (1 shl 29)        ## SPI_FLASH_WRDI
  SPICMDRDID* = (1 shl 28)        ## SPI_FLASH_RDID
  SPICMDRDSR* = (1 shl 27)        ## SPI_FLASH_RDSR
  SPICMDWRSR* = (1 shl 26)        ## SPI_FLASH_WRSR
  SPICMDPP* = (1 shl 25)          ## SPI_FLASH_PP
  SPICMDSE* = (1 shl 24)          ## SPI_FLASH_SE
  SPICMDBE* = (1 shl 23)          ## SPI_FLASH_BE
  SPICMDCE* = (1 shl 22)          ## SPI_FLASH_CE
  SPICMDDP* = (1 shl 21)          ## SPI_FLASH_DP
  SPICMDRES* = (1 shl 20)         ## SPI_FLASH_RES
  SPICMDHPM* = (1 shl 19)         ## SPI_FLASH_HPM
  SPICMDUSR* = (1 shl 18)         ## SPI_FLASH_USR
  #SPIBUSY* = (1 shl 18)           ## SPI_USR

## SPI CTRL (SPIxC)

const
  SPICWBO* = (1 shl 26)           ## SPI_WR_BIT_ODER
  SPICRBO* = (1 shl 25)           ## SPI_RD_BIT_ODER
  SPICQIO* = (1 shl 24)           ## SPI_QIO_MODE
  SPICDIO* = (1 shl 23)           ## SPI_DIO_MODE
  SPIC2BSE* = (1 shl 22)          ## SPI_TWO_BYTE_STATUS_EN
  SPICWPR* = (1 shl 21)           ## SPI_WP_REG
  SPICQOUT* = (1 shl 20)          ## SPI_QOUT_MODE
  SPICSHARE* = (1 shl 19)         ## SPI_SHARE_BUS
  SPICHOLD* = (1 shl 18)          ## SPI_HOLD_MODE
  SPICAHB* = (1 shl 17)           ## SPI_ENABLE_AHB
  SPICSSTAAI* = (1 shl 16)        ## SPI_SST_AAI
  SPICRESANDRES* = (1 shl 15)     ## SPI_RESANDRES
  SPICDOUT* = (1 shl 14)          ## SPI_DOUT_MODE
  SPICFASTRD* = (1 shl 13)        ## SPI_FASTRD_MODE

## SPI CTRL1 (SPIxC1)

const
  SPIC1TCSH* = 0x0000000F
  SPIC1TCSH_S* = 28
  SPIC1TRES* = 0x00000FFF
  SPIC1TRES_S* = 16
  SPIC1BTL* = 0x0000FFFF
  SPIC1BTL_S* = 0

## SPI Status (SPIxRS)

const
  SPIRSEXT* = 0x000000FF
  SPIRSEXT_S* = 24
  SPIRSWB* = 0x000000FF
  SPIRSWB_S* = 16
  SPIRSSP* = (1 shl 7)            ## SPI_FLASH_STATUS_PRO_FLAG
  SPIRSTBP* = (1 shl 5)           ## SPI_FLASH_TOP_BOT_PRO_FLAG
  SPIRSBP2* = (1 shl 4)           ## SPI_FLASH_BP2
  SPIRSBP1* = (1 shl 3)           ## SPI_FLASH_BP1
  SPIRSBP0* = (1 shl 2)           ## SPI_FLASH_BP0
  SPIRSWRE* = (1 shl 1)           ## SPI_FLASH_WRENABLE_FLAG
  SPIRSBUSY* = (1 shl 0)          ## SPI_FLASH_BUSY_FLAG

## SPI CTRL2 (SPIxC2)

const
  SPIC2CSDN* = 0x0000000F
  SPIC2CSDN_S* = 28
  SPIC2CSDM* = 0x00000003
  SPIC2CSDM_S* = 26
  SPIC2MOSIDN* = 0x00000007
  SPIC2MOSIDN_S* = 23
  SPIC2MOSIDM* = 0x00000003
  SPIC2MOSIDM_S* = 21
  SPIC2MISODN* = 0x00000007
  SPIC2MISODN_S* = 18
  SPIC2MISODM* = 0x00000003
  SPIC2MISODM_S* = 16
  SPIC2CKOHM* = 0x0000000F
  SPIC2CKOHM_S* = 12
  SPIC2CKOLM* = 0x0000000F
  SPIC2CKOLM_S* = 8
  SPIC2HT* = 0x0000000F
  SPIC2HT_S* = 4
  SPIC2ST* = 0x0000000F
  SPIC2ST_S* = 0

## SPI CLK (SPIxCLK)

const
  SPICLK_EQU_SYSCLK* = (1 shl 31) ## SPI_CLK_EQU_SYSCLK
  SPICLKDIVPRE* = 0x00001FFF
  SPICLKDIVPRE_S* = 18
  SPICLKCN* = 0x0000003F
  SPICLKCN_S* = 12
  SPICLKCH* = 0x0000003F
  SPICLKCH_S* = 6
  SPICLKCL* = 0x0000003F
  SPICLKCL_S* = 0

## SPI Phases (SPIxU)

const
  SPIUCOMMAND* = (1 shl 31)       ## COMMAND pahse, SPI_USR_COMMAND
  SPIUADDR* = (1 shl 30)          ## ADDRESS phase, SPI_FLASH_USR_ADDR
  SPIUDUMMY* = (1 shl 29)         ## DUMMY phase, SPI_FLASH_USR_DUMMY
  SPIUMISO* = (1 shl 28)          ## MISO phase, SPI_FLASH_USR_DIN
  SPIUMOSI* = (1 shl 27)          ## MOSI phase, SPI_FLASH_DOUT
  SPIUDUMMYIDLE* = (1 shl 26)     ## SPI_USR_DUMMY_IDLE
  SPIUMOSIH* = (1 shl 25)         ## MOSI phase uses W8-W15, SPI_USR_DOUT_HIGHPART
  SPIUMISOH* = (1 shl 24)         ## MISO pahse uses W8-W15, SPI_USR_DIN_HIGHPART
  SPIUPREPHOLD* = (1 shl 23)      ## SPI_USR_PREP_HOLD
  SPIUCMDHOLD* = (1 shl 22)       ## SPI_USR_CMD_HOLD
  SPIUADDRHOLD* = (1 shl 21)      ## SPI_USR_ADDR_HOLD
  SPIUDUMMYHOLD* = (1 shl 20)     ## SPI_USR_DUMMY_HOLD
  SPIUMISOHOLD* = (1 shl 19)      ## SPI_USR_DIN_HOLD
  SPIUMOSIHOLD* = (1 shl 18)      ## SPI_USR_DOUT_HOLD
  SPIUHOLDPOL* = (1 shl 17)       ## SPI_USR_HOLD_POL
  SPIUSIO* = (1 shl 16)           ## SPI_SIO
  SPIUFWQIO* = (1 shl 15)         ## SPI_FWRITE_QIO
  SPIUFWDIO* = (1 shl 14)         ## SPI_FWRITE_DIO
  SPIUFWQUAD* = (1 shl 13)        ## SPI_FWRITE_QUAD
  SPIUFWDUAL* = (1 shl 12)        ## SPI_FWRITE_DUAL
  SPIUWRBYO* = (1 shl 11)         ## SPI_WR_BYTE_ORDER
  SPIURDBYO* = (1 shl 10)         ## SPI_RD_BYTE_ORDER
  SPIUAHBEM* = 0x00000003
  SPIUAHBEM_S* = 8
  SPIUSME* = (1 shl 7)            ## SPI Master Edge (0:falling, 1:rising), SPI_CK_OUT_EDGE
  SPIUSSE* = (1 shl 6)            ## SPI Slave Edge (0:falling, 1:rising), SPI_CK_I_EDGE
  SPIUCSSETUP* = (1 shl 5)        ## SPI_CS_SETUP
  SPIUCSHOLD* = (1 shl 4)         ## SPI_CS_HOLD
  SPIUAHBUCMD* = (1 shl 3)        ## SPI_AHB_USR_COMMAND
  SPIUAHBUCMD4B* = (1 shl 1)      ## SPI_AHB_USR_COMMAND_4BYTE
  SPIUDUPLEX* = (1 shl 0)         ## SPI_DOUTDIN

## SPI Phase Length Locations

const
  SPILCOMMAND* = 28
  SPILADDR* = 26
  SPILDUMMY* = 0
  SPILMISO* = 8
  SPILMOSI* = 17
  SPIMCOMMAND* = 0x0000000F
  SPIMADDR* = 0x0000003F
  SPIMDUMMY* = 0x000000FF
  SPIMMISO* = 0x000001FF
  SPIMMOSI* = 0x000001FF

## SPI Slave (SPIxS)

const
  SPISSRES* = (1 shl 31)          ## SYNC RESET, SPI_SYNC_RESET
  SPISE* = (1 shl 30)             ## Slave Enable, SPI_SLAVE_MODE
  SPISBE* = (1 shl 29)            ## WR/RD BUF enable, SPI_SLV_WR_RD_BUF_EN
  SPISSE* = (1 shl 28)            ## STA enable, SPI_SLV_WR_RD_STA_EN
  SPISCD* = (1 shl 27)            ## CMD define, SPI_SLV_CMD_DEFINE
  SPISTRCNT* = 0x0000000F
  SPISTRCNT_S* = 23
  SPISSLS* = 0x00000007
  SPISSLS_S* = 20
  SPISSLC* = 0x00000007
  SPISSLC_S* = 17
  SPISCSIM* = 0x00000003
  SPIDCSIM_S* = 10
  SPISTRIE* = (1 shl 9)           ## TRANS interrupt enable
  SPISWSIE* = (1 shl 8)           ## WR_STA interrupt enable
  SPISRSIE* = (1 shl 7)           ## RD_STA interrupt enable
  SPISWBIE* = (1 shl 6)           ## WR_BUF interrupt enable
  SPISRBIE* = (1 shl 5)           ## RD_BUF interrupt enable
  SPISTRIS* = (1 shl 4)           ## TRANS interrupt status
  SPISWSIS* = (1 shl 3)           ## WR_STA interrupt status
  SPISRSIS* = (1 shl 2)           ## RD_STA interrupt status
  SPISWBIS* = (1 shl 1)           ## WR_BUF interrupt status
  SPISRBIS* = (1 shl 0)           ## RD_BUF interrupt status

## SPI Slave1 (SPIxS1)

const
  SPIS1LSTA* = 27
  SPIS1FE* = (1 shl 26)           ## SPI_SLV_STATUS_FAST_EN
  SPIS1RSTA* = (1 shl 25)         ## default:0 enable STA read from Master, SPI_SLV_STATUS_READBACK
  SPIS1LBUF* = 16
  SPIS1LRBA* = 10
  SPIS1LWBA* = 4
  SPIS1WSDE* = (1 shl 3)          ## SPI_SLV_WRSTA_DUMMY_EN
  SPIS1RSDE* = (1 shl 2)          ## SPI_SLV_RDSTA_DUMMY_EN
  SPIS1WBDE* = (1 shl 1)          ## SPI_SLV_WRBUF_DUMMY_EN
  SPIS1RBDE* = (1 shl 0)          ## SPI_SLV_RDBUF_DUMMY_EN

## SPI Slave2 (SPIxS2)

const
  SPIS2WBDL* = 0x000000FF
  SPIS2WBDL_S* = 24
  SPIS2RBDL* = 0x000000FF
  SPIS2RBDL_S* = 16
  SPIS2WSDL* = 0x000000FF
  SPIS2WSDL_S* = 8
  SPIS2RSDL* = 0x000000FF
  SPIS2RSDL_S* = 0

## SPI Slave3 (SPIxS3)

const
  SPIS3WSCV* = 0x000000FF
  SPIS3WSCV_S* = 24
  SPIS3RSCV* = 0x000000FF
  SPIS3RSCV_S* = 16
  SPIS3WBCV* = 0x000000FF
  SPIS3WBCV_S* = 8
  SPIS3RBCV* = 0x000000FF
  SPIS3RBCV_S* = 0

## SPI EXT0 (SPIxE0)

const
  SPIE0TPPEN* = (1 shl 31)        ## SPI_T_PP_ENA
  SPIE0TPPS* = 0x0000000F
  SPIE0TPPS_S* = 16
  SPIE0TPPT* = 0x00000FFF
  SPIE0TPPT_S* = 0

## SPI EXT1 (SPIxE1)

const
  SPIE1TEREN* = (1 shl 31)        ## SPI_T_ERASE_ENA
  SPIE1TERS* = 0x0000000F
  SPIE1TERS_S* = 16
  SPIE1TERT* = 0x00000FFF
  SPIE1TERT_S* = 0

## SPI EXT2 (SPIxE2)

const
  SPIE2ST* = 0x00000007
  SPIE2ST_S* = 0

## SPI EXT3 (SPIxE3)

const
  SPIE2IHEN* = 0x00000003
  SPIE2IHEN_S* = 0

## SPI PIN (SPIxP)

const
  SPIPCS2DIS* = (1 shl 2)
  SPIPCS1DIS* = (1 shl 1)
  SPIPCS0DIS* = (1 shl 0)

## SLC (DMA) Registers

const
  SLCC0* = ESP8266_REG(0x00000B00) ## SLC_CONF0
  SLCIR* = ESP8266_REG(0x00000B04) ## SLC_INT_RAW
  SLCIS* = ESP8266_REG(0x00000B08) ## SLC_INT_STATUS
  SLCIE* = ESP8266_REG(0x00000B0C) ## SLC_INT_ENA
  SLCIC* = ESP8266_REG(0x00000B10) ## SLC_INT_CLR
  SLCRXS* = ESP8266_REG(0x00000B14) ## SLC_RX_STATUS
  SLCRXP* = ESP8266_REG(0x00000B18) ## SLC_RX_FIFO_PUSH
  SLCTXS* = ESP8266_REG(0x00000B1C) ## SLC_TX_STATUS
  SLCTXP* = ESP8266_REG(0x00000B20) ## SLC_TX_FIFO_POP
  SLCRXL* = ESP8266_REG(0x00000B24) ## SLC_RX_LINK
  SLCTXL* = ESP8266_REG(0x00000B28) ## SLC_TX_LINK
  SLCIVTH* = ESP8266_REG(0x00000B2C) ## SLC_INTVEC_TOHOST
  SLCT0* = ESP8266_REG(0x00000B30) ## SLC_TOKEN0
  SLCT1* = ESP8266_REG(0x00000B34) ## SLC_TOKEN1
  SLCC1* = ESP8266_REG(0x00000B38) ## SLC_CONF1
  SLCS0* = ESP8266_REG(0x00000B3C) ## SLC_STATE0
  SLCS1* = ESP8266_REG(0x00000B40) ## SLC_STATE1
  SLCBC* = ESP8266_REG(0x00000B44) ## SLC_BRIDGE_CONF
  SLCRXEDA* = ESP8266_REG(0x00000B48) ## SLC_RX_EOF_DES_ADDR
  SLCTXEDA* = ESP8266_REG(0x00000B4C) ## SLC_TX_EOF_DES_ADDR
  SLCRXEBDA* = ESP8266_REG(0x00000B50) ## SLC_RX_EOF_BFR_DES_ADDR
  SLCAT* = ESP8266_REG(0x00000B54) ## SLC_AHB_TEST
  SLCSS* = ESP8266_REG(0x00000B58) ## SLC_SDIO_ST
  SLCRXDC* = ESP8266_REG(0x00000B5C) ## SLC_RX_DSCR_CONF
  SLCTXD* = ESP8266_REG(0x00000B60) ## SLC_TXLINK_DSCR
  SLCTXDB0* = ESP8266_REG(0x00000B64) ## SLC_TXLINK_DSCR_BF0
  SLCTXDB1* = ESP8266_REG(0x00000B68) ## SLC_TXLINK_DSCR_BF1
  SLCRXD* = ESP8266_REG(0x00000B6C) ## SLC_RXLINK_DSCR
  SLCRXDB0* = ESP8266_REG(0x00000B70) ## SLC_RXLINK_DSCR_BF0
  SLCRXDB1* = ESP8266_REG(0x00000B74) ## SLC_RXLINK_DSCR_BF1
  SLCDT* = ESP8266_REG(0x00000B78) ## SLC_DATE
  SLCID* = ESP8266_REG(0x00000B7C) ## SLC_ID
  SLCHIR* = ESP8266_REG(0x00000B88) ## SLC_HOST_INTR_RAW
  SLCHC0* = ESP8266_REG(0x00000B94) ## SLC_HOST_CONF_W0
  SLCHC1* = ESP8266_REG(0x00000B98) ## SLC_HOST_CONF_W1
  SLCHIS* = ESP8266_REG(0x00000B9C) ## SLC_HOST_INTR_ST
  SLCHC2* = ESP8266_REG(0x00000BA0) ## SLC_HOST_CONF_W2
  SLCHC3* = ESP8266_REG(0x00000BA4) ## SLC_HOST_CONF_W3
  SLCHC4* = ESP8266_REG(0x00000BA8) ## SLC_HOST_CONF_W4
  SLCHIC* = ESP8266_REG(0x00000BB0) ## SLC_HOST_INTR_CLR
  SLCHIE* = ESP8266_REG(0x00000BB4) ## SLC_HOST_INTR_ENA
  SLCHC5* = ESP8266_REG(0x00000BBC) ## SLC_HOST_CONF_W5

## SLC (DMA) CONF0

const
  SLCMM* = (0x00000003)         ## SLC_MODE
  SLCM* = (12)                  ## SLC_MODE_S
  SLCDTBE* = (1 shl 9)            ## SLC_DATA_BURST_EN
  SLCDBE* = (1 shl 8)             ## SLC_DSCR_BURST_EN
  SLCRXNRC* = (1 shl 7)           ## SLC_RX_NO_RESTART_CLR
  SLCRXAW* = (1 shl 6)            ## SLC_RX_AUTO_WRBACK
  SLCRXLT* = (1 shl 5)            ## SLC_RX_LOOP_TEST
  SLCTXLT* = (1 shl 4)            ## SLC_TX_LOOP_TEST
  SLCAR* = (1 shl 3)              ## SLC_AHBM_RST
  SLCAFR* = (1 shl 2)             ## SLC_AHBM_FIFO_RST
  SLCRXLR* = (1 shl 1)            ## SLC_RXLINK_RST
  SLCTXLR* = (1 shl 0)            ## SLC_TXLINK_RST

## SLC (DMA) INT

const
  SLCITXDE* = (1 shl 21)          ## SLC_TX_DSCR_EMPTY_INT
  SLCIRXDER* = (1 shl 20)         ## SLC_RX_DSCR_ERR_INT
  SLCITXDER* = (1 shl 19)         ## SLC_TX_DSCR_ERR_INT
  SLCITH* = (1 shl 18)            ## SLC_TOHOST_INT
  SLCIRXEOF* = (1 shl 17)         ## SLC_RX_EOF_INT
  SLCIRXD* = (1 shl 16)           ## SLC_RX_DONE_INT
  SLCITXEOF* = (1 shl 15)         ## SLC_TX_EOF_INT
  SLCITXD* = (1 shl 14)           ## SLC_TX_DONE_INT
  SLCIT0* = (1 shl 13)            ## SLC_TOKEN1_1TO0_INT
  SLCIT1* = (1 shl 12)            ## SLC_TOKEN0_1TO0_INT
  SLCITXO* = (1 shl 11)           ## SLC_TX_OVF_INT
  SLCIRXU* = (1 shl 10)           ## SLC_RX_UDF_INT
  SLCITXS* = (1 shl 9)            ## SLC_TX_START_INT
  SLCIRXS* = (1 shl 8)            ## SLC_RX_START_INT
  SLCIFH7* = (1 shl 7)            ## SLC_FRHOST_BIT7_INT
  SLCIFH6* = (1 shl 6)            ## SLC_FRHOST_BIT6_INT
  SLCIFH5* = (1 shl 5)            ## SLC_FRHOST_BIT5_INT
  SLCIFH4* = (1 shl 4)            ## SLC_FRHOST_BIT4_INT
  SLCIFH3* = (1 shl 3)            ## SLC_FRHOST_BIT3_INT
  SLCIFH2* = (1 shl 2)            ## SLC_FRHOST_BIT2_INT
  SLCIFH1* = (1 shl 1)            ## SLC_FRHOST_BIT1_INT
  SLCIFH0* = (1 shl 0)            ## SLC_FRHOST_BIT0_INT

## SLC (DMA) RX_STATUS

const
  SLCRXE* = (1 shl 1)             ## SLC_RX_EMPTY
  SLCRXF* = (1 shl 0)             ## SLC_RX_FULL

## SLC (DMA) TX_STATUS

const
  SLCTXE* = (1 shl 1)             ## SLC_TX_EMPTY
  SLCTXF* = (1 shl 0)             ## SLC_TX_FULL

## SLC (DMA) RX_FIFO_PUSH

const
  SLCRXFP* = (1 shl 16)           ## SLC_RXFIFO_PUSH
  SLCRXWDM* = (0x000001FF)      ## SLC_RXFIFO_WDATA
  SLCRXWD* = (0)                ## SLC_RXFIFO_WDATA_S

## SLC (DMA) TX_FIFO_POP

const
  SLCTXFP* = (1 shl 16)           ## SLC_TXFIFO_POP
  SLCTXRDM* = (0x000007FF)      ## SLC_TXFIFO_RDATA
  SLCTXRD* = (0)                ## SLC_TXFIFO_RDATA_S

## SLC (DMA) RX_LINK

const
  SLCRXLP* = (1 shl 31)           ## SLC_RXLINK_PARK
  SLCRXLRS* = (1 shl 30)          ## SLC_RXLINK_RESTART
  SLCRXLS* = (1 shl 29)           ## SLC_RXLINK_START
  SLCRXLE* = (1 shl 28)           ## SLC_RXLINK_STOP
  SLCRXLAM* = (0x0000FFFF)      ## SLC_RXLINK_DESCADDR_MASK
  SLCRXLA* = (0)                ## SLC_RXLINK_ADDR_S

## SLC (DMA) TX_LINK

const
  SLCTXLP* = (1 shl 31)           ## SLC_TXLINK_PARK
  SLCTXLRS* = (1 shl 30)          ## SLC_TXLINK_RESTART
  SLCTXLS* = (1 shl 29)           ## SLC_TXLINK_START
  SLCTXLE* = (1 shl 28)           ## SLC_TXLINK_STOP
  SLCTXLAM* = (0x0000FFFF)      ## SLC_TXLINK_DESCADDR_MASK
  SLCTXLA* = (0)                ## SLC_TXLINK_ADDR_S

## SLC (DMA) TOKENx

const
  SLCTM* = (0x00000FFF)         ## SLC_TOKENx_MASK
  SLCTT* = (16)                 ## SLC_TOKENx_S
  SLCTIM* = (1 shl 14)            ## SLC_TOKENx_LOCAL_INC_MORE
  SLCTI* = (1 shl 13)             ## SLC_TOKENx_LOCAL_INC
  SLCTW* = (1 shl 12)             ## SLC_TOKENx_LOCAL_WR
  SLCTDM* = (0x00000FFF)        ## SLC_TOKENx_LOCAL_WDATA
  SLCTD* = (0)                  ## SLC_TOKENx_LOCAL_WDATA_S

## SLC (DMA) BRIDGE_CONF

const
  SLCBFMEM* = (0x0000000F)      ## SLC_FIFO_MAP_ENA
  SLCBFME* = (8)                ## SLC_FIFO_MAP_ENA_S
  SLCBTEEM* = (0x0000003F)      ## SLC_TXEOF_ENA
  SLCBTEE* = (0)                ## SLC_TXEOF_ENA_S

## SLC (DMA) AHB_TEST

const
  SLCATAM* = (0x00000003)       ## SLC_AHB_TESTADDR
  SLCATA* = (4)                 ## SLC_AHB_TESTADDR_S
  SLCATMM* = (0x00000007)       ## SLC_AHB_TESTMODE
  SLCATM* = (0)                 ## SLC_AHB_TESTMODE_S

## SLC (DMA) SDIO_ST

const
  SLCSBM* = (0x00000007)        ## SLC_BUS_ST
  SLCSB* = (12)                 ## SLC_BUS_ST_S
  SLCSW* = (1 shl 8)              ## SLC_SDIO_WAKEUP
  SLCSFM* = (0x0000000F)        ## SLC_FUNC_ST
  SLCSF* = (4)                  ## SLC_FUNC_ST_S
  SLCSCM* = (0x00000007)        ## SLC_CMD_ST
  SLCSC* = (0)                  ## SLC_CMD_ST_S

## SLC (DMA) RX_DSCR_CONF

const
  SLCBRXFE* = (1 shl 20)          ## SLC_RX_FILL_EN
  SLCBRXEM* = (1 shl 19)          ## SLC_RX_EOF_MODE
  SLCBRXFM* = (1 shl 18)          ## SLC_RX_FILL_MODE
  SLCBINR* = (1 shl 17)           ## SLC_INFOR_NO_REPLACE
  SLCBTNR* = (1 shl 16)           ## SLC_TOKEN_NO_REPLACE
  SLCBPICM* = (0x0000FFFF)      ## SLC_POP_IDLE_CNT
  SLCBPIC* = (0)                ## SLC_POP_IDLE_CNT_S

##  I2S Registers

const
  i2c_bbpll* = 0x00000067
  i2c_bbpll_hostid* = 4
  i2c_bbpll_en_audio_clock_out* = 4
  i2c_bbpll_en_audio_clock_out_msb* = 7
  i2c_bbpll_en_audio_clock_out_lsb* = 7

template I2S_CLK_ENABLE*(): untyped =
  i2c_writeReg_Mask_def(i2c_bbpll, i2c_bbpll_en_audio_clock_out, 1)

const
  I2SBASEFREQ* = (160000000)
  I2STXF* = ESP8266_REG(0x00000E00) ## I2STXFIFO (32bit)
  I2SRXF* = ESP8266_REG(0x00000E04) ## I2SRXFIFO (32bit)
  I2SC* = ESP8266_REG(0x00000E08) ## I2SCONF
  I2SIR* = ESP8266_REG(0x00000E0C) ## I2SINT_RAW
  I2SIS* = ESP8266_REG(0x00000E10) ## I2SINT_ST
  I2SIE* = ESP8266_REG(0x00000E14) ## I2SINT_ENA
  I2SIC* = ESP8266_REG(0x00000E18) ## I2SINT_CLR
  I2ST* = ESP8266_REG(0x00000E1C) ## I2STIMING
  I2SFC* = ESP8266_REG(0x00000E20) ## I2S_FIFO_CONF
  I2SRXEN* = ESP8266_REG(0x00000E24) ## I2SRXEOF_NUM (32bit)
  I2SCSD* = ESP8266_REG(0x00000E28) ## I2SCONF_SIGLE_DATA (32bit)
  I2SCC* = ESP8266_REG(0x00000E2C) ## I2SCONF_CHAN

##  I2S CONF

const
  I2SBDM* = (0x0000003F)        ## I2S_BCK_DIV_NUM
  I2SBD* = (22)                 ## I2S_BCK_DIV_NUM_S
  I2SCDM* = (0x0000003F)        ## I2S_CLKM_DIV_NUM
  I2SCD* = (16)                 ## I2S_CLKM_DIV_NUM_S
  I2SBMM* = (0x0000000F)        ## I2S_BITS_MOD
  I2SBM* = (12)                 ## I2S_BITS_MOD_S
  I2SRMS* = (1 shl 11)            ## I2S_RECE_MSB_SHIFT
  I2STMS* = (1 shl 10)            ## I2S_TRANS_MSB_SHIFT
  I2SRXS* = (1 shl 9)             ## I2S_I2S_RX_START
  I2STXS* = (1 shl 8)             ## I2S_I2S_TX_START
  I2SMR* = (1 shl 7)              ## I2S_MSB_RIGHT
  I2SRF* = (1 shl 6)              ## I2S_RIGHT_FIRST
  I2SRSM* = (1 shl 5)             ## I2S_RECE_SLAVE_MOD
  I2STSM* = (1 shl 4)             ## I2S_TRANS_SLAVE_MOD
  I2SRXFR* = (1 shl 3)            ## I2S_I2S_RX_FIFO_RESET
  I2STXFR* = (1 shl 2)            ## I2S_I2S_TX_FIFO_RESET
  I2SRXR* = (1 shl 1)             ## I2S_I2S_RX_RESET
  I2STXR* = (1 shl 0)             ## I2S_I2S_TX_RESET
  I2SRST* = (0x0000000F)        ## I2S_I2S_RESET_MASK

## I2S INT

const
  I2SITXRE* = (1 shl 5)           ## I2S_I2S_TX_REMPTY_INT
  I2SITXWF* = (1 shl 4)           ## I2S_I2S_TX_WFULL_INT
  I2SIRXRE* = (1 shl 3)           ## I2S_I2S_RX_REMPTY_INT
  I2SIRXWF* = (1 shl 2)           ## I2S_I2S_RX_WFULL_INT
  I2SITXPD* = (1 shl 1)           ## I2S_I2S_TX_PUT_DATA_INT
  I2SIRXTD* = (1 shl 0)           ## I2S_I2S_RX_TAKE_DATA_INT

## I2S TIMING

const
  I2STBII* = (1 shl 22)           ## I2S_TRANS_BCK_IN_INV
  I2SRDS* = (1 shl 21)            ## I2S_RECE_DSYNC_SW
  I2STDS* = (1 shl 20)            ## I2S_TRANS_DSYNC_SW
  I2SRBODM* = (0x00000003)      ## I2S_RECE_BCK_OUT_DELAY
  I2SRBOD* = (18)               ## I2S_RECE_BCK_OUT_DELAY_S
  I2SRWODM* = (0x00000003)      ## I2S_RECE_WS_OUT_DELAY
  I2SRWOD* = (16)               ## I2S_RECE_WS_OUT_DELAY_S
  I2STSODM* = (0x00000003)      ## I2S_TRANS_SD_OUT_DELAY
  I2STSOD* = (14)               ## I2S_TRANS_SD_OUT_DELAY_S
  I2STWODM* = (0x00000003)      ## I2S_TRANS_WS_OUT_DELAY
  I2STWOD* = (12)               ## I2S_TRANS_WS_OUT_DELAY_S
  I2STBODM* = (0x00000003)      ## I2S_TRANS_BCK_OUT_DELAY
  I2STBOD* = (10)               ## I2S_TRANS_BCK_OUT_DELAY_S
  I2SRSIDM* = (0x00000003)      ## I2S_RECE_SD_IN_DELAY
  I2SRSID* = (8)                ## I2S_RECE_SD_IN_DELAY_S
  I2SRWIDM* = (0x00000003)      ## I2S_RECE_WS_IN_DELAY
  I2SRWID* = (6)                ## I2S_RECE_WS_IN_DELAY_S
  I2SRBIDM* = (0x00000003)      ## I2S_RECE_BCK_IN_DELAY
  I2SRBID* = (4)                ## I2S_RECE_BCK_IN_DELAY_S
  I2STWIDM* = (0x00000003)      ## I2S_TRANS_WS_IN_DELAY
  I2STWID* = (2)                ## I2S_TRANS_WS_IN_DELAY_S
  I2STBIDM* = (0x00000003)      ## I2S_TRANS_BCK_IN_DELAY
  I2STBID* = (0)                ## I2S_TRANS_BCK_IN_DELAY_S

## I2S FIFO CONF

const
  I2SRXFMM* = (0x00000007)      ## I2S_I2S_RX_FIFO_MOD
  I2SRXFM* = (16)               ## I2S_I2S_RX_FIFO_MOD_S
  I2STXFMM* = (0x00000007)      ## I2S_I2S_TX_FIFO_MOD
  I2STXFM* = (13)               ## I2S_I2S_TX_FIFO_MOD_S
  I2SDE* = (1 shl 12)             ## I2S_I2S_DSCR_EN
  I2STXDNM* = (0x0000003F)      ## I2S_I2S_TX_DATA_NUM
  I2STXDN* = (6)                ## I2S_I2S_TX_DATA_NUM_S
  I2SRXDNM* = (0x0000003F)      ## I2S_I2S_RX_DATA_NUM
  I2SRXDN* = (0)                ## I2S_I2S_RX_DATA_NUM_S

## I2S CONF CHAN

const
  I2SRXCMM* = (0x00000003)      ## I2S_RX_CHAN_MOD
  I2SRXCM* = (3)                ## I2S_RX_CHAN_MOD_S
  I2STXCMM* = (0x00000007)      ## I2S_TX_CHAN_MOD
  I2STXCM* = (0)                ## I2S_TX_CHAN_MOD_S

## *
##  Random Number Generator 32bit
##  http://esp8266-re.foogod.com/wiki/Random_Number_Generator
## 

const
  RANDOM_REG32* = ESP8266_DREG(0x00020E44)
