##  
##   stdlib_noniso.h - nonstandard (but usefull) conversion functions
## 
##   Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
##  
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

proc atoi*(s: cstring): cint {.cdecl, importcpp: "atoi(@)", header: "stdlib_noniso.h".}
proc atol*(s: cstring): clong {.cdecl, importcpp: "atol(@)", header: "stdlib_noniso.h".}
proc atof*(s: cstring): cdouble {.cdecl, importcpp: "atof(@)",
                              header: "stdlib_noniso.h".}
proc itoa*(val: cint; s: cstring; radix: cint): cstring {.cdecl, importcpp: "itoa(@)",
    header: "stdlib_noniso.h".}
proc ltoa*(val: clong; s: cstring; radix: cint): cstring {.cdecl, importcpp: "ltoa(@)",
    header: "stdlib_noniso.h".}
proc utoa*(val: cuint; s: cstring; radix: cint): cstring {.cdecl, importcpp: "utoa(@)",
    header: "stdlib_noniso.h".}
proc ultoa*(val: culong; s: cstring; radix: cint): cstring {.cdecl,
    importcpp: "ultoa(@)", header: "stdlib_noniso.h".}
proc dtostrf*(val: cdouble; width: cchar; prec: cuchar; s: cstring): cstring {.cdecl,
    importcpp: "dtostrf(@)", header: "stdlib_noniso.h".}
proc reverse*(begin: cstring; `end`: cstring) {.cdecl, importcpp: "reverse(@)",
    header: "stdlib_noniso.h".}