## 
##   Udp.cpp: Library to send/receive UDP packets.
## 
##  NOTE: UDP is fast, but has some important limitations (thanks to Warren Gray for mentioning these)
##  1) UDP does not guarantee the order in which assembled UDP packets are received. This
##  might not happen often in practice, but in larger network topologies, a UDP
##  packet can be received out of sequence. 
##  2) UDP does not guard against lost packets - so packets *can* disappear without the sender being
##  aware of it. Again, this may not be a concern in practice on small local networks.
##  For more information, see http://www.cafeaulait.org/course/week12/35.html
## 
##  MIT License:
##  Copyright (c) 2008 Bjoern Hartmann
##  Permission is hereby granted, free of charge, to any person obtaining a copy
##  of this software and associated documentation files (the "Software"), to deal
##  in the Software without restriction, including without limitation the rights
##  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
##  copies of the Software, and to permit persons to whom the Software is
##  furnished to do so, subject to the following conditions:
##  
##  The above copyright notice and this permission notice shall be included in
##  all copies or substantial portions of the Software.
##  
##  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
##  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
##  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
##  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
##  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
##  THE SOFTWARE.
## 
##  bjoern@cs.stanford.edu 12/30/2008
## 

type
  UDP* {.importcpp: "UDP", header: "Udp.h", bycopy.} = object of Stream
  

proc begin*(this: var UDP; a3: uint16_t): uint8_t {.cdecl, importcpp: "begin",
    header: "Udp.h".}
proc stop*(this: var UDP) {.cdecl, importcpp: "stop", header: "Udp.h".}
proc beginPacket*(this: var UDP; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "beginPacket", header: "Udp.h".}
proc beginPacket*(this: var UDP; host: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "beginPacket", header: "Udp.h".}
proc endPacket*(this: var UDP): cint {.cdecl, importcpp: "endPacket", header: "Udp.h".}
proc write*(this: var UDP; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "Udp.h".}
proc write*(this: var UDP; buffer: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "Udp.h".}
proc parsePacket*(this: var UDP): cint {.cdecl, importcpp: "parsePacket",
                                    header: "Udp.h".}
proc available*(this: var UDP): cint {.cdecl, importcpp: "available", header: "Udp.h".}
proc read*(this: var UDP): cint {.cdecl, importcpp: "read", header: "Udp.h".}
proc read*(this: var UDP; buffer: ptr cuchar; len: csize): cint {.cdecl, importcpp: "read",
    header: "Udp.h".}
proc read*(this: var UDP; buffer: cstring; len: csize): cint {.cdecl, importcpp: "read",
    header: "Udp.h".}
proc peek*(this: var UDP): cint {.cdecl, importcpp: "peek", header: "Udp.h".}
proc flush*(this: var UDP) {.cdecl, importcpp: "flush", header: "Udp.h".}
proc remoteIP*(this: var UDP): IPAddress {.cdecl, importcpp: "remoteIP", header: "Udp.h".}
proc remotePort*(this: var UDP): uint16_t {.cdecl, importcpp: "remotePort",
                                       header: "Udp.h".}