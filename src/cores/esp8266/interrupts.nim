import
  c_types, ets_sys

##  these auto classes wrap up xt_rsil so your code can be simplier, but can only be
##  used in an ino or cpp files.
##  InterruptLock is used when you want to completely disable interrupts
## {
##     {
##       InterruptLock lock; 
##       // do work within interrupt lock here
##     }
##     do work outside of interrupt lock here outside its scope
## }
## 

type
  InterruptLock* {.importcpp: "InterruptLock", header: "interrupts.h", bycopy.} = object
  

proc constructInterruptLock*(): InterruptLock {.cdecl, constructor,
    importcpp: "InterruptLock(@)", header: "interrupts.h".}
proc destroyInterruptLock*(this: var InterruptLock) {.cdecl,
    importcpp: "#.~InterruptLock()", header: "interrupts.h".}
proc savedInterruptLevel*(this: InterruptLock): uint32_t {.noSideEffect, cdecl,
    importcpp: "savedInterruptLevel", header: "interrupts.h".}
##  AutoInterruptLock is when you need to set a specific level, A normal use pattern is like
## 
## {
##     {
##       AutoInterruptLock(1); // this routine will allow level 2 and above
##       // do work within interrupt lock here
##     }
##     do work outside of interrupt lock here outside its scope
## }
## 

template AutoInterruptLock*(intrLevel: untyped): void =
  type
    _AutoDisableIntr* {.importcpp: "_AutoDisableIntr", header: "interrupts.h", bycopy.} = object
    
  proc construct_AutoDisableIntr*(): _AutoDisableIntr {.cdecl, constructor,
      importcpp: "_AutoDisableIntr(@)", header: "interrupts.h".}
  proc destroy_AutoDisableIntr*(this: var _AutoDisableIntr) {.cdecl,
      importcpp: "#.~_AutoDisableIntr()", header: "interrupts.h".}
  