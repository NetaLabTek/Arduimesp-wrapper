## 
##  flash_utils.h - Flash access function and data structures
##  Copyright (c) 2015 Ivan Grokhotkov.  All right reserved.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

proc SPIEraseBlock*(`block`: uint32_t): cint {.cdecl, importcpp: "SPIEraseBlock(@)",
    header: "flash_utils.h".}
proc SPIEraseSector*(sector: uint32_t): cint {.cdecl, importcpp: "SPIEraseSector(@)",
    header: "flash_utils.h".}
proc SPIRead*(`addr`: uint32_t; dest: pointer; size: csize): cint {.cdecl,
    importcpp: "SPIRead(@)", header: "flash_utils.h".}
proc SPIWrite*(`addr`: uint32_t; src: pointer; size: csize): cint {.cdecl,
    importcpp: "SPIWrite(@)", header: "flash_utils.h".}
proc SPIEraseAreaEx*(start: uint32_t; size: uint32_t): cint {.cdecl,
    importcpp: "SPIEraseAreaEx(@)", header: "flash_utils.h".}
const
  FLASH_SECTOR_SIZE* = 0x00001000
  FLASH_BLOCK_SIZE* = 0x00010000
  APP_START_OFFSET* = 0x00001000

type
  image_header_t* {.importcpp: "image_header_t", header: "flash_utils.h", bycopy.} = object
    magic* {.importc: "magic".}: cuchar
    num_segments* {.importc: "num_segments".}: cuchar ##  SPI Flash Interface (0 = QIO, 1 = QOUT, 2 = DIO, 0x3 = DOUT)
    flash_mode* {.importc: "flash_mode".}: cuchar ##  High four bits: 0 = 512K, 1 = 256K, 2 = 1M, 3 = 2M, 4 = 4M, 8 = 8M, 9 = 16M
                                              ##        Low four bits:  0 = 40MHz, 1= 26MHz, 2 = 20MHz, 0xf = 80MHz
    flash_size_freq* {.importc: "flash_size_freq".}: cuchar
    entry* {.importc: "entry".}: uint32_t

  section_header_t* {.importcpp: "section_header_t", header: "flash_utils.h", bycopy.} = object
    address* {.importc: "address".}: uint32_t
    size* {.importc: "size".}: uint32_t

