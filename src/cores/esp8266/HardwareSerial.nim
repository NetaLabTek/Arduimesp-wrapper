## 
##  HardwareSerial.h - Hardware serial library for Wiring
##  Copyright (c) 2006 Nicholas Zambetti.  All right reserved.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##  Modified 28 September 2010 by Mark Sproul
##  Modified 14 August 2012 by Alarus
##  Modified 3 December 2013 by Matthijs Kooijman
##  Modified 18 December 2014 by Ivan Grokhotkov (esp8266 platform support)
##  Modified 31 March 2015 by Markus Sattler (rewrite the code for UART0 + UART1 support in ESP8266)
##  Modified 25 April 2015 by Thomas Flayols (add configuration different from 8N1 in ESP8266)
## 

import
  Stream, uart

type
  SerialConfig* {.size: sizeof(cint), importcpp: "SerialConfig",
                 header: "HardwareSerial.h".} = enum
    SERIAL_5N1 = UART_5N1, SERIAL_6N1 = UART_6N1, SERIAL_7N1 = UART_7N1,
    SERIAL_8N1 = UART_8N1, SERIAL_5N2 = UART_5N2, SERIAL_6N2 = UART_6N2,
    SERIAL_7N2 = UART_7N2, SERIAL_8N2 = UART_8N2, SERIAL_5E1 = UART_5E1,
    SERIAL_6E1 = UART_6E1, SERIAL_7E1 = UART_7E1, SERIAL_8E1 = UART_8E1,
    SERIAL_5E2 = UART_5E2, SERIAL_6E2 = UART_6E2, SERIAL_7E2 = UART_7E2,
    SERIAL_8E2 = UART_8E2, SERIAL_5O1 = UART_5O1, SERIAL_6O1 = UART_6O1,
    SERIAL_7O1 = UART_7O1, SERIAL_8O1 = UART_8O1, SERIAL_5O2 = UART_5O2,
    SERIAL_6O2 = UART_6O2, SERIAL_7O2 = UART_7O2, SERIAL_8O2 = UART_8O2


type
  SerialMode* {.size: sizeof(cint), importcpp: "SerialMode",
               header: "HardwareSerial.h".} = enum
    SERIAL_FULL = UART_FULL, SERIAL_RX_ONLY = UART_RX_ONLY,
    SERIAL_TX_ONLY = UART_TX_ONLY


type
  HardwareSerial* {.importcpp: "HardwareSerial", header: "HardwareSerial.h", bycopy.} = object of Stream
  

proc constructHardwareSerial*(uart_nr: cint): HardwareSerial {.cdecl, constructor,
    importcpp: "HardwareSerial(@)", header: "HardwareSerial.h".}
proc destroyHardwareSerial*(this: var HardwareSerial) {.cdecl,
    importcpp: "#.~HardwareSerial()", header: "HardwareSerial.h".}
proc begin*(this: var HardwareSerial; baud: culong) {.cdecl, importcpp: "begin",
    header: "HardwareSerial.h".}
proc begin*(this: var HardwareSerial; baud: culong; config: SerialConfig) {.cdecl,
    importcpp: "begin", header: "HardwareSerial.h".}
proc begin*(this: var HardwareSerial; baud: culong; config: SerialConfig;
           mode: SerialMode) {.cdecl, importcpp: "begin", header: "HardwareSerial.h".}
proc begin*(this: var HardwareSerial; baud: culong; config: SerialConfig;
           mode: SerialMode; tx_pin: uint8_t) {.cdecl, importcpp: "begin",
    header: "HardwareSerial.h".}
proc `end`*(this: var HardwareSerial) {.cdecl, importcpp: "end",
                                    header: "HardwareSerial.h".}
proc setRxBufferSize*(this: var HardwareSerial; size: csize): csize {.cdecl,
    importcpp: "setRxBufferSize", header: "HardwareSerial.h".}
proc swap*(this: var HardwareSerial) {.cdecl, importcpp: "swap",
                                   header: "HardwareSerial.h".}
proc swap*(this: var HardwareSerial; tx_pin: uint8_t) {.cdecl, importcpp: "swap",
    header: "HardwareSerial.h".}
  ## toggle between use of GPIO13/GPIO15 or GPIO3/GPIO(1/2) as RX and TX
proc set_tx*(this: var HardwareSerial; tx_pin: uint8_t) {.cdecl, importcpp: "set_tx",
    header: "HardwareSerial.h".}
proc pins*(this: var HardwareSerial; tx: uint8_t; rx: uint8_t) {.cdecl,
    importcpp: "pins", header: "HardwareSerial.h".}
proc available*(this: var HardwareSerial): cint {.cdecl, importcpp: "available",
    header: "HardwareSerial.h".}
proc peek*(this: var HardwareSerial): cint {.cdecl, importcpp: "peek",
                                        header: "HardwareSerial.h".}
proc read*(this: var HardwareSerial): cint {.cdecl, importcpp: "read",
                                        header: "HardwareSerial.h".}
proc availableForWrite*(this: var HardwareSerial): cint {.cdecl,
    importcpp: "availableForWrite", header: "HardwareSerial.h".}
proc flush*(this: var HardwareSerial) {.cdecl, importcpp: "flush",
                                    header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; c: uint8_t): csize {.cdecl, importcpp: "write",
    header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; n: culong): csize {.cdecl, importcpp: "write",
    header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; n: clong): csize {.cdecl, importcpp: "write",
    header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; n: cuint): csize {.cdecl, importcpp: "write",
    header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; n: cint): csize {.cdecl, importcpp: "write",
    header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; buffer: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "HardwareSerial.h".}
proc write*(this: var HardwareSerial; buffer: cstring): csize {.cdecl,
    importcpp: "write", header: "HardwareSerial.h".}
converter `bool`*(this: HardwareSerial): bool {.noSideEffect, cdecl,
    importcpp: "HardwareSerial::operator bool", header: "HardwareSerial.h".}
proc setDebugOutput*(this: var HardwareSerial; a3: bool) {.cdecl,
    importcpp: "setDebugOutput", header: "HardwareSerial.h".}
proc isTxEnabled*(this: var HardwareSerial): bool {.cdecl, importcpp: "isTxEnabled",
    header: "HardwareSerial.h".}
proc isRxEnabled*(this: var HardwareSerial): bool {.cdecl, importcpp: "isRxEnabled",
    header: "HardwareSerial.h".}
proc baudRate*(this: var HardwareSerial): cint {.cdecl, importcpp: "baudRate",
    header: "HardwareSerial.h".}
proc hasOverrun*(this: var HardwareSerial): bool {.cdecl, importcpp: "hasOverrun",
    header: "HardwareSerial.h".}
var Serial* {.importcpp: "Serial", header: "HardwareSerial.h".}: HardwareSerial

var Serial1* {.importcpp: "Serial1", header: "HardwareSerial.h".}: HardwareSerial
