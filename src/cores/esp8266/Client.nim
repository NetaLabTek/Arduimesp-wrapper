## 
##  Client.h - Base class that provides Client
##  Copyright (c) 2011 Adrian McEwen.  All right reserved.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  Print, Stream, IPAddress

type
  Client* {.importcpp: "Client", header: "Client.h", bycopy.} = object of Stream
  

proc connect*(this: var Client; ip: IPAddress; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "Client.h".}
proc connect*(this: var Client; host: cstring; port: uint16_t): cint {.cdecl,
    importcpp: "connect", header: "Client.h".}
proc write*(this: var Client; a3: uint8_t): csize {.cdecl, importcpp: "write",
    header: "Client.h".}
proc write*(this: var Client; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "Client.h".}
proc available*(this: var Client): cint {.cdecl, importcpp: "available",
                                     header: "Client.h".}
proc read*(this: var Client): cint {.cdecl, importcpp: "read", header: "Client.h".}
proc read*(this: var Client; buf: ptr uint8_t; size: csize): cint {.cdecl,
    importcpp: "read", header: "Client.h".}
proc peek*(this: var Client): cint {.cdecl, importcpp: "peek", header: "Client.h".}
proc flush*(this: var Client) {.cdecl, importcpp: "flush", header: "Client.h".}
proc stop*(this: var Client) {.cdecl, importcpp: "stop", header: "Client.h".}
proc connected*(this: var Client): uint8_t {.cdecl, importcpp: "connected",
                                        header: "Client.h".}
converter `bool`*(this: var Client): bool {.cdecl, importcpp: "Client::operator bool",
                                       header: "Client.h".}