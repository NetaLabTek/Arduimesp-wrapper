## 
##  Stream.h - base class for character-based streams.
##  Copyright (c) 2010 David A. Mellis.  All right reserved.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 
##  parsing functions based on TextFinder library by Michael Margolis
## 

import
  Print

##  compatability macros for testing
## 
##  #define   getInt()            parseInt()
##  #define   getInt(skipChar)    parseInt(skipchar)
##  #define   getFloat()          parseFloat()
##  #define   getFloat(skipChar)  parseFloat(skipChar)
##  #define   getString( pre_string, post_string, buffer, length)
##  readBytesBetween( pre_string, terminator, buffer, length)
## 

type
  Stream* {.importcpp: "Stream", header: "Stream.h", bycopy.} = object of Print
    ##  number of milliseconds to wait for the next char before aborting timed read
    ##  used for timeout measurement
  

proc available*(this: var Stream): cint {.cdecl, importcpp: "available",
                                     header: "Stream.h".}
proc read*(this: var Stream): cint {.cdecl, importcpp: "read", header: "Stream.h".}
proc peek*(this: var Stream): cint {.cdecl, importcpp: "peek", header: "Stream.h".}
proc constructStream*(): Stream {.cdecl, constructor, importcpp: "Stream(@)",
                               header: "Stream.h".}
proc setTimeout*(this: var Stream; timeout: culong) {.cdecl, importcpp: "setTimeout",
    header: "Stream.h".}
proc find*(this: var Stream; target: cstring): bool {.cdecl, importcpp: "find",
    header: "Stream.h".}
proc find*(this: var Stream; target: ptr uint8_t): bool {.cdecl, importcpp: "find",
    header: "Stream.h".}
proc find*(this: var Stream; target: cstring; length: csize): bool {.cdecl,
    importcpp: "find", header: "Stream.h".}
proc find*(this: var Stream; target: ptr uint8_t; length: csize): bool {.cdecl,
    importcpp: "find", header: "Stream.h".}
proc find*(this: var Stream; target: char): bool {.cdecl, importcpp: "find",
    header: "Stream.h".}
proc findUntil*(this: var Stream; target: cstring; terminator: cstring): bool {.cdecl,
    importcpp: "findUntil", header: "Stream.h".}
proc findUntil*(this: var Stream; target: ptr uint8_t; terminator: cstring): bool {.cdecl,
    importcpp: "findUntil", header: "Stream.h".}
proc findUntil*(this: var Stream; target: cstring; targetLen: csize; terminate: cstring;
               termLen: csize): bool {.cdecl, importcpp: "findUntil",
                                    header: "Stream.h".}
proc findUntil*(this: var Stream; target: ptr uint8_t; targetLen: csize;
               terminate: cstring; termLen: csize): bool {.cdecl,
    importcpp: "findUntil", header: "Stream.h".}
proc parseInt*(this: var Stream): clong {.cdecl, importcpp: "parseInt",
                                     header: "Stream.h".}
proc parseFloat*(this: var Stream): cfloat {.cdecl, importcpp: "parseFloat",
                                        header: "Stream.h".}
proc readBytes*(this: var Stream; buffer: cstring; length: csize): csize {.cdecl,
    importcpp: "readBytes", header: "Stream.h".}
proc readBytes*(this: var Stream; buffer: ptr uint8_t; length: csize): csize {.cdecl,
    importcpp: "readBytes", header: "Stream.h".}
proc readBytesUntil*(this: var Stream; terminator: char; buffer: cstring; length: csize): csize {.
    cdecl, importcpp: "readBytesUntil", header: "Stream.h".}
proc readBytesUntil*(this: var Stream; terminator: char; buffer: ptr uint8_t;
                    length: csize): csize {.cdecl, importcpp: "readBytesUntil",
    header: "Stream.h".}
proc readString*(this: var Stream): String {.cdecl, importcpp: "readString",
                                        header: "Stream.h".}
proc readStringUntil*(this: var Stream; terminator: char): String {.cdecl,
    importcpp: "readStringUntil", header: "Stream.h".}