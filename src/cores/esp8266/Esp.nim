## 
##  Esp.h - ESP8266-specific APIs
##  Copyright (c) 2015 Ivan Grokhotkov. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

## *
##  AVR macros for WDT managment
## 

type
  WDTO_t* {.size: sizeof(cint), importcpp: "WDTO_t", header: "Esp.h".} = enum
    WDTO_0MS = 0,               ## !< WDTO_0MS
    WDTO_15MS = 15,             ## !< WDTO_15MS
    WDTO_30MS = 30,             ## !< WDTO_30MS
    WDTO_60MS = 60,             ## !< WDTO_60MS
    WDTO_120MS = 120,           ## !< WDTO_120MS
    WDTO_250MS = 250,           ## !< WDTO_250MS
    WDTO_500MS = 500,           ## !< WDTO_500MS
    WDTO_1S = 1000,             ## !< WDTO_1S
    WDTO_2S = 2000,             ## !< WDTO_2S
    WDTO_4S = 4000,             ## !< WDTO_4S
    WDTO_8S = 8000


template wdt_enable*(time: untyped): untyped =
  ESP.wdtEnable(time)

template wdt_disable*(): untyped =
  ESP.wdtDisable()

template wdt_reset*(): untyped =
  ESP.wdtFeed()

template cli*(): untyped =
  ets_intr_lock()             ##  IRQ Disable
  
template sei*(): untyped =
  ets_intr_unlock()           ##  IRQ Enable
  
type
  RFMode* {.size: sizeof(cint), importcpp: "RFMode", header: "Esp.h".} = enum
    RF_DEFAULT = 0,             ##  RF_CAL or not after deep-sleep wake up, depends on init data byte 108.
    RF_CAL = 1,                 ##  RF_CAL after deep-sleep wake up, there will be large current.
    RF_NO_CAL = 2,              ##  no RF_CAL after deep-sleep wake up, there will only be small current.
    RF_DISABLED = 4


#template RF_MODE*(mode: untyped): void =
#  proc get_rf_mode*(): cint {.cdecl.}

template RF_PRE_INIT*(): void =
  proc run_user_rf_pre_init*() {.cdecl, importcpp: "__run_user_rf_pre_init(@)",
                                 header: "Esp.h".}

##  compatibility definitions

const
  WakeMode* = RFMode
  WAKE_RF_DEFAULT* = RF_DEFAULT
  WAKE_RFCAL* = RF_CAL
  WAKE_NO_RFCAL* = RF_NO_CAL
  WAKE_RF_DISABLED* = RF_DISABLED

type
  ADCMode* {.size: sizeof(cint), importcpp: "ADCMode", header: "Esp.h".} = enum
    ADC_TOUT = 33, ADC_VCC = 255

const
  ADC_TOUT_3V3 = ADC_TOUT
  ADC_VDD = ADC_VCC

#template ADC_MODE*(mode: untyped): void =
#  proc get_adc_mode*(): cint {.cdecl.}

type
  FlashMode_t* {.size: sizeof(cint), importcpp: "FlashMode_t", header: "Esp.h".} = enum
    FM_QIO = 0x00000000, FM_QOUT = 0x00000001, FM_DIO = 0x00000002, FM_DOUT = 0x00000003,
    FM_UNKNOWN = 0x000000FF


type
  EspClass* {.importcpp: "EspClass", header: "Esp.h", bycopy.} = object ##  TODO: figure out how to set WDT timeout
  

proc wdtEnable*(this: var EspClass; timeout_ms: uint32 = 0) {.cdecl,
    importcpp: "wdtEnable", header: "Esp.h".}
proc wdtEnable*(this: var EspClass; timeout_ms: WDTO_t = WDTO_0MS) {.cdecl,
    importcpp: "wdtEnable", header: "Esp.h".}
proc wdtDisable*(this: var EspClass) {.cdecl, importcpp: "wdtDisable", header: "Esp.h".}
proc wdtFeed*(this: var EspClass) {.cdecl, importcpp: "wdtFeed", header: "Esp.h".}
proc deepSleep*(this: var EspClass; time_us: uint64; mode: RFMode = RF_DEFAULT) {.cdecl,
    importcpp: "deepSleep", header: "Esp.h".}
proc deepSleepMax*(this: var EspClass): uint64 {.cdecl, importcpp: "deepSleepMax",
    header: "Esp.h".}
proc rtcUserMemoryRead*(this: var EspClass; offset: uint32; data: ptr uint32;
                       size: csize): bool {.cdecl, importcpp: "rtcUserMemoryRead",
    header: "Esp.h".}
proc rtcUserMemoryWrite*(this: var EspClass; offset: uint32; data: ptr uint32;
                        size: csize): bool {.cdecl, importcpp: "rtcUserMemoryWrite",
    header: "Esp.h".}
proc reset*(this: var EspClass) {.cdecl, importcpp: "reset", header: "Esp.h".}
proc restart*(this: var EspClass) {.cdecl, importcpp: "restart", header: "Esp.h".}
proc getVcc*(this: var EspClass): uint16 {.cdecl, importcpp: "getVcc", header: "Esp.h".}
proc getFreeHeap*(this: var EspClass): uint32 {.cdecl, importcpp: "getFreeHeap",
    header: "Esp.h".}
proc getChipId*(this: var EspClass): uint32 {.cdecl, importcpp: "getChipId",
    header: "Esp.h".}
proc getSdkVersion*(this: var EspClass): cstring {.cdecl, importcpp: "getSdkVersion",
    header: "Esp.h".}
proc getCoreVersion*(this: var EspClass): cstring {.cdecl, importcpp: "getCoreVersion",
    header: "Esp.h".}
proc getFullVersion*(this: var EspClass): cstring {.cdecl, importcpp: "getFullVersion",
    header: "Esp.h".}
proc getBootVersion*(this: var EspClass): uint8 {.cdecl,
    importcpp: "getBootVersion", header: "Esp.h".}
proc getBootMode*(this: var EspClass): uint8 {.cdecl, importcpp: "getBootMode",
    header: "Esp.h".}
proc getCpuFreqMHz*(this: var EspClass): uint8 {.cdecl, importcpp: "getCpuFreqMHz",
    header: "Esp.h".}
proc getFlashChipId*(this: var EspClass): uint32 {.cdecl,
    importcpp: "getFlashChipId", header: "Esp.h".}
proc getFlashChipRealSize*(this: var EspClass): uint32 {.cdecl,
    importcpp: "getFlashChipRealSize", header: "Esp.h".}
proc getFlashChipSize*(this: var EspClass): uint32 {.cdecl,
    importcpp: "getFlashChipSize", header: "Esp.h".}
proc getFlashChipSpeed*(this: var EspClass): uint32 {.cdecl,
    importcpp: "getFlashChipSpeed", header: "Esp.h".}
proc getFlashChipMode*(this: var EspClass): FlashMode_t {.cdecl,
    importcpp: "getFlashChipMode", header: "Esp.h".}
proc getFlashChipSizeByChipId*(this: var EspClass): uint32 {.cdecl,
    importcpp: "getFlashChipSizeByChipId", header: "Esp.h".}
proc magicFlashChipSize*(this: var EspClass; byte: uint8): uint32 {.cdecl,
    importcpp: "magicFlashChipSize", header: "Esp.h".}
proc magicFlashChipSpeed*(this: var EspClass; byte: uint8): uint32 {.cdecl,
    importcpp: "magicFlashChipSpeed", header: "Esp.h".}
proc magicFlashChipMode*(this: var EspClass; byte: uint8): FlashMode_t {.cdecl,
    importcpp: "magicFlashChipMode", header: "Esp.h".}
proc checkFlashConfig*(this: var EspClass; needsEquals: bool = false): bool {.cdecl,
    importcpp: "checkFlashConfig", header: "Esp.h".}
proc flashEraseSector*(this: var EspClass; sector: uint32): bool {.cdecl,
    importcpp: "flashEraseSector", header: "Esp.h".}
proc flashWrite*(this: var EspClass; offset: uint32; data: ptr uint32; size: csize): bool {.
    cdecl, importcpp: "flashWrite", header: "Esp.h".}
proc flashRead*(this: var EspClass; offset: uint32; data: ptr uint32; size: csize): bool {.
    cdecl, importcpp: "flashRead", header: "Esp.h".}
proc getSketchSize*(this: var EspClass): uint32 {.cdecl, importcpp: "getSketchSize",
    header: "Esp.h".}
proc getSketchMD5*(this: var EspClass): cstring {.cdecl, importcpp: "getSketchMD5",
    header: "Esp.h".}
proc getFreeSketchSpace*(this: var EspClass): uint32 {.cdecl,
    importcpp: "getFreeSketchSpace", header: "Esp.h".}
#proc updateSketch*(this: var EspClass; `in`: var stream; size: uint32;
#                  restartOnFail: bool = false; restartOnSuccess: bool = true): bool {.
#    cdecl, importcpp: "updateSketch", header: "Esp.h".}
proc getResetReason*(this: var EspClass): cstring {.cdecl, importcpp: "getResetReason",
    header: "Esp.h".}
proc getResetInfo*(this: var EspClass): cstring {.cdecl, importcpp: "getResetInfo",
    header: "Esp.h".}
#proc getResetInfoPtr*(): ptr rst_info {.cdecl, importcpp: "getResetInfoPtr(@)",
#                                    header: "Esp.h".}
proc eraseConfig*(this: var EspClass): bool {.cdecl, importcpp: "eraseConfig",
    header: "Esp.h".}
proc getCycleCount*(this: var EspClass): uint32 {.cdecl, importcpp: "getCycleCount",
    header: "Esp.h".}
#proc getCycleCount*(this: var EspClass): uint32 {.cdecl, importcpp: "getCycleCount",
#    header: "Esp.h".}
var ESP* {.importcpp: "ESP", header: "Esp.h".}: EspClass
