const
  UPDATE_ERROR_OK* = (0)
  UPDATE_ERROR_WRITE* = (1)
  UPDATE_ERROR_ERASE* = (2)
  UPDATE_ERROR_READ* = (3)
  UPDATE_ERROR_SPACE* = (4)
  UPDATE_ERROR_SIZE* = (5)
  UPDATE_ERROR_STREAM* = (6)
  UPDATE_ERROR_MD5* = (7)
  UPDATE_ERROR_FLASH_CONFIG* = (8)
  UPDATE_ERROR_NEW_FLASH_CONFIG* = (9)
  UPDATE_ERROR_MAGIC_BYTE* = (10)
  UPDATE_ERROR_BOOTSTRAP* = (11)
  U_FLASH* = 0
  U_SPIFFS* = 100
  U_AUTH* = 200

when defined(DEBUG_ESP_UPDATER):
  when defined(DEBUG_ESP_PORT):
    const
      DEBUG_UPDATER* = DEBUG_ESP_PORT
type
  UpdaterClass* {.importcpp: "UpdaterClass", header: "Updater.h", bycopy.} = object
    ##  amount of data written into _buffer
    ##  total size of _buffer
  

proc constructUpdaterClass*(): UpdaterClass {.cdecl, constructor,
    importcpp: "UpdaterClass(@)", header: "Updater.h".}
proc begin*(this: var UpdaterClass; size: csize; command: cint = U_FLASH): bool {.cdecl,
    importcpp: "begin", header: "Updater.h".}
proc runAsync*(this: var UpdaterClass; async: bool) {.cdecl, importcpp: "runAsync",
    header: "Updater.h".}
proc write*(this: var UpdaterClass; data: ptr uint8_t; len: csize): csize {.cdecl,
    importcpp: "write", header: "Updater.h".}
proc writeStream*(this: var UpdaterClass; data: var Stream): csize {.cdecl,
    importcpp: "writeStream", header: "Updater.h".}
proc `end`*(this: var UpdaterClass; evenIfRemaining: bool = false): bool {.cdecl,
    importcpp: "end", header: "Updater.h".}
proc printError*(this: var UpdaterClass; `out`: var Print) {.cdecl,
    importcpp: "printError", header: "Updater.h".}
proc setMD5*(this: var UpdaterClass; expected_md5: cstring): bool {.cdecl,
    importcpp: "setMD5", header: "Updater.h".}
proc md5String*(this: var UpdaterClass): String {.cdecl, importcpp: "md5String",
    header: "Updater.h".}
proc md5*(this: var UpdaterClass; result: ptr uint8_t) {.cdecl, importcpp: "md5",
    header: "Updater.h".}
proc getError*(this: var UpdaterClass): uint8_t {.cdecl, importcpp: "getError",
    header: "Updater.h".}
proc clearError*(this: var UpdaterClass) {.cdecl, importcpp: "clearError",
                                       header: "Updater.h".}
proc hasError*(this: var UpdaterClass): bool {.cdecl, importcpp: "hasError",
    header: "Updater.h".}
proc isRunning*(this: var UpdaterClass): bool {.cdecl, importcpp: "isRunning",
    header: "Updater.h".}
proc isFinished*(this: var UpdaterClass): bool {.cdecl, importcpp: "isFinished",
    header: "Updater.h".}
proc size*(this: var UpdaterClass): csize {.cdecl, importcpp: "size",
                                       header: "Updater.h".}
proc progress*(this: var UpdaterClass): csize {.cdecl, importcpp: "progress",
    header: "Updater.h".}
proc remaining*(this: var UpdaterClass): csize {.cdecl, importcpp: "remaining",
    header: "Updater.h".}
proc write*[T](this: var UpdaterClass; data: var T): csize {.cdecl, importcpp: "write",
    header: "Updater.h".}
var Update* {.importcpp: "Update", header: "Updater.h".}: UpdaterClass
