## 
##   md5.h - exposed md5 ROM functions for esp8266
## 
##   Copyright (c) 2015 Hristo Gochkov. All rights reserved.
##   This file is part of the esp8266 core for Arduino environment.
## 
##   This library is free software; you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public
##   License as published by the Free Software Foundation; either
##   version 2.1 of the License, or (at your option) any later version.
## 
##   This library is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##   Lesser General Public License for more details.
## 
##   You should have received a copy of the GNU Lesser General Public
##   License along with this library; if not, write to the Free Software
##   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  md5

type
  MD5Builder* {.importcpp: "MD5Builder", header: "MD5Builder.h", bycopy.} = object
  

proc begin*(this: var MD5Builder) {.cdecl, importcpp: "begin", header: "MD5Builder.h".}
proc add*(this: var MD5Builder; data: ptr uint8_t; len: uint16_t) {.cdecl,
    importcpp: "add", header: "MD5Builder.h".}
proc add*(this: var MD5Builder; data: cstring) {.cdecl, importcpp: "add",
    header: "MD5Builder.h".}
proc add*(this: var MD5Builder; data: cstring) {.cdecl, importcpp: "add",
    header: "MD5Builder.h".}
proc add*(this: var MD5Builder; data: String) {.cdecl, importcpp: "add",
    header: "MD5Builder.h".}
proc addHexString*(this: var MD5Builder; data: cstring) {.cdecl,
    importcpp: "addHexString", header: "MD5Builder.h".}
proc addHexString*(this: var MD5Builder; data: cstring) {.cdecl,
    importcpp: "addHexString", header: "MD5Builder.h".}
proc addHexString*(this: var MD5Builder; data: String) {.cdecl,
    importcpp: "addHexString", header: "MD5Builder.h".}
proc addStream*(this: var MD5Builder; stream: var Stream; maxLen: csize): bool {.cdecl,
    importcpp: "addStream", header: "MD5Builder.h".}
proc calculate*(this: var MD5Builder) {.cdecl, importcpp: "calculate",
                                    header: "MD5Builder.h".}
proc getBytes*(this: var MD5Builder; output: ptr uint8_t) {.cdecl,
    importcpp: "getBytes", header: "MD5Builder.h".}
proc getChars*(this: var MD5Builder; output: cstring) {.cdecl, importcpp: "getChars",
    header: "MD5Builder.h".}
proc toString*(this: var MD5Builder): String {.cdecl, importcpp: "toString",
    header: "MD5Builder.h".}