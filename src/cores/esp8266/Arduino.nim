## 
##  Arduino.h - Main include file for the Arduino SDK
##  Copyright (c) 2005-2013 Arduino Team.  All right reserved.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  stdlib_noniso, binary, esp8266_peri, twi, core_esp8266_features

const
  HIGH* = 0x00000001
  LOW* = 0x00000000
  PWMRANGE* = 1023

## GPIO FUNCTIONS

const
  INPUT* = 0x00000000
  INPUT_PULLUP* = 0x00000002
  INPUT_PULLDOWN_16* = 0x00000004
  OUTPUT* = 0x00000001
  OUTPUT_OPEN_DRAIN* = 0x00000003
  WAKEUP_PULLUP* = 0x00000005
  WAKEUP_PULLDOWN* = 0x00000007
  SPECIAL* = 0x000000F8
  FUNCTION_0* = 0x00000008
  FUNCTION_1* = 0x00000018
  FUNCTION_2* = 0x00000028
  FUNCTION_3* = 0x00000038
  FUNCTION_4* = 0x00000048
  PI* = 3.141592653589793
  HALF_PI* = 1.570796326794897
  TWO_PI* = 6.283185307179586
  DEG_TO_RAD* = 0.0174532925199433
  RAD_TO_DEG* = 57.29577951308232
  EULER* = 2.718281828459045
  SERIAL* = 0x00000000
  DISPLAY* = 0x00000001
  LSBFIRST* = 0
  MSBFIRST* = 1

## Interrupt Modes

const
  RISING* = 0x00000001
  FALLING* = 0x00000002
  CHANGE* = 0x00000003
  ONLOW* = 0x00000004
  ONHIGH* = 0x00000005
  ONLOW_WE* = 0x0000000C
  ONHIGH_WE* = 0x0000000D
  DEFAULT* = 1
  EXTERNAL* = 0

## timer dividers

type
  TIM_DIV_ENUM* {.size: sizeof(cint), importcpp: "TIM_DIV_ENUM", header: "Arduino.h".} = enum
    TIM_DIV1 = 0,               ## 80MHz (80 ticks/us - 104857.588 us max)
    TIM_DIV16 = 1,              ## 5MHz (5 ticks/us - 1677721.4 us max)
    TIM_DIV256 = 3


## timer int_types

const
  TIM_EDGE* = 0
  TIM_LEVEL* = 1

## timer reload values

const
  TIM_SINGLE* = 0
  TIM_LOOP* = 1

template timer1_read*(): untyped =
  (T1V)

template timer1_enabled*(): untyped =
  ((T1C and (1 shl TCTE)) != 0)

template timer1_interrupted*(): untyped =
  ((T1C and (1 shl TCIS)) != 0)

type
  timercallback* = proc () {.cdecl.}

proc timer1_isr_init*() {.cdecl, importcpp: "timer1_isr_init(@)", header: "Arduino.h".}
proc timer1_enable*(divider: uint8; int_type: uint8; reload: uint8) {.cdecl,
    importcpp: "timer1_enable(@)", header: "Arduino.h".}
proc timer1_disable*() {.cdecl, importcpp: "timer1_disable(@)", header: "Arduino.h".}
proc timer1_attachInterrupt*(userFunc: timercallback) {.cdecl,
    importcpp: "timer1_attachInterrupt(@)", header: "Arduino.h".}
proc timer1_detachInterrupt*() {.cdecl, importcpp: "timer1_detachInterrupt(@)",
                               header: "Arduino.h".}
proc timer1_write*(ticks: uint32) {.cdecl, importcpp: "timer1_write(@)",
                                   header: "Arduino.h".}
## maximum ticks 8388607
##  timer0 is a special CPU timer that has very high resolution but with
##  limited control.
##  it uses CCOUNT (ESP.GetCycleCount()) as the non-resetable timer counter
##  it does not support divide, type, or reload flags
##  it is auto-disabled when the compare value matches CCOUNT
##  it is auto-enabled when the compare value changes

template timer0_interrupted*(): untyped =
  (ETS_INTR_PENDING() and BV(ETS_COMPARE0_INUM))

## #define timer0_read() ((__extension__({uint32 count;__asm__ __volatile__("esync; rsr %0,ccompare0":"=a" (count));count;})))
## #define timer0_write(count) __asm__ __volatile__("wsr %0,ccompare0; esync"::"a" (count) : "memory")

proc timer0_isr_init*() {.cdecl, importcpp: "timer0_isr_init(@)", header: "Arduino.h".}
proc timer0_attachInterrupt*(userFunc: timercallback) {.cdecl,
    importcpp: "timer0_attachInterrupt(@)", header: "Arduino.h".}
proc timer0_detachInterrupt*() {.cdecl, importcpp: "timer0_detachInterrupt(@)",
                               header: "Arduino.h".}
##  undefine stdlib's abs if encountered

#when defined(abs): TODO undefine
template abs*(x: untyped): untyped =
  (if (x) > 0: (x) else: -(x))

template constrain*(amt, low, high: untyped): untyped =
  (if (amt) < (low): (low) else: (if (amt) > (high): (high) else: (amt)))

template round*(x: untyped): untyped =
  (if (x) >= 0: (long)((x) + 0.5) else: (long)((x) - 0.5))

template radians*(deg: untyped): untyped =
  ((deg) * DEG_TO_RAD)

template degrees*(rad: untyped): untyped =
  ((rad) * RAD_TO_DEG)

template sq*(x: untyped): untyped =
  ((x) * (x))

proc ets_intr_lock*() {.cdecl, importcpp: "ets_intr_lock(@)", header: "Arduino.h".}
proc ets_intr_unlock*() {.cdecl, importcpp: "ets_intr_unlock(@)", header: "Arduino.h".}
## #ifndef __STRINGIFY
## #define __STRINGIFY(a) #a
## #endif
##  these low level routines provide a replacement for SREG interrupt save that AVR uses
##  but are esp8266 specific. A normal use pattern is like
## 
## {
##     uint32 savedPS = xt_rsil(1); // this routine will allow level 2 and above
##     // do work here
##     xt_wsr_ps(savedPS); // restore the state
## }
## 
##  level (0-15), interrupts of the given level and above will be active
##  level 15 will disable ALL interrupts,
##  level 0 will enable ALL interrupts,
## 
## #define xt_rsil(level) (__extension__({uint32 state; __asm__ __volatile__("rsil %0," __STRINGIFY(level) : "=a" (state)); state;}))
## #define xt_wsr_ps(state)  __asm__ __volatile__("wsr %0,ps; isync" :: "a" (state) : "memory")

template interrupts*(): untyped =
  xt_rsil(0)

template noInterrupts*(): untyped =
  xt_rsil(15)

template clockCyclesPerMicrosecond*(): untyped =
  (F_CPU div 1000000)

template clockCyclesToMicroseconds*(a: untyped): untyped =
  ((a) div clockCyclesPerMicrosecond())

template microsecondsToClockCycles*(a: untyped): untyped =
  ((a) * clockCyclesPerMicrosecond())

template lowByte*(w: untyped): untyped =
  ((uint8)((w) and 0x000000FF))

template highByte*(w: untyped): untyped =
  ((uint8)((w) shr 8))

template bitRead*(value, bit: untyped): untyped =
  (((value) shr (bit)) and 0x00000001)

template bitSet*(value, bit: untyped): untyped =
  ((value) = (value) or (1 shl (bit)))

template bitClear*(value, bit: untyped): untyped =
  ((value) = (value) and not (1 shl (bit)))

template bitWrite*(value, bit, bitvalue: untyped): untyped =
  (if bitvalue: bitSet(value, bit) else: bitClear(value, bit))

##  avr-libc defines _NOP() since 1.6.2

when not defined(´_NOP´):
  ## #define _NOP() do { __asm__ volatile ("nop"); } while (0)
type
  word* = uint16

template bit*(b: untyped): untyped =
  (1 shl (b))

template BV*(b: untyped): untyped =
  (1 shl (b))

type
  boolean* = uint8
  byte* = uint8

proc init*() {.cdecl, importcpp: "init(@)", header: "Arduino.h".}
proc initVariant*() {.cdecl, importcpp: "initVariant(@)", header: "Arduino.h".}
## int atexit(void (*func)()) __attribute__((weak));

proc pinMode*(pin: uint8; mode: uint8) {.cdecl, importcpp: "pinMode(@)",
                                        header: "Arduino.h".}
proc digitalWrite*(pin: uint8; val: uint8) {.cdecl, importcpp: "digitalWrite(@)",
    header: "Arduino.h".}
proc digitalRead*(pin: uint8): cint {.cdecl, importcpp: "digitalRead(@)",
                                    header: "Arduino.h".}
proc analogRead*(pin: uint8): cint {.cdecl, importcpp: "analogRead(@)",
                                   header: "Arduino.h".}
proc analogReference*(mode: uint8) {.cdecl, importcpp: "analogReference(@)",
                                    header: "Arduino.h".}
proc analogWrite*(pin: uint8; val: cint) {.cdecl, importcpp: "analogWrite(@)",
                                        header: "Arduino.h".}
proc analogWriteFreq*(freq: uint32) {.cdecl, importcpp: "analogWriteFreq(@)",
                                     header: "Arduino.h".}
proc analogWriteRange*(range: uint32) {.cdecl, importcpp: "analogWriteRange(@)",
                                       header: "Arduino.h".}
proc millis*(): culong {.cdecl, importcpp: "millis(@)", header: "Arduino.h".}
proc micros*(): culong {.cdecl, importcpp: "micros(@)", header: "Arduino.h".}
proc micros64*(): uint64 {.cdecl, importcpp: "micros64(@)", header: "Arduino.h".}
proc delay*(a2: culong) {.cdecl, importcpp: "delay(@)", header: "Arduino.h".}
proc delayMicroseconds*(us: cuint) {.cdecl, importcpp: "delayMicroseconds(@)",
                                  header: "Arduino.h".}
proc pulseIn*(pin: uint8; state: uint8; timeout: culong): culong {.cdecl,
    importcpp: "pulseIn(@)", header: "Arduino.h".}
proc pulseInLong*(pin: uint8; state: uint8; timeout: culong): culong {.cdecl,
    importcpp: "pulseInLong(@)", header: "Arduino.h".}
proc shiftOut*(dataPin: uint8; clockPin: uint8; bitOrder: uint8; val: uint8) {.
    cdecl, importcpp: "shiftOut(@)", header: "Arduino.h".}
proc shiftIn*(dataPin: uint8; clockPin: uint8; bitOrder: uint8): uint8 {.cdecl,
    importcpp: "shiftIn(@)", header: "Arduino.h".}
proc attachInterrupt*(pin: uint8; a3: proc () {.cdecl.}; mode: cint) {.cdecl,
    importcpp: "attachInterrupt(@)", header: "Arduino.h".}
proc detachInterrupt*(pin: uint8) {.cdecl, importcpp: "detachInterrupt(@)",
                                   header: "Arduino.h".}
proc setup*() {.cdecl, importcpp: "setup(@)", header: "Arduino.h".}
proc loop*() {.cdecl, importcpp: "loop(@)", header: "Arduino.h".}
proc `yield`*() {.cdecl, importcpp: "yield(@)", header: "Arduino.h".}
proc optimistic_yield*(interval_us: uint32) {.cdecl,
    importcpp: "optimistic_yield(@)", header: "Arduino.h".}
template digitalPinToPort*(pin: untyped): untyped =
  (0)

template digitalPinToBitMask*(pin: untyped): untyped =
  (1 shl (pin))

template digitalPinToTimer*(pin: untyped): untyped =
  (0)

template portOutputRegister*(port: untyped): untyped =
  (cast[ptr uint32](addr(GPO)))

template portInputRegister*(port: untyped): untyped =
  (cast[ptr uint32](addr(GPI)))

template portModeRegister*(port: untyped): untyped =
  (cast[ptr uint32](addr(GPE)))

const
  NOT_A_PIN* = -1
  NOT_A_PORT* = -1
  NOT_AN_INTERRUPT* = -1
  NOT_ON_TIMER* = 0

## for compatibility, below 4 lines to be removed in release 3.0.0

## const int TIM_DIV265 __attribute__((deprecated, weak)) = TIM_DIV256;

#TODO: This is board-dependant, see variants folder
#import
#  pins_arduino

when defined(DEBUG_ESP_OOM):
  ##  reinclude *alloc redefinition because of <cstdlib> undefining them
  ##  this is mandatory for allowing OOM *alloc definitions in .ino files
  import
    umm_malloc/umm_malloc_cfg
