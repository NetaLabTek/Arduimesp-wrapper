##  ----------------------------------------------------------------------------
##  umm_malloc.h - a memory allocator for embedded systems (microcontrollers)
## 
##  See copyright notice in LICENSE.TXT
##  ----------------------------------------------------------------------------
## 

##  ------------------------------------------------------------------------

import
  umm_malloc_cfg

type                          ##  user-dependent
  UMM_HEAP_INFO* {.importcpp: "UMM_HEAP_INFO", header: "umm_malloc.h", bycopy.} = object
    totalEntries* {.importc: "totalEntries".}: cushort
    usedEntries* {.importc: "usedEntries".}: cushort
    freeEntries* {.importc: "freeEntries".}: cushort
    totalBlocks* {.importc: "totalBlocks".}: cushort
    usedBlocks* {.importc: "usedBlocks".}: cushort
    freeBlocks* {.importc: "freeBlocks".}: cushort
    maxFreeContiguousBlocks* {.importc: "maxFreeContiguousBlocks".}: cushort


var ummHeapInfo* {.importcpp: "ummHeapInfo", header: "umm_malloc.h".}: UMM_HEAP_INFO

proc umm_init*() {.cdecl, importcpp: "umm_init(@)", header: "umm_malloc.h".}
proc umm_info*(`ptr`: pointer; force: cint): pointer {.cdecl, importcpp: "umm_info(@)",
    header: "umm_malloc.h".}
proc umm_malloc*(size: csize): pointer {.cdecl, importcpp: "umm_malloc(@)",
                                     header: "umm_malloc.h".}
proc umm_calloc*(num: csize; size: csize): pointer {.cdecl, importcpp: "umm_calloc(@)",
    header: "umm_malloc.h".}
proc umm_realloc*(`ptr`: pointer; size: csize): pointer {.cdecl,
    importcpp: "umm_realloc(@)", header: "umm_malloc.h".}
proc umm_free*(`ptr`: pointer) {.cdecl, importcpp: "umm_free(@)",
                              header: "umm_malloc.h".}
proc umm_free_heap_size*(): csize {.cdecl, importcpp: "umm_free_heap_size(@)",
                                 header: "umm_malloc.h".}
##  ------------------------------------------------------------------------
