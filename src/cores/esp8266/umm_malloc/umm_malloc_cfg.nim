## 
##  Configuration for umm_malloc
## 

import
  c_types

## 
##  There are a number of defines you can set at compile time that affect how
##  the memory allocator will operate.
##  You can set them in your config file umm_malloc_cfg.h.
##  In GNU C, you also can set these compile time defines like this:
## 
##  -D UMM_TEST_MAIN
## 
##  Set this if you want to compile in the test suite at the end of this file.
## 
##  If you leave this define unset, then you might want to set another one:
## 
##  -D UMM_REDEFINE_MEM_FUNCTIONS
## 
##  If you leave this define unset, then the function names are left alone as
##  umm_malloc() umm_free() and umm_realloc() so that they cannot be confused
##  with the C runtime functions malloc() free() and realloc()
## 
##  If you do set this define, then the function names become malloc()
##  free() and realloc() so that they can be used as the C runtime functions
##  in an embedded environment.
## 
##  -D UMM_BEST_FIT (defualt)
## 
##  Set this if you want to use a best-fit algorithm for allocating new
##  blocks
## 
##  -D UMM_FIRST_FIT
## 
##  Set this if you want to use a first-fit algorithm for allocating new
##  blocks
## 
##  -D UMM_DBG_LOG_LEVEL=n
## 
##  Set n to a value from 0 to 6 depending on how verbose you want the debug
##  log to be
## 
##  ----------------------------------------------------------------------------
## 
##  Support for this library in a multitasking environment is provided when
##  you add bodies to the UMM_CRITICAL_ENTRY and UMM_CRITICAL_EXIT macros
##  (see below)
## 
##  ----------------------------------------------------------------------------
## 
## ///////////////////////////////////////////////

when defined(DEBUG_ESP_OOM):
  const
    MEMLEAK_DEBUG* = true
  ##  umm_*alloc are not renamed to *alloc
  proc umm_malloc*(size: csize): pointer {.cdecl, importcpp: "umm_malloc(@)",
                                       header: "umm_malloc_cfg.h".}
  proc umm_calloc*(num: csize; size: csize): pointer {.cdecl,
      importcpp: "umm_calloc(@)", header: "umm_malloc_cfg.h".}
  proc umm_realloc*(`ptr`: pointer; size: csize): pointer {.cdecl,
      importcpp: "umm_realloc(@)", header: "umm_malloc_cfg.h".}
  const
    umm_free* = free
  template umm_zalloc*(s: untyped): untyped =
    umm_calloc(1, s)

  proc malloc_loc*(s: csize; file: cstring; line: cint): pointer {.cdecl,
      importcpp: "malloc_loc(@)", header: "umm_malloc_cfg.h".}
  proc calloc_loc*(n: csize; s: csize; file: cstring; line: cint): pointer {.cdecl,
      importcpp: "calloc_loc(@)", header: "umm_malloc_cfg.h".}
  proc realloc_loc*(p: pointer; s: csize; file: cstring; line: cint): pointer {.cdecl,
      importcpp: "realloc_loc(@)", header: "umm_malloc_cfg.h".}
  ##  *alloc are macro calling *alloc_loc calling+checking umm_*alloc()
  ##  they are defined at the bottom of this file
  ## ///////////////////////////////////////////////
else:
  ##  umm_*alloc are renamed to *alloc
  const
    UMM_REDEFINE_MEM_FUNCTIONS* = true
const
  UMM_BEST_FIT* = true

##  Start addresses and the size of the heap

var _heap_start* {.importcpp: "_heap_start", header: "umm_malloc_cfg.h".}: char

const
  UMM_MALLOC_CFG__HEAP_ADDR* = ((uint32_t) and _heap_start)
  UMM_MALLOC_CFG__HEAP_SIZE* = ((size_t)(0x3FFFC000 - UMM_MALLOC_CFG__HEAP_ADDR))

##  A couple of macros to make packing structures less compiler dependent

const
  UMM_H_ATTPACKPRE* = true
  UMM_H_ATTPACKSUF* = __attribute__((__packed__))

## 
##  A couple of macros to make it easier to protect the memory allocator
##  in a multitasking system. You should set these macros up to use whatever
##  your system uses for this purpose. You can disable interrupts entirely, or
##  just disable task switching - it's up to you
## 
##  NOTE WELL that these macros MUST be allowed to nest, because umm_free() is
##  called from within umm_malloc()
## 

template UMM_CRITICAL_ENTRY*(): untyped =
  ets_intr_lock()

template UMM_CRITICAL_EXIT*(): untyped =
  ets_intr_unlock()

## 
##  -D UMM_INTEGRITY_CHECK :
## 
##  Enables heap integrity check before any heap operation. It affects
##  performance, but does NOT consume extra memory.
## 
##  If integrity violation is detected, the message is printed and user-provided
##  callback is called: `UMM_HEAP_CORRUPTION_CB()`
## 
##  Note that not all buffer overruns are detected: each buffer is aligned by
##  4 bytes, so there might be some trailing "extra" bytes which are not checked
##  for corruption.
## 
## 
## #define UMM_INTEGRITY_CHECK
## 
## 
##  -D UMM_POISON :
## 
##  Enables heap poisoning: add predefined value (poison) before and after each
##  allocation, and check before each heap operation that no poison is
##  corrupted.
## 
##  Other than the poison itself, we need to store exact user-requested length
##  for each buffer, so that overrun by just 1 byte will be always noticed.
## 
##  Customizations:
## 
##     UMM_POISON_SIZE_BEFORE:
##       Number of poison bytes before each block, e.g. 2
##     UMM_POISON_SIZE_AFTER:
##       Number of poison bytes after each block e.g. 2
##     UMM_POISONED_BLOCK_LEN_TYPE
##       Type of the exact buffer length, e.g. `short`
## 
##  NOTE: each allocated buffer is aligned by 4 bytes. But when poisoning is
##  enabled, actual pointer returned to user is shifted by
##  `(sizeof(UMM_POISONED_BLOCK_LEN_TYPE) + UMM_POISON_SIZE_BEFORE)`.
##  It's your responsibility to make resulting pointers aligned appropriately.
## 
##  If poison corruption is detected, the message is printed and user-provided
##  callback is called: `UMM_HEAP_CORRUPTION_CB()`
## 

when defined(DEBUG_ESP_PORT) or defined(DEBUG_ESP_CORE):
  const
    UMM_POISON* = true
const
  UMM_POISON_SIZE_BEFORE* = 4
  UMM_POISON_SIZE_AFTER* = 4
  UMM_POISONED_BLOCK_LEN_TYPE* = uint32_t

template UMM_HEAP_CORRUPTION_CB*(): untyped =
  panic()
