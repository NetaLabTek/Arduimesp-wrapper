## 
##  FSImpl.h - base file system interface
##  Copyright (c) 2015 Ivan Grokhotkov. All rights reserved.
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

type
  FileImpl* {.importcpp: "fs::FileImpl", header: "FSImpl.h", bycopy.} = object
  

proc destroyFileImpl*(this: var FileImpl) {.cdecl, importcpp: "#.~FileImpl()",
                                        header: "FSImpl.h".}
proc write*(this: var FileImpl; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "FSImpl.h".}
proc read*(this: var FileImpl; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "read", header: "FSImpl.h".}
proc flush*(this: var FileImpl) {.cdecl, importcpp: "flush", header: "FSImpl.h".}
proc seek*(this: var FileImpl; pos: uint32_t; mode: SeekMode): bool {.cdecl,
    importcpp: "seek", header: "FSImpl.h".}
proc position*(this: FileImpl): csize {.noSideEffect, cdecl, importcpp: "position",
                                    header: "FSImpl.h".}
proc size*(this: FileImpl): csize {.noSideEffect, cdecl, importcpp: "size",
                                header: "FSImpl.h".}
proc close*(this: var FileImpl) {.cdecl, importcpp: "close", header: "FSImpl.h".}
proc name*(this: FileImpl): cstring {.noSideEffect, cdecl, importcpp: "name",
                                  header: "FSImpl.h".}
type
  OpenMode* {.size: sizeof(cint), importcpp: "fs::OpenMode", header: "FSImpl.h".} = enum
    OM_DEFAULT = 0, OM_CREATE = 1, OM_APPEND = 2, OM_TRUNCATE = 4


type
  AccessMode* {.size: sizeof(cint), importcpp: "fs::AccessMode", header: "FSImpl.h".} = enum
    AM_READ = 1, AM_WRITE = 2, AM_RW = AM_READ or AM_WRITE


type
  DirImpl* {.importcpp: "fs::DirImpl", header: "FSImpl.h", bycopy.} = object
  

proc destroyDirImpl*(this: var DirImpl) {.cdecl, importcpp: "#.~DirImpl()",
                                      header: "FSImpl.h".}
proc openFile*(this: var DirImpl; openMode: OpenMode; accessMode: AccessMode): FileImplPtr {.
    cdecl, importcpp: "openFile", header: "FSImpl.h".}
proc fileName*(this: var DirImpl): cstring {.cdecl, importcpp: "fileName",
                                        header: "FSImpl.h".}
proc fileSize*(this: var DirImpl): csize {.cdecl, importcpp: "fileSize",
                                      header: "FSImpl.h".}
proc next*(this: var DirImpl): bool {.cdecl, importcpp: "next", header: "FSImpl.h".}
type
  FSImpl* {.importcpp: "fs::FSImpl", header: "FSImpl.h", bycopy.} = object
  

proc begin*(this: var FSImpl): bool {.cdecl, importcpp: "begin", header: "FSImpl.h".}
proc `end`*(this: var FSImpl) {.cdecl, importcpp: "end", header: "FSImpl.h".}
proc format*(this: var FSImpl): bool {.cdecl, importcpp: "format", header: "FSImpl.h".}
proc info*(this: var FSImpl; info: var FSInfo): bool {.cdecl, importcpp: "info",
    header: "FSImpl.h".}
proc open*(this: var FSImpl; path: cstring; openMode: OpenMode; accessMode: AccessMode): FileImplPtr {.
    cdecl, importcpp: "open", header: "FSImpl.h".}
proc exists*(this: var FSImpl; path: cstring): bool {.cdecl, importcpp: "exists",
    header: "FSImpl.h".}
proc openDir*(this: var FSImpl; path: cstring): DirImplPtr {.cdecl,
    importcpp: "openDir", header: "FSImpl.h".}
proc rename*(this: var FSImpl; pathFrom: cstring; pathTo: cstring): bool {.cdecl,
    importcpp: "rename", header: "FSImpl.h".}
proc remove*(this: var FSImpl; path: cstring): bool {.cdecl, importcpp: "remove",
    header: "FSImpl.h".}
##  namespace fs
