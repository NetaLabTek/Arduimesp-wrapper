## 
##  spiffs_api.h - file system wrapper for SPIFFS
##  Copyright (c) 2015 Ivan Grokhotkov. All rights reserved.
## 
##  This code was influenced by NodeMCU and Sming libraries, and first version of
##  Arduino wrapper written by Hristo Gochkov.
## 
##  This file is part of the esp8266 core for Arduino environment.
## 
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
## 
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

import
  FS

import
  FSImpl, spiffs/spiffs, debug, flash_utils

nil
proc spiffs_hal_write*(`addr`: uint32_t; size: uint32_t; src: ptr uint8_t): int32_t {.
    cdecl, importcpp: "spiffs_hal_write(@)", header: "spiffs_api.h".}
proc spiffs_hal_erase*(`addr`: uint32_t; size: uint32_t): int32_t {.cdecl,
    importcpp: "spiffs_hal_erase(@)", header: "spiffs_api.h".}
proc spiffs_hal_read*(`addr`: uint32_t; size: uint32_t; dst: ptr uint8_t): int32_t {.
    cdecl, importcpp: "spiffs_hal_read(@)", header: "spiffs_api.h".}
proc getSpiffsMode*(openMode: OpenMode; accessMode: AccessMode): cint {.cdecl,
    importcpp: "getSpiffsMode(@)", header: "spiffs_api.h".}
proc isSpiffsFilenameValid*(name: cstring): bool {.cdecl,
    importcpp: "isSpiffsFilenameValid(@)", header: "spiffs_api.h".}
discard "forward decl of SPIFFSFileImpl"
discard "forward decl of SPIFFSDirImpl"
type
  SPIFFSImpl* {.importcpp: "SPIFFSImpl", header: "spiffs_api.h", bycopy.} = object of FSImpl
  

proc constructSPIFFSImpl*(start: uint32_t; size: uint32_t; pageSize: uint32_t;
                         blockSize: uint32_t; maxOpenFds: uint32_t): SPIFFSImpl {.
    cdecl, constructor, importcpp: "SPIFFSImpl(@)", header: "spiffs_api.h".}
proc open*(this: var SPIFFSImpl; path: cstring; openMode: OpenMode;
          accessMode: AccessMode): FileImplPtr {.cdecl, importcpp: "open",
    header: "spiffs_api.h".}
proc exists*(this: var SPIFFSImpl; path: cstring): bool {.cdecl, importcpp: "exists",
    header: "spiffs_api.h".}
proc openDir*(this: var SPIFFSImpl; path: cstring): DirImplPtr {.cdecl,
    importcpp: "openDir", header: "spiffs_api.h".}
proc rename*(this: var SPIFFSImpl; pathFrom: cstring; pathTo: cstring): bool {.cdecl,
    importcpp: "rename", header: "spiffs_api.h".}
proc info*(this: var SPIFFSImpl; info: var FSInfo): bool {.cdecl, importcpp: "info",
    header: "spiffs_api.h".}
proc remove*(this: var SPIFFSImpl; path: cstring): bool {.cdecl, importcpp: "remove",
    header: "spiffs_api.h".}
proc begin*(this: var SPIFFSImpl): bool {.cdecl, importcpp: "begin",
                                     header: "spiffs_api.h".}
proc `end`*(this: var SPIFFSImpl) {.cdecl, importcpp: "end", header: "spiffs_api.h".}
proc format*(this: var SPIFFSImpl): bool {.cdecl, importcpp: "format",
                                      header: "spiffs_api.h".}
template CHECKFD*(): void =
  while _fd == 0:
  panic()

type
  SPIFFSFileImpl* {.importcpp: "SPIFFSFileImpl", header: "spiffs_api.h", bycopy.} = object of FileImpl
  

proc constructSPIFFSFileImpl*(fs: ptr SPIFFSImpl; fd: spiffs_file): SPIFFSFileImpl {.
    cdecl, constructor, importcpp: "SPIFFSFileImpl(@)", header: "spiffs_api.h".}
proc destroySPIFFSFileImpl*(this: var SPIFFSFileImpl) {.cdecl,
    importcpp: "#.~SPIFFSFileImpl()", header: "spiffs_api.h".}
proc write*(this: var SPIFFSFileImpl; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "write", header: "spiffs_api.h".}
proc read*(this: var SPIFFSFileImpl; buf: ptr uint8_t; size: csize): csize {.cdecl,
    importcpp: "read", header: "spiffs_api.h".}
proc flush*(this: var SPIFFSFileImpl) {.cdecl, importcpp: "flush",
                                    header: "spiffs_api.h".}
proc seek*(this: var SPIFFSFileImpl; pos: uint32_t; mode: SeekMode): bool {.cdecl,
    importcpp: "seek", header: "spiffs_api.h".}
proc position*(this: SPIFFSFileImpl): csize {.noSideEffect, cdecl,
    importcpp: "position", header: "spiffs_api.h".}
proc size*(this: SPIFFSFileImpl): csize {.noSideEffect, cdecl, importcpp: "size",
                                      header: "spiffs_api.h".}
proc close*(this: var SPIFFSFileImpl) {.cdecl, importcpp: "close",
                                    header: "spiffs_api.h".}
proc name*(this: SPIFFSFileImpl): cstring {.noSideEffect, cdecl, importcpp: "name",
                                        header: "spiffs_api.h".}
type
  SPIFFSDirImpl* {.importcpp: "SPIFFSDirImpl", header: "spiffs_api.h", bycopy.} = object of DirImpl
  

proc constructSPIFFSDirImpl*(pattern: String; fs: ptr SPIFFSImpl; dir: var spiffs_DIR): SPIFFSDirImpl {.
    cdecl, constructor, importcpp: "SPIFFSDirImpl(@)", header: "spiffs_api.h".}
proc destroySPIFFSDirImpl*(this: var SPIFFSDirImpl) {.cdecl,
    importcpp: "#.~SPIFFSDirImpl()", header: "spiffs_api.h".}
proc openFile*(this: var SPIFFSDirImpl; openMode: OpenMode; accessMode: AccessMode): FileImplPtr {.
    cdecl, importcpp: "openFile", header: "spiffs_api.h".}
proc fileName*(this: var SPIFFSDirImpl): cstring {.cdecl, importcpp: "fileName",
    header: "spiffs_api.h".}
proc fileSize*(this: var SPIFFSDirImpl): csize {.cdecl, importcpp: "fileSize",
    header: "spiffs_api.h".}
proc next*(this: var SPIFFSDirImpl): bool {.cdecl, importcpp: "next",
                                       header: "spiffs_api.h".}