when defined(__ets__):
  import
    ets_sys, osapi

template _SFR_BYTE*(n: untyped): untyped =
  (n)

when defined(__PROG_TYPES_COMPAT__):
  type
    prog_void* = nil
    prog_char* = char
    prog_uchar* = cuchar
    prog_int8_t* = int8_t
    prog_uint8_t* = uint8_t
    prog_int16_t* = int16_t
    prog_uint16_t* = uint16_t
    prog_int32_t* = int32_t
    prog_uint32_t* = uint32_t
const
  SIZE_IRRELEVANT* = 0x7FFFFFFF

##  memchr_P and memrchr_P are not implemented due to danger in its use, and
##  how uninteresting their use is
##  since its a flash string, you should already know where the char is within it,
##  further, it could return a pointer into the flash memory that is not 32bit aligned
##  which could cause an exception if read
##  PGM_VOID_P memchr_P(PGM_VOID_P bufP, int c, size_t count);
##  PGM_VOID_P memrchr_P(PGM_VOID_P bufP, int c, size_t count);

proc memcmp_P*(buf1: pointer; buf2P: PGM_VOID_P; size: csize): cint {.cdecl,
    importcpp: "memcmp_P(@)", header: "pgmspace.h".}
##  memccpy_P is only valid when used with pointers to 8bit data, due to size aligned pointers
##  and endianess of the values greater than 8bit, matching c may return invalid aligned pointers

proc memccpy_P*(dest: pointer; src: PGM_VOID_P; c: cint; count: csize): pointer {.cdecl,
    importcpp: "memccpy_P(@)", header: "pgmspace.h".}
proc memmem_P*(buf: pointer; bufSize: csize; findP: PGM_VOID_P; findPSize: csize): pointer {.
    cdecl, importcpp: "memmem_P(@)", header: "pgmspace.h".}
proc memcpy_P*(dest: pointer; src: PGM_VOID_P; count: csize): pointer {.cdecl,
    importcpp: "memcpy_P(@)", header: "pgmspace.h".}
proc strncpy_P*(dest: cstring; src: PGM_P; size: csize): cstring {.cdecl,
    importcpp: "strncpy_P(@)", header: "pgmspace.h".}
template strcpy_P*(dest, src: untyped): untyped =
  strncpy_P((dest), (src), SIZE_IRRELEVANT)

proc strncat_P*(dest: cstring; src: PGM_P; size: csize): cstring {.cdecl,
    importcpp: "strncat_P(@)", header: "pgmspace.h".}
template strcat_P*(dest, src: untyped): untyped =
  strncat_P((dest), (src), SIZE_IRRELEVANT)

proc strncmp_P*(str1: cstring; str2P: PGM_P; size: csize): cint {.cdecl,
    importcpp: "strncmp_P(@)", header: "pgmspace.h".}
template strcmp_P*(str1, str2P: untyped): untyped =
  strncmp_P((str1), (str2P), SIZE_IRRELEVANT)

proc strncasecmp_P*(str1: cstring; str2P: PGM_P; size: csize): cint {.cdecl,
    importcpp: "strncasecmp_P(@)", header: "pgmspace.h".}
template strcasecmp_P*(str1, str2P: untyped): untyped =
  strncasecmp_P((str1), (str2P), SIZE_IRRELEVANT)

proc strnlen_P*(s: PGM_P; size: csize): csize {.cdecl, importcpp: "strnlen_P(@)",
    header: "pgmspace.h".}
template strlen_P*(strP: untyped): untyped =
  strnlen_P((strP), SIZE_IRRELEVANT)

proc strstr_P*(haystack: cstring; needle: PGM_P): cstring {.cdecl,
    importcpp: "strstr_P(@)", header: "pgmspace.h".}
proc printf_P*(formatP: PGM_P): cint {.varargs, cdecl, importcpp: "printf_P(@)",
                                   header: "pgmspace.h".}
proc sprintf_P*(str: cstring; formatP: PGM_P): cint {.varargs, cdecl,
    importcpp: "sprintf_P(@)", header: "pgmspace.h".}
proc snprintf_P*(str: cstring; strSize: csize; formatP: PGM_P): cint {.varargs, cdecl,
    importcpp: "snprintf_P(@)", header: "pgmspace.h".}
proc vsnprintf_P*(str: cstring; strSize: csize; formatP: PGM_P; ap: va_list): cint {.
    cdecl, importcpp: "vsnprintf_P(@)", header: "pgmspace.h".}
##  flash memory must be read using 32 bit aligned addresses else a processor
##  exception will be triggered
##  order within the 32 bit values are
##  --------------
##  b3, b2, b1, b0
##      w1,     w0

when defined(__ets__):
  proc pgm_read_byte_inlined*(`addr`: pointer): uint8_t {.cdecl.}


  ##  Although this says "word", it's actually 16 bit, i.e. half word on Xtensa
  proc pgm_read_word_inlined*(`addr`: pointer): uint16_t {.cdecl.}


  ##  Make sure, that libraries checking existence of this macro are not failing
  when defined(__PROG_TYPES_COMPAT__):
    template pgm_read_byte*(`addr`: untyped): untyped =
      pgm_read_byte_inlined(cast[pointer]((`addr`)))

    template pgm_read_word*(`addr`: untyped): untyped =
      pgm_read_word_inlined(cast[pointer]((`addr`)))

  else:
    template pgm_read_byte*(`addr`: untyped): untyped =
      pgm_read_byte_inlined(`addr`)

    template pgm_read_word*(`addr`: untyped): untyped =
      pgm_read_word_inlined(`addr`)

else:
  template pgm_read_byte*(`addr`: untyped): untyped =
    (reinterpret_cast[ptr uint8_t](`addr`)[])

  template pgm_read_word*(`addr`: untyped): untyped =
    (reinterpret_cast[ptr uint16_t](`addr`)[])

template pgm_read_dword*(`addr`: untyped): untyped =
  (reinterpret_cast[ptr uint32_t](`addr`)[])

template pgm_read_float*(`addr`: untyped): untyped =
  (reinterpret_cast[ptr cfloat](`addr`)[])

template pgm_read_ptr*(`addr`: untyped): untyped =
  (reinterpret_cast[ptr pointer](`addr`)[])

template pgm_read_byte_near*(`addr`: untyped): untyped =
  pgm_read_byte(`addr`)

template pgm_read_word_near*(`addr`: untyped): untyped =
  pgm_read_word(`addr`)

template pgm_read_dword_near*(`addr`: untyped): untyped =
  pgm_read_dword(`addr`)

template pgm_read_float_near*(`addr`: untyped): untyped =
  pgm_read_float(`addr`)

template pgm_read_ptr_near*(`addr`: untyped): untyped =
  pgm_read_ptr(`addr`)

template pgm_read_byte_far*(`addr`: untyped): untyped =
  pgm_read_byte(`addr`)

template pgm_read_word_far*(`addr`: untyped): untyped =
  pgm_read_word(`addr`)

template pgm_read_dword_far*(`addr`: untyped): untyped =
  pgm_read_dword(`addr`)

template pgm_read_float_far*(`addr`: untyped): untyped =
  pgm_read_float(`addr`)

template pgm_read_ptr_far*(`addr`: untyped): untyped =
  pgm_read_ptr(`addr`)
