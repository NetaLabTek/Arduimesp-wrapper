## 
##  WCharacter.h - Character utility functions for Wiring & Arduino
##  Copyright (c) 2010 Hernando Barragan.  All right reserved.
##  
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either
##  version 2.1 of the License, or (at your option) any later version.
##  
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.
##  
##  You should have received a copy of the GNU Lesser General Public
##  License along with this library; if not, write to the Free Software
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
## 

template isascii*(__c: untyped): untyped =
  ((unsigned)(__c) <= 0o000000000177)

template toascii*(__c: untyped): untyped =
  ((__c) and 0o000000000177)

##  WCharacter.h prototypes

proc isAlphaNumeric*(c: cint): boolean {.cdecl, importcpp: "isAlphaNumeric(@)",
                                     header: "WCharacter.h".}
proc isAlpha*(c: cint): boolean {.cdecl, importcpp: "isAlpha(@)",
                              header: "WCharacter.h".}
proc isAscii*(c: cint): boolean {.cdecl, importcpp: "isAscii(@)",
                              header: "WCharacter.h".}
proc isWhitespace*(c: cint): boolean {.cdecl, importcpp: "isWhitespace(@)",
                                   header: "WCharacter.h".}
proc isControl*(c: cint): boolean {.cdecl, importcpp: "isControl(@)",
                                header: "WCharacter.h".}
proc isDigit*(c: cint): boolean {.cdecl, importcpp: "isDigit(@)",
                              header: "WCharacter.h".}
proc isGraph*(c: cint): boolean {.cdecl, importcpp: "isGraph(@)",
                              header: "WCharacter.h".}
proc isLowerCase*(c: cint): boolean {.cdecl, importcpp: "isLowerCase(@)",
                                  header: "WCharacter.h".}
proc isPrintable*(c: cint): boolean {.cdecl, importcpp: "isPrintable(@)",
                                  header: "WCharacter.h".}
proc isPunct*(c: cint): boolean {.cdecl, importcpp: "isPunct(@)",
                              header: "WCharacter.h".}
proc isSpace*(c: cint): boolean {.cdecl, importcpp: "isSpace(@)",
                              header: "WCharacter.h".}
proc isUpperCase*(c: cint): boolean {.cdecl, importcpp: "isUpperCase(@)",
                                  header: "WCharacter.h".}
proc isHexadecimalDigit*(c: cint): boolean {.cdecl,
    importcpp: "isHexadecimalDigit(@)", header: "WCharacter.h".}
proc toAscii*(c: cint): cint {.cdecl, importcpp: "toAscii(@)", header: "WCharacter.h".}
proc toLowerCase*(c: cint): cint {.cdecl, importcpp: "toLowerCase(@)",
                               header: "WCharacter.h".}
proc toUpperCase*(c: cint): cint {.cdecl, importcpp: "toUpperCase(@)",
                               header: "WCharacter.h".}
##  Checks for an alphanumeric character. 
##  It is equivalent to (isalpha(c) || isdigit(c)).

proc isAlphaNumeric*(c: cint): boolean {.cdecl.}
##  Checks for an alphabetic character. 
##  It is equivalent to (isupper(c) || islower(c)).

proc isAlpha*(c: cint): boolean {.cdecl.}
##  Checks whether c is a 7-bit unsigned char value 
##  that fits into the ASCII character set.

proc isAscii*(c: cint): boolean {.cdecl.}
##  Checks for a blank character, that is, a space or a tab.

proc isWhitespace*(c: cint): boolean {.cdecl.}
##  Checks for a control character.

proc isControl*(c: cint): boolean {.cdecl.}
##  Checks for a digit (0 through 9).

proc isDigit*(c: cint): boolean {.cdecl.}
##  Checks for any printable character except space.

proc isGraph*(c: cint): boolean {.cdecl.}
##  Checks for a lower-case character.

proc isLowerCase*(c: cint): boolean {.cdecl.}
##  Checks for any printable character including space.

proc isPrintable*(c: cint): boolean {.cdecl.}
##  Checks for any printable character which is not a space 
##  or an alphanumeric character.

proc isPunct*(c: cint): boolean {.cdecl.}
##  Checks for white-space characters. For the avr-libc library, 
##  these are: space, formfeed ('\f'), newline ('\n'), carriage 
##  return ('\r'), horizontal tab ('\t'), and vertical tab ('\v').

proc isSpace*(c: cint): boolean {.cdecl.}
##  Checks for an uppercase letter.

proc isUpperCase*(c: cint): boolean {.cdecl.}
##  Checks for a hexadecimal digits, i.e. one of 0 1 2 3 4 5 6 7 
##  8 9 a b c d e f A B C D E F.

proc isHexadecimalDigit*(c: cint): boolean {.cdecl.}
##  Converts c to a 7-bit unsigned char value that fits into the 
##  ASCII character set, by clearing the high-order bits.

proc toAscii*(c: cint): cint {.cdecl.}
##  Warning:
##  Many people will be unhappy if you use this function. 
##  This function will convert accented letters into random 
##  characters.
##  Converts the letter c to lower case, if possible.

proc toLowerCase*(c: cint): cint {.cdecl.}
##  Converts the letter c to upper case, if possible.

proc toUpperCase*(c: cint): cint {.cdecl.}